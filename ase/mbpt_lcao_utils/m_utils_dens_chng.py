from __future__ import division
import numpy as np
from ase.mbpt_lcao_utils.utils import pts_coord_to_Array_ind
import ase.units as un
import warnings
#import time

"""
  Routines to calculate the centroid and the mean value of the 
  density change for different cases:
    along an axis of a cylinder
    in half a sphere
"""

def calc_d_param_new_method_peter(prop, data_re, data_im, nsh, lc, delta = 0.0, inte_atom_correc = 0.0, 
    fort=False, fullBox=False, Edir='x', xyz_name = None, factor=None, sign=1.0):
  from ase.cluster.icosahedron import Icosahedron
  nsh = nsh
  cl = Icosahedron('Ag', nsh, latticeconstant=lc)
  del cl [[atom.index for atom in cl if atom.tag<nsh]]
  cl.positions = (cl.positions - cl.get_center_of_mass())/un.Bohr

  # get surface ico
  #ico_bound = cl.positions * 2.0
  #ico_shell = cl.positions# / abs(np.amin(cl.positions))
  npnt = cl.positions.shape[0]
  p2xyz = np.zeros((npnt,3), dtype='float64')
  print("delta: {0}, inte_atom_correc = {1}".format(delta, inte_atom_correc))
  for pnt, xyz in enumerate(cl.positions):
    #xyz = cl.positions[pnt]#+ inte_atom_correc*(ico_shell[pnt]/np.sqrt(np.dot(ico_shell[pnt], ico_shell[pnt])))
    r = np.sqrt(sum(xyz**2))
    t = np.arccos(xyz[2]/r) if r>0.0 else 0.0
    p = np.arctan2(xyz[1],xyz[0])
    cl.positions[pnt, :] = (r+delta*inte_atom_correc)*np.array([np.sin(t)*np.cos(p), np.sin(t)*np.sin(p), np.cos(t)])
    p2xyz[pnt, :] = (r+delta*inte_atom_correc)*np.array([np.sin(t)*np.cos(p), np.sin(t)*np.sin(p), np.cos(t)])

  if xyz_name is not None:
    import ase.io as io
    io.write(xyz_name, cl)
    np.save(xyz_name.split(".")[0]+'npy', p2xyz)


  xyz_arr=np.array([data_re.xmesh[:, 0, 0], data_re.ymesh[0, :, 0], data_re.zmesh[0, 0, :]])

  I0 = np.zeros((2), dtype=np.float64)
  I1 = np.zeros((2), dtype=np.float64)
  I2 = np.zeros((2), dtype=np.float64)
  #d_arr = np.zeros(data_re.Array.shape, dtype=np.float64)
  #d_dn_arr = np.zeros((data_re.Array.shape[0], data_re.Array.shape[1], 
  #                     data_re.Array.shape[2], 2), dtype=np.float64)

  Edir_fort = 0
  if  Edir == 'x':
    Edir_fort = 1
  elif  Edir == 'y':
    Edir_fort = 2
  elif  Edir == 'z':
    Edir_fort = 3
  
  if fullBox:
    Edir_fort = 0

  if factor is None:
    fact_fort = -1.0
  else:
    fact_fort = float(factor)

  print('Edir_fort = ', Edir_fort)
  #data_re.Array = sign*data_re.Array
  #data_im.Array = sign*data_im.Array


  if fort:
    from ctypes import CDLL, POINTER, c_int, c_float, c_double
    lib_dir = '/home/barbry/programs/ase/ase/mbpt_lcao_utils/lib'
    fort_lib = CDLL(lib_dir + "/fortran_lib.so")
    print(p2xyz.shape)
    fort_lib.calc_d_param_new_method_peter_fort(data_re.Array.ctypes.data_as(POINTER(c_float)),\
          data_im.Array.ctypes.data_as(POINTER(c_float)), c_float(inte_atom_correc), p2xyz.ctypes.data_as(POINTER(c_double)),\
          xyz_arr[0].ctypes.data_as(POINTER(c_double)), xyz_arr[1].ctypes.data_as(POINTER(c_double)),\
          xyz_arr[2].ctypes.data_as(POINTER(c_double)), c_int(Edir_fort), I0.ctypes.data_as(POINTER(c_double)),\
          I1.ctypes.data_as(POINTER(c_double)), I2.ctypes.data_as(POINTER(c_double)),\
          c_int(xyz_arr[0].shape[0]), c_int(xyz_arr[1].shape[0]), c_int(xyz_arr[2].shape[0]), c_int(npnt),\
          c_float(fact_fort))

  else:
    for i, x in enumerate(xyz_arr[0]):
      for j, y in enumerate(xyz_arr[0]):
        for k, z in enumerate(xyz_arr[0]):
          xyz = np.array([x, y, z])
          r    = np.sqrt( sum(xyz**2) )
          p2d  = [sum((xyz-p2xyz[pnt])**2) for pnt in range(npnt)]
          pmin = np.argmin(p2d)
          r0 = np.sqrt(sum(p2xyz[pmin])**2)
          d  = np.sign(r-r0)*np.sqrt(p2d[pmin])
          I0[:] = I0[:] + np.array([data_re.Array[i, j, k], data_im.Array[i, j, k]])
          I1[:] = I1[:] + r*np.array([data_re.Array[i, j, k], data_im.Array[i, j, k]])
          I2[:] = I2[:] + d*np.array([data_re.Array[i, j, k], data_im.Array[i, j, k]])

  return np.array([complex(I0[0], I0[1]),
                   complex(I1[0], I1[1]),
                   complex(I2[0], I2[1])])*data_re.dr[0]*data_re.dr[1]*data_re.dr[2]

  


def calc_centroid_dens_chng_cylinder(prop, data_re, box, center, radius, data_im = None, cyl_dir = 'y', ax = None):
  """
  calculate the centroid of the density change along a direction in a cylinder 
  """

  ind_min = pts_coord_to_Array_ind(data_re, np.array([box[0, 0], box[1, 0], box[2, 0]]))
  ind_max = pts_coord_to_Array_ind(data_re, np.array([box[0, 1], box[1, 1], box[2, 1]]))

  print('ind_min: ', ind_min)
  print('ind_max: ', ind_max)

  arr_re = data_re.Array[ind_min[0]:ind_max[0], ind_min[1]:ind_max[1], ind_min[2]:ind_max[2]]
  if data_im is None:
    arr_im = None
  else:
    arr_im = data_im.Array[ind_min[0]:ind_max[0], ind_min[1]:ind_max[1], ind_min[2]:ind_max[2]]
  print('data.box: ', data_re.box )
  print('arr_box: ', arr_re.shape)
  print('data.Array: ', data_re.Array.shape)

  if isinstance(cyl_dir, str):
    if cyl_dir == 'x':
      x=data_re.xmesh[ind_min[0]:ind_max[0], 0, 0]
      y=data_re.ymesh[0, ind_min[1]:ind_max[1], 0] - center[1]
      z=data_re.zmesh[0, 0, ind_min[2]:ind_max[2]] - center[2]

      return calc_centroid_dens_chng_cylinder_xdir(x, y, z, arr_re, arr_im, radius)
    elif cyl_dir == 'y':
      x=data_re.xmesh[ind_min[0]:ind_max[0], 0, 0] - center[0]
      y=data_re.ymesh[0, ind_min[1]:ind_max[1], 0]
      z=data_re.zmesh[0, 0, ind_min[2]:ind_max[2]] - center[2]

      return calc_centroid_dens_chng_cylinder_ydir(x, y, z, arr_re, arr_im, radius)
    elif cyl_dir == 'z':
      x=data_re.xmesh[ind_min[0]:ind_max[0], 0, 0] - center[0]
      y=data_re.ymesh[0, ind_min[1]:ind_max[1], 0] - center[1]
      z=data_re.zmesh[0, 0, ind_min[2]:ind_max[2]]

      return calc_centroid_dens_chng_cylinder_zdir(x, y, z, arr_re, arr_im, radius)
    else:
      raise ValueError('cyl_dir must be x, y, z or an array of shape (3)')
  elif isinstance(cyl_dir, type(np.array([0.0]))):
    if cyl_dir.shape[0] == 3:
      return calc_centroid_dens_chng_cylinder_pecular_dir(data_re, 
            data_im, radius, cyl_dir)
    else:
      raise ValueError('cyl_dir.shape /= 3')
  else:
    raise ValueError('cyl_dir must be x, y, z or an array of shape (3)')


def calc_centroid_dens_chng_cylinder_xdir(x, y, z, arr_re, arr_im, radius):
  """
  calculate the centroid density change of a cylinder which axis is the x direction
  """

  dn = np.zeros((x.shape[0], 2), dtype = float)
  dx = x[1]-x[0]
  dy = y[1]-y[0]
  dz = z[1]-z[0]

  for j in range(arr_re.shape[1]):
    for k in range(arr_re.shape[2]):
      rho = np.sqrt(y[j]**2 + z[k]**2)
      if  rho < radius:
        for i in range(arr_re.shape[0]):
          dn[i, 0] = dn[i, 0] + x[i]*arr_re[i, j, k]*dx*dy*dz
          if arr_im is not None:
            dn[i, 1] = dn[i, 1] + x[i]*arr_im[i, j, k]*dx*dy*dz

  return x, dn

def calc_centroid_dens_chng_cylinder_ydir(x, y, z, arr_re, arr_im, radius):
  """
  calculate the centroid density change of a cylinder which axis is the y direction
  """

  dn = np.zeros((y.shape[0], 2), dtype = float)
  dx = x[1]-x[0]
  dy = y[1]-y[0]
  dz = z[1]-z[0]

  for i in range(arr_re.shape[0]):
    for k in range(arr_re.shape[2]):
      rho = np.sqrt(x[i]**2 + z[k]**2)
      if  rho < radius:
        for j in range(arr_re.shape[1]):
          dn[j, 0] = dn[j, 0] + y[j]*arr_re[i, j, k]*dx*dy*dz
          if arr_im is not None:
            dn[j, 1] = dn[j, 1] + y[j]*arr_im[i, j, k]*dx*dy*dz

  return y, dn

def calc_centroid_dens_chng_cylinder_zdir(x, y, z, arr_re, arr_im, radius):
  """
  calculate the centroid density change of a cylinder which axis is the z direction
  """


  dn = np.zeros((z.shape[0], 2), dtype = float)
  dx = x[1]-x[0]
  dy = y[1]-y[0]
  dz = z[1]-z[0]

  for i in range(arr_re.shape[0]):
    for j in range(arr_re.shape[1]):
      rho = np.sqrt(x[i]**2 + y[j]**2)
      if  rho < radius:
        for k in range(arr_re.shape[2]):
          dn[k, 0] = dn[k, 0] + z[k]*arr_re[i, j, k]*dx*dy*dz
          if arr_im is not None:
            dn[k, 1] = dn[k, 1] + z[k]*arr_im[i, j, k]*dx*dy*dz

  return z, dn

def calc_centroid_dens_chng_cylinder_pecular_dir(data_re, data_im, radius, vect, dz = 0.1, fort = True, arr_im=None):
  """
  calculate the centroid density change of a cylinder which is along the vector vect
  """

  data_re.rotate(vect)
  # only one data set is necessary to rotate since daata_re and data_im share
  # the same mesh
  z = np.arange(0.0, np.max(data_re.Rotzmesh), dz)
  dn = np.zeros((z.shape[0], 2), dtype = float)
  for i in range(data_re.Array.shape[0]):
    for j in range(data_re.Array.shape[1]):
      for k in range(data_re.Array.shape[2]):
        if (data_re.Rotzmesh[i, j, k] > 0.0):
          rho = np.sqrt(data_re.Rotxmesh[i, j, k]**2 + data_re.Rotymesh[i, j, k]**2)
          if (rho < radius):
            wh = np.where(z>=data_re.Rotzmesh[i,j,k])[0]
            zind = wh[0]
            if round(z[zind] - data_re.Rotzmesh[i,j,k]) > 0.0:
              zind = zind + 1
            dn[zind, 0] = dn[zind, 0] + data_re.Rotzmesh[i,j,k]*data_re.Array[i, j, k]
            if arr_im is not None:
              dn[zind, 1] = dn[zind, 1] + data_re.Rotzmesh[i,j,k]*data_im.Array[i, j, k]


  return z, dn

def calc_mean_dens_chng_cylinder(prop, data_re, center, radius, data_im = None, 
    box = None, cyl_dir = 'y', ax = None, numba = ""):
    """
      calculate the mean of the density change along a direction in a cylinder 
    """

    if isinstance(cyl_dir, str):
        if box is None:
            box = data_re.box
        ind_min = pts_coord_to_Array_ind(data_re, np.array([box[0, 0], box[1, 0], box[2, 0]]))
        ind_max = pts_coord_to_Array_ind(data_re, np.array([box[0, 1], box[1, 1], box[2, 1]]))

        print('ind_min: ', ind_min)
        print('ind_max: ', ind_max)

        arr_re = data_re.Array[ind_min[0]:ind_max[0], ind_min[1]:ind_max[1], ind_min[2]:ind_max[2]]
        if data_im is None:
            arr_im = None
        else:
            arr_im = data_im.Array[ind_min[0]:ind_max[0], ind_min[1]:ind_max[1], ind_min[2]:ind_max[2]]
        print('data.box: ', data_re.box )
        print('arr_box: ', arr_re.shape)
        print('data.Array: ', data_re.Array.shape)


        if cyl_dir == 'x':
            x=data_re.xmesh[ind_min[0]:ind_max[0], 0, 0]
            y=data_re.ymesh[0, ind_min[1]:ind_max[1], 0] - center[1]
            z=data_re.zmesh[0, 0, ind_min[2]:ind_max[2]] - center[2]
            return calc_mean_dens_chng_cylinder_xdir(x, y, z, arr_re, arr_im, radius)
        
        elif cyl_dir == 'y':
            x=data_re.xmesh[ind_min[0]:ind_max[0], 0, 0] - center[0]
            y=data_re.ymesh[0, ind_min[1]:ind_max[1], 0]
            z=data_re.zmesh[0, 0, ind_min[2]:ind_max[2]] - center[2]
            return calc_mean_dens_chng_cylinder_ydir(x, y, z, arr_re, arr_im, radius)
        
        elif cyl_dir == 'z':
            x=data_re.xmesh[ind_min[0]:ind_max[0], 0, 0] - center[0]
            y=data_re.ymesh[0, ind_min[1]:ind_max[1], 0] - center[1]
            z=data_re.zmesh[0, 0, ind_min[2]:ind_max[2]]
            return calc_mean_dens_chng_cylinder_zdir(x, y, z, arr_re, arr_im, radius)
        else:
            raise ValueError('cyl_dir must be x, y, z or an array of shape (3)')
    elif isinstance(cyl_dir, type(np.array([0.0]))):
        if cyl_dir.shape[0] == 3:
            return calc_mean_dens_chng_cylinder_pecular_dir(data_re, 
                        data_im, radius, cyl_dir, numba=numba)
        else:
            raise ValueError('cyl_dir.shape /= 3')
    else:
        raise ValueError('cyl_dir must be x, y, z or an array of shape (3)')


def calc_mean_dens_chng_cylinder_xdir(x, y, z, arr_re, arr_im, radius):
  """
  calculate the mean density change of a cylinder which axis is the x direction
  """

  dn = np.zeros((x.shape[0], 2), dtype = float)
  dx = x[1]-x[0]
  dy = y[1]-y[0]
  dz = z[1]-z[0]

  for j in range(arr_re.shape[1]):
    for k in range(arr_re.shape[2]):
      rho = np.sqrt(y[j]**2 + z[k]**2)
      if  rho < radius:
        for i in range(arr_re.shape[0]):
          dn[i, 0] = dn[i, 0] + arr_re[i, j, k]*dx*dy*dz
          if arr_im is not None:
            dn[i, 1] = dn[i, 1] + arr_im[i, j, k]*dx*dy*dz

  return x, dn

def calc_mean_dens_chng_cylinder_ydir(x, y, z, arr_re, arr_im, radius):
  """
  calculate the mean density change of a cylinder which axis is the y direction
  """

  dn = np.zeros((y.shape[0], 2), dtype = float)
  dx = x[1]-x[0]
  dy = y[1]-y[0]
  dz = z[1]-z[0]

  for i in range(arr_re.shape[0]):
    for k in range(arr_re.shape[2]):
      rho = np.sqrt(x[i]**2 + z[k]**2)
      if  rho < radius:
        for j in range(arr_re.shape[1]):
          dn[j, 0] = dn[j, 0] + arr_re[i, j, k]*dx*dy*dz
          if arr_im is not None:
            dn[j, 1] = dn[j, 1] + arr_im[i, j, k]*dx*dy*dz

  return y, dn

def calc_mean_dens_chng_cylinder_zdir(x, y, z, arr_re, arr_im, radius):
  """
  calculate the mean density change of a cylinder which axis is the z direction
  """


  dn = np.zeros((z.shape[0], 2), dtype = float)
  dx = x[1]-x[0]
  dy = y[1]-y[0]
  dz = z[1]-z[0]

  for i in range(arr_re.shape[0]):
    for j in range(arr_re.shape[1]):
      rho = np.sqrt(x[i]**2 + y[j]**2)
      if  rho < radius:
        for k in range(arr_re.shape[2]):
          dn[k, 0] = dn[k, 0] + arr_re[i, j, k]*dx*dy*dz
          if arr_im is not None:
            dn[k, 1] = dn[k, 1] + arr_im[i, j, k]*dx*dy*dz

  return z, dn

def calc_mean_dens_chng_cylinder_pecular_dir(data_re, data_im, radius, vect, dz=0.1, numba="numba"):
  """
  calculate the mean density change of a cylinder which is along the vector vect
  """
  data_re.rotate(vect)
  # only one data set is necessary to rotate since daata_re and data_im share
  # the same mesh
  z = np.arange(0.0, np.max(data_re.Rotzmesh), dz, dtype=np.float32)
  dn = np.zeros((z.shape[0], 2), dtype = np.float32)

  if numba == "numba":
    try:
      from ase.mbpt_lcao_utils.numba_funcs import calc_mean_dens_chng_cylinder_pecular_dir_numba
      #t0 = time.process_time()
      calc_mean_dens_chng_cylinder_pecular_dir_numba(data_re.Array, 
        data_im.Array, np.array([radius]), data_re.Rotxmesh, 
        data_re.Rotymesh, data_re.Rotzmesh, z, dn[:, 0], dn[:, 1])
      #t1 = time.process_time()
      #print("numba time: ", t1-t0)
    except:
      warnings.warn("Numba import error, shifting to python routine")
      numba == "C"
  elif numba == "C":
    from ctypes import CDLL, POINTER, c_int, c_float, c_double
    lib_dir = '/home/barbry/programs/ase/ase/mbpt_lcao_utils/lib'
    #c_lib = CDLL(lib_dir + "/c_lib.so")
    fort_lib = CDLL(lib_dir + "/fortran_lib.so")
    dn_re = np.zeros((z.shape[0]), dtype = np.float32)
    dn_im = np.zeros((z.shape[0]), dtype = np.float32)

    #t0 = time.process_time()
    M, N, P = data_re.Array.shape
    Q = z.shape[0]

    #print('python: sum(dn) = ', np.sum(abs(data_re.Array)), np.sum(abs(data_im.Array)))

    #c_lib.calc_mean_dens_chng_cylinder_pecular_dir_c(data_re.Array.ctypes.data_as(POINTER(c_float)),
    #        data_im.Array.ctypes.data_as(POINTER(c_float)), c_double(radius),
    #        data_re.Rotxmesh.ctypes.data_as(POINTER(c_float)), data_re.Rotymesh.ctypes.data_as(POINTER(c_float)),
    #        data_re.Rotzmesh.ctypes.data_as(POINTER(c_float)), z.ctypes.data_as(POINTER(c_float)),
    #        dn.ctypes.data_as(POINTER(c_float)), c_int(M), c_int(N), c_int(P), c_int(Q))
    fort_lib.calc_mean_dens_chng_cylinder_pecular_dir_fort(data_re.Array.ctypes.data_as(POINTER(c_float)),
            data_im.Array.ctypes.data_as(POINTER(c_float)), c_double(radius),
            data_re.Rotxmesh.ctypes.data_as(POINTER(c_float)), data_re.Rotymesh.ctypes.data_as(POINTER(c_float)),
            data_re.Rotzmesh.ctypes.data_as(POINTER(c_float)), z.ctypes.data_as(POINTER(c_float)),
            dn_re.ctypes.data_as(POINTER(c_float)), dn_im.ctypes.data_as(POINTER(c_float)),
            c_int(M), c_int(N), c_int(P), c_int(Q))

    dn[:, 0] = dn_re
    dn[:, 1] = dn_im


    #t1 = time.process_time()
    #print("C time: ", t1-t0)

  else:
    #t0 = time.process_time()
    M, N, P = data_re.Array.shape

    for i in range(M):
      for j in range(N):
        for k in range(P):
          if (data_re.Rotzmesh[i, j, k] > 0.0):
            rho = np.sqrt(data_re.Rotxmesh[i, j, k]**2 + data_re.Rotymesh[i, j, k]**2)
            if (rho < radius):
              wh = np.where(z>=data_re.Rotzmesh[i,j,k])[0]
              zind = wh[0]
              dn[zind, 0] = dn[zind, 0] + data_re.Array[i, j, k]
              dn[zind, 1] = dn[zind, 1] + data_im.Array[i, j, k]
    #t1 = time.process_time()
    #print("python time: ", t1-t0)

  return z, dn


def centroid_dens_chang(prop, data_re, data_im, numba=False, fullBox=False, Edir='x'):
  """
	Get the centroid  of the data in half a sphere
	
	Output Parameters:
	------------------
		y
		d
		inte_num : array of the centroid along the axis Edir
		inte_den : array of the mean value along the axis Edir
		
  """

  arr_mod = np.sqrt(data_re.Array**2 + data_im.Array**2)
  x=data_re.xmesh[:, 0, 0]
  y=data_re.ymesh[0, :, 0]
  z=data_re.zmesh[0, 0, :]

  #dre_num = 0.0
  #dre_den = 0.0
  #dim_num = 0.0
  #dim_den = 0.0
  #dmod_num = 0.0
  #dmod_den = 0.0

  d = np.zeros((3, 3), dtype=np.float64)
  dxyz = np.array([x[1]-x[0], y[1]-y[0], z[1]-z[0]])

  #t0 = time.time()
  print(data_re.Array.shape)
  if numba:
    try:
      import numba
      print(numba.__version__)
      if fullBox:
        from numba_funcs import centroid_dens_chang_numba_full_numba
        inte_num = np.zeros((x.shape[0], 3), dtype=np.float64)
        inte_den = np.zeros((x.shape[0], 3), dtype=np.float64)
        centroid_dens_chang_numba_full_numba(data_re.Array, data_im.Array, 
                                             arr_mod, x, y, z, dxyz,
                                             d, inte_num, inte_den)
        return y, d, inte_num, inte_den
      else:
        if Edir == 'x':
          inte_num = np.zeros((x.shape[0], 3), dtype=np.float64)
          inte_den = np.zeros((x.shape[0], 3), dtype=np.float64)
          from numba_funcs import centroid_dens_chang_numba_half_numba_xdir
          centroid_dens_chang_numba_half_numba_xdir(data_re.Array, data_im.Array, 
                                             arr_mod, x, y, z, dxyz,
                                             d, inte_num, inte_den)
          return x, d, inte_num, inte_den

        elif Edir == 'y':
          inte_num = np.zeros((y.shape[0], 3), dtype=np.float64)
          inte_den = np.zeros((y.shape[0], 3), dtype=np.float64)
          from numba_funcs import centroid_dens_chang_numba_half_numba_ydir
          centroid_dens_chang_numba_half_numba_ydir(data_re.Array, data_im.Array, 
                                             arr_mod, x, y, z, dxyz,
                                             d, inte_num, inte_den)
          return y, d, inte_num, inte_den

        elif Edir == 'z':
          inte_num = np.zeros((z.shape[0], 3), dtype=np.float64)
          inte_den = np.zeros((z.shape[0], 3), dtype=np.float64)
          from numba_funcs import centroid_dens_chang_numba_half_numba_zdir
          centroid_dens_chang_numba_half_numba_zdir(data_re.Array, data_im.Array, 
                                             arr_mod, x, y, z, dxyz,
                                             d, inte_num, inte_den)
          return z, d, inte_num, inte_den
        else:
          raise ValueError("Edir only x, y or z")


    except ImportError:
      raise ValueError('import numba failed try with numba=false (or install numba)')
  else:
    centroid_dens_chang_py(data_re.Array, data_im.Array, 
                                 arr_mod, x, y, z, dxyz,
                                 d, inte_num, inte_den, fullBox=fullBox)
    return y, d, inte_num, inte_den

  #t1 = time.time()
  print('d = ', d[0, 0]/d[0, 1], d[1, 0]/d[1, 1], d[2, 0]/d[2, 1])
  #print('timing = ', t1-t0)

def centroid_dens_chang_py(dn_re, dn_im, dn_mod, x, y, z, dxyz, d, inte_num, inte_den, fullBox):

  M, N, K = dn_re.shape
  for j in range(N):
    if fullBox:
      if (y[j]>=0.0):
        centroid_xz_calc(M, N, K, j, x, y, z, dn_re, dn_im, dn_mod, inte_num, inte_den, d)
        inte_num[j, :] = inte_num[j, :]*dxyz[0]*dxyz[1]*dxyz[2]
    else:
      centroid_xz_calc(M, N, K, j, x, y, z, dn_re, dn_im, dn_mod, inte_num, inte_den, d)
      inte_num[j, :] = inte_num[j, :]*dxyz[0]*dxyz[1]*dxyz[2]

  d = d*dxyz[0]*dxyz[1]*dxyz[2]


def centroid_xz_calc(M, N, K, j, x, y, z, dn_re, dn_im, dn_mod, inte_num, inte_den, d):
    for i in range(M):
      for k in range(K):
        r = np.sqrt(x[i]**2 + y[j]**2 + z[k]**2)
        if r>0.0:
          if (dn_re[i, j, k] != 0.0):
            inte_num[j, 0] = inte_num[j, 0] + r*dn_re[i, j, k]
            inte_den[j, 0] = inte_den[j, 0] + dn_re[i, j, k]
            
            d[0, 0] = d[0, 0] + r*dn_re[i, j, k]
            d[0, 1] = d[0, 1] + dn_re[i, j, k]
          
          if (dn_im[i, j, k] != 0.0):
            inte_num[j, 1] = inte_num[j, 1] + r*dn_im[i, j, k]
            inte_den[j, 1] = inte_den[j, 1] + dn_im[i, j, k]

            d[1, 0] = d[1, 0] + r*dn_im[i, j, k]
            d[1, 1] = d[1, 1] + dn_im[i, j, k]
          
          if (dn_mod[i, j, k] != 0.0):
            d[2, 0] = d[2, 0] + r*dn_mod[i, j, k]
            d[2, 1] = d[2, 1] + dn_mod[i, j, k]
