#file that contain the class for the graphical interface of the program pyfip

from __future__ import division
import pygtk
pygtk.require('2.0')
import gtk
import subprocess
from ase.calculators.siesta.mbpt_lcao_io import MBPT_LCAO_Parameters, MBPT_LCAO_Properties_figure
from ase.mbpt_lcao_utils.inp_files import tddft_inp, siesta
from ase.mbpt_lcao_utils.readxyz import str2int, str2float, Add_element_list
from ase.mbpt_lcao_utils.plot_lib import plot_3D
from ase.mbpt_lcao_utils.FFT_routines import list2array
import ase.mbpt_lcao_utils.widget as wid
import numpy as np
import os



#################################################
#                                               #
#       Class for the title window              #
#                                               #
#################################################


class Main_Window:
  """
  title window, allow to chose between run tddft_iter or to 
  plot the data
  """
  def __init__(self):

    mainWindow = gtk.Window()
    mainWindow.set_title('tddft_iter')
    mainWindow.set_size_request(550, 550)
    mainWindow.set_position(gtk.WIN_POS_CENTER)
    mainWindow.connect('destroy', self.on_mainWindow_destroy)

    self.menu_items = (
        ( "/_File",         None,         None, 0, "<Branch>" ),
        ( "/File/Quit",     "<control>Q", gtk.main_quit, 0, None ),
        ( "/_Help",         None,         None, 0, "<LastBranch>" ),
        ( "/_Help/About",   None,         None, 0, None ),
      )
    
    menubar = self.get_main_menu(mainWindow)

    Vbox = gtk.VBox(True, spacing = 0)
    Vbox.pack_start(menubar, False, False, 0)


    #widget
    self.Label_title = gtk.Label('Welcome in the tddft_iter Program')
    
    self.button_run_siesta = gtk.Button('Run Siesta')
    self.button_run_siesta.connect("clicked", self.on_button_run_siesta_clicked)

    self.button_run_tddft = gtk.Button('Run tddft__iter')
    self.button_run_tddft.connect("clicked", self.on_button_run_tddft_clicked)

    self.button_run_plot = gtk.Button('Plot data')
    self.button_run_plot.connect("clicked", self.on_button_run_plot_clicked)

    Vbox.pack_start(self.Label_title, False, True, 0)
    
    Hbox = gtk.HBox(True, 0)
    Hbox.pack_start(self.button_run_siesta, False, True, 0)
    Hbox.pack_start(self.button_run_tddft, False, True, 0)
    Hbox.pack_start(self.button_run_plot, False, True, 0)

    Vbox.pack_start(Hbox, False, True, 0)

    mainWindow.add(Vbox)
    mainWindow.show_all()


  def on_mainWindow_destroy(self, widget):
    gtk.main_quit()

  def on_button_run_siesta_clicked(self, widget):
    window_siesta()

  def on_button_run_tddft_clicked(self, widget):
    window_tddft()

  def on_button_run_plot_clicked(self, widget):
    window_plot()

  def get_main_menu(self, window):
    accel_group = gtk.AccelGroup()

    item_factory = gtk.ItemFactory(gtk.MenuBar, "<main>", accel_group)

    item_factory.create_items(self.menu_items)

    window.add_accel_group(accel_group)
    self.item_factory = item_factory
    

    return item_factory.get_widget("<main>")

#################################################
#                                               #
#       Class for the siesta window              #
#                                               #
#################################################

class window_siesta:
  """
  Class that define the graphical interface to run siesta
  """
  def __init__(self):

    self.prop_fig = MBPT_LCAO_Properties_figure()
    self.prop = Properties_windows(self.prop_fig)
    self.prop_bol = False
    self.siesta = siesta()
    
    self.siesta_window = gtk.Window()
    self.siesta_window.set_title('pyfip: run Siesta')
    self.siesta_window.set_size_request(1250, 550)
    self.siesta_window.set_position(gtk.WIN_POS_CENTER)
    self.siesta_window.connect('destroy', self.on_siesta_window_destroy)


    self.menu_items = (
        ( "/_File",         None,         None, 0, "<Branch>" ),
        ( "/File/Quit",     "<control>Q", gtk.main_quit, 0, None ),
        ( "/_Options",      None,         None, 0, "<Branch>" ),
        ( "/Options/_Parameter Siesta", None, self.prop_siesta, 0, None ),
        ( "/_Help",         None,         None, 0, "<LastBranch>" ),
        ( "/_Help/About",   None,         None, 0, None ),
      )


    menubar = self.get_main_menu(self.siesta_window)
        
    self.entry_label = ['SystemName:', 'SystemLabel:', 'Atomic file name (xyz):']
    self.entry_func = list()

    length_unit = ['Ang', 'm', 'cm', 'nm', 'Bohr']
    self.combo_box_label = ['Atomic Coordinates Format:']
    self.combo_box = [length_unit]
    self.combo_box_func = list()

    table = gtk.Table(4, 4, False)
    for i in enumerate(self.entry_label):
      self.entry_func.append(wid.Entry_function(i[1]))
      table.attach(self.entry_func[i[0]].Entry_label, 0, 1, i[0], i[0]+1)
      table.attach(self.entry_func[i[0]].Entry, 1, 2, i[0], i[0]+1)

    for i in enumerate(self.combo_box_label):
      self.combo_box_func.append(wid.combo_box(i[1], self.combo_box[i[0]]))
      table.attach(self.combo_box_func[i[0]].combo_box_label, 2, 3, i[0], i[0]+1)
      table.attach(self.combo_box_func[i[0]].combo_box, 3, 4, i[0], i[0]+1)

    
    self.button_chose_file = gtk.Button('....')
    self.button_chose_file.connect("clicked", self.on_button_chose_file_clicked)
    
    self.button_run_siesta = gtk.Button('Run Siesta')
    self.button_run_siesta.connect("clicked", self.on_button_run_siesta_clicked)

    self.button_siesta_fdf = gtk.Button('Generate fdf file')
    self.button_siesta_fdf.connect("clicked", self.on_button_siesta_fdf_clicked)

    self.button_run_siesta_fdf = gtk.Button('Generate and run')
    self.button_run_siesta_fdf.connect("clicked", self.on_button_run_siesta_fdf_clicked)

    
    table.attach(self.button_chose_file, 2, 3, 2, 3)
    table.attach(self.button_siesta_fdf, 0, 1, 3, 4)
    table.attach(self.button_run_siesta, 1, 2, 3, 4)
    table.attach(self.button_run_siesta_fdf, 2, 3, 3, 4)

   
    vBox = gtk.VBox()
    vBox.set_border_width(10)
    vBox.pack_start(menubar, False, True, 0)
    vBox.pack_end(table, True, True, 0)

    self.siesta_window.add(vBox)
    self.siesta_window.show_all()


  #window properties
  def on_siesta_window_destroy(self, widget):
    self.siesta_window.destroy()
  
  def get_main_menu(self, window):
    accel_group = gtk.AccelGroup()

    item_factory = gtk.ItemFactory(gtk.MenuBar, "<main>", accel_group)

    item_factory.create_items(self.menu_items)

    window.add_accel_group(accel_group)
    self.item_factory = item_factory
    

    return item_factory.get_widget("<main>")

  def on_button_chose_file_clicked(self, widget):
    self.siesta.fname = wid.choose_file()

  #running buttons
  def on_button_run_siesta_clicked(self, widget):
    subprocess.call('siesta <' + self.siesta.param['SystemLabel'] + '.fdf |tee ' + self.siesta.param['SystemLabel'] + '.out', shell=True)
    #bol = test_tddft_iter()

    #if bol:
    #  windows_path_exec()
  
  def on_button_siesta_fdf_clicked(self, widget):

    if self.prop_bol:
      for key, value in self.prop.siesta_prop.param.items():
        self.siesta.param[key] = value
      self.siesta.list_param = self.prop.siesta_prop.list_param

    self.set_param_siesta_fdf()
    self.siesta.write_siesta_file()
  
  def on_button_run_siesta_fdf_clicked(self, widget):
    if self.prop_bol:
      for key, value in self.prop.siesta_prop.param.items():
        self.siesta.param[key] = value
      self.siesta.list_param = self.prop.siesta_prop.list_param

    self.set_param_siesta_fdf()
    self.siesta.write_siesta_file()

    subprocess.call('siesta <' + self.siesta.param['SystemLabel'] + '.fdf |tee ' + self.siesta.param['SystemLabel'] + '.out', shell=True)
    #bol = test_tddft_iter()

    #if bol:
    #  windows_path_exec()

  def prop_siesta(self, w, data):
    self.prop_bol = True
    self.prop.prop_siesta_fdf()

  

  def set_param_siesta_fdf(self):
    self.siesta.param['AtomicCoordinatesFormat'] = self.combo_box_func[0].param

    self.entry_label = ['SystemName:', 'SystemLabel:', 'Atomic file name (xyz):']
    if self.entry_func[0].get_text != '':
      self.siesta.param['SystemName'] = self.entry_func[0].get_text
    if self.entry_func[1].get_text != '':
      self.siesta.param['SystemLabel'] = self.entry_func[1].get_text
    if self.entry_func[2].get_text != '':
      self.siesta.fname = self.entry_func[2].get_text


#################################################
#                                               #
#       Class for the tddft window              #
#                                               #
#################################################

class window_tddft:
  """
  Class that define the graphical interface to run 
  tddft_iter
  """
  def __init__(self):

    self.prop_fig = MBPT_LCAO_Properties_figure()
    self.prop = Properties_windows(self.prop_fig)
    self.prop_bol = False
    self.tddft_lr = tddft_inp()
    
    self.tddft_window = gtk.Window()
    self.tddft_window.set_title('pyfip: run tddft_iter')
    self.tddft_window.set_size_request(1250, 550)
    self.tddft_window.set_position(gtk.WIN_POS_CENTER)
    self.tddft_window.connect('destroy', self.on_tddft_window_destroy)


    self.menu_items = (
        ( "/_File",         None,         None, 0, "<Branch>" ),
        ( "/File/Quit",     "<control>Q", gtk.main_quit, 0, None ),
        ( "/_Options",      None,         None, 0, "<Branch>" ),
        ( "/Options/_Parameter tddft_lr.inp", None, self.prop_tddft_lr, 0, None ),
        ( "/_Help",         None,         None, 0, "<LastBranch>" ),
        ( "/_Help/About",   None,         None, 0, None ),
      )


    menubar = self.get_main_menu(self.tddft_window)
        
    self.entry_label = ['nff:', 'System label:', 'freq__eps__win1:', 'omega__max__win1:', 'omega__min__tddft:', 'omega__max__tddft']
    self.entry_func = list()

    self.check_box_label = ['comp__dens__chng__and__polarizability', 'enh__given__coordinate__freq_set', 'enh__given__segment__and__freq',\
        'enh__given__volume__and__freq']
    self.check_box_func = list()
    check = [True, False, False, False]


    field_dir = ['x', 'y', 'z']
    
    interacting = ['Interacting', 'Non-Interacting']
    
    plot = ['Nothing', 'plot interactive', 'plot non-interactive', 'plot both']

    self.combo_box_label = ['external field direction:', 'interacting:', 'plot:']
    self.combo_box = [field_dir, interacting, plot]
    self.combo_box_func = list()


    table = gtk.Table(4, 8, False)
    for i in enumerate(self.entry_label):
      self.entry_func.append(wid.Entry_function(i[1]))
      table.attach(self.entry_func[i[0]].Entry_label, 0, 1, i[0], i[0]+1)
      table.attach(self.entry_func[i[0]].Entry, 1, 2, i[0], i[0]+1)

    for i in enumerate(self.combo_box_label):
      self.combo_box_func.append(wid.combo_box(i[1], self.combo_box[i[0]]))
      table.attach(self.combo_box_func[i[0]].combo_box_label, 2, 3, i[0], i[0]+1)
      table.attach(self.combo_box_func[i[0]].combo_box, 3, 4, i[0], i[0]+1)

    for i in enumerate(self.check_box_label):
      self.check_box_func.append(wid.check_button(i[1], check[i[0]]))
      table.attach(self.check_box_func[i[0]].check_button, 3, 4, i[0]+3, i[0]+4)

    self.button_run = gtk.Button('Run tddft__iter')
    self.button_run.connect("clicked", self.on_button_run_clicked)

    self.button_tddft_lr = gtk.Button('Generate tddft__lr.inp')
    self.button_tddft_lr.connect("clicked", self.on_button_tddft_lr_clicked)

    self.button_run_tddft = gtk.Button('Generate and run')
    self.button_run_tddft.connect("clicked", self.on_button_run_tddft_clicked)

    
    table.attach(self.button_tddft_lr, 0, 1, 7, 8)
    table.attach(self.button_run, 1, 2, 7, 8)
    table.attach(self.button_run_tddft, 2, 3, 7, 8)

   
    vBox = gtk.VBox()
    vBox.set_border_width(10)
    vBox.pack_start(menubar, False, True, 0)
    vBox.pack_end(table, True, True, 0)

    self.tddft_window.add(vBox)
    self.tddft_window.show_all()


  #window properties
  def on_tddft_window_destroy(self, widget):
    self.tddft_window.destroy()
  
  def get_main_menu(self, window):
    accel_group = gtk.AccelGroup()

    item_factory = gtk.ItemFactory(gtk.MenuBar, "<main>", accel_group)

    item_factory.create_items(self.menu_items)

    window.add_accel_group(accel_group)
    self.item_factory = item_factory
    

    return item_factory.get_widget("<main>")

  #running buttons
  def on_button_run_clicked(self, widget):
    subprocess.call('tddft', shell=True)
    bol = test_tddft_iter()

    if bol:
      windows_path_exec()
  
  def on_button_tddft_lr_clicked(self, widget):

    if self.prop_bol:
      for key, value in self.prop.tddft_lr_prop.param.items():
        self.tddft_lr.param[key] = value
      self.tddft_lr.list_param = self.prop.tddft_lr_prop.list_param

    self.set_param_tddft_iter()
    self.tddft_lr.write_tddft_inp()
  
  def on_button_run_tddft_clicked(self, widget):
    if self.prop_bol:
      for key, value in self.prop.tddft_lr_prop.param.items():
        self.tddft_lr.param[key] = value
    
    self.set_param_tddft_iter()
    self.tddft_lr.write_tddft_inp()
    
    subprocess.call('tddft', shell=True)
    bol = test_tddft_iter()

    if bol:
      windows_path_exec()

  def prop_tddft_lr(self, w, data):
    self.prop_bol = True
    self.prop.prop_tddft_lr()

  

  def set_param_tddft_iter(self):
    if self.combo_box_func[0].param == 'x':
      self.tddft_lr.param['ext_field_direction'] = 1 
    if self.combo_box_func[0].param == 'y':
      self.tddft_lr.param['ext_field_direction'] = 2
    if self.combo_box_func[0].param == 'z':
      self.tddft_lr.param['ext_field_direction'] = 3 

    if self.combo_box_func[1].param == 'Interacting':
      self.tddft_lr.list_param.append('ihartree')
      self.tddft_lr.list_param.append('iexchange')
      self.tddft_lr.list_param.append('icorrelation')
   
      self.tddft_lr.param['ihartree'] = 1
      self.tddft_lr.param['iexchange'] = 1
      self.tddft_lr.param['icorrelation'] = 1
    elif self.combo_box_func[1].param == 'Non-Interacting':
      self.tddft_lr.list_param.append('ihartree')
      self.tddft_lr.list_param.append('iexchange')
      self.tddft_lr.list_param.append('icorrelation')
      
      self.tddft_lr.param['ihartree'] = 0
      self.tddft_lr.param['iexchange'] = 0
      self.tddft_lr.param['icorrelation'] = 0
    
    if self.combo_box_func[2].param == 'Nothing':
      self.tddft_lr.list_param.append('plot_inter')
      self.tddft_lr.list_param.append('plot_nonin')
      self.tddft_lr.param['plot_inter'] = 0
      self.tddft_lr.param['plot_nonin'] = 0
    elif self.combo_box_func[2].param == 'plot interactive':
      self.tddft_lr.list_param.append('plot_inter')
      self.tddft_lr.list_param.append('plot_nonin')
      self.tddft_lr.param['plot_inter'] = 1
      self.tddft_lr.param['plot_nonin'] = 0
    elif self.combo_box_func[2].param == 'plot non-interactive':
      self.tddft_lr.list_param.append('plot_inter')
      self.tddft_lr.list_param.append('plot_nonin')
      self.tddft_lr.param['plot_inter'] = 0
      self.tddft_lr.param['plot_nonin'] = 1
    elif self.combo_box_func[2].param == 'plot both':
      self.tddft_lr.list_param.append('plot_inter')
      self.tddft_lr.list_param.append('plot_nonin')
      self.tddft_lr.param['plot_inter'] = 1
      self.tddft_lr.param['plot_nonin'] = 1


    if len(self.entry_func[0].get_text) > 0:
      self.tddft_lr.param['nff'] = str2int(self.entry_func[0].get_text)[0]
    if len(self.entry_func[1].get_text) > 0:
      self.tddft_lr.param['SystemLabel'] = self.entry_func[1].get_text
      self.tddft_lr.list_param.append('SystemLabel')
    if len(self.entry_func[2].get_text) > 0:
      self.tddft_lr.param['freq_eps_win1'] = str2float(self.entry_func[2].get_text)[0]
    if len(self.entry_func[3].get_text) > 0:
      self.tddft_lr.param['omega_max_win1'] = str2float(self.entry_func[3].get_text)[0]
    if len(self.entry_func[4].get_text) > 0:
      self.tddft_lr.param['omega_min_tddft'] = str2float(self.entry_func[4].get_text)[0]
    if len(self.entry_func[5].get_text) > 0:
      self.tddft_lr.param['omega_max_tddft'] = str2float(self.entry_func[5].get_text)[0]
    

    self.tddft_lr.list_param.append('comp_dens_chng_and_polarizability')
    self.tddft_lr.list_param.append('enh_given_coordinate_freq_set')
    self.tddft_lr.list_param.append('enh_given_segment_and_freq')
    self.tddft_lr.list_param.append('enh_given_volume_and_freq')
    self.tddft_lr.param['comp_dens_chng_and_polarizability'] = self.check_box_func[0].param
    self.tddft_lr.param['enh_given_coordinate_freq_set'] = self.check_box_func[1].param
    self.tddft_lr.param['enh_given_segment_and_freq'] = self.check_box_func[2].param
    self.tddft_lr.param['enh_given_volume_and_freq'] = self.check_box_func[3].param

#################################################
#                                               #
#       Class for the plotting part             #
#                                               #
#################################################

class window_plot:
  """
  Class that define the graphical interface of the 
  program
  """
  def __init__(self):

    self.param = MBPT_LCAO_Parameters()
    self.prop_fig = MBPT_LCAO_Properties_figure()
    self.prop = Properties_windows(self.prop_fig)
    self.ReIm = 'im'
    self.inter = 'nonin'
    self.x = 0
    self.y = 0

    #Main Window
    self.plot_window = gtk.Window()
    self.plot_window.set_title('pyfip: plot data')
    self.plot_window.set_size_request(1250, 550)
    self.plot_window.set_position(gtk.WIN_POS_CENTER)
    self.plot_window.connect('destroy', self.on_plot_window_destroy)


    #definition of the menu of the program
    self.menu_items = (
        ( "/_File",         None,         None, 0, "<Branch>" ),
        ( "/File/Quit",     "<control>Q", gtk.main_quit, 0, None ),
        ( "/_Options",      None,         None, 0, "<Branch>" ),
        ( "/Options/_Graphics", None, self.prop_graph, 0, None ),
        ( "/Options/_Arrow", None, self.prop_arrow, 0, None ),
        ( "/_Help",         None,         None, 0, "<LastBranch>" ),
        ( "/_Help/About",   None,         None, 0, None ),
      )
    
    menubar = self.get_main_menu(self.plot_window)

    #widgets
    self.entry_label = ['=', 'abscisse', 'ordonnee', 'coordinnate\n for profile']
    self.entry_func = list()

    self.check_box_label = ['spectrum', 'polarizability', 'save an\n animation (Mayavi plot only)',\
        'Show direction\n of the external field', 'iter/tem']
    self.check_box_func = list()
    check = [False, False, False, False, False]


    plan = ['x', 'y', 'z']
    
    interacting = ['Interacting', 'Non-Interacting']
    
    plot = ['Profile', 'Contour plot', 'Contour plot in relief', '3D plot (Mayavi)']

    quantity = ['Intensity', 'Efield', 'Density', 'Potential']

    output = ['pdf', 'png', 'ps', 'eps', 'svg']

    re_im = ['Real part', 'Imaginary part']

    binary = ['binary files .npy', 'HDF5 files .hdf5', 'text files .dat']

    self.combo_box_label = ['Quantity:', 'Plotting method:', 'Output:', 'Plan:', 'Interacting:', 'Re/Im', 'Binary']
    self.combo_box = [quantity, plot, output, plan, interacting, re_im, binary]
    self.combo_box_func = list()


    table = gtk.Table(7, 8, False)
    comp = 0
    for i in enumerate(self.combo_box_label):
      self.combo_box_func.append(wid.combo_box(i[1], self.combo_box[i[0]]))
      if i[1] == 'Interacting:' or i[1] == 'Re/Im' or i[1] == 'Binary':
        table.attach(self.combo_box_func[i[0]].combo_box_label, 4, 5, comp, comp+1)
        table.attach(self.combo_box_func[i[0]].combo_box, 5, 6, comp, comp+1)
        comp = comp + 1
      else:
        table.attach(self.combo_box_func[i[0]].combo_box_label, 0, 1, i[0], i[0]+1)
        table.attach(self.combo_box_func[i[0]].combo_box, 1, 2, i[0], i[0]+1)


    for i in enumerate(self.entry_label):
      self.entry_func.append(wid.Entry_function(i[1]))
      if i[1] == '=':
        table.attach(self.entry_func[i[0]].Entry, 3, 4, 3, 4)
        table.attach(self.entry_func[i[0]].Entry_label, 2, 3, 3, 4)
      elif i[1] == 'abscisse':
        table.attach(self.entry_func[i[0]].Entry_label, 4, 5, 4, 5)
        table.attach(self.entry_func[i[0]].Entry, 4, 5, 5, 6)
      elif i[1] == 'ordonnee':
        table.attach(self.entry_func[i[0]].Entry_label, 5, 6, 4, 5)
        table.attach(self.entry_func[i[0]].Entry, 5, 6, 5, 6)
      elif i[1] == 'coordinnate\n for profile':
        table.attach(self.entry_func[i[0]].Entry, 1, 2, 4, 5)
        table.attach(self.entry_func[i[0]].Entry_label, 0, 1, 4, 5)

    for i in enumerate(self.check_box_label):
      self.check_box_func.append(wid.check_button(i[1], check[i[0]]))
      if i[1] == 'spectrum':
        table.attach(self.check_box_func[i[0]].check_button, 1, 2, 5, 6)
      elif i[1] == 'polarizability':
        table.attach(self.check_box_func[i[0]].check_button, 1, 2, 6, 7)
      elif i[1] == 'save an\n animation (Mayavi plot only)':
        table.attach(self.check_box_func[i[0]].check_button, 2, 3, 5, 6)
      elif i[1] == 'Show direction\n of the external field':
        table.attach(self.check_box_func[i[0]].check_button, 2, 3, 6, 7)
      elif i[1] == 'iter/tem':
        table.attach(self.check_box_func[i[0]].check_button, 3, 4, 5, 6)


    self.label_Ang = gtk.Label('Bohr')
    self.button_run = gtk.Button('Plot')
    self.button_run.connect("clicked", self.on_button_run_clicked)

    table.attach(self.label_Ang, 4, 5, 3, 4)
    table.attach(self.button_run, 3, 4, 6, 7)
  
    vBox = gtk.VBox()
    vBox.set_border_width(10)
    vBox.pack_start(menubar, False, True, 0)
    vBox.pack_end(table, True, True, 0)

    self.plot_window.add(vBox)
    self.plot_window.show_all()


  def on_plot_window_destroy(self, widget):
    self.plot_window.destroy()

  def set_parameter_graph(self):

    if self.check_box_func[0].param == 1:
      self.param.fname = 'freq_dep_dens_chng_'
    if self.check_box_func[1].param == 1:
      self.param.fname = 'Polarizability'

    if self.check_box_func[0].param == 0 and self.check_box_func[1].param == 0:
      if self.combo_box_func[0].param == 'Efield':
        self.param.fname = 'Efield'
      elif self.combo_box_func[0].param == 'Intensity':
        self.param.fname = 'intensity'
      elif self.combo_box_func[0].param == 'Density':
        self.param.fname = 'density'
      elif self.combo_box_func[0].param == 'Potential':
        self.param.fname = 'potential'
      else:
        self.param.fname = 'intensity'

  
    if self.combo_box_func[1].param == 'Contour plot':
      self.prop_fig.plot = '2D'
    if self.combo_box_func[1].param == 'Profile':
      self.prop_fig.plot = '1D'
    if self.combo_box_func[1].param == 'Contour plot in relief':
      self.prop_fig.plot = '3D'
    if self.combo_box_func[1].param == '3D plot (Mayavi)':
      self.prop_fig.plot = 'Mayavi'

  
    if self.combo_box_func[2].param == 'png':
      self.prop_fig.output = 'png'
    if self.combo_box_func[2].param == 'pdf':
      self.prop_fig.output = 'pdf'
    if self.combo_box_func[2].param == 'ps':
      self.prop_fig.output = 'ps'
    if self.combo_box_func[2].param == 'eps':
      self.prop_fig.output = 'eps'
    if self.combo_box_func[2].param == 'svg':
      self.prop_fig.plan = 'svg'
  
    if self.combo_box_func[3].param == 'x':
      self.prop_fig.plan = 'x'
    if self.combo_box_func[3].param == 'y':
      self.prop_fig.plan = 'y'
    if self.combo_box_func[3].param == 'z':
      self.prop_fig.plan = 'z'

    if self.combo_box_func[4].param == 'Interacting':
      self.param.interacting = 1
      self.inter = 'inter'
    if self.combo_box_func[4].param == 'Non-Interacting':
      self.param.interacting = 0
      self.inter = 'nonin'

    if self.combo_box_func[5].param == 'Real part':
      self.param.ReIm = 're'
      self.ReIm = 're'
    if self.combo_box_func[5].param == 'Imaginary part':
      self.param.ReIm = 'im'
      self.ReIm = 'im'

    if self.combo_box_func[6].param == 'binary files .npy':
      self.param.format_input = '.npy'
    if self.combo_box_func[6].param == 'text files .dat':
      self.param.format_input = '.dat'
    if self.combo_box_func[6].param == 'HDF5 files .hdf5':
      self.param.format_input = '.hdf5'
    

    if self.check_box_func[2].param == 1: 
      self.prop_fig.animation = 1
    else:
      self.prop_fig.animation = 0
    
    if self.check_box_func[3].param == 1:
      self.prop_fig.Edir = 1
    else:
      self.prop_fig.Edir = 0

    if self.check_box_func[4].param == 1:
      self.param.tem_iter = 1
    else:
      self.param.tem_iter = 0


    if len(str2float(self.entry_func[0].get_text)) > 0:
      self.prop_fig.plan_coord = str2float(self.entry_func[0].get_text)[0]
    if len(str2float(self.entry_func[1].get_text)) > 0:
      self.prop_fig.coord_Ef = [(str2float(self.entry_func[1].get_text)[0], str2float(self.entry_func[2].get_text)[0]), (0, 0)]
    if len(str2float(self.entry_func[3].get_text)) > 0:
      self.prop_fig.coord = np.array([str2float(self.entry_func[3].get_text)[0], str2float(self.entry_func[3].get_text)[1], str2float(self.entry_func[3].get_text)[2]])


  #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  #Run Button
  def on_button_run_clicked(self, widget):
    self.set_parameter_graph()
    if self.param.fname == 'Efield':
      name = 'Efield'
      print(self.param.fname)
      plot_3D(self.param, self.prop.prop_fig)
    elif self.param.fname == 'intensity':
      name = 'intensity'
      #self.param.fname = self.param.fname + '_' + self.inter
      print(self.param.fname)
      plot_3D(self.param, self.prop.prop_fig)
    elif self.param.fname == 'freq_dep_dens_chng_':
      name = 'freq_dep_dens_chng_'
      self.param.fname = self.param.fname + self.inter
      print(self.param.fname)
      plot_3D(self.param, self.prop.prop_fig)

    elif self.param.fname == 'Polarizability':
      name = 'Polarizability'
      #self.param.fname = self.param.fname + '_' + self.inter + '_iter_krylov_' + self.ReIm + '.txt'
      #if self.inter == 'nonin':
      #  self.param.fname = name + '_' + self.inter + '_iter_krylov_im' + '.txt'
      print(self.param.fname)
      plot_3D(self.param, self.prop.prop_fig)
    else:
      name = 'density'
      #self.param.fname = self.param.fname + '_' + self.ReIm + '_' + self.inter
      print(self.param.fname)
      plot_3D(self.param, self.prop.prop_fig)
    self.param.fname = name

  #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1

  #definition of the menu
  def get_main_menu(self, window):
    accel_group = gtk.AccelGroup()

    item_factory = gtk.ItemFactory(gtk.MenuBar, "<main>", accel_group)

    item_factory.create_items(self.menu_items)

    window.add_accel_group(accel_group)
    self.item_factory = item_factory

    return item_factory.get_widget("<main>")

  #Access Properties windows
  def prop_graph(self, w, data):
    self.prop.prop_graphic()
  
  def prop_arrow(self, w, data):
    self.prop.prop_arrow()

  def prop_tddft_lr(self, w, data):
    self.prop.prop_tddft_lr(self.tddft_lr)


#################################################################
#                                                               #
#                     Properties Class                          #
#                                                               #
#################################################################

class Properties_windows:
  """
  class that define the properties for different parameters of the program.
  In particular the properties of the plotting wiindows.
  Parameters:
  -----------
    self.fontsize (float, default: 30): fontsize of the labels
    self.axis_size (list of float, default: [20, 20]): fontsize of the tickle
    self.folder (string, default: 'images/'): folders where are save the pictures
    self.figx = 16
    self.figy = 12
    self.figsize (tuple, default: (self.figx, self.figy)): size of the figure (width, heigth)
    self.fatoms (string, default:'domiprod_atom2coord.xyz'): name of the file for the atomic positions
    self.plot_atoms (bolleen, default: True): plotting atoms or not
    self.color_arrow = 'red'
    self.ft_arrow_label = 30
    self.arrow_label = r'$E_{ext}$'
    self.title = 'none'
    self.linewidth = 3
    self.linecolor = 'red'
  """
  def __init__(self, prop_fig):

    self.prop_fig = prop_fig#Properties_figure()

  def prop_graphic(self):
    self.GraphicWindow = gtk.Window()
    self.GraphicWindow.set_title('pyfip: Properties graphics')
    self.GraphicWindow.set_size_request(600, 600)
    self.GraphicWindow.set_position(gtk.WIN_POS_CENTER)
    self.GraphicWindow.connect('destroy', self.on_GraphicWindow_destroy)

    self.entry_label = ['Label font size:', 'x axis font size:', 'y axis font size:', 'folder name:',\
        'width:', 'height:', 'file atoms position:']
    
    self.entry_func = list()

    atoms = ['yes', 'no']
    
    self.combo_box_label = ['Plot atoms (option only for contour plot mode)']
    self.combo_box = [atoms]
    self.combo_box_func = list()

    table = gtk.Table(2, 10, False)
    for i in enumerate(self.combo_box_label):
      self.combo_box_func.append(wid.combo_box(i[1], self.combo_box[i[0]]))
      table.attach(self.combo_box_func[i[0]].combo_box_label, 0, 1, i[0], i[0]+1)
      table.attach(self.combo_box_func[i[0]].combo_box, 1, 2, i[0], i[0]+1)

    for i in enumerate(self.entry_label):
      self.entry_func.append(wid.Entry_function(i[1]))
      table.attach(self.entry_func[i[0]].Entry, 1, 2, i[0]+2, i[0]+3)
      table.attach(self.entry_func[i[0]].Entry_label, 0, 1, i[0]+2, i[0]+3)

    self.button_ok_graphic = gtk.Button('Ok')
    self.button_ok_graphic.connect("clicked", self.on_GraphicWindow_destroy)

    table.attach(self.button_ok_graphic, 1, 2, 9, 10)
    
    vBox = gtk.VBox()
    vBox.set_border_width(10)
    vBox.pack_end(table, True, True, 0)

    self.GraphicWindow.add(vBox)
    self.GraphicWindow.show_all()

    self.set_graphic_parameters()

  def prop_arrow(self):

    self.ArrowWindow = gtk.Window()
    self.ArrowWindow.set_title('pyfip: Properties arrow')
    self.ArrowWindow.set_size_request(400, 200)
    self.ArrowWindow.set_position(gtk.WIN_POS_CENTER)
    self.ArrowWindow.connect('destroy', self.on_ArrowWindow_destroy)

    self.entry_label = ['Color arrow:', 'Label arrow:', 'Fontsize label arrow:']
    
    self.entry_func = list()

    table = gtk.Table(2, 4, False)

    for i in enumerate(self.entry_label):
      self.entry_func.append(wid.Entry_function(i[1]))
      table.attach(self.entry_func[i[0]].Entry, 1, 2, i[0], i[0]+1)
      table.attach(self.entry_func[i[0]].Entry_label, 0, 1, i[0], i[0]+1)

    self.button_ok_arrow = gtk.Button('Ok')
    self.button_ok_arrow.connect("clicked", self.on_ArrowWindow_destroy)


    table.attach(self.button_ok_arrow, 1, 2, 3, 4)

    vBox = gtk.VBox()
    vBox.set_border_width(10)
    vBox.pack_end(table, True, True, 0)

    self.ArrowWindow.add(vBox)
    self.ArrowWindow.show_all()

  def prop_siesta_fdf(self):
    self.siesta_prop = siesta()

    self.Siesta_Window = gtk.Window()
    self.Siesta_Window.set_title('pyfip: Parameter siesta.fdf')
    self.Siesta_Window.set_size_request(600, 800)
    self.Siesta_Window.set_position(gtk.WIN_POS_CENTER)
    self.Siesta_Window.connect('destroy', self.on_Siesta_Window_destroy)

    scrolled_window = gtk.ScrolledWindow()
    scrolled_window.set_border_width(10)
    scrolled_window.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_ALWAYS)

    self.Siesta_Window.add(scrolled_window)

    self.entry_label_nb = self.siesta_prop.list_param_nb
    self.entry_func_nb = list()
    
    self.entry_label_vec = self.siesta_prop.list_param_vec
    self.entry_func_vec = list()

    self.combo_box_label_logic = self.siesta_prop.list_param_logic
    self.combo_box_func_logic = list()
    
    self.entry_label_str = self.siesta_prop.list_param_str
    self.entry_func_str = list()
    
    self.entry_label_dic = self.siesta_prop.list_param_dic
    self.entry_func_dic = list()
    self.combo_box_func_dic = list()

    self.entry_label_block = ['LatticeVectors', 'SuperCell', 'kgrid_Monkhorst_Pack']
    self.block_dim = [[3, 3], [3, 3], [3, 4]]
    self.entry_func_block = list()
    
    table = gtk.Table(5, len(self.entry_label_nb)+ len(self.entry_label_vec)+ len(self.combo_box_label_logic)+\
        len(self.entry_label_str) + len(self.entry_label_dic) + 3*len(self.entry_label_block) + 1, False)

    for i in enumerate(self.entry_label_nb):
      self.entry_func_nb.append(wid.Entry_function(i[1] + '  (scalar)'))
      table.attach(self.entry_func_nb[i[0]].Entry, 1, 2, i[0], i[0]+1)
      table.attach(self.entry_func_nb[i[0]].Entry_label, 0, 1, i[0], i[0]+1)


    for i in enumerate(self.entry_label_vec):
      if i[1] == 'ExternalElectricField':
        self.entry_func_vec.append(wid.Entry_function(i[1] + '  (vector)'))
        table.attach(self.entry_func_vec[i[0]].Entry, 1, 2, len(self.entry_label_nb) + i[0], len(self.entry_label_nb) + i[0]+1)
        self.combo_box_efield = gtk.combo_box_new_text()
        self.combo_box_efield.connect('changed', self.on_changed_efield)
        self.unit_efield = self.siesta_prop.param['Efield_unit'][0]
        for j in self.siesta_prop.param['Efield_unit']:
          self.combo_box_efield.append_text(j)

        table.attach(self.combo_box_efield, 2, 3, len(self.entry_label_nb) + i[0], len(self.entry_label_nb) + i[0]+1)
        table.attach(self.entry_func_vec[i[0]].Entry_label, 0, 1, len(self.entry_label_nb) + i[0], len(self.entry_label_nb) + i[0]+1)
      else:
        self.entry_func_vec.append(wid.Entry_function(i[1] + '  (vector)'))
        table.attach(self.entry_func_vec[i[0]].Entry, 1, 2, len(self.entry_label_nb) + i[0], len(self.entry_label_nb) + i[0]+1)
        table.attach(self.entry_func_vec[i[0]].Entry_label, 0, 1, len(self.entry_label_nb) + i[0], len(self.entry_label_nb) + i[0]+1)

    for i in enumerate(self.combo_box_label_logic):
      self.combo_box_func_logic.append(wid.combo_box(i[1] + '  (logic)', ['.true.', '.false.']))
      table.attach(self.combo_box_func_logic[i[0]].combo_box, 1, 2, len(self.entry_label_nb) + len(self.entry_label_vec) + i[0],\
          len(self.entry_label_nb) + len(self.entry_label_vec) + i[0]+1)
      table.attach(self.combo_box_func_logic[i[0]].combo_box_label, 0, 1, len(self.entry_label_nb) + len(self.entry_label_vec) + i[0],\
          len(self.entry_label_nb) + len(self.entry_label_vec) + i[0]+1)

    for i in enumerate(self.entry_label_str):
      self.entry_func_str.append(wid.Entry_function(i[1] + '  (string)'))
      table.attach(self.entry_func_str[i[0]].Entry, 1, 2, len(self.entry_label_nb) + len(self.entry_label_vec) + len(self.combo_box_label_logic)  + i[0],\
          len(self.entry_label_nb) + len(self.entry_label_vec) + len(self.combo_box_label_logic) + i[0]+1)
      table.attach(self.entry_func_str[i[0]].Entry_label, 0, 1, len(self.entry_label_nb) + len(self.entry_label_vec) + len(self.combo_box_label_logic) + i[0],\
          len(self.entry_label_nb) + len(self.entry_label_vec) + len(self.combo_box_label_logic) + i[0]+1)

    for i in enumerate(self.entry_label_dic):
      self.entry_func_dic.append(wid.Entry_function(i[1] + '  (scalar)'))
      if self.siesta_prop.param[i[1]]['quantity'] == 'energie':
        self.combo_box_func_dic.append(wid.combo_box('', self.siesta_prop.param['energy_unit']))
      elif self.siesta_prop.param[i[1]]['quantity'] == 'pressure':
        self.combo_box_func_dic.append(wid.combo_box('', self.siesta_prop.param['pressure_unit']))
      elif self.siesta_prop.param[i[1]]['quantity'] == 'length':
        self.combo_box_func_dic.append(wid.combo_box('', self.siesta_prop.param['length_unit']))
      elif self.siesta_prop.param[i[1]]['quantity'] == 'time':
        self.combo_box_func_dic.append(wid.combo_box('', self.siesta_prop.param['time_unit']))
      elif self.siesta_prop.param[i[1]]['quantity'] == 'mass':
        self.combo_box_func_dic.append(wid.combo_box('', self.siesta_prop.param['mass_unit']))
      elif self.siesta_prop.param[i[1]]['quantity'] == 'force':
        self.combo_box_func_dic.append(wid.combo_box('', self.siesta_prop.param['force_unit']))
      elif self.siesta_prop.param[i[1]]['quantity'] == 'charge':
        self.combo_box_func_dic.append(wid.combo_box('', self.siesta_prop.param['charge_unit']))
      elif self.siesta_prop.param[i[1]]['quantity'] == 'dipole':
        self.combo_box_func_dic.append(wid.combo_box('', self.siesta_prop.param['dipole_unit']))
      elif self.siesta_prop.param[i[1]]['quantity'] == 'MomInert':
        self.combo_box_func_dic.append(wid.combo_box('', self.siesta_prop.param['MomInert_unit']))
      elif self.siesta_prop.param[i[1]]['quantity'] == 'Efield':
        self.combo_box_func_dic.append(wid.combo_box('', self.siesta_prop.param['Efield_unit']))
      elif self.siesta_prop.param[i[1]]['quantity'] == 'angle':
        self.combo_box_func_dic.append(wid.combo_box('', self.siesta_prop.param['angle_unit']))
      elif self.siesta_prop.param[i[1]]['quantity'] == 'torque':
        self.combo_box_func_dic.append(wid.combo_box('', self.siesta_prop.param['torque_unit']))

      table.attach(self.entry_func_dic[i[0]].Entry, 1, 2, len(self.entry_label_nb) + len(self.entry_label_vec) + len(self.combo_box_label_logic) + len(self.entry_label_str)+ i[0],\
          len(self.entry_label_nb) + len(self.entry_label_vec) + len(self.combo_box_label_logic) + len(self.entry_label_str) + i[0]+1)
      table.attach(self.entry_func_dic[i[0]].Entry_label, 0, 1, len(self.entry_label_nb) + len(self.entry_label_vec) + len(self.combo_box_label_logic) + len(self.entry_label_str) + i[0],\
          len(self.entry_label_nb) + len(self.entry_label_vec) + len(self.combo_box_label_logic) + len(self.entry_label_str) + i[0]+1)
      table.attach(self.combo_box_func_dic[i[0]].combo_box, 2, 3, len(self.entry_label_nb) + len(self.entry_label_vec) + len(self.combo_box_label_logic) + len(self.entry_label_str) + i[0],\
          len(self.entry_label_nb) + len(self.entry_label_vec) + len(self.combo_box_label_logic) + len(self.entry_label_str) + i[0]+1)

    #block
    for i in enumerate(self.entry_label_block):
      self.entry_func_block.append(wid.Entry_function_block(i[1] + '  (block)', self.block_dim[i[0]][0], self.block_dim[i[0]][1]))

      table.attach(self.entry_func_block[i[0]].Entry_label, 0, 1, len(self.entry_label_nb) + len(self.entry_label_vec) + len(self.combo_box_label_logic) +\
          len(self.entry_label_dic) + len(self.entry_label_str) + 3*i[0], len(self.entry_label_nb) + len(self.entry_label_vec) + len(self.combo_box_label_logic)\
          + len(self.entry_label_dic) + len(self.entry_label_str) + 3*i[0]+1)
      comp = 0
      for j in range(self.block_dim[i[0]][0]):
        for k in range(self.block_dim[i[0]][1]):
          table.attach(self.entry_func_block[i[0]].Entry[comp].Entry, 1 + k, 2 + k, len(self.entry_label_nb) + len(self.entry_label_vec) + len(self.combo_box_label_logic)  +\
              len(self.entry_label_dic) + len(self.entry_label_str) + j + 3*i[0],len(self.entry_label_nb) + len(self.entry_label_vec) + len(self.combo_box_label_logic) +\
              len(self.entry_label_dic) + len(self.entry_label_str) + j+ 3*i[0]+1)
          comp = comp + 1


    self.button_ok_Siesta = gtk.Button('Ok')
    self.button_ok_Siesta.connect("clicked", self.on_Siesta_Window_destroy)

    table.attach(self.button_ok_Siesta, 1, 2, len(self.entry_label_nb) + len(self.entry_label_vec) + len(self.combo_box_label_logic)+len(self.entry_label_str) + len(self.entry_label_dic) + 3*len(self.entry_label_block),\
        len(self.entry_label_nb) + len(self.entry_label_vec) + len(self.combo_box_label_logic) + len(self.entry_label_str) + len(self.entry_label_dic) + 3*len(self.entry_label_block) + 1)

    scrolled_window.add_with_viewport(table)
    self.Siesta_Window.show_all()


  def prop_tddft_lr(self):
    self.tddft_lr_prop = tddft_inp()

    self.TDDFT_lrWindow = gtk.Window()
    self.TDDFT_lrWindow.set_title('pyfip: Parameter tddft_lr.inp')
    self.TDDFT_lrWindow.set_size_request(600, 800)
    self.TDDFT_lrWindow.set_position(gtk.WIN_POS_CENTER)
    self.TDDFT_lrWindow.connect('destroy', self.on_TDDFT_lrWindow_destroy)

    scrolled_window = gtk.ScrolledWindow()
    scrolled_window.set_border_width(10)
    scrolled_window.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_ALWAYS)

    self.TDDFT_lrWindow.add(scrolled_window)

    self.entry_label = self.tddft_lr_prop.list_param_total
    self.entry_func = list()

    table = gtk.Table(4, len(self.entry_label)+1, False)

    for i in enumerate(self.entry_label):
      self.entry_func.append(wid.Entry_function(i[1]))
      table.attach(self.entry_func[i[0]].Entry, 1, 2, i[0], i[0]+1)
      table.attach(self.entry_func[i[0]].Entry_label, 0, 1, i[0], i[0]+1)



    self.button_ok_TDDFT_lr = gtk.Button('Ok')
    self.button_ok_TDDFT_lr.connect("clicked", self.on_TDDFT_lrWindow_destroy)

    table.attach(self.button_ok_TDDFT_lr, 1, 2, len(self.entry_label), len(self.entry_label)+1)

    scrolled_window.add_with_viewport(table)
    self.TDDFT_lrWindow.show_all()




  #close window function!!!
  def on_GraphicWindow_destroy(self, widget):
    self.set_graphic_parameters()
    self.GraphicWindow.destroy()

  def on_ArrowWindow_destroy(self, widget):
    self.set_arrow_parameters()
    self.ArrowWindow.destroy()

  def on_TDDFT_lrWindow_destroy(self, widget):
    self.set_tddft_parameters()
    self.TDDFT_lrWindow.destroy()
  
  def on_Siesta_Window_destroy(self, widget):
    self.set_siesta_parameters()
    self.Siesta_Window.destroy()



  ###############################
  #       Graphic functions     #
  ###############################
  def set_graphic_parameters(self):
    if len(self.entry_func[0].get_text) > 0:
      self.prop_fig.fontsize = str2float(self.entry_func[0].get_text)[0]
    if len(self.entry_func[1].get_text) > 0:
      self.prop_fig.axis_size[0] = str2float(self.entry_func[1].get_text)[0]
    if len(self.entry_func[2].get_text) > 0:
      self.prop_fig.axis_size[1] = str2float(self.entry_func[2].get_text)[0]
    if len(self.entry_func[3].get_text) > 0:
      self.prop_fig.folder = self.entry_func[3].get_text
    if len(self.entry_func[4].get_text) > 0:
      self.prop_fig.figsize = (str2float(self.entry_func[4].get_text)[0], str2float(self.entry_func[5].get_text)[0])
    if len(self.entry_func[6].get_text) > 0:
      self.prop_fig.fatoms = self.entry_func[6].get_text

    if self.combo_box_func[0].param == 'yes':
      self.prop_fig.plot_atm = True
    elif self.combo_box_func[0].param == 'no':
      self.prop_fig.plot_atm = False
  
  #################################
  #       Arrow functions         #
  #################################
  def set_arrow_parameters(self):
    if len(self.entry_func[0].get_text) > 0:
      self.prop_fig.color_arrow = self.entry_func[0].get_text
    if len(self.entry_func[1].get_text) > 0:
      self.prop_fig.arrow_label = self.entry_func[1].get_text
    if len(self.entry_func[2].get_text) > 0:
      self.prop_fig.ft_arrow_label = str2float(self.entry_func[2].get_text)[0]

  #########################################
  #         TDDFT_lr.inp functions        #
  #########################################

  def set_tddft_parameters(self):

    for i in enumerate(self.entry_label):
      if len(self.entry_func[i[0]].get_text) > 0:
        self.tddft_lr_prop.list_param = Add_element_list(self.tddft_lr_prop.list_param, i[1])
        if type(self.tddft_lr_prop.param[i[1]]) == type(np.array([0.5])):
          self.tddft_lr_prop.param[i[1]] = np.array(str2float(self.entry_func[i[0]].get_text))
        elif type(self.tddft_lr_prop.param[i[1]]) == type('string'):
          self.tddft_lr_prop.param[i[1]] = self.entry_func[i[0]].get_text
        elif type(self.tddft_lr_prop.param[i[1]]) == type(1):
          self.tddft_lr_prop.param[i[1]] = str2int(self.entry_func[i[0]].get_text)[0]
        else:
          self.tddft_lr_prop.param[i[1]] = str2float(self.entry_func[i[0]].get_text)[0]
  
  #########################################
  #         siesta.fdf functions          #
  #########################################


  def set_siesta_parameters(self):
    for i in enumerate(self.entry_label_nb):
      if len(self.entry_func_nb[i[0]].get_text) > 0:
        self.siesta_prop.list_param = Add_element_list(self.siesta_prop.list_param, i[1])
        if type(self.siesta_prop.param[i[1]]) == type(1):
          self.siesta_prop.param[i[1]] = str2int(self.entry_func_nb[i[0]].get_text)[0]
        else:
          self.siesta_prop.param[i[1]] = str2float(self.entry_func_nb[i[0]].get_text)[0]

    for i in enumerate(self.entry_label_vec):
      if len(self.entry_func_vec[i[0]].get_text) > 0:
        self.siesta_prop.list_param = Add_element_list(self.siesta_prop.list_param, i[1])
        if type(self.siesta_prop.param[i[1]]) == type(np.array([0.5])):
          self.siesta_prop.param[i[1]] = np.array(str2float(self.entry_func_vec[i[0]].get_text))
      if i[1] == 'ExternalElectricField' and len(self.entry_func_vec[i[0]].get_text)>0:
        self.siesta_prop.param[i[1]][0] = str2float(self.entry_func_vec[i[0]].get_text)[0]
        self.siesta_prop.param[i[1]][1] = str2float(self.entry_func_vec[i[0]].get_text)[1]
        self.siesta_prop.param[i[1]][2] = str2float(self.entry_func_vec[i[0]].get_text)[2]
        self.siesta_prop.param[i[1]][3] = self.unit_efield

    for i in enumerate(self.combo_box_label_logic):
      if self.combo_box_func_logic[i[0]].activate:
        self.siesta_prop.list_param = Add_element_list(self.siesta_prop.list_param, i[1])
        self.siesta_prop.param[i[1]] = self.combo_box_func_logic[i[0]].param

    for i in enumerate(self.entry_label_str):
      if len(self.entry_func_str[i[0]].get_text) > 0:
        self.siesta_prop.list_param = Add_element_list(self.siesta_prop.list_param, i[1])
        self.siesta_prop.param[i[1]] = self.entry_func_str[i[0]].get_text

    for i in enumerate(self.entry_label_dic):
      if len(self.entry_func_dic[i[0]].get_text) > 0:
        self.siesta_prop.list_param = Add_element_list(self.siesta_prop.list_param, i[1])
        self.siesta_prop.param[i[1]]['Valeur'] = str2float(self.entry_func_dic[i[0]].get_text)[0]
        self.siesta_prop.param[i[1]]['unit'] = self.combo_box_func_dic[i[0]].param

    for i in enumerate(self.entry_label_block):
      number_list = list()
      for j in enumerate(self.entry_func_block[i[0]].Entry):
        if len(j[1].get_text)>0:
          self.entry_func_block[i[0]].activate = True
      if self.entry_func_block[i[0]].activate:
        self.siesta_prop.list_param = Add_element_list(self.siesta_prop.list_param, i[1])
        for j in enumerate(self.entry_func_block[i[0]].Entry):
          if len(j[1].get_text)>0:
            number_list.append(str2float(j[1].get_text)[0])
          elif len(j[1].get_text)==0:
            number_list.append(0.0)

        self.siesta_prop.param[i[1]] = list2array(number_list, self.siesta_prop.param[i[1]].shape[0], self.siesta_prop.param[i[1]].shape[1])



  def on_changed_efield(self, widget):
    self.unit_efield = widget.get_active_text()
#################################################################
#                                                               #
#           Class for path tddft_iter executable                #
#                                                               #
#################################################################

class windows_path_exec:
  """
  enter the path of the tddft_iter executable in a dialog window
  """
  def __init__(self):

    self.path = ''
    self.Path_Window = gtk.Window()
    #self.TDDFT_lrWindow = gtk.Dialog()
    self.Path_Window.set_title('pyfip: Parameter tddft_lr.inp')
    self.Path_Window.set_size_request(500, 200)
    self.Path_Window.set_position(gtk.WIN_POS_CENTER)
    self.Path_Window.connect('destroy', self.on_Path_Window_destroy)

    self.Label_Path = gtk.Label('Path of tddft__iter executable:')
    self.Entry_Path = gtk.Entry()
    self.Entry_Path.add_events(gtk.gdk.KEY_RELEASE_MASK)
    self.Entry_Path.connect('key-release-event', self.on_Entry_Path_key)
    self.Entry_Path_label = gtk.Label('....')

    self.button_ok_Path = gtk.Button('Ok')
    self.button_ok_Path.connect("clicked", self.on_button_ok_Path_key)


    table = gtk.Table(2, 2, False)

    table.attach(self.Label_Path, 0, 1, 0, 1)
    table.attach(self.Entry_Path, 1, 2, 0, 1)

    table.attach(self.button_ok_Path, 1, 2, 1, 2)

    vBox = gtk.VBox()
    vBox.set_border_width(10)
    vBox.pack_end(table, True, True, 0)
    #vBox.pack_start(scrolled_window, False, True, 0)

    self.Path_Window.add(vBox)
    self.Path_Window.show_all()


  def on_Entry_Path_key(self, widget, event):
    self.Entry_Path_label.set_text(widget.get_text())
    if len(widget.get_text())>0:
      print(self.path)
      self.path = widget.get_text()

  def on_Path_Window_destroy(self, widget):
    self.Path_Window.destroy()
  
  def on_button_ok_Path_key(self, widget):
    #run_tddft_iter(self.path)
    subprocess.call(self.path, shell=True)
    self.Path_Window.destroy()


#Run tddft_iter
def test_tddft_iter():

  direc = os.getcwd()
  files= []
  for (dirpath, dirnames, filenames) in os.walk(direc):
    files.extend(filenames)
    break

  print(direc)
  print(files)

  tddft_notin_bashrc = True
  for i in files:
    if i == 'domiprod_atom2coord.xyz':
      tddft_notin_bashrc = False

  return tddft_notin_bashrc

