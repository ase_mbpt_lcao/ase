from __future__ import division
import matplotlib.pyplot as plt
import numpy as np
import math
from ase.calculators.siesta.mbpt_lcao_io import read_mbpt_lcao_output
import ase.io as io
#import look_dens
import time
#import ase.units as units
#import sys

def upper_tri_matrix2dense(ap, N):
    a = np.zeros((N, N))

    count = 0
    for i in range(N):
        for j in range(i, N):
            a[i, j] = ap[count]
            count += 1
        for j in range(0, i):
            a[i, j] = a[j, i]

    return a
#
#
#
def Array_ind_to_pts_coord(data, indice, axis='z'):
  """
  trnasform the indices of an array into coordinate (in Bohr) 
  """

  if indice.shape[0] == 2:
    if axis == 'x':
      coord = (indice + data.lbound[1:2])*data.dr[1:2] + data.origin[1:2]
    elif axis == 'y':
      coord = (indice + [data.lbound[0], data.lbound[2]])*[data.dr[0], data.dr[2]] +\
              [data.origin[0], data.origin[2]]
    elif axis == 'z':
      coord = (indice + data.lbound[0:1])*data.dr[0:1] + data.origin[0:1]
    else:
      raise ValueError("axis must be x, y, or z")

  elif indice.shape[0] == 3:
    coord = (indice + data.lbound)*data.dr + data.origin
  else:
    raise ValueError("indice must be an array of shape 2 or 3")

  return coord

#
#
#
def pts_coord_to_Array_ind(data, coord):
  """
    transform a point coordinate (in Angstrom) into array index
    lbound is negative.
    the box is centered on 0
  """
  ind = np.zeros((3), dtype=int)

  for i, c in enumerate(coord):
    ind[i] = np.rint(((c-data.origin[i])/data.dr[i]) - data.lbound[i])

  return ind
#
#
#
def interpolate(x, y, **kw):
    """
    perform a 1D interpolation.
    x and y are arrays of values used to approximate some 
    function f: y = f(x). This class returns a function whose call 
    method uses interpolation to find the value of new points.
    INPUT PARAMETERS
    ----------------
        x (1D np array)
        y (1D np array)
        kind (string, default: quadratic): 
            Specifies the kind of interpolation as a string 
            (lorentz, gauss, linear, nearest, zero, slinear, quadratic, cubic where 
            zero, slinear, quadratic and cubic refer to a spline 
            interpolation of zeroth, first, second or third order). Default is quadratic.
    OUTPUT PARAMETERS
    -----------------
        func : the returned function
        popt : the argument of the function (empty list if not lorentz or gauss)

    Example:
    --------
       >>> import numpy as np
       >>> from ase.mbpt_lcao_utils.utils import interpolate
       >>> x = np.arange(0, 10)
       >>> y = np.exp(-x/3.0)
       >>> f, popt = interpolate(x, y)
       >>> xnew = np.arange(0, 9, 0.1)
       >>> ynew = f(xnew) 
       >>> import matplotlib.pyplot as plt
       >>> plt.plot(x, y, 'o', xnew, ynew, '-')
       >>> plt.show()
    """
    import scipy.interpolate as interp
    import scipy.optimize as opti
    import warnings

    kind = kw["kind"] if "kind" in kw else "cubic"

    if kind == "lorentz":
        try:
            func = fit_lorentzian
            popt, pcov = opti.curve_fit(func, x, y, **kw)
        except:
            warnings.warn('Lorentz fit failed, falling back to quadratic')
            func = interp.interp1d(x, y, kind="cubic")
            popt = []
    elif kind == "gauss":
        try:
            func = fit_gaussian
            popt, pcov = opti.curve_fit(func, x, y, **kw)
        except:
            warnings.warn('Gauss fit failed, falling back to quadratic')
            func = interp.interp1d(x, y, kind="cubic")
            popt = []
    else:
        func = interp.interp1d(x, y, kind=kind)
        popt = []

    return func, popt

#
#
#
def fit_lorentzian(x, x0, gamma, I):
    return I/(1+((x-x0)/gamma)**2)

#
#
#
def fit_gaussian(x, x0, gamma, I):
    return I*np.exp(-((x-x0)**2)/(2*gamma**2))

  
 #
 #
 #
def rotMat3D(axis, t, tol=1e-12):
    """
    Return the rotation matrix for 3D rotation by angle `angle` degrees about an
    arbitrary axis `axis`.
    
    t: angle in radian
    """
    #t = np.radians(angle)
    x, y, z = axis
    R = (np.cos(t))*np.eye(3) +\
        (1-np.cos(t))*np.matrix(((x**2,x*y,x*z),(x*y,y**2,y*z),(z*x,z*y,z**2))) + \
        np.sin(t)*np.matrix(((0,-z,y),(z,0,-x),(-y,x,0)))
    R[np.abs(R)<tol]=0.0
    return R

#
#
#
def icosahedron_caract(edge):
  """
  Return the different caracteristic of an icosahedron of 
  edge length edge.
  ru = edge*sin(2pi/5) : circumscrubed sphere radius
  ri = sqrt(3)*(3+sqrt(5))*edge/12 : inscribed sphere
  A : Area
  V : Volume
  """
  import collections

  ico_tuple = collections.namedtuple('ICO', ['ru', 'ri', 'A', 'V'])

  ru = edge*np.sin(2*np.pi/5)
  ri = np.sqrt(3)*(3+np.sqrt(5))*edge/12
  A = 5*np.sqrt(3)*edge**2
  V = 5*(3+np.sqrt(5))*(edge**3)/12

  return ico_tuple(ru, ri, A, V)

#
#
#
def get_radius_ico(edge):
  """
  Return the radius of a fake sphere that would be
  the equivalent of the icosahedhral.
  the first value is the mean value of the circumscrubed sphere radius
  andof the inscribed sphere radius.
  the second value is the radius of the sphere of the same ico area.
  """
  ico_carac = icosahedron_caract(edge)
  return np.array([(ico_carac.ri+ico_carac.ru)/2.0,
                    np.sqrt(ico_carac.A/(4*np.pi))])

#
#
#
def pts_in_triangle(A1, A2, A3, coord, nbpts=1000):
  """
  Look is a point is incide the triangle
  """
  #xmin = min(A1[0], A2[0], A3[0])
  #xmax = max(A1[0], A2[0], A3[0])
  
  l1 = np.linspace(A1[0], A2[0], 100)
  a1 = (A2[1]-A1[1])/(A2[0]-A1[0])
  b1 =  A1[1]-a1*A1[0]
  
  l2 = np.linspace(A2[0], A3[0], 100)
  a2 = (A3[1]-A2[1])/(A3[0]-A2[0])
  b2 =  A2[1]-a2*A2[0]
  
  l3 = np.linspace(A3[0], A1[0], 100)
  a3 = (A1[1]-A3[1])/(A1[0]-A3[0])
  b3 =  A3[1]-a3*A3[0]

  x = coord[0]
  if x < np.max(l1) and x>np.min(l1):
    if x < np.max(l2) and x>np.min(l2):
      ymin = min(a1*x+b1, a2*x+b2)
      ymax = max(a1*x+b1, a2*x+b2)
    elif x < np.max(l3) and x>np.min(l3):
      ymin = min(a1*x+b1, a3*x+b3)
      ymax = max(a1*x+b1, a3*x+b3)
    else:
      raise ValueError('something is incorrect here!!')
    
    if coord[1]>ymin and coord[1]<ymax:
      return True

  elif x < np.max(l2) and x>np.min(l2):
    if x < np.max(l3) and x>np.min(l3):
      ymin = min(a2*x+b2, a3*x+b3)
      ymax = max(a2*x+b2, a3*x+b3)
    else:
      raise ValueError('something is incorrect here!!')

    if coord[1]>ymin and coord[1]<ymax:
      return True

  return False

#
#
#
def get_area_triangle(A1, A2, A3):
  """
  In a plane
  """
  r1 = A2-A1
  r2 = A3-A2
  r3 = A1-A3

  r1n = np.sqrt(np.dot(r1, r1))
  r2n = np.sqrt(np.dot(r2, r2))
  r3n = np.sqrt(np.dot(r3, r3))

  #gamma angle between r1 and r2
  gamma = np.arccos((r1n**2 + r2n**2 - r3n**2)/(2*r1n*r2n))
  h = r1n*np.sin(gamma) #height triangle
  return h*r2n/2.0

#
#
#
def get_radius_center_incircle(A1, A2, A3):
  """
  In 3D
  """

  #r1 = A2-A1
  #r2 = A3-A2
  #r3 = A1-A3

  #r1n = np.sqrt(np.dot(r1, r1))
  #r2n = np.sqrt(np.dot(r2, r2))
  #r3n = np.sqrt(np.dot(r3, r3))

  cnt = (A1+A2+A3)/3
  # true only for triangle in the plan xz
  #semi_perim = (r1n + r2n + r3n)/2.0
  #a1 = np.array([A1[0], A1[2]])
  #a2 = np.array([A2[0], A2[2]])
  #a3 = np.array([A3[0], A3[2]])

  #area_triangle = get_area_triangle(a1, a2, a3)
  #radius = area_triangle/semi_perim

  # look the coordinate of the middle of one of the line vector
  # then the radius is given by the norm of the vector define by the 
  # difference of this point with the center of the triangle
  md = (A1+A2)/2
  radius = np.sqrt(np.dot(md-cnt, md-cnt))
  return radius, cnt

#
#
#
def sphe_part_deriv(r, phi, theta, dx, dy, dz):

  mat_conv = np.array([[np.cos(phi)*np.sin(theta), np.sin(phi)*np.sin(theta), np.cos(theta)],
                       [-np.sin(phi)/r*np.sin(theta), np.cos(phi)/r*np.sin(theta), 0.0],
                       [np.cos(phi)*np.cos(theta)/r, np.sin(phi)*np.cos(theta)/r, -np.sin(theta)/r]])

  return mat_conv.dot(np.array([dx, dy, dz]))

#
#
#
def gen_mat_con(r, phi, theta):
  return np.array([[math.cos(phi)*math.sin(theta), math.sin(phi)*math.sin(theta), math.cos(theta)],
                   [-math.sin(phi)/r*math.sin(theta), math.cos(phi)/r*math.sin(theta), math.sin(0.0)],
                   [math.cos(phi)*math.cos(theta)/r, math.sin(phi)*math.cos(theta)/r, -math.sin(theta)/r]])

#
#
#
def select_integration_box(data, box):
  """
  """

  ind_min = pts_coord_to_Array_ind(data, [box[0, 0], box[1, 0], box[2, 0]])
  ind_max = pts_coord_to_Array_ind(data, [box[0, 1], box[1, 1], box[2, 1]])
  print('ind: ', ind_min[1], ind_max[1])

  return data.Array[ind_min[0]:ind_max[0], ind_min[1]:ind_max[1], ind_min[2]:ind_max[2]]

#
#
#
def get_potential(prop, data, numba=False):

  x=data.xmesh[:, 0, 0]
  y=data.ymesh[0, :, 0]
  z=data.zmesh[0, 0, :]

  V = np.zeros(data.Array.shape, dtype=np.float64)
  t0 = time.time()
  if numba:
    try:
      import numba
      print(numba.__version__)
      from numba_funcs import calc_potential_dens_numba 
      calc_potential_dens_numba(data.Array, x, y, z, V)
    except ImportError:
      raise ValueError('import numba failed try with numba=false (or install numba)')
  else:
      calc_potential_dens(data.Array, x, y, z, V)

  t1 = time.time()
  print('timing = ', t1-t0)
  return V
 
 #
 #
 # 
def calc_potential_dens(dens, x, y, z, potential):
  M, N, K = dens.shape

  dx = x[1]-x[0]
  dy = y[1]-y[0]
  dz = z[1]-z[0]
  eps = np.array([dx, dy, dz])/10.0
  for i in range(M):
    for j in range(N):
      for k in range(K):
        xp = x[i]+eps[0] 
        yp = y[j]+eps[1]
        zp = z[k]+eps[2]

        for l in range(M):
          for m in range(N):
            for n in range(K):
              potential[i, j, k] = potential[i, j, k] +\
                  dens[l, m, n]/np.sqrt((x[l]-xp)**2 +\
                                        (y[m]-yp)**2 +\
                                        (z[n]-zp)**2)
        potential[i, j, k] = potential[i, j, k]*dx*dy*dz

#
#
#
def get_potential_fft(prop, data, numba=False):
  x=data.xmesh[:, 0, 0]
  y=data.ymesh[0, :, 0]
  z=data.zmesh[0, 0, :]

  V = np.zeros(data.Array.shape, dtype=np.float64)

  coul_pot = np.zeros(data.Array.shape, dtype=np.float64)
  if numba:
    try:
      import numba
      print(numba.__version__)
      from numba_funcs import calc_coul_pot_numba 
      calc_coul_pot_numba(x, y, z, data.dr, coul_pot)
    except ImportError:
      raise ValueError('import numba failed try with numba=false (or install numba)')
  else:
      calc_coul_pot(x, y, z, data.dr, coul_pot)
  coul_pot = np.fft.fftn(coul_pot, norm='ortho')
  dens_fft = np.fft.fftn(data.Array, norm='ortho')

  V = coul_pot*dens_fft

  return np.fft.fftshift(np.fft.ifftn(V, norm='ortho'))#/np.sqrt(V.size)

#
#
#
def calc_coul_pot(x, y, z, dr, coul_pot):
  M, N, K = coul_pot.shape

  for i in range(M):
    for j in range(N):
      for k in range(K):
        coul_pot[i, j, k] = 1.0/np.sqrt((x[i]+dr[0]/10.0)**2 +\
                                        (y[j]+dr[1]/10.0)**2 +\
                                        (z[k]+dr[2]/10.0)**2)

#
#
#
def get_radius_andrei(prop, data, numba=False):

  x=data.xmesh[:, 0, 0]
  y=data.ymesh[0, :, 0]
  z=data.zmesh[0, 0, :]

  rad = np.array([0.0])
  dxyz = np.array([x[1]-x[0], y[1]-y[0], z[1]-z[0]])
  dn_max = np.array([np.max(data.Array)])

  t0 = time.time()
  print(data.Array.shape, np.sum(abs(data.Array)))
  grad = np.gradient(data.Array, dxyz[0], dxyz[1], dxyz[2])

  for i in range(len(grad)):
    grad[i] = np.sqrt(grad[i]**2 + grad[i]**2)
  if numba:
    try:
      import numba
      print(numba.__version__)
      from numba_funcs import get_radius_andrei_numba
      get_radius_andrei_numba(data.Array, grad[0], grad[1], grad[2], x, y, z, dxyz, dn_max, rad)
    except ImportError:
      raise ValueError('import numba failed try with numba=false (or install numba)')
  else:
    get_radius_andrei_py(data.Array, grad, x, y, z, dxyz, dn_max, rad)

  t1 = time.time()
  print('timing = ', t1-t0)
  return rad

#
#
#
def get_radius_andrei_py(dn, grad, x, y, z, dxyz, dn_max, rad):

  M, N, K = dn.shape
  num = 0.0
  den = 0.0

  for i in range(M):
    for j in range(N):
      for k in range(K):
          r = np.sqrt(x[i]**2 + y[j]**2 + z[k]**2)
          num = num + r*grad[0][i,j,k]*grad[1][i,j,k]*grad[2][i,j,k]*delta_gauss(dn[i, j, k], dn_max[0]/2.0)
          den = den + grad[0][i,j,k]*grad[1][i,j,k]*grad[2][i,j,k]*delta_gauss(dn[i, j, k], dn_max[0]/2.0)

  num = num*dxyz[0]*dxyz[1]*dxyz[2]
  den = den*dxyz[0]*dxyz[1]*dxyz[2]

  rad[0] = num/den

#
#
#
def delta_gauss(x, x0, sigma=1e-2):
  return (x-x0)*np.exp(-((x-x0)/sigma)**2)

#
#
#
def centroid_dens_GS(prop, data, numba=False):

  x=data.xmesh[:, 0, 0]
  y=data.ymesh[0, :, 0]
  z=data.zmesh[0, 0, :]

  d_num = np.array([0.0])
  d_den = np.array([0.0])

  dxyz = np.array([x[1]-x[0], y[1]-y[0], z[1]-z[0]])

  t0 = time.time()
  print(data.Array.shape, np.sum(abs(data.Array)))
  if numba:
    try:
      import numba
      print(numba.__version__)
      from numba_funcs import centroid_dens_GS_numba 
      centroid_dens_GS_numba(data.Array, x, y, z, dxyz, d_num, d_den)
    except ImportError:
      raise ValueError('import numba failed try with numba=false (or install numba)')
  else:
    centroid_dens_GS_py(data.Array, x, y, z, dxyz, d_num, d_den)

  t1 = time.time()
  print('timing = ', t1-t0)
  return y, d_num, d_den

#
#
#
def centroid_dens_GS_py(dens, x, y, z, dxyz, d_num, d_den):

  M, N, K = dens.shape
  for i in range(M):
    for j in range(N):
      for k in range(K):
        r = np.sqrt(x[i]**2 + y[j]**2 + z[k]**2)
        if r>0.0:
          if (dens[i, j, k] != 0.0):
            d_num[0] = d_num[0] + r*dens[i, j, k]
            d_den[0] = d_den[0]+ dens[i, j, k]

  d_num[0] = d_num[0]*dxyz[0]*dxyz[1]*dxyz[2]
  d_den[0] = d_den[0]*dxyz[0]*dxyz[1]*dxyz[2]
   
#
#
#
def effective_dens_box_spher_coord_numba(prop, data_re, data_im):

  try:
    import numba
    print(numba.__version__)
  except:
    raise ValueError('numba not install, install it or try effective_dens_box_spher_coord')

  arr_mod = np.sqrt(data_re.Array**2 + data_im.Array**2)
  x=data_re.xmesh[:, 0, 0]
  y=data_re.ymesh[0, :, 0]
  z=data_re.zmesh[0, 0, :]

  d = np.zeros((3, 3), dtype=np.float64)
  dxyz = np.array([x[1]-x[0], y[1]-y[0], z[1]-z[0]])

  inte_num = np.zeros((y.shape[0], 3), dtype=np.float64)
  inte_den = np.zeros((y.shape[0], 3), dtype=np.float64)


  t0 = time.time()
  print(data_re.Array.shape)
  from numba_funcs import calc_eff_dens_sphe_numba
  calc_eff_dens_sphe_numba(data_re.Array, data_im.Array, arr_mod, x, y, z, dxyz,
          d, inte_num, inte_den)
  t1 = time.time()
  print('d = ', d[0, 0]/d[0, 1], d[1, 0]/d[1, 1], d[2, 0]/d[2, 1])
  print('timing = ', t1-t0)
  return y, np.array([d[0, 0]/d[0, 1], d[1, 0]/d[1, 1], d[2, 0]/d[2, 1]]),\
    inte_num, inte_den

#
#
#
def effective_dens_box_spher_coord(prop, data_re, data_im):

  arr_mod = np.sqrt(data_re.Array**2 + data_im.Array**2)
  x=data_re.xmesh[:, 0, 0]
  y=data_re.ymesh[0, :, 0]
  z=data_re.zmesh[0, 0, :]
#  print(x)
#  print(y)
#  print(z)

  dre_num = 0.0
  dre_den = 0.0
  dim_num = 0.0
  dim_den = 0.0
  dmod_num = 0.0
  dmod_den = 0.0
  dx = x[1]-x[0]
  dy = y[1]-y[0]
  dz = z[1]-z[0]

  inte_num = np.zeros((y.shape[0], 3), dtype=float)
  inte_den = np.zeros((y.shape[0], 3), dtype=float)

  print('dx, dy, dz = ', dx, dy, dz)
  r_list = []

  for j in range(data_re.Array.shape[1]):
    if (y[j]>=0.0):
      if y[j] == 0.0:
        print('y[{0}] = {1}, d = 0.0, 0.0, 0.0'.format(j, y[j]))
      else:
        print('y[{0}] = {1}, d = '.format(j, y[j]),
          dre_num/dre_den, dim_num/dim_den, dmod_num/dmod_den)
        print(dre_num, dre_den, dim_num, dim_den, dmod_num, dmod_den)
      for i in range(data_re.Array.shape[0]):
        for k in range(data_re.Array.shape[2]):
          r = np.sqrt(x[i]**2 + y[j]**2 + z[k]**2)
          r_list.append(r)
          if r>0.0:
            theta = np.arccos(z[k]/r)
            if np.sin(theta) != 0.0:
              phi = np.arctan2(y[j], x[i])
              sphe_deriv = sphe_part_deriv(r, phi, theta, dx, dy, dz)

              if (data_re.Array[i, j, k] != 0.0):
                inte_num[j, 0] = inte_num[j, 0] + (data_re.Array[i, j, k]*
                    np.sin(theta)*r**3)*sphe_deriv[0]*sphe_deriv[1]*sphe_deriv[2]
                inte_den[j, 0] = inte_den[j, 0] + (data_re.Array[i, j, k]*
                    np.sin(theta)*r**2)*sphe_deriv[0]*sphe_deriv[1]*sphe_deriv[2]
                
                dre_num = dre_num + (data_re.Array[i, j, k]*
                    np.sin(theta)*r**3)*sphe_deriv[0]*sphe_deriv[1]*sphe_deriv[2]
                dre_den = dre_den + (data_re.Array[i, j, k]*
                    np.sin(theta)*r**2)*sphe_deriv[0]*sphe_deriv[1]*sphe_deriv[2]
              
              if (data_im.Array[i, j, k] != 0.0):
                inte_num[j, 1] = inte_num[j, 1] + (data_im.Array[i, j, k]*
                    np.sin(theta)*r**3)*sphe_deriv[0]*sphe_deriv[1]*sphe_deriv[2]
                inte_den[j, 1] = inte_den[j, 1] + (data_im.Array[i, j, k]*
                    np.sin(theta)*r**2)*sphe_deriv[0]*sphe_deriv[1]*sphe_deriv[2]
   
                dim_num = dim_num + (data_im.Array[i, j, k]*
                    np.sin(theta)*r**3)*sphe_deriv[0]*sphe_deriv[1]*sphe_deriv[2]
                dim_den = dim_den + (data_im.Array[i, j, k]*
                    np.sin(theta)*r**2)*sphe_deriv[0]*sphe_deriv[1]*sphe_deriv[2]
              
              if (arr_mod[i, j, k] != 0.0):
                inte_num[j, 2] = inte_num[j, 2] + (arr_mod[i, j, k]*
                    np.sin(theta)*r**3)*sphe_deriv[0]*sphe_deriv[1]*sphe_deriv[2]
                inte_den[j, 2] = inte_den[j, 2] + (arr_mod[i, j, k]*
                    np.sin(theta)*r**2)*sphe_deriv[0]*sphe_deriv[1]*sphe_deriv[2]
   
                dmod_num = dmod_num + (arr_mod[i, j, k]*
                    np.sin(theta)*r**3)*sphe_deriv[0]*sphe_deriv[1]*sphe_deriv[2]
                dmod_den = dmod_den + (arr_mod[i, j, k]*
                    np.sin(theta)*r**2)*sphe_deriv[0]*sphe_deriv[1]*sphe_deriv[2]

  print('d = ', dre_num/dre_den, dim_num/dim_den, dmod_num/dmod_den)
  return y, np.array([dre_num/dre_den, dim_num/dim_den, dmod_num/dmod_den]),\
    inte_num, inte_den

#
#
#
def effective_dens_box_modulus(prop, data_re, data_im, data_gs, box, center, radius, yfacet=None, ax = None,
  inte_vol = 'cylinder', a1=np.zeros((2), dtype=float), a2=np.zeros((2), dtype=float), a3=np.zeros((2), dtype=float),
  dens_thres = 1e-3):
  """
  The box must be in Ang
  """

  ind_min = pts_coord_to_Array_ind(data_re, np.array([box[0, 0], box[1, 0], box[2, 0]]))
  ind_max = pts_coord_to_Array_ind(data_re, np.array([box[0, 1], box[1, 1], box[2, 1]]))

  #print('ind_min: ', ind_min)
  #print('ind_max: ', ind_max)
  arr_box_re = data_re.Array[ind_min[0]:ind_max[0], ind_min[1]:ind_max[1], ind_min[2]:ind_max[2]]
  arr_box_im = data_im.Array[ind_min[0]:ind_max[0], ind_min[1]:ind_max[1], ind_min[2]:ind_max[2]]
  arr_box = np.sqrt(arr_box_re**2 + arr_box_im**2)
  print('data.box: ', data_re.box )
  print('arr_box: ', arr_box.shape)
  print('data.Array: ', data_re.Array.shape)


  ind_min = pts_coord_to_Array_ind(data_gs, np.array([box[0, 0], box[1, 0], box[2, 0]]))
  ind_max = pts_coord_to_Array_ind(data_gs, np.array([box[0, 1], box[1, 1], box[2, 1]]))
  arr_box_gs = data_gs.Array[ind_min[0]:ind_max[0], ind_min[1]:ind_max[1], ind_min[2]:ind_max[2]]
  print('data_gs.box: ', data_gs.box )
  print('arr_box_gs: ', arr_box_gs.shape)
  print('data_gs.Array: ', data_gs.Array.shape)


  x=data_re.xmesh[ind_min[0]:ind_max[0], 0, 0]
  y=data_re.ymesh[0, ind_min[1]:ind_max[1], 0]#-data_re.ymesh[0, ind_min[1], 0]
  z=data_re.zmesh[0, 0, ind_min[2]:ind_max[2]]

  x_gs=data_gs.xmesh[ind_min[0]:ind_max[0], 0, 0]
  y_gs=data_gs.ymesh[0, ind_min[1]:ind_max[1], 0] #-data_re.ymesh[0, ind_min[1], 0]
  z_gs=data_gs.zmesh[0, 0, ind_min[2]:ind_max[2]]

  print(x)

  dx = x[1]-x[0]
  dy = y[1]-y[0]
  dz = z[1]-z[0]


  fig2 = plt.figure(2, figsize = (10, 20))
  ax2=fig2.add_subplot(411)
  i = 0
  while(abs(z[i])>0.25):
    i = i+1
  #print('Z[{0}] = {1}'.format(i, z[i]))
  ext = [y[0], y[y.shape[0]-1], x[0], x[x.shape[0]-1]]
  aspect = 0.9#abs((ext[1]-ext[0])/(ext[3]-ext[2]))/1.0
  print('aspect  =', aspect)
  fig2.suptitle('Ag{0}'.format(data_re.atoms.get_number_of_atoms()), fontsize=20)
  ax2.set_title(r'Re($\delta n$)', fontsize=20)
  im=ax2.imshow(arr_box_re[:, :, i], extent=ext, aspect=aspect)
  ax2.set_ylabel('x (Bohr)')
  plt.colorbar(im)
  
  ax2=fig2.add_subplot(412)
  ax2.set_title(r'Im($\delta n$)', fontsize=20)
  im=ax2.imshow(arr_box_im[:, :, i], extent=ext, aspect=aspect)
  ax2.set_ylabel('x (Bohr)')
  plt.colorbar(im)
  
  ax2=fig2.add_subplot(413)
  ax2.set_title(r'$|\delta n|$', fontsize=20)
  im=ax2.imshow(arr_box[:, :, i], extent=ext, aspect=aspect)
  plt.colorbar(im)

  i = 0
  while(abs(z[i])>0.25):
    i = i+1
 
  ext = [y_gs[0], y_gs[y_gs.shape[0]-1], x_gs[0], x_gs[x_gs.shape[0]-1]]
  ax2=fig2.add_subplot(414)
  ax2.set_title(r'$|\delta n|$', fontsize=20)
  im=ax2.imshow(arr_box_gs[:, :, i], extent=ext, aspect=aspect)

  ax2.set_xlabel('y (Bohr)')
  ax2.set_ylabel('x (Bohr)')
  plt.colorbar(im)

  fig2.savefig('dens_dist_z_0_modulus_Na{0}.pdf'.format(data_re.atoms.get_number_of_atoms()), format='pdf')
  #plt.show()
  #sys.exit()
  
  inte_re  = 0.0
  inte_im  = 0.0
  inte = 0.0
  somme_re = 0.0
  somme_im = 0.0
  somme = 0.0

  integrand1 = np.zeros((y.shape[0], 3), dtype=float)
  integrand2 = np.zeros((y.shape[0], 3), dtype=float)
  ysurf_arr = np.zeros((arr_box.shape[0], arr_box.shape[1]), dtype = float)
  
  for i in range(arr_box.shape[0]):
    xp = x[i]-center[0]
    indx = get_indxyz(x_gs, x[i])
    for k in range(arr_box.shape[2]):
      zp = z[k]-center[2]
      indz = get_indxyz(z_gs, z[k])
      if inte_vol == 'cylinder':
        rho = np.sqrt(xp**2 + zp**2)
        if  rho < radius:
          if yfacet is None:
            ysurf = get_surf_cluster(arr_box_gs, y_gs, indx, indz, dens_thres)
          else:
            ysurf = yfacet
          if ax is not None:
            ax.plot(x[i], z[k], 'og', markersize=3)
          ysurf_arr[i, k] = ysurf
          for j in range(arr_box.shape[1]):
            inte_re = inte_re + (y[j]-ysurf)*arr_box_re[i, j, k]*dx*dy*dz
            inte_im = inte_im + (y[j]-ysurf)*arr_box_im[i, j, k]*dx*dy*dz
            inte = inte + (y[j]-ysurf)*arr_box[i, j, k]*dx*dy*dz
            somme_re = somme_re + arr_box_re[i, j, k]*dx*dy*dz
            somme_im = somme_im + arr_box_im[i, j, k]*dx*dy*dz
            somme = somme + arr_box[i, j, k]*dx*dy*dz
      elif inte_vol == 'triangle':
        if  pts_in_triangle(a1, a2, a3, np.array([xp, zp])):
          if ax is not None:
            ax.plot(x[i], z[k], 'og', markersize=3)
          for j in range(arr_box.shape[1]):
            inte_re = inte_re + (y[j]-ysurf)*arr_box_re[i, j, k]*dx*dy*dz
            inte_im = inte_im + (y[j]-ysurf)*arr_box_im[i, j, k]*dx*dy*dz
            inte = inte + (y[j]-ysurf)*arr_box[i, j, k]*dx*dy*dz
            somme_re = somme_re + arr_box_re[i, j, k]*dx*dy*dz
            somme_im = somme_im + arr_box_im[i, j, k]*dx*dy*dz
            somme = somme + arr_box[i, j, k]*dx*dy*dz

  for i in range(arr_box.shape[0]):
    xp = x[i]-center[0]
    indx = get_indxyz(x_gs, x[i])
    for k in range(arr_box.shape[2]):
      zp = z[k]-center[2]
      indz = get_indxyz(z_gs, z[k])
      rho = np.sqrt(xp**2 + zp**2)
      if  rho < radius:
        if yfacet is None:
          ysurf = get_surf_cluster(arr_box_gs, y_gs, indx, indz, dens_thres)
        else:
          ysurf = yfacet

        for j in range(arr_box.shape[1]):
          integrand1[j, 0] = integrand1[j, 0] + (y[j]-ysurf)*arr_box_re[i, j, k]
          integrand1[j, 1] = integrand1[j, 1] + (y[j]-ysurf)*arr_box_im[i, j, k]
          integrand1[j, 2] = integrand1[j, 2] + (y[j]-ysurf)*arr_box[i, j, k]

          integrand2[j, 0] = integrand2[j, 0] + arr_box_re[i, j, k]
          integrand2[j, 1] = integrand2[j, 1] + arr_box_im[i, j, k]
          integrand2[j, 2] = integrand2[j, 2] + arr_box[i, j, k]

  integrand1 = integrand1*dx*dz
  integrand2 = integrand2*dx*dz

#  xmesh, zmesh = np.meshgrid(x, z)
  np.save('xrange.npy', x)
  np.save('zrange.npy', z)
  fig3 = plt.figure(3)
  ax2=fig3.add_subplot(111)
  ext = [x[0], x[x.shape[0]-1], z[0], z[z.shape[0]-1]]
  im=ax2.imshow(ysurf_arr, extent=ext, aspect=aspect, vmin=32.0, vmax=35.0)
  ax2.set_xlabel('x (Bohr)')
  ax2.set_ylabel('z (Bohr)')
  plt.colorbar(im)
  plt.show()



  return y, ysurf_arr, integrand1, integrand2,\
    np.array([inte_re/somme, inte_im/somme, inte/somme, somme_re, somme_im, somme])

#
#
#
def get_indxyz(x, xv):
  i = 0
  if (xv<x[0]):
    i = 0
  elif xv>x[x.shape[0]-1]:
    i = x.shape[0]-1
  else:
    while x[i]<xv:
      i = i + 1
  return i

#
#
#
def get_surf_cluster(dens, y_gs, indx, indz, thres):

  i = 0
  while dens[indx, i, indz] > thres:
    i = i+1

  #print(y_gs[i])
  #fig3 = plt.figure(3)
  #ax=fig3.add_subplot(111)
  #ax.plot(y_gs, dens[indx, :, indz])
  #plt.show()


  return y_gs[i]

#
#
#
def get_means_dens_bulk(data_re, data_im, rad_cluster):

  x=data_re.xmesh[:, 0, 0]
  y=data_re.ymesh[0, :, 0]
  z=data_re.zmesh[0, 0, :]

  mean_dens_re = 0.0
  mean_dens_im = 0.0

  for i in range(x.shape[0]):
    for j in range(y.shape[0]):
      for k in range(z.shape[0]):

        rad = np.sqrt(np.dot(np.array([x[i], y[j], z[k]]), np.array([x[i], y[j], z[k]])))
        if rad < rad_cluster:
          mean_dens_re = mean_dens_re + data_re.Array[i, j, k]
          mean_dens_im = mean_dens_im + data_re.Array[i, j, k]

#
#
#
def check_array_size(x, ref_shape, xmin, xmax, dx, direction):
  if x.shape[0] != ref_shape:
    print('Changing box in '+direction+' dir')
    if x.shape[0]< ref_shape:
      i = 1
      while x.shape[0]!=ref_shape:
        x = np.arange(xmin, xmax+i*0.5*dx, dx)
        i = i+1
        if i>10:
          raise ValueError('too much change')
    elif x.shape[0]> ref_shape:
      i = 1
      while x.shape[0]!=ref_shape:
        x = np.arange(xmin, xmax-0.5*i*dx, dx)
        i = i+1
        if i>10:
          raise ValueError('too much change')
    else:
      raise ValueError('something is odd here')

  return x

#
#
#
def effective_dens_dist(x, y, f):
  dx = x[1]-x[0]
  dy = y[1]-y[0]
  inte = 0.0

  for i in range(f.shape[0]):
    for j in range(f.shape[1]):
      inte = inte + f[i, j]*x[i]
  
  somme = np.sum(f)*dx*dy

  return inte*dx*dy/somme

#
#
#
def get_data_tddft_plan(args, prop, data, zmin, zmax):

  if prop.plan == 'x':
    if prop.line == 'y':
      rmin = pts_coord_to_Array_ind(data, [prop.plan_coord, 0.0, zmin])
      rmax = pts_coord_to_Array_ind(data, [prop.plan_coord, 0.0, zmax])
      if args.time == 1:
        z = data.Array[args.time_num, rmin[0], :, rmin[2]:rmax[2]]
      else:
        z = data.Array[rmin[0], ::-1, rmin[2]:rmax[2]]

      x=np.linspace(data.box[0][1], data.box[1][1], z.shape[0])*prop.bohr_rad
      y=np.linspace(zmin, zmax, z.shape[1])*prop.bohr_rad
    elif prop.line == 'z':
      rmin = pts_coord_to_Array_ind(data, [prop.plan_coord, zmin, 0.0])
      rmax = pts_coord_to_Array_ind(data, [prop.plan_coord, zmax, 0.0])
      if args.time == 1:
        z = data.Array[args.time_num, rmin[0], rmin[1]:rmax[1], :]
      else:
        z = data.Array[rmin[0], rmin[1]:rmax[1], :]

      x=np.linspace(data.box[0][2], data.box[1][2], z.shape[1])*prop.bohr_rad
      y=np.linspace(zmin, zmax, z.shape[0])*prop.bohr_rad
    else:
      raise ValueError('prop.line can be only y or z')

  elif prop.plan == 'y':
    rmin = pts_coord_to_Array_ind(data, [0.0, prop.plan_coord, zmin])
    rmax = pts_coord_to_Array_ind(data, [0.0, prop.plan_coord, zmax])
    if args.time == 1:
      z = data.Array[args.time_num, :, rmin[1], rmin[2]:rmax[2]]
    else:
      z = data.Array[:, rmin[1], rmin[2]:rmax[2]]
      z = z[:, ::-1]

    x=np.linspace(data.box[0][0]-prop.xorig, data.box[1][0]-prop.xorig, z.shape[0])*prop.bohr_rad
    y=np.linspace(zmin, zmax, z.shape[1])*prop.bohr_rad
    print(y)
    print('zmin, zmax = ', zmin, zmax)
  elif prop.plan == 'z':
    rmin = pts_coord_to_Array_ind(data, [0.0, zmin, prop.plan_coord])
    rmax = pts_coord_to_Array_ind(data, [0.0, zmax, prop.plan_coord])
    if args.time == 1:
      z = data.Array[args.time_num, :, rmin[1]:rmax[1], rmin[2]]
    else:
      z = data.Array[:, rmin[1]:rmax[1], rmin[2]]

    x=np.linspace(data.box[0][0], data.box[1][0], z.shape[0])*prop.bohr_rad
    y=np.linspace(zmin, zmax, z.shape[1])*prop.bohr_rad
  else:
    raise ValueError('prop.plan can be only x, y, or z')

  print('x: ', x.shape, x[1]-x[0], x[0])
  print('y: ', y.shape, y[1]-y[0], y[0])
  i = 0
  while (x[i]<0):
    i = i + 1
  print(i, x[i])
  args.exp_value = effective_dens_dist(x[i:z.shape[0]]/0.529, y/0.529, z[i:z.shape[0], :])
  #args.exp_value = effective_dens_dist(y/0.529, x[i:z.shape[1]]/0.529, z[:, i:z.shape[1]])

#
#
#
def get_data_tddft(args, prop, data):

  if prop.plan == 'x':
    r = pts_coord_to_Array_ind(data, [prop.plan_coord, 0.0, 0.0])
    if args.time == 1:
      z = data.Array[args.time_num, r[0], :, :]
    else:
      z = data.Array[r[0], ::-1, :]

    ind = pts_coord_to_Array_ind(data, np.array([0.0, prop.coord[1], prop.coord[2]]))
    Ix = z[:, ind[2]]
    #Iy = z[ind[1], :]
    x=np.linspace(data.box[0][1], data.box[1][1], len(Ix))*prop.bohr_rad
    #y=np.linspace(data.box[0][2], data.box[1][2], len(Iy))*prop.bohr_rad
  elif prop.plan == 'y':
    r = pts_coord_to_Array_ind(data, [0.0, prop.plan_coord, 0.0])
    if args.time == 1:
      z = data.Array[args.time_num, :, r[1], :]
    else:
      z = data.Array[:, r[1], ::-1]

    ind = pts_coord_to_Array_ind(data, np.array([prop.coord[0], 0.0, prop.coord[2]]))
    Ix = z[:, ind[2]]
    #Iy = z[ind[0], :]
    x=np.linspace(data.box[0][0]-prop.xorig, data.box[1][0]-prop.xorig, len(Ix))*prop.bohr_rad
    #y=np.linspace(data.box[0][2], data.box[1][2], len(Iy))*prop.bohr_rad
    
  elif prop.plan == 'z':
    r = pts_coord_to_Array_ind(data, [0.0, 0.0, prop.plan_coord])
    if args.time == 1:
      z = data.Array[args.time_num, :, :, r[2]]
    else:
      z = data.Array[:, :, r[2]]

    ind = pts_coord_to_Array_ind(data, np.array([prop.coord[0], prop.coord[1], 0.0]))
    Ix = z[:, ind[1]]
    #Iy = z[ind[0], :]
    x=np.linspace(data.box[0][0], data.box[1][0], len(Ix))*prop.bohr_rad
    #y=np.linspace(data.box[0][1], data.box[1][1], len(Iy))*prop.bohr_rad

  print('x: ', x.shape, x[1]-x[0], x[0])
  i = 0
  while (x[i]<0):
    i = i + 1
  print(i, x[i])
  args.peak_max = x[i+np.argmax(abs(Ix[i:Ix.shape[0]]))]
  args.exp_value = expected_value_1D(x[i:Ix.shape[0]]/0.529, Ix[i:Ix.shape[0]])
  print(i, x[i], np.argmax(abs(Ix[i:Ix.shape[0]])), x[i+np.argmax(abs(Ix[i:Ix.shape[0]]))])
  print('Peak max = ', args.peak_max)

#
#
#
def calculate_dimension(fname='domiprod_atom2coord.xyz'):
  atoms = io.read(fname)

  r0 = atoms.get_center_of_mass()
  num = 0.0
  denom = 0.0
  for pos, Z in zip(atoms.positions, atoms.numbers):
    num = num + np.dot(pos-r0, pos-r0)*Z
    denom = denom + Z

  return np.sqrt(num/denom)/0.529

#
#
#
def expected_value_1D(x, f):
  dx = x[1]-x[0] 

  inte = 0.0
  for i in range(f.shape[0]):
    inte = inte + f[i]*x[i]

  return inte*dx

#
#
#
def integral_dens_chng_python(freq, folder, r_guess, fname=None):
  r = read_mbpt_lcao_output()
  r.args.quantity = 'dens'
  r.args.plot_freq = freq
  r.args.folder = folder
  dr = np.array([0.3, 0.3, 0.3])

  data = r.Read(YFname=fname)

  
  #print((data.box[1, 0]-data.box[0, 0])/dr[0])
  #print(data.Array.shape)

  inte = 0.0
  r = data.box[0, :]
  V = (data.box[1, 0]-data.box[0, 0])*(data.box[1, 1]-data.box[0, 1])*\
      (data.box[1, 2]-data.box[0, 2])
  print(data.box)
  print(V)
  for i in range(data.Array.shape[0]):
    r[1] = data.box[0, 1]
    for j in range(data.Array.shape[0]):
      r[2] = data.box[0, 2]
      for k in range(data.Array.shape[0]):
        norm = np.sqrt(np.dot(r, r))
        if norm>r_guess:
          inte = inte + data.Array[i, j, k]*norm
        r[2] = r[2] + dr[2]
      r[1] = r[1] + dr[1]
    r[0] = r[0] + dr[0]

  return inte*dr[0]*dr[1]*dr[2]/V

#
#
#
def integral_dens_chng(freq, folder, r_guess, fname=None):
  raise ValueError("Broken")
#  r = read_mbpt_lcao_output()
#  r.args.quantity = 'dens'
#  r.args.plot_freq = freq
#  r.args.folder = folder
#  #dr = np.array([0.3, 0.3, 0.3])
#
#  data = r.Read(YFname=fname)
#
#  inte = 0.0
#  r = data.box[0, :]
#  V = (data.box[1, 0]-data.box[0, 0])*(data.box[1, 1]-data.box[0, 1])*\
#      (data.box[1, 2]-data.box[0, 2])
#  print(data.box)
#  print(V)
#
#  inte = look_dens.integral_dens(data.Array, np.array(data.box[0, :]), dr, order=1)
#
#  return inte

#
#
#
def integral_dens_chng_sphere(r, rmax, fname=None):
  raise ValueError("Broken")
#
#  data = r.Read(YFname=fname)
#
#  inte = 0.0
#  r0 = np.array(data.box[0, :])
#  print(r0)
#  print(rmax)
#  print(data.dr)
#  xr = np.linspace(data.box[0, 0], data.box[1, 0], data.Array.shape[0])
#  yr = np.linspace(data.box[0, 1], data.box[1, 1], data.Array.shape[1])
#  zr = np.linspace(data.box[0, 2], data.box[1, 2], data.Array.shape[2])

  #for i, x in enumerate(xr): 
  #  for j, y in enumerate(yr): 
  #    for k, z in enumerate(zr): 
  #      r = np.array([x, y, z])
  #      norm = np.sqrt(np.dot(r, r))
  #      if norm == 0.0:
  #        norm =1e-10
  #      theta = np.arccos(z/norm)
  #      if norm <rmax:
  #        inte = inte + (norm**2)*np.sin(theta)*data.Array[i, j, k]
  #
  #return inte*data.dr[0]*data.dr[1]*data.dr[2]
#  return look_dens.integral_dens_sphere(data.Array, xr, yr, zr, data.dr, rmax)

#
#
#
def peter_int_chng(freq, folder, fname=None):
  raise ValueError("Broken")
#  r = read_mbpt_lcao_output()
#  r.args.quantity = 'intensity'
#  r.args.plot_freq = freq
#  r.args.folder = folder
#  dr = np.array([0.3, 0.3, 0.3])
#
#  data = r.Read(YFname=fname)
#
#  inte1 = 0.0
#  inte2 = 0.0
#  r = data.box[0, :]
#  V = (data.box[1, 0]-data.box[0, 0])*(data.box[1, 1]-data.box[0, 1])*\
#      (data.box[1, 2]-data.box[0, 2])
#  print(data.box)
#  print(V)
#
##  inte1 = look_dens.integral_dens(data.Array, np.array(data.box[0, :]), dr, order=1)
##  inte2 = look_dens.integral_dens(data.Array, np.array(data.box[0, :]), dr, order=2)
#
 # print('inte1 = ', inte1)
 # print('inte2 = ', inte2)
 # return inte2/inte1

#
#
#
def peter_dens_chng(freq, folder, fname=None):
  raise ValueError("Broken")
#  r = read_mbpt_lcao_output()
#  r.args.quantity = 'dens'
#  r.args.plot_freq = freq
#  r.args.folder = folder
#  dr = np.array([0.3, 0.3, 0.3])
#
#  data = r.Read(YFname=fname)
#
 # inte1 = 0.0
#  inte2 = 0.0
#  r = data.box[0, :]
#  V = (data.box[1, 0]-data.box[0, 0])*(data.box[1, 1]-data.box[0, 1])*\
#      (data.box[1, 2]-data.box[0, 2])
#  print(data.box)
#  print(V)
#
#  inte1 = look_dens.integral_dens(data.Array, np.array(data.box[0, :]), dr, order=0)
##  inte2 = look_dens.integral_dens(data.Array, np.array(data.box[0, :]), dr, order=2)
#
#  print('inte1 = ', inte1)
#  print('inte2 = ', inte2)
#  return np.sqrt(abs(inte2))/abs(inte1)

#
#
#
def variance_dens_chng(freq, folder, fname=None):
  raise ValueError("Broken")
#  r = read_mbpt_lcao_output()
#  r.args.quantity = 'dens'
#  r.args.plot_freq = freq
#  r.args.folder = folder
#  dr = np.array([0.3, 0.3, 0.3])
#
#  data = r.Read(YFname=fname)
#
#  inte1 = 0.0
#  inte2 = 0.0
#  r = data.box[0, :]
#  V = (data.box[1, 0]-data.box[0, 0])*(data.box[1, 1]-data.box[0, 1])*\
#      (data.box[1, 2]-data.box[0, 2])
#  print(data.box)
#  print(V)
#
#  inte1 = look_dens.integral_dens(data.Array, np.array(data.box[0, :]), dr, order=1)
#  inte2 = look_dens.integral_dens(data.Array, np.array(data.box[0, :]), dr, order=2)
#
#  return inte2-inte1**2

#
#
#
def check_rad_pos(data, i, j, k, x, y, z, array, thres, ri, rad_f, points):
  r = np.sqrt(x[i]**2 + y[j]**2 + z[k]**2)
  if data.Array[i, j, k]<thres:
    rad_f.append(r)
    points[i, j, k] = 1.0
    k = z.shape[0]+1
  else:
    k = k+1
  return k

#
#
#
def check_rad_neg(data, i, j, k, x, y, z, array, thres, rad_f, points):
  r = np.sqrt(x[i]**2 + y[j]**2 + z[k]**2)
  if data.Array[i, j, k]<thres:
    rad_f.append(r)
    points[i, j, k] = 1.0
    k = -1
  else:
    k = k-1
  return k

#
#
#
def get_rad_dens(data, thres):
  
  rad_f =[]
  x = data.xmesh[:, 0, 0]
  y = data.ymesh[0, :, 0]
  z = data.zmesh[0, 0, :]

#  dxyz = np.array([x[1]-x[0], y[1]-y[0], z[1]-z[0]])
  points = np.zeros(data.Array.shape, dtype = float)
  ixst = 0
  while x[ixst] < 0.0:
    ixst = ixst+1

  iyst = 0
  while y[iyst] < 0.0:
    iyst = iyst+1

  izst = 0
  while z[izst] < 0.0:
    izst = izst+1

#  xmax = np.max(x)
#  xmin = np.min(x)
#
#  ymax = np.max(y)
#  ymin = np.min(y)
#
#  zmax = np.max(z)
#  zmin = np.min(z)

  i = ixst
  while i< x.shape[0]:
    #print('x: ',  i, x.shape)
    j = iyst
    while j < y.shape[0]:
      k = izst
      #print('y: ',  j, y.shape)
      while k < z.shape[0]:
        #print('z: ',  k, z.shape)
        k = check_rad_pos(i, j, k, x, y, z, data.Array, thres, rad_f, points)

      k = izst
      while k > 0:
        #print('z: ',  k, z.shape)
        k = check_rad_neg(i, j, k, x, y, z, data.Array, thres, rad_f, points)
      j = j+1
    
    j = iyst
    while j >0:
      #print('y: ',  j, y.shape)
      k = izst
      while k < z.shape[0]:
        k = check_rad_pos(i, j, k, x, y, z, data.Array, thres, rad_f, points)

      k = izst
      while k > 0:
        k = check_rad_neg(i, j, k, x, y, z, data.Array, thres, rad_f, points)
      j = j-1

    i = i+1

  return np.array(rad_f), points
