"""
Output support for X3D and X3DOM file types.
See http://www.web3d.org/x3d/specifications/
X3DOM outputs to html pages that should display 3-d manipulatable atoms in
modern web browsers.
Original code from ase
"""
#import sys

def convert_X3D2X3DOM(iname, oname=None):
  
  if oname is None:
    oname = iname[0:len(iname)-4]+'.html'


  fout = open(oname, 'w')

  fout.write('<html>\n')
  fout.write('<head>\n')
  fout.write('<title>ASE atomic visualization</title>\n')
  fout.write('<link rel="stylesheet" type="text/css\n"')
  fout.write(' href="http://www.x3dom.org/x3dom/release/x3dom.css">\n')
  fout.write('</link>\n')
  fout.write('<script type="text/javascript"\n')
  fout.write(' src="http://www.x3dom.org/x3dom/release/x3dom.js">\n')
  fout.write('</script>\n')
  fout.write('</head>\n')
  fout.write('<body>\n')
  fout.write('<X3D style="margin:0; padding:0; width:100%; height:100%;'
       ' border:none;">\n')

  start = False
  with open(iname, 'r') as f:
    for i, line in enumerate(f):
      line_bis = ''.join(line.split())
      if line_bis == '<Scene>' and not start:
        start = True
      elif line == '</Scene>' and start:
        start=False

      if start:
        fout.write(line)

  fout.write('</Scene>\n')
  fout.write('</X3D>\n')
  fout.write('</body>\n')
  fout.write('</html>\n')
  
  fout.close()






#class X3D:
#    """Class to write either X3D (readable by open-source rendering
#    programs such as Blender) or X3DOM html, readable by modern web
#    browsers.
#    """
#
#    def __init__(self, atoms):
#        self._atoms = atoms
#
#    def write(self, filename):
#        """Writes output to either an 'X3D' or an 'X3DOM' file, based on
#        the extension. For X3D, filename should end in '.x3d'. For X3DOM,
#        filename should end in '.html'."""
#        if filename.endswith('.x3d'):
#            datatype = 'X3D'
#        elif filename.endswith('.html'):
#            datatype = 'X3DOM'
#        else:
#            raise ValueError("filename must end in '.x3d' or '.html'.")
#        w = WriteToFile(filename, 'w')
#        if datatype == 'X3DOM':
#            w(0, '<html>')
#            w(1, '<head>')
#            w(2, '<title>ASE atomic visualization</title>')
#            w(2, '<link rel="stylesheet" type="text/css"')
#            w(2, ' href="http://www.x3dom.org/x3dom/release/x3dom.css">')
#            w(2, '</link>')
#            w(2, '<script type="text/javascript"')
#            w(2, ' src="http://www.x3dom.org/x3dom/release/x3dom.js">')
#            w(2, '</script>')
#            w(1, '</head>')
#            w(1, '<body>')
#            w(2, '<X3D style="margin:0; padding:0; width:100%; height:100%;'
#                 ' border:none;">')
#        elif datatype == 'X3D':
#            w(0, '<?xml version="1.0" encoding="UTF-8"?>')
#            w(0, '<!DOCTYPE X3D PUBLIC "ISO//Web3D//DTD X3D 3.2//EN" '
#              '"http://www.web3d.org/specifications/x3d-3.2.dtd">')
#            w(0, '<X3D profile="Interchange" version="3.2" '
#              'xmlns:xsd="http://www.w3.org/2001/XMLSchema-instance" '
#              'xsd:noNamespaceSchemaLocation='
#              '"http://www.web3d.org/specifications/x3d-3.2.xsd">')
#
#        w(3, '<Scene>')
#
#        for atom in self._atoms:
#            for indent, line in atom_lines(atom):
#                w(4 + indent, line)
#
#        w(3, '</Scene>')
#        
#        if datatype == 'X3DOM':
#            w(2, '</X3D>')
#            w(1, '</body>')
#            w(0, '</html>')
#        elif datatype == 'X3D':
#            w(0, '</X3D>')
#

#def atom_lines(atom):
#    """Generates a segment of X3D lines representing an atom."""
#    x, y, z = atom.position
#    lines = [(0, '<Transform translation="%.2f %.2f %.2f">' % (x, y, z))]
#    lines += [(1, '<Shape>')]
#    lines += [(2, '<Appearance>')]
#    color = tuple(jmol_colors[atom.number])
#    color = 'diffuseColor="%.3f %.3f %.3f"' % color
#    lines += [(3, '<Material %s specularColor="0.5 0.5 0.5">' % color)]
#    lines += [(3, '</Material>')]
#    lines += [(2, '</Appearance>')]
#    lines += [(2, '<Sphere radius="%.2f">' % covalent_radii[atom.number])]
#    lines += [(2, '</Sphere>')]
#    lines += [(1, '</Shape>')]
#    lines += [(0, '</Transform>')]
#    return lines
