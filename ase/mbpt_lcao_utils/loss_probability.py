from __future__ import division
import numpy as np
from scipy import special
from math import factorial

#
#
#
def drude_model(w, wp, gamma, eps_inf):
  return eps_inf - (wp**2)/(w**2 + complex(0.0, 1.0)*gamma*w)

#
#
#
def Clausius_Mosotti(P, r, nb_atm):
  #all in atomic unit
  V = 4*np.pi*(r**3)/3
  coeff = nb_atm/(V)#3.7536*10**-9 #Bohr**-3 for Na
  eps = ((2.0/3.0)*coeff*P +1)/(1-(1.0/3.0)*coeff*P)
  return eps

#
#
#
def loss_probability(w, b, radius, v, eps, l=2):
  
  P = np.zeros((w.shape[0]), dtype = float)
  for il in range(l+1):
    gamma = calc_gamma(il, eps)
    f1 = (abs(w)*radius/v)**(2*il)
    for im in range(0, il+1, 1):
      bessel = special.kn(im, abs(w)*b/v)**2
      P = P + gamma.imag*f1*(2-delta(im, 0))*bessel/(factorial(il-im)*factorial(il+im))
  return 4*radius*P/(np.pi*(v**2))

#
#
#
def delta(l, m):
  if l == m:
    return 1
  else:
    return 0

#
#
#
def calc_gamma(l, eps):
  gamma = l*(eps-1)/(l*eps+l+1)
  return gamma


#
#
#
def Kramers_Kronig(x, f, ReIm= 'Re', eps=1E-04):
  
  dx = x[1]-x[0]
  if (ReIm == 'Re'):
    f_im =np.zeros(f.shape, dtype = float)
    for i in range(x.shape[0]):
      for j in range(x.shape[0]):
        if i == j:
          f_im[i] = f_im[i] -(2/(np.pi*x[i]))*((x[j]-eps)*(f[j]-1)/((x[j]-eps)**2 - x[i]**2))*dx -\
            (2/(np.pi*x[i]))*((x[j]+eps)*(f[j]-1)/((x[j]+eps)**2 - x[i]**2))*dx
        else:
          f_im[i] = f_im[i] -(2/(np.pi*x[i]))*(x[j]*(f[j]-1)/(x[j]**2 - x[i]**2))*dx
    return f_im

  elif ReIm =='Im':
    f_re =np.zeros(f.shape, dtype = float)
    for i in range(x.shape[0]):
      for j in range(x.shape[0]):
        if i == j:
          f_re[i] = f_re[i] + dx*(2/np.pi)*((((x[j]-eps)**2)*(f[j] - 1))/((x[j]-eps)**2 - x[i]**2) +\
                          (((x[j]+eps)**2)*(f[j] - 1))/((x[j]+eps)**2 - x[i]**2))
        else:
          f_re[i] = f_re[i] +dx*(2/np.pi)*((((x[j])**2)*(f[j] - 1))/((x[j])**2 - x[i]**2))

    return f_re + 1
  else:
    raise ValueError('ReIm must be Re or Im')
