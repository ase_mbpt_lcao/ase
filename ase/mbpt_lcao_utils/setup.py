from distutils.core import setup
from Cython.Build import cythonize

#to compile python setup.py build_ext --inplace
setup(
    ext_modules=cythonize("look_dens.pyx"),
    )
