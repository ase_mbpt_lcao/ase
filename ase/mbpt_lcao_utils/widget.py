#!/usr/bin/python
from __future__ import division
#file that contains the Class and functions relative to the widget
#for the graphical interface
#list of class: Entry_function, Entry_function_block, check_button, combo_box

import pygtk 
pygtk.require('2.0')
import gtk

class Entry_function:
  """
  Class used to generate a entry Frame for the graphical interface.
  Input: 
    label (string), label to print in front of the frame
  Output:
    get_text (string), what the user enter in the frame
  """
  def __init__(self, label):
    self.get_text = ''

    self.Entry_label = gtk.Label(label)
    self.Entry = gtk.Entry()
    self.Entry.add_events(gtk.gdk.KEY_RELEASE_MASK)
    self.Entry.connect('key-release-event', self.on_Entry_key)
    self.Entry_label_f = gtk.Label('....')

  def on_Entry_key(self, widget, event):
    self.Entry_label_f.set_text(widget.get_text())
    if len(widget.get_text())>0:
      self.get_text = widget.get_text()

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

class Entry_function_block:
  """
  Class used to create an array of entry frame with one label
  very useful to create matrix of entry.
  Input:
    label (string), label to print in front of the frames
    dim1 (integer), first dimension of the array
    dim2 (integer), second dimension of the array
  Output:
    Entry (list of class Entry_function), the text that the user enter in the frames
    is save in Entry[i].get_text, where i is the index of the array
    activate (bolleen), to know if the user used the box or not
  """
  def __init__(self, label, dim1, dim2):
 
    self.Entry_label = gtk.Label(label)
    self.Entry = []
    self.activate = False

    for i in range(dim1*dim2):
      self.Entry.append(Entry_function(''))
 
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

class check_button:
  """
  Class used to create a check button.
  Input:
    label (string), label to print in front of the check button
    check (bolleen), determine if the button is originaly cross or not
  Output:
    param (integer), 0 or 1 if the box is cross or not
  """
  def __init__(self, label, check):
    if check:
      self.param = 1
    else:
      self.param = 0
    self.check = check

    self.check_button = gtk.CheckButton(label)
    self.check_button.set_active(check)
    self.check_button.connect('clicked', self.on_check_button_clicked)


  def on_check_button_clicked(self, widget):
    if widget.get_active() or self.check:
      self.param = 1
    else:
      self.param = 0

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

class combo_box:
  """
  Class used to generate a combo box.
  Input:
    label (string), label to print in front of the box
    argument (list of string), arguments of the combo box
  Output:
    param (string), argument that the user selected
    activate (bolleen), to know if the user used the box or not
  """
  def __init__(self, label, argument):
    
    self.arg = argument
    self.activate = False
    self.combo_box_label = gtk.Label(label)
    self.combo_box = gtk.combo_box_new_text()
    self.combo_box.connect('changed', self.on_changed)

    for cle in argument:
      self.combo_box.append_text(cle)

    self.param = argument[0]

  def on_changed(self, widget):
    for cle in self.arg:
      if widget.get_active_text() == cle:
        self.activate = True
        self.param = cle

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def choose_file():
  dialog = gtk.FileChooserDialog("Open..",
                               None,
                               gtk.FILE_CHOOSER_ACTION_OPEN,
                               (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                                gtk.STOCK_OPEN, gtk.RESPONSE_OK))
  dialog.set_default_response(gtk.RESPONSE_OK)

  filter = gtk.FileFilter()
  filter.set_name("All files")
  filter.add_pattern("*")
  dialog.add_filter(filter)

  filter = gtk.FileFilter()
  filter.set_name("Images")
  filter.add_mime_type("image/png")
  filter.add_mime_type("image/jpeg")
  filter.add_mime_type("image/gif")
  filter.add_pattern("*.png")
  filter.add_pattern("*.jpg")
  filter.add_pattern("*.gif")
  filter.add_pattern("*.tif")
  filter.add_pattern("*.xpm")
  dialog.add_filter(filter)

  response = dialog.run()
  if response == gtk.RESPONSE_OK:
    fname = dialog.get_filename()
  elif response == gtk.RESPONSE_CANCEL:
    fname = 'Closed, no files selected'
  dialog.destroy()

  return fname

#except ImportError:
#  import Tkinter as tk
#
#  class Entry_function:
#    """
#    Class used to generate a entry Frame for the graphical interface.
#    Input: 
#      label (string), label to print in front of the frame
#    Output:
#      get_text (string), what the user enter in the frame
#    """
#    def __init__(self, root, label, row=0, column=0):
#      self.get_text = ''
#
#      self.Entry_label = tk.Label(root, text=label).grid(row = row, column=column)
#      self.Entry = tk.Entry(root)
#      self.Entry.grid(row = row, column=column+1)
#      self.Entry.bind('<Key>', self.on_Entry_key)
#
#    def on_Entry_key(self, event):
#      self.get_text = self.get_text + event.char
#
##!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#
#  class Entry_function_block:
#    """
#    Class used to create an array of entry frame with one label
#    very useful to create matrix of entry.
#    Input:
#      label (string), label to print in front of the frames
#      row (integer), the row where to put the box
#      column (integer), the column where to put the box
#    Output:
#      Entry (list of class Entry_function), the text that the user enter in the frames
#      is save in Entry[i].get_text, where i is the index of the array
#      activate (bolleen), to know if the user used the box or not
#    """
#    def __init__(self, root, label, row=0, column=0, nb_row=0, nb_col=0):
#   
#      self.Entry_label = tk.Label(root, text=label).grid(row = row, column=column)
#      self.Entry = []
#      self.activate = False
#
#      for i in range(nb_row):
#        for j in range(nb_col):
#          self.Entry.append(Entry_function(root, '', row=row+i, column=column+j))
#   
##!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#
#  class check_button:
#    """
#    Class used to create a check button.
#    Input:
#      label (string), label to print in front of the check button
#      check (bolleen), determine if the button is originaly cross or not
#    Output:
#      param (integer), 0 or 1 if the box is cross or not
#    """
#    def __init__(self, root, label, check, row=0, column=0):
#      if check:
#        self.param = 1
#      else:
#        self.param = 0
#      self.check = check
#
#      self.check_button = tk.Checkbutton(root, text=label,\
#          variable=self.check).grid(row=row, column=column, sticky=W)
#
#
#    def on_check_button_clicked(self, widget):
#      if self.check:
#        self.param = 1
#      else:
#        self.param = 0
#      print(self.param)
#
##!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#

