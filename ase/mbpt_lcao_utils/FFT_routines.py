from __future__ import division
import numpy as np
import sys

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def transform_NDto2D(A):
  """
  Transform a ND array in a 2D array
  Input parameters:
  -----------------
    A (ND numpy array): array to transform
  Output parameters:
  ------------------
    param (2D numpy array): the resulting array
  """
  prod = 1
  for i in range(1, len(A.shape)):
    prod = prod*A.shape[i]

  b = A.reshape((A.shape[0], prod))

  return b

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def transform_2DtoND(param, shape):
  """
  Transform a 2D array in a ND array
  Input parameters:
  -----------------
    param (2D numpy array): array to transform
    shape (array shape): shape of the new array
  Output parameters:
  ------------------
    A (ND numpy array): the resulting array
  """
 
  A = param.reshape(shape)
  return A

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def array2matrice(A, dtype=float):
  """
  Transform an array in a matricial form to dot product purpose, use for
  suceptibility file provide by TDDFT program
  Input parameters:
  -----------------
    A (2D numpy array), array of dimension dim(omg)x9 containing the suceptibility
    dtype (type default: float): type of array
  Output parameters:
  ------------------
    mat (3D numpy array), array of dimension dim(omg)x3x3
  """
  mat = np.zeros((A.shape[0], 3, 3), dtype=dtype)

  comp = 0
  for i in range(3):
    for j in range(3):
      mat[:, i, j] = A[:, comp]
      comp = comp + 1

  return mat
        
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def fft_freq_1D(t):
  """
  Determine the Fourier variable of t in 1D
  input:
  ------
    t, 1D numpy array containing the time variable
  output:
  ------
    w, 1D numpy array containing the frequency variable
  """
  dt = t[1]-t[0]
  dw = 2*np.pi/(t.size*dt)
  
  w_min = - 2*np.pi*(t.size-1)/(dt*t.size) / 2
  arange = np.arange(t.shape[0])
  w = arange*dw +w_min
  
  return w

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  

def fft_freq_inter(t, v):
  """
  Determine the Fourier variable of t in 1D
  input:
  ------
    t, 1D numpy array containing the time variable
  output:
  ------
    w, 1D numpy array containing the frequency variable
  """
  dt = t[1]-t[0]
  dw = 2*np.pi*v/(t.size*dt)
  
  w_min = - 2*np.pi*(t.size-1)*v/(dt*t.size) / 2
  arange = np.arange(t.shape[0])
  w = arange*dw +w_min
  
  return w

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def mod_FFT_ND(w, t, f, axis=0):
  """
  Calculate the FFT of a ND dimensionnal array over the axes axes
  corresponding to the FT by using numpy fft
  The Fourier transform is done on the first dimension.
  input:
  ------
    w, 1D numpy array containing the frequency variable from fft_freq
    t, 1D numpy array containing the time variable
    f, 1D or 2D numpy array containing the data to Fourier transform (f(t, x, ....))
  output:
  -------
    F, 1D or 2D numpy array containing the FT
  """

  tmin = t[0]
  wmin = w[0]
  dt = t[1] - t[0]
  dw = w[1] - w[0]
  N = t.size

  param = dw*dt*N/(2*np.pi)
  if abs(param-1)>1e-10 : 
    sys.stdout.write('cannot use fft, param != 1\n')
    sys.stdout.write('param = %s\n' %(param, ))
    sys.stdout.write('dw = %s\n' % (dw, ))
    sys.stdout.write('dt = %s\n' % (dt, ))
    sys.stdout.write('N = %s\n' % (N, ))
    sys.exit()


  tp = np.linspace(0.0, 2*tmax, N)
 
  f_new = dt*f*np.exp(-complex(0, 1)*wmin*(t-tmin))
  F = np.fft.fft(f_new, axis=axis)
  F = F*np.exp(-complex(0, 1)*(wmin*tmin + (w-wmin)*tmin))/np.sqrt(2*np.pi)
  #F = F*np.exp(complex(0.0, 1.0)*wmin*t)/np.sqrt(pi)

  return F

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def mod_iFFT_ND(t, w, F, axis = 0):
  """
  Calculate the FFT of a 1D dimensionnal array
  corresponding to the FT by using numpy fft
  input:
  ------
    t, 1D numpy array containing the time variable
    w, 1D numpy array containing the frequency variable from fft_freq
    F, 1D numpy array containing the data to inverse Fourier transform
  output:
  -------
    F, 1D numpy array containing the iFT
  """
  
  tmin = t[0]
  wmin = w[0]
  dt = t[1] - t[0]
  dw = w[1] - w[0]
  N = t.size

  param = dw*dt*N/2/np.pi
  if abs(param-1)>1e-10 : 
    print('cannot use fft, param != 1')
    print('param = ', param)
    print('dw = ', dw)
    print('dt = ', dt)
    print('N = ', N)
    sys.exit()


  F_new = np.zeros(F.shape, dtype = np.complex128)

  if axis != 0:
    raise ValueError("Not yet implemented, for the moment you must use axis =0")
 
  for iw, wv in enumerate(w):
    F_new[iw] = dw*F[iw]*np.exp(complex(0, 1)*tmin*(wv-wmin))
  
  f = N*np.fft.ifft(F_new, axis = axis)
  
  for it, tv in enumerate(t):
    f[it] = f[it]*np.exp(complex(0, 1)*(wmin*tmin + wmin*(tv-tmin)))/np.sqrt(2*np.pi)

  return f

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  

def FFT_fort(w, t, f, dtype=float):
  """
  Calculate the FFT of a 1D dimensionnal array
  corresponding to the FT by using fftw3 and fortran.
  The Fourier transform is done on the first dimension.
  input:
  ------
    w, 1D numpy array containing the frequency variable from fft_freq
    t, 1D numpy array containing the time variable
    f, 1D or 2D numpy array containing the data to Fourier transform
  output:
  -------
    F, 1D numpy array containing the FT
  """

  import fftw_routines as fft_for

  if len(f.shape) == 1:
    if dtype == float:
      F = fft_for.fft_1d_real(w, t, f)
    elif dtype == complex:
      F = fft_for.fft_1d_complex(w, t, f)
    else:
      raise ValueError('Only float and complex dtype are supported')
  elif len(f.shape) == 2:
    if dtype == float:
      F = fft_for.fft_2d_real(w, t, f)
    elif dtype == complex:
      F = fft_for.fft_2d_complex(w, t, f)
    else:
      raise ValueError('Only float and complex dtype are supported')
  else:
    raise ValueError('This function is aims to work with array of 1 or 2D,\n \
                      if you want more, please reshape your array!')

  return F


#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 

def mod_FFT_1D(w, t, f):
  """
  Calculate the FFT of a 1D dimensionnal array
  corresponding to the FT by using numpy fft
  The Fourier transform is done on the first dimension.
  input:
  ------
    w, 1D numpy array containing the frequency variable from fft_freq
    t, 1D numpy array containing the time variable
    f, 1D or 2D numpy array containing the data to Fourier transform (f(t, x, ....))
  output:
  -------
    F, 1D or 2D numpy array containing the FT
  """

  tmin = t[0]
  wmin = w[0]
  dt = t[1] - t[0]
  dw = w[1] - w[0]
  N = t.size

  param = dw*dt*N/(2*np.pi)
  if abs(param-1)>1e-10 : 
    sys.stdout.write('cannot use fft, param != 1\n')
    sys.stdout.write('param = %s\n' %(param, ))
    sys.stdout.write('dw = %s\n' % (dw, ))
    sys.stdout.write('dt = %s\n' % (dt, ))
    sys.stdout.write('N = %s\n' % (N, ))
    sys.exit()

 
  if len(f.shape) == 1:
    f_new = dt*f*np.exp(-complex(0, 1)*wmin*(t-tmin))
    F = np.fft.fft(f_new)
    F = F*np.exp(-complex(0, 1)*(wmin*tmin + (w-wmin)*tmin))/np.sqrt(2*np.pi)
  elif len(f.shape) == 2:
    F = np.zeros(f.shape, dtype = complex)
    f_new = np.zeros(f.shape, dtype=complex)
    for i in range(f_new.shape[1]):
      f_new[:, i] = dt*f[:, i]*np.exp(-complex(0, 1)*wmin*(t-tmin))
      F[:, i] = np.fft.fft(f_new[:, i])
      F[:, i] = F[:, i]*np.exp(-complex(0, 1)*(wmin*tmin + (w-wmin)*tmin))/np.sqrt(2*np.pi)
  else:
    raise ValueError('Can only perform FFt of 1D or 2D array\n please, use transform function!!\n Exit!!')


  return F


#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def FFT_1D_inter(w, t, f, v):
  """
  Calculate the FFT of a ND dimensionnal array over the axis 
  corresponding to the FT by using numpy fft
  The Fourier transform is done on the first dimension.
  input:
  ------
    w, 1D numpy array containing the frequency variable from fft_freq
    t, 1D numpy array containing the time variable
    f, 1D or 2D numpy array containing the data to Fourier transform (f(t, x, ....))
  output:
  -------
    F, 1D or 2D numpy array containing the FT
  """

  tmin = t[0]
  wmin = w[0]
  dt = t[1] - t[0]
  dw = w[1] - w[0]
  N = t.size

  if v == 0.0:
    raise ValueError('the coefficient can not be nul')

  param = dw*dt*N/(2*np.pi*v)
  if abs(param-1)>1e-10 : 
    sys.stdout.write('cannot use fft, param != 1\n')
    sys.stdout.write('param = %s\n' %(param, ))
    sys.stdout.write('dw = %s\n' % (dw, ))
    sys.stdout.write('dt = %s\n' % (dt, ))
    sys.stdout.write('N = %s\n' % (N, ))
    sys.exit()

 
  if len(f.shape) == 1:
    f_new = dt*f*np.exp(-complex(0, 1)*wmin*(t-tmin)/v)
    F = np.fft.fft(f_new)
    F = F*np.exp(-complex(0, 1)*(wmin*tmin + (w-wmin)*tmin)/v)
  elif len(f.shape) == 2:
    F = np.zeros(f.shape, dtype = complex)
    f_new = np.zeros(f.shape, dtype=complex)
    for i in range(f_new.shape[1]):
      f_new[:, i] = dt*f[:, i]*np.exp(-complex(0, 1)*wmin*(t-tmin)/v)
      F[:, i] = np.fft.fft(f_new[:, i])
      F[:, i] = F[:, i]*np.exp(-complex(0, 1)*(wmin*tmin + (w-wmin)*tmin)/v)
  elif len(f.shape) == 3:
    F = np.zeros(f.shape, dtype = complex)
    f_new = np.zeros(f.shape, dtype=complex)
    for i in range(f_new.shape[1]):
      for j in range(f_new.shape[2]):
        f_new[:, i, j] = dt*f[:, i, j]*np.exp(-complex(0, 1)*wmin*(t-tmin)/v)
        F[:, i, j] = np.fft.fft(f_new[:, i, j])
        F[:, i, j] = F[:, i, j]*np.exp(-complex(0, 1)*(wmin*tmin + (w-wmin)*tmin)/v)
  else:
    raise ValueError('Can only perform FFt of 1D or 2D array\n please, use transform function!!\n Exit!!')


  return F


#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


def mod_iFFT_1D(t, w, F):
  """
  Calculate the FFT of a 1D dimensionnal array
  corresponding to the FT by using numpy fft
  input:
  ------
    t, 1D numpy array containing the time variable
    w, 1D numpy array containing the frequency variable from fft_freq
    F, 1D numpy array containing the data to inverse Fourier transform
  output:
  -------
    F, 1D numpy array containing the iFT
  """
  
  tmin = t[0]
  wmin = w[0]
  dt = t[1] - t[0]
  dw = w[1] - w[0]
  N = t.size

  param = dw*dt*N/2/np.pi
  if abs(param-1)>1e-10 : 
    print('cannot use fft, param != 1')
    print('param = ', param)
    print('dw = ', dw)
    print('dt = ', dt)
    print('N = ', N)
    sys.exit()

  f = np.zeros(F.shape, dtype = complex)
  F_new = np.zeros(F.shape, dtype=complex)
 
  if len(F.shape)==1:
    F_new = dw*F*np.exp(complex(0, 1)*tmin*(w-wmin))
    f = N*np.fft.ifft(F_new)
    f = f*np.exp(complex(0, 1)*(wmin*tmin + wmin*(t-tmin)))/np.sqrt(2*np.pi)
  elif len(F.shape) == 2:
    for i in range(F.shape[1]):
      F_new[:, i] = dw*F[:, i]*np.exp(complex(0, 1)*tmin*(w-wmin))
      f[:, i] = N*np.fft.ifft(F_new[:, i])
      f[:, i] = f[:, i]*np.exp(complex(0, 1)*(wmin*tmin + wmin*(t-tmin)))/np.sqrt(2*np.pi)
  else:
    raise ValueError('Can only perform iFFt of 1D or 2D array\n \
        please, use transform function!!\n \
        Exit!!')

  return f

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!11111

def data_freq_sym(d, op='symmetric', dtype = float) :
  """
  Do the symmetric of an 1D or 2D array.
  Input parameters:
  -----------------
    d (1D or 2D numpy array): the array to symmetrize
    op (string): can be symmetric or hermitian:
      if op == symmetric:
        the new array will be the symmetric of the old array 
      if op == hermitian:
        the new array will be the hermitian of the old array
        Work only if dtype = complex
    dtype (type): type of the array
  Output parameters:
  ------------------
    d_sym (1D or 2D numpy array): the array containing the symetric or the
                                  herminian of the initial array
  """

  if len(d.shape) == 1:
    d_sym = np.zeros((2*d.shape[0] +1), dtype=dtype)
    
    d_sym[d.shape[0]+1:d_sym.shape[0]] = d

    if op=='symmetric' :
      d_sym[0:d.shape[0]] = -d[::-1]
      d_sym[d.shape[0]] = 0
    elif op=='hermitian' :
      d_sym[0:d.shape[0]] = d[::-1].conjugate()
      d_sym[d.shape[0]] = (d[0].conjugate()+d[0].conjugate())/2.0
    else:
      raise ValueError('op is unknown')

  elif len(d.shape) == 2:
    d_sym = np.zeros((2*d.shape[0] +1, d.shape[1]), dtype=dtype)
    
    d_sym[d.shape[0]+1:d_sym.shape[0], :] = d
    
    if op=='symmetric' :
      d_sym[0:d.shape[0], :] = -d[::-1]
      d_sym[d.shape[0], :] = 0
    elif op=='hermitian' :
      d_sym[0:d.shape[0], :] = d[::-1].conjugate()
      d_sym[d.shape[0], :] = (d[0, :].conjugate()+d[0, :])/2.0
    else:
      raise ValueError('op is unknown')
  else:
    raise ValueError('Can only symmetrize 1 or 2D array\n \
        Please use the transform function to transform your array!!\n \
        Exit!!')

  return d_sym

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def Partie_entiere(float_nb):
  if (abs(float_nb - int(float_nb)) <0.5) and float_nb >0:
    PE = int(float_nb)
  elif (abs(float_nb - int(float_nb)) >= 0.5) and float_nb>0:
    PE = int(float_nb) + 1
  elif (abs(float_nb - int(float_nb)) < 0.5) and float_nb<0:
    PE = int(float_nb)
  elif (abs(float_nb - int(float_nb)) >= 0.5) and float_nb<0:
    PE = int(float_nb) -1

  return PE

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def seach_maximum_2D(A):
  maxi = np.amax(A)
  M = list()
  for i in range(A.shape[0]):
    for j in range(A.shape[1]):
      if A[i, j] == maxi:
        M.append(np.array([i, j]))
  return M

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
 
def seach_maximum_3D(A):
  maxi = np.amax(A)
  M = list()
  for i in range(A.shape[0]):
    for j in range(A.shape[1]):
      for k in range(A.shape[2]):
        if A[i, j, k] == maxi:
          M.append(np.array([i, j, k]))

  return maxi, M

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def list2array(L, dim1, dim2):
  
  A = np.zeros((dim1, dim2), dtype = float)
  comp = 0
  
  for i in range(dim1):
    for j in range(dim2):
      A[i, j] = L[comp]
      comp = comp + 1
  
  return A

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def fft_freq(*args):
  """
  Determine the Fourier variable of the multidimensionnal array t
  input:
  ------
    args, list of 1D numpy array containing the time variables
  output:
  ------
    w, list of 1D numpy array containing the frequency variable

  !!!!!!!!!!!!!!!!!!!!!
  !                   !
  !       Remark      !
  !                   !
  !!!!!!!!!!!!!!!!!!!!!
  I noticed that in order to get the right value for the frequency range,
  one have to multiply omg by the factor: 
  2*np.max(t)/N
  But this rescaling must be done only after the fft, for plotting for 
  example, otherwise mod_FFT_1D will crash!!!!!
  """

  N = list()
  dv = list()
  dw = list()
  w = list()

  for i, j in enumerate(args[0]):
    N.append(j.size)
    print(j)
    dv.append(j[1]-j[0])

    dw.append(2*np.pi/(N[i]*dv[i]))
    w_min = - 2*np.pi*(N[i]-1)/(dv[i]*N[i]) / 2
    w.append(np.zeros((j.shape[0]), dtype = float))
    for k in range(j.shape[0]):
      w[i][k] = dw[i]*k + w_min

  return N, w

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def mod_FFTn(f, *args, **kwargs):
  """
  Calculate the FFT of a nD dimensionnal array
  corresponding to the FT by using numpy fft
  input:
  ------
    f, nD numpy array containing the data to Fourier transform
    *args, list of 1D numpy array containing the time variables
    **kwargs (optional):
        s(default=None), sequence of ints, optional
            Shape (length of each transformed axis) of the output (s[0] refers to axis 0, 
            s[1] to axis 1, etc.). This corresponds to n for fft(x, n). Along any axis, if 
            the given shape is smaller than that of the input, the input is cropped. If it is 
            larger, the input is padded with zeros. if s is not given, the shape of the 
            input along the axes specified by axes is used.
        axes(Default=None), sequence of ints, optional
              Axes over which to compute the FFT. If not given, the last len(s) axes are used, 
              or all axes if s is also not specified. Repeated indices in axes means that 
              the transform over that axis is performed multiple times.
        dtype(Default=float), numpy type
              type of the unput array
  output:
  -------
    W_mesh: list of nD array containing the mesh of the time combine with fourier variable
    F, nD numpy array containing the FT


  """

  keys = {'axes': None, 'dtype': float, 's': None}

  if len(kwargs) >0:
    for i, j in kwargs.items():
      keys[i] = j


  #check dimension
  if len(args) != len(f.shape):
    print('Error: dimension of the function and the time variable are not matching')
    print('len(f.shape) = {0}, len(args) = {1}'.format(len(f.shape), len(args)))
    print('Exit!!')
    sys.exit()
  else:
    for i in range(len(f.shape)):
      if f.shape[i] != args[i].shape[0]:
        print('Error: dimension', i, ' of the function and the time variable is not matching')
        print('f.shape[{0}] = {1}, var[{0}].shape[0] = {2}'.format(i, f.shape[i], args[i].shape[0]))
        print('Exit!!')
        sys.exit()


  t = []
  tmin = []
  wmin = []
  dt = []
  dw = []

  for i in args:
    t.append(i)
  N, w = fft_freq(t)

  F = np.zeros(f.shape, dtype = complex)
  f_new = np.zeros(f.shape, dtype=keys['dtype'])

  for i in range(len(t)):
    tmin.append(t[i][0])
    wmin.append(w[i][0])
    dt.append(t[i][1] - t[i][0])
    dw.append(w[i][1] - w[i][0])
    
    param = dw[i]*dt[i]*N[i]/2/np.pi

    if abs(param-1)>1e-10 : 
      print('cannot use fft, param != 1')
      print('param = ', param)
      print('dw = ', dw[i])
      print('dt = ', dt[i])
      print('N = ', N[i])
      sys.exit()

  if len(f.shape) == 1:
    f_new = dt[0]*f*np.exp(-complex(0, 1)*wmin[0]*(t[0]-tmin[0]))
    posfactor = np.exp(-complex(0, 1)*(wmin[0]*tmin[0] + tmin[0]*(w[0]-wmin[0]))) 
 
    F = np.fft.fftn(f_new)*posfactor
    return w[0], F

  elif len(f.shape) == 2:
    if keys['axes'] == (0, 0):
      T1, T2 = np.meshgrid(t[0], t[1])
      W1, W2 = np.meshgrid(w[0], w[1])
      W_mesh = np.meshgrid(w[0], t[1])

      f_new = dt[0]*f*np.exp(-complex(0, 1)*wmin[0]*(T1-tmin[0]))
      posfactor = np.exp(-complex(0, 1)*(wmin[0]*tmin[0] + tmin[0]*(W1-wmin[0])))
      F = np.fft.fftn(f_new, s = keys['s'], axes=keys['axes'])*posfactor
    
    elif keys['axes'] == (1, 0) or keys['axes'] == (0, 1):
      T1, T2 = np.meshgrid(t[0], t[1])
      W1, W2 = np.meshgrid(w[0], w[1])
      W_mesh = np.meshgrid(w[0], w[1])

      f_new = dt[0]*dt[1]*f*np.exp(-complex(0, 1)*(wmin[0]*(T1-tmin[0]) + wmin[1]*(T2-tmin[1])))
      posfactor = np.exp(-complex(0, 1)*(wmin[0]*tmin[0] + tmin[0]*(W1-wmin[0]) + wmin[1]*tmin[1] + tmin[1]*(W2-wmin[1])))
      F = np.fft.fftn(f_new, s = keys['s'], axes=keys['axes'])*posfactor

    elif  keys['axes'] == (1, 1):
      T1, T2 = np.meshgrid(t[0], t[1])
      W1, W2 = np.meshgrid(w[0], w[1])
      W_mesh = np.meshgrid(t[0], w[1])
      
      f_new = dt[1]*f*np.exp(-complex(0, 1)*wmin[1]*(T2-tmin[1]))
      posfactor = np.exp(-complex(0, 1)*(wmin[1]*tmin[1] + tmin[1]*(W2-wmin[1])))
      F = np.fft.fftn(f_new, s = keys['s'], axes=keys['axes'])*posfactor

    else:
      print('Error: F and axes not matching!!')
      print('f.shape = {0}, axes = {1}'.format(f.shape, keys['axes']))
      print('Exit!!')
      sys.exit()
  
    return W_mesh, F

  elif len(f.shape) == 3:
    if keys['axes'] == (0, 0, 0):
      T1, T2, T3 = np.meshgrid(t[0], t[1], t[2])
      W1, W2, W3 = np.meshgrid(w[0], w[1], w[2])
      W_mesh = np.meshgrid(w[0], t[1], t[2])

      f_new = dt[0]*f*np.exp(-complex(0, 1)*wmin[0]*(T1-tmin[0]))
      posfactor = np.exp(-complex(0, 1)*(wmin[0]*tmin[0] + tmin[0]*(W1-wmin[0])))
      F = np.fft.fftn(f_new, s = keys['s'], axes=keys['axes'])*posfactor
    
    elif  keys['axes'] == (1, 1, 1):
      T1, T2, T3 = np.meshgrid(t[0], t[1], t[2])
      W1, W2, W3 = np.meshgrid(w[0], w[1], w[2])
      W_mesh = np.meshgrid(t[0], w[1], t[2])

      f_new = dt[1]*f*np.exp(-complex(0, 1)*wmin[1]*(T2-tmin[1]))
      posfactor = np.exp(-complex(0, 1)*(wmin[1]*tmin[1] + tmin[1]*(W2-wmin[1])))
      F = np.fft.fftn(f_new, s = keys['s'], axes=keys['axes'])*posfactor

    elif  keys['axes'] == (2, 2, 2):
      T1, T2, T3 = np.meshgrid(t[0], t[1], t[2])
      W1, W2, W3 = np.meshgrid(w[0], w[1], w[2])
      W_mesh = np.meshgrid(t[0], t[1], w[2])

      f_new = dt[2]*f*np.exp(-complex(0, 1)*wmin[2]*(T3-tmin[2]))
      posfactor = np.exp(-complex(0, 1)*(wmin[2]*tmin[2] + tmin[2]*(W3-wmin[2])))
      F = np.fft.fftn(f_new, s = keys['s'], axes=keys['axes'])*posfactor

    elif keys['axes'] == (0, 0):# or keys['axes'] == (0, 0, 1):
      print('Not implemented!')
      print('Exit!')
      sys.exit()

    elif keys['axes'] == (0, 1):# or keys['axes'] == (0, 0, 1):
      print('Not implemented!')
      print('Exit!')
      sys.exit()
  
    elif keys['axes'] == (1, 0):# or keys['axes'] == (0, 0, 1):
      print('Not implemented!')
      print('Exit!')
      sys.exit()

    elif keys['axes'] == (1, 1):# or keys['axes'] == (0, 0, 1):
      print('Not implemented!')
      print('Exit!')
      sys.exit()

    elif keys['axes'] == (0, 2):# or keys['axes'] == (0, 0, 1):
      print('Not implemented!')
      print('Exit!')
      sys.exit()

    elif keys['axes'] == (2, 0):# or keys['axes'] == (0, 0, 1):
      print('Not implemented!')
      print('Exit!')
      sys.exit()
  
    elif keys['axes'] == (1, 2):# or keys['axes'] == (0, 0, 1):
      print('Not implemented!')
      print('Exit!')
      sys.exit()

    elif keys['axes'] == (2, 1):# or keys['axes'] == (0, 0, 1):
      print('Not implemented!')
      print('Exit!')
      sys.exit()

    elif keys['axes'] == (2, 2):# or keys['axes'] == (0, 0, 1):
      print('Not implemented!')
      print('Exit!')
      sys.exit()


    elif keys['axes'] == (0, 0, 1):
      T1, T2, T3 = np.meshgrid(t[0], t[1], t[2])
      W1, W2, W3 = np.meshgrid(w[0], w[1], w[2])
      W_mesh = np.meshgrid(w[0], w[1], t[2])

      f_new = dt[0]*dt[1]*f*np.exp(-complex(0, 1)*(wmin[0]*(T1-tmin[0]) + wmin[1]*(T2-tmin[1])))
      posfactor = np.exp(-complex(0, 1)*(wmin[0]*tmin[0] + tmin[0]*(W1-wmin[0]) + wmin[1]*tmin[1] + tmin[1]*(W2-wmin[1])))
      F = np.fft.fftn(f_new, s = keys['s'], axes=keys['axes'])*posfactor

    #elif keys['axes'] == (1, 0) or keys['axes'] == (0, 0, 1):

    else:
      print('Error: F and axes not matching!!')
      print('f.shape = {0}, axes = {1}'.format(f.shape, keys['axes']))
      print('Exit!!')
      sys.exit()
    
    return W_mesh, F
  
  elif len(f.shape) == 4:
    if keys['axes'] == (0, 0, 0, 0):
      T1, T2, T3, T4 = np.meshgrid(t[1], t[0], t[2], t[3])
      W1, W2, W3, W4 = np.meshgrid(w[1], w[0], w[2], w[3])
      W_mesh = np.meshgrid(w[1], t[0], t[2], t[3])

      f_new = dt[0]*f*np.exp(-complex(0, 1)*wmin[0]*(T1-tmin[0]))
      posfactor = np.exp(-complex(0, 1)*(wmin[0]*tmin[0] + tmin[0]*(W1-wmin[0])))
      F = np.fft.fftn(f_new, s = keys['s'], axes=keys['axes'])*posfactor
    
    elif  keys['axes'] == (3, 3, 3, 3):
      T1, T2, T3, T4 = np.meshgrid(t[1], t[0], t[2], t[3])
      W1, W2, W3, W4 = np.meshgrid(w[1], w[0], w[2], w[3])
      W_mesh = np.meshgrid(t[1], t[0], t[2], w[3])

      f_new = dt[3]*f*np.exp(-complex(0, 1)*wmin[3]*(T4-tmin[3]))
      posfactor = np.exp(-complex(0, 1)*(wmin[3]*tmin[3] + tmin[3]*(W4-wmin[3])))
      F = np.fft.fftn(f_new, s = keys['s'], axes=keys['axes'])*posfactor

    else:
      print('Error: F and axes not matching or axis combinaison not implemented!!')
      print('f.shape = {0}, axes = {1}'.format(f.shape, keys['axes']))
      print('Exit!!')
      sys.exit()
    
    return W_mesh, F
  
  else:
    print('Error: the maximal number of implementation is 4!!!')
    print('Exit!!')
    sys.exit()

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def convolution_fft_1D(x, f, g):
  """
  Perform the convolution product f*g by using
  the function mod_FFT_1D and mod_FFT_1D in 1D.
  input:
  ------
    x, 1D numpy array containing the abscisse of the function f and g
    f, 1D numpy array containing the first function
    g, 1D numpy array containing the second function
  output:
  -------
    xb, 1D numpy array containing the abscisse after FFT and iFFT
    conv, 1D numpy array of the convolution product
  """
  
  k = fft_freq_1D(x)
  F = mod_FFT_1D(k, x, f)

  G = mod_FFT_1D(k, x, g)

  C = F*G
  
  xb = fft_freq_1D(k)
  conv = mod_iFFT_1D(xb, k, C)

  return xb, conv

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
def mirror(A, axe=0, dtype=float):
  """
  do the mirror of an array along the axe axes
  input:
  ------
    A : np.array of any dimension until 4
    axe: integer, axe along which the mirror should be done
    dtype: string, type of array
  output:
  -------
    A_mir: np.array, the new mirror array
  example:
  --------
    A = np.array([1, 2, 5, 6, 4, 3]))
    mirror(A, 0, int) = [5, 2, 1, 3, 4, 6]
  """
  A_mir = np.zeros(A.shape, dtype = dtype)
  dim = len(A.shape)
  N = A.shape[axe]

  if dim == 1:
    for i in range(int(N/2)):
      A_mir[i] = A[int(N/2)-i-1]
      A_mir[N-i-1] = A[int(N/2)+i]
  
  elif dim ==2:
    if axe == 0:
      for i in range(int(N/2)):
        A_mir[i, :] = A[int(N/2)-i-1, :]
        A_mir[N-i-1, :] = A[int(N/2)+i, :]
  
    elif axe == 1:
      for i in range(int(N/2)):
        A_mir[:, i] = A[:, int(N/2)-i-1]
        A_mir[:, N-i-1] = A[:, int(N/2)+i]
    
    else:
      print('Error: axis must be 0 or 1')
      print('Exit!!')
      sys.exit()

  elif dim ==3:
    if axe == 0:
      for i in range(int(N/2)):
        A_mir[i, :, :] = A[int(N/2)-i-1, :, :]
        A_mir[N-i-1, :, :] = A[int(N/2)+i, :, :]
  
    elif axe == 2:
      for i in range(int(N/2)):
        A_mir[:, i, :] = A[:, int(N/2)-i-1, :]
        A_mir[:, N-i-1, :] = A[:, int(N/2)+i, :]

    elif axe == 3:
      for i in range(int(N/2)):
        A_mir[:, :, i] = A[:, :, int(N/2)-i-1]
        A_mir[:, :, N-i-1] = A[:, :, int(N/2)+i]

    else:
      print('Error: axis must be 0, 1 or 2')
      print('Exit!!')
      sys.exit()

  elif dim ==4:
    if axe == 0:
      for i in range(int(N/2)):
        A_mir[i, :, :, :] = A[int(N/2)-i-1, :, :, :]
        A_mir[N-i-1, :, :, :] = A[int(N/2)+i, :, :, :]
 
    elif axe == 1:
      for i in range(int(N/2)):
        A_mir[:, i, :, :] = A[:, int(N/2)-i-1, :, :]
        A_mir[:, N-i-1, :, :] = A[:, int(N/2)+i, :, :]
 
    elif axe == 2:
      for i in range(int(N/2)):
        A_mir[:, :, i, :] = A[:, :, int(N/2)-i-1, :]
        A_mir[:, :, N-i-1, :] = A[:, :, int(N/2)+i, :]
 
    elif axe == 3:
      for i in range(int(N/2)):
        A_mir[:, :, :, i] = A[:, :, :, int(N/2)-i-1]
        A_mir[:, :, :, N-i-1] = A[:, :,  :, int(N/2)+i]
 
    else:
      print('Error: axis must be 0, 1, 2 or 3')
      print('Exit!!')
      sys.exit()
  
  else:
      print('Error: dim(Array) must be < 5')
      print('Exit!!')
      sys.exit()

  return A_mir


#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


def reoder_array(A):
  """
  Reorder an 1D array from its minimal value to its maximal value
  input:
    A (1D array): the input array to reorder (Attention 1D!!, 
              example A = np.array([1, 2, 5, 6, 4, 3]))
  output:
    new (1D array): the ordered array, same values thn A, but the array goes from min to max
                  => new = np.array([1, 2, 3, 4, 5, 6])
  """
  new = np.zeros(A.shape, dtype=float)
        
  for i in range(new.shape[0]):
    new[i] = np.min(A)
    A = np.delete(A, np.argmin(A))


  return new

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def reverse(A, axe=0, dtype=float):
  """
  reverse all the element of an array along the axe axes
  input:
  ------
    A : np.array of any dimension until 4
    axe: integer, axe along which the reverse should be done
    dtype: string, type of array
  output:
  -------
    A_rev: np.array, the new array reversed
  example:
  --------
    A = np.array([1, 2, 5, 6, 4, 3]))
    reverse(A, 0, int) = [3, 4, 6, 5, 2, 1]
  """
  A_rev = np.zeros(A.shape, dtype = dtype)
  dim = len(A.shape)
  N = A.shape[axe]

  if dim == 1:
    for i in range(N):
      A_rev[i] = A[N-i-1]
  
  elif dim ==2:
    if axe == 0:
      for i in range(N):
        A_rev[i, :] = A[N-i-1, :]

    elif axe == 1:
      for i in range(N):
        A_rev[:, i] = A[:, N-i-1]
    
    else:
      print('Error: axis must be 0 or 1')
      print('Exit!!')
      sys.exit()

  elif dim ==3:
    if axe == 0:
      for i in range(N):
        A_rev[i, :, :] = A[N-i-1, :, :]

    elif axe == 2:
      for i in range(N):
        A_rev[:, i, :] = A[:, N-i-1, :]
    
    elif axe == 3:
      for i in range(N):
        A_rev[:, :, i] = A[:, :, N-i-1]

    else:
      print('Error: axis must be 0, 1 or 2')
      print('Exit!!')
      sys.exit()

  elif dim ==4:
    if axe == 0:
      for i in range(N):
        A_rev[i, :, :, :] = A[N-i-1, :, :, :]

    elif axe == 1:
      for i in range(N):
        A_rev[:, i, :, :] = A[:, N-i-1, :, :]

    elif axe == 2:
      for i in range(N):
        A_rev[:, :, i, :] = A[:, :, N-i-1, :]

    elif axe == 3:
      for i in range(N):
        A_rev[:, :, :, i] = A[:, :, :, N-i-1]

    else:
      print('Error: axis must be 0, 1, 2 or 3')
      print('Exit!!')
      sys.exit()
  
  else:
      print('Error: dim(Array) must be < 5')
      print('Exit!!')
      sys.exit()

  return A_rev

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 
#                                                           !
#               Parallel with Anaconda                      !
#                                                           !
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    

class FFT_parallel:
  """
  Perform the FFT calculation using the numbapro library from the 
  accelerate package of Anaconda (https://store.continuum.io/cshop/accelerate/).
  Can perform calculations using cpu, parallel or gpu.
  Input Parameters:
  -----------------
    target (string, default: cpu): determine the parallelization type, can be
                                  _cpu, only one core, but can improve calculations speed compare to numpy
                                  _parallel, calculations done in parallel between the cpu of the computer
                                  _gpu, calculations doe in parallel between the gpu of the graphic card
                                      !!WARNING: gpu work only if a nvidia graphic card is present with the CUDA technology
  Output Parameters:
  ------------------
    Return the FT (mod_FFT_para) or the iFT (mod_iFFT_para) of a functions
  Functions of the class:
  -----------------------
    fft_freq_para
    mod_FFT_para
    mod_iFFT_para
  """
  def __init__(self, target='cpu'):
    self.para = True
    self.target = target

    import func_parallel as fp
    if self.para:
      print('Parallel calculation, with: '  + self.target)
    else:
      print('No parallel calculation!!!')
      self.target = 'cpu'

    self.freq_calc_para_vec = fp.vectorize(['float32(float32, float32, float32)',
              'float64(float64, float64, float64)'], target=self.target)(fp.freq_calc_para)
    self.array_mult_vec = fp.vectorize(['complex128(float64, complex128, complex128)',
                                        'complex128(complex128, complex128, complex128)',
                                        'complex128(float64, float64, complex128)',
                                        'float64(float64, float64, float64)',], 
                                                  target=self.target)(fp.array_mult)
    #self.fft_para_vec = fp.vectorize(['complex128(float64)', 'complex128(complex128)'],    !!! Not Working !!!
    #                                                    target=self.target)(fp.fft_para)

  #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  def fft_freq_para(self, t):
    """
    Determine the Fourier variable of t in 1D
    input:
    ------
      t, 1D numpy array containing the time variable
    output:
    ------
      w, 1D numpy array containing the frequency variable

    !!!!!!!!!!!!!!!!!!!!!
    !                   !
    !       Remark      !
    !                   !
    !!!!!!!!!!!!!!!!!!!!!
    I noticed that in order to get the right value for the frequency range,
    one have to multiply omg by the factor: 
    2*np.max(t)/N
    But this rescaling must be done only after the fft, for plotting for 
    example, otherwise mod_FFT_1D will crash!!!!!
    """
   
    dt = t[1]-t[0]
    dw = 2*np.pi/(t.size*dt)
    
    w_min = - 2*np.pi*(t.size-1)/(dt*t.size) / 2
    arange = np.arange(t.shape[0])
    w = self.freq_calc_para_vec(dw, w_min, arange)
    
    return w

  #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  

  def mod_FFT_para(self, w, t, f):
    """
    Calculate the FFT of a 1D dimensionnal array
    corresponding to the FT by using numpy fft
    input:
    ------
      w, 1D numpy array containing the frequency variable from fft_freq
      t, 1D numpy array containing the time variable
      f, 1D or 2D numpy array containing the data to Fourier transform
    output:
    -------
      F, 1D numpy array containing the FT
    """

    tmin = t[0]
    wmin = w[0]
    dt = t[1] - t[0]
    dw = w[1] - w[0]
    N = t.size

    param = dw*dt*N/2/np.pi
    if abs(param-1)>1e-10 : 
      print('cannot use fft, param != 1')
      print('param = ', param)
      print('dw = ', dw)
      print('dt = ', dt)
      print('N = ', N)
      sys.exit()

    F = np.zeros(f.shape, dtype = complex)
   
    if len(f.shape) == 1:
      f_new = self.array_mult_vec(dt, f, np.exp(-complex(0, 1)*wmin*(t-tmin)))
      #F = self.fft_para_vec(f_new)  Not working, possible to do???
      F = np.fft.fft(f_new)
      F = self.array_mult_vec(F, np.exp(-complex(0, 1)*(wmin*tmin + (w-wmin)*tmin)), 1.0)
    elif len(f.shape) == 2:
      f_new = np.zeros(f.shape, dtype = complex)
      for i in range(f.shape[1]):
        f_new[:, i] = self.array_mult_vec(dt, f[:, i], np.exp(-complex(0, 1)*wmin*(t-tmin)))
        F[:, i] = np.fft.fft(f_new[:, i])
        F[:, i] = self.array_mult_vec(F[:, i], np.exp(-complex(0, 1)*(wmin*tmin + (w-wmin)*tmin)), 1.0)
    else:
      raise ValueError('Can only perform FFt of 1D or 2D array\n \
          please, use transform function!!\n \
          Exit!!')


    return F

  #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
  def mod_iFFT_para(self, t, w, F):
    """
    Calculate the FFT of a 1D dimensionnal array
    corresponding to the FT by using numpy fft
    input:
    ------
      t, 1D numpy array containing the time variable
      w, 1D numpy array containing the frequency variable from fft_freq
      F, 1D numpy array containing the data to inverse Fourier transform
    output:
    -------
      F, 1D numpy array containing the iFT
    """
    
    tmin = t[0]
    wmin = w[0]
    dt = t[1] - t[0]
    dw = w[1] - w[0]
    N = t.size

    param = dw*dt*N/2/np.pi
    if abs(param-1)>1e-10 : 
      print('cannot use fft, param != 1')
      print('param = ', param)
      print('dw = ', dw)
      print('dt = ', dt)
      print('N = ', N)
      sys.exit()

    f = np.zeros(F.shape, dtype = complex)
    F_new = np.zeros(F.shape, dtype=complex)
   
    if len(F.shape)==1:
      F_new = self.array_mult_vec(dw, F, np.exp(complex(0, 1)*tmin*(w-wmin)))
      f = N*np.fft.ifft(F_new)
      f = self.array_mult_vec(f, np.exp(complex(0, 1)*(wmin*tmin + wmin*(t-tmin)))/(2*np.pi), 1.0)
    elif len(F.shape) == 2:
      for i in range(F.shape[1]):
        F_new[:, i] = self.array_mult_vec(dw, F[:, i], np.exp(complex(0, 1)*tmin*(w-wmin)))
        f[:, i] = N*np.fft.ifft(F_new[:, i])
        f[:, i] = self.array_mult_vec(f[:, i], np.exp(complex(0, 1)*(wmin*tmin + wmin*(t-tmin)))/(2*np.pi), 1.0)
    else:
      raise ValueError('Can only perform iFFt of 1D or 2D array\n \
          please, use transform function!!\n \
          Exit!!')

    return f

  #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  def transform_NDto2D(A, dtype = float):
    """
    Transform a ND array in a 2D array
    Input parameters:
    -----------------
      A (ND numpy array): array to transform
      dtype (type): type of array
    Output parameters:
    ------------------
      param (2D numpy array): the resulting array
    """
    if len(A.shape) < 3:
      raise ValueError('A should be of dimension >3!!\nExit!!')
    
    prod_shape=1
    i = 1
    while i<len(A.shape):
      prod_shape = prod_shape*A.shape[i]
      i = i + 1

    param = np.zeros((A.shape[0], prod_shape), dtype=dtype)
    if len(A.shape) == 3:
      h = 0
      for i in range(A.shape[1]):
        for j in range(A.shape[2]):
          param[:, h] = A[:, i, j]
          h = h + 1
    elif len(A.shape) == 4:
      h = 0
      for i in range(A.shape[1]):
        for j in range(A.shape[2]):
          for k in range(A.shape[3]):
            param[:, h] = A[:, i, j, k]
            h = h + 1
   
    elif len(A.shape) == 5:
      h = 0
      for i in range(A.shape[1]):
        for j in range(A.shape[2]):
          for k in range(A.shape[3]):
            for l in range(A.shape[4]):
              param[:, h] = A[:, i, j, k, l]
              h = h + 1

    else:
      raise ValueError('Dimension Larger than 5 not implemented, Im lazy, I know!!!')

    return param
   
