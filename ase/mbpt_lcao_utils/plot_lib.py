"""
  Python routines for plotting purpose used with the mbpt_lcao program
  http://mbpt-domiprod.wikidot.com/

  Writen by Marc Barbry (marc.barbry@mailoo.org)
  in the frame of his PHD at the University of the Basque Country 
  Centro de Fisica de Materiales (CFM)
  under the Supervision of Peter Koval and Daniel Sanchez-Portal

  This version need ASE
  https://wiki.fysik.dtu.dk/ase/index.html
"""

from __future__ import division
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.patches import FancyArrowPatch
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
from decimal import Decimal
import ase.units as units
import numpy as np
from ase.mbpt_lcao_utils.utils import interpolate, Array_ind_to_pts_coord, pts_coord_to_Array_ind
import os

from ase.mbpt_lcao_utils.FFT_routines import mod_FFT_1D, mod_iFFT_1D


#############################################
#                                           #
#       Class of the plot_lib library       #
#                                           #
#############################################
"""
  class ImageFollower:Use to create a commun color bar
          in matplotlib subplot
  class recup_array: old class not use anymore but keep
          load array from a txt file
  class select_surface: class use to select a surface of an 3D array
          use use read_data
"""

      ###################
      #   ImageFollower #
      ###################
class ImageFollower:
  'update image in response to changes in clim or cmap on another image'
  def __init__(self, follower):
    self.follower = follower
  def __call__(self, leader):
    self.follower.set_cmap(leader.get_cmap())
    self.follower.set_clim(leader.get_clim())


        ###################
        #   recup_array   #
        ###################
class recup_array:
  """
  """
  def __init__(self, fname):
    from Read_data import create_list
    self.fname = fname
    fichier = os.listdir(os.getcwd())
    length = len(fichier)

    compt = 0
    for i in range(length):
      if fichier[i][0:len(fname)]==fname:
        compt=compt+1

    if fname == 'density':
      compt = compt -2
    L = create_list(compt, fname)

    a = list()
    for i in range(len(L)):
      a.append(np.loadtxt(L[i]))

    dim = np.shape(a[0])
    self.data = np.zeros((dim[0], dim[1], len(a)), dtype=float)

    for i in range(len(a)):
      self.data[:, :, i] = a[i]



        #######################
        #   select_surface    #
        #######################
class select_surface:
  """
  Select a particulary surface in all the data from read_data
  input:
    data: class read_data
    coord_pts: list contenning the coordinnate of the 2 point limiting the plan (square or rectangle)
              [r0, r2] with ri = np.array([xi, yi, zi])
  Output:
    self.Array: 2D array containing the wished data
  """
  def __init__(self, data, coord_pts):

    self.data = data
    self.coord_pts = coord_pts #in Bohr!!!
    self.indices = []

    for i in self.coord_pts:
      self.indices.append(pts_coord_to_Array_ind(self.data, i))

    if self.coord_pts[0][0] == self.coord_pts[1][0]:
      self.plan = 'x'
    elif self.coord_pts[0][1] == self.coord_pts[1][1]:
      self.plan = 'y'
    elif self.coord_pts[0][2] == self.coord_pts[1][2]:
      self.plan = 'z'
    else:

      print('The coordinate of the limitating point of the plan are ills define')
      print('One of the coordinate should be the same for all the points:')
      for i, j in enumerate(self.coord_pts):
        print('point {0}: x = {1}, y = {2}, z = {3}'.format(i, j[0], j[1], j[2]))
      raise ValueError("Error in select_surface!!")

    if self.plan == 'x':
      self.Array = self.data.Array[self.indices[0][0], self.indices[0][1]:self.indices[1][1], self.indices[0][2]:self.indices[1][2]]
    elif self.plan == 'y':
      self.Array = self.data.Array[self.indices[0][0]:self.indices[1][0], self.indices[1][0], self.indices[0][2]:self.indices[1][2]]
    elif self.plan == 'z':
      self.Array = self.data.Array[self.indices[0][0]:self.indices[1][0], self.indices[0][1]:self.indices[1][1], self.indices[0][2]]



        #######################
        #   	plot_fig	  #
        #######################
class plot_fig:
  """
  Class that plot imshow subplot graphic type in an efficient way with colorbars.
  Use to plot most of the Figure of the article:
    Atomistic Near-Field Nanoplasmonics: Reaching Atomic-scale Resolution in Nanooptics,
      M. Barbry et al., Nanoletters, http://pubs.acs.org/doi/abs/10.1021/acs.nanolett.5b00759
  Input parameters:
  -----------------
    fnames: (list of string) list of the names of the data files (.dat, .npy or .hdf5) 
                            from the program tddft. Must contains at least one file.
    folders: (list of string) list of the folders where are localize the data files,
                              use to load the box dimesion if .dat ot .npy input files.
    args: (class Parameter) the input parameter to plot, see help(Parameter) for more details
    prop: (class Properties_figure) related to the properties of the output figure, see 
                                    help(Properties_figure) for more details
  Output parameters:
  ------------------
    None, only the graphics

  Plotting Parameters:
  --------------------
    self.figsize (tuple, default:(30, 18)) the figure size
    self.y_pl (float, default:0.0) the plan coordinate where to plot
    self.ft (float default: 35) fontsize
    self.ft_title (float default: 40) title fontsize
    self.ft_title (float default: 40) text fontsize
    self.plan (string default: 'x') the plan to plot, must be x, y, or z
    self.nrows (int default: 3) number of rows
    self.ncols (int default: 3) number of columns
    self.title (dictionnary default: {}) key: int->graph number, value: list->[x coordinate, y coordinate, title text]
    self.text (dictionnary default: {}) key: int->graph number, value: list->[x coordinate, y coordinate, title text]
    self.color_text (string, default: 'black') color of the text
    self.numerotation (dictionnary default: {}) key: int->graph number, value: list->[x coordinate, y coordinate, title text]
    self.axis_tick (list of booleen, default: []) one element for each subplot, every element contains a list of two
                                                  boleen, one for the x axis, one for the y axis.
    self.label_show (list of booleen, default: []) one element for each subplot, every element contains a list of two
                                                    boleen, one for the x axis, one for the y axis.
    self.xlabel (string, default: 'x') label of the x axis
    self.ylabel (string, default: 'y') label of the y axis
    self.data (dictionnary of dictionnary, default: {}) contains the information about the data
                                    key: number of the grap
                                    value: dictionnaty with the keys are:
                                      graph_type, data_path, data_folder, xlabel, ylabel
                                        where graph_type can be:
                                        tddft (data from the tddft program)
                                        imshow
                                        pcolormesh
                                        plot
    self.radius_fname (dictionnary, default:{})  key: int->graph number, value: list->[data_path, data_folder]
                                                  use only if data_type = pcolormesh
    self.z_fname (dictionnary, default:{})  key: int->graph number, value: list->[data_path, data_folder]
                                                  use only if data_type = pcolormesh
    self.arrow (dictionnary, default:{}) key: int->graph number, value: list-> {}
                                                key-> ax.arrow parameter, values: the values that you wish for the corresponding parameters
    self.arrow_label (dictionnary, default:{}) key: int->graph number, value: list-> {}
                                                key-> pyplot.text parameter, values: the values that you wish for the corresponding parameters
    
    #colorbar
    self.pos_cb (list of list 4 float, default: []) one for each graphique, every element is compose by [x, y, w, h] with
                                                      x: x coordinate of the colorbar
                                                      y: y coordinate of the colorbar
                                                      w: width of the colorbar
                                                      h: height of the colorbar
                                                      see fig.add_axes (http://matplotlib.org/api/figure_api.html)
    self.title_cb (dictionnary, default: {}) key: int->graph number, value: list->[x coordinate, y coordinate, title cb text]
    
    self.wspace (float, default: 0.25) the horizontal space between the subplot
    self.hspace (float, default: 0.05) the vertical space between the subplot
    self.right  (float, default: 0.9) the right space in the figure

    self.cmap (matplotlib colormap, default: cm.jet) the colormap to use
  """
  def __init__ (self, fnames, folders, r):
    from matplotlib import matplotlib_fname, cm
    
    self.fnames = fnames
    self.folders = folders
    
    self.r = r
    self.args = r.args
    self.prop = r.prop

    #parameter initialization
    self.figsize=None
    self.y_pl = 0.0
    self.ft=35
    self.ft_title=40
    self.ft_text=40
    self.ft_cb_title = 20
    self.ft_cb_tick = 20
    self.plan = 'x'
    self.nrows = 3
    self.ncols = 3
    self.title = {}
    self.text = {}
    self.color_text = 'black'
    self.numerotation = {}
    self.axis_tick = []
    self.label_show = []
    self.xlabel = 'x'
    self.ylabel = 'y'
    self.data = {}
    self.radius_fname = {}
    self.z_fname = {} 
    self.arrow = {}
    self.arrow_label = {}
    self.axis = {}
    self.ext = []
    self.aspect = {}
    self.color_numerotation = 'black'
    self.plot_atoms = False
    self.color_plot = ['red']
    self.linestyle = ['_']
    self.linewidth = [3]
    self.set_ticksx = 0
    self.set_ticksy = 0
    self.intensity_max = []
    self.times = [] #list of the different time that you wish to plot
    self.ftime = '' #name of the file for time plotting
    
    #colorbar
    self.pos_cb = []
    self.title_cb = {}
    self.cb_precision = 1
    self.cb_scientifique = False

    
    self.wspace = 0.25
    self.hspace=0.05
    self.right=0.9
    self.nb_graph = self.nrows*self.ncols

    self.cmap = cm.jet

    #TEM
    self.elec_dir = 'x'


    #check configuration file
    print('####################################')
    print('#                                  #')
    print('#        plot_fig programs         #')
    print('#                                  #')
    print('####################################')
    print(' ')
    print('Plot high quality imshow graphics subplot in an efficient way')
    print('Configuration file path:', matplotlib_fname())
    
  def load_data(self):
    self.dat = []
    for i in enumerate(self.fnames):
      self.args.folder = self.folders[i[0]]
      self.dat.append(self.r.Read(YFname=self.data[i]['fname']))
      

  def plot_graph(self):
    import matplotlib.pyplot as plt

    #fig, axes = plt.subplots(nrows=self.nrows, ncols=self.ncols, figsize=self.figsize)
    if self.figsize is None:
      fig = plt.figure(1)
    else:
      fig = plt.figure(1, figsize=self.figsize)

    if self.args.time == 1: #time-plot
      if len(self.times) != self.nb_graph:
        raise ValueError('the number of time and the number of graph are differents!!!')
      elif len(self.times) != len(self.data):
        raise ValueError('the number of time and the number of data are differents!!!')
      for i in range(self.nb_graph):
        if self.data[i]['graph_type'] != 'tddft_tem' and self.data[i]['graph_type'] != 'tddft_iter':
          print(self.data[i]['graph_type'])
          raise ValueError('graph type must be tddft_tem or tddft_iter if time activate!!!')
      else:

        #data = read_data()
        data = self.r.Read(YFname=self.data[i]['fname'])
        x_min = data.box[0][0]*units.Bohr
        y_min = data.box[0][1]*units.Bohr
        z_min = data.box[0][2]*units.Bohr
        x_max = data.box[1][0]*units.Bohr
        y_max = data.box[1][1]*units.Bohr
        z_max = data.box[1][2]*units.Bohr

        R0, tmin = detemine_R0(self.args)
        rp = electron_trajectory(data.t, R0, self.args.tem_input['v'], tmin)

        for i, time in enumerate(self.times):
          ax = fig.add_subplot(self.nrows, self.ncols, i+1)
          self.args.folder = self.data[i]['folder']

          if self.plan == 'x':
            num = bohrtoplnum(data, 0.0, 'x')
            y = data.Array[time, :, :, num].T[:, ::-1]
            ext=[y_min, y_max, z_min, z_max]
          elif self.plan == 'y':
            num = bohrtoplnum(data, 0.0, 'y')
            y = data.Array[time, :, num, :].T[:, :]
            ext=[x_min, x_max, z_min, z_max]
          elif self.plan == 'z':
            num = bohrtoplnum(data, 0.0, 'z')
            y = data.Array[time, num, :, :].T[:, ::-1]
            ext=[x_min, x_max, y_min, y_max]
          else:
            raise ValueError('ERROR!! plan must be: x, y or z!!!')

        
          if self.args.quantity == 'intensity':
            y = np.sqrt(y)

          self.intensity_max.append(np.max(y))
          print('Max(data) = ', np.max(y))
          y = y.transpose()
          vmin =  np.amin(y)
          vmax =  np.amax(y)

          #if len(self.axis) == 0:
          #self.axis[i] = ext

          im = ax.imshow(y, extent=ext, cmap=self.cmap, aspect=self.aspect[i])
    
          if self.data[i]['graph_type'] == 'tddft_tem':
            if self.plan == 'x':
              ax.plot(rp[time, 1]*units.Bohr, rp[time, 2]*units.Bohr, 'o', color='black', markersize=10)
            elif self.plan == 'y':
              ax.plot(rp[time, 0]*units.Bohr, rp[time, 2]*units.Bohr, 'o', color='black', markersize=10)
            elif self.plan == 'z':
              ax.plot(rp[time, 0]*units.Bohr, rp[time, 1]*units.Bohr, 'o', color='black', markersize=10)

          #COLOR BAR
          tick = np.arange(vmin, vmax, (vmax-vmin)/5)
          tick_list = []
          tick_l1 = []

          for p in tick:
            #tick_list.append(round(p))#, self.cb_precision))
            if self.cb_scientifique:
              tick_list.append('%.{0}e'.format(self.cb_precision) % p)#, self.cb_precision))
            else:
              tick_list.append('%.{0}f'.format(self.cb_precision) % p)#, self.cb_precision))
            tick_l1.append(p)
          
          cax = fig.add_axes(self.pos_cb[i])
          cbar = fig.colorbar(im, cax=cax, ticks = tick_l1, orientation='vertical')
          cbar.ax.set_yticklabels(tick_list, fontsize = self.ft_cb_tick)

          for key, value in self.title_cb.items():
            if key == i:
              cbar.ax.set_title(value[2], rotation='horizontal', position= (value[0],\
                                value[1]), fontsize=self.ft_cb_title)


          if self.plot_atoms:
            from ase.data.colors import cpk_colors
            if self.plan == 'x':
              r = 2*abs(data.box[1][0]-data.box[0][0])/100
              for pos, Z in zip(data.atoms.positions, data.atoms.numbers):
                ax.add_patch(Circle((pos[1], pos[2]), r, fc=tuple(cpk_colors[Z]), alpha=0.6))
            elif self.plan == 'y':
              r = 2*abs(data.box[1][1]-data.box[0][1])/100
              for pos, Z in zip(data.atoms.positions, data.atoms.numbers):
                ax.add_patch(Circle((pos[0], pos[2]), r, fc=tuple(cpk_colors[Z]), alpha=0.6))
            elif self.plan == 'z':
              r = 2*abs(data.box[1][2]-data.box[0][2])/100
              for pos, Z in zip(data.atoms.positions, data.atoms.numbers):
                ax.add_patch(Circle((pos[0], pos[1]), r, fc=tuple(cpk_colors[Z]), alpha=0.6))
            else:
              raise ValueError('plan can be only x, y, or z')

          if self.data[i]['axis'] is not None:
            ax.axis(self.data[i]['axis'])
          if self.label_show[i][0]:
            ax.set_xlabel(self.data[i]['xlabel'], fontsize=self.ft)
          if self.label_show[i][1]:
            ax.set_ylabel(self.data[i]['ylabel'], fontsize=self.ft)

          for tick_ax in ax.xaxis.get_major_ticks():
            tick_ax.label1On = self.axis_tick[i][0]
            tick_ax.label1.set_fontweight('bold')
        
          for tick_ax in ax.yaxis.get_major_ticks():
            tick_ax.label1On = self.axis_tick[i][1]
            tick_ax.label1.set_fontweight('bold')

          if self.set_ticksx != 0:
            ax.xaxis.set_ticks(self.set_ticksx)
          if self.set_ticksy != 0:
            ax.yaxis.set_ticks(self.set_ticksy)
          #ax.yaxis.set_ticks([-30, -15, 0, 15, 30])

          for key, value in self.title.items():
            if key == i:
              print(value)
              ax.text(value[0], value[1], value[2], fontsize = self.ft_title, color='black')
           
          for key, value in self.arrow.items():
            if key == i:
              ax.arrow(value['x'], value['y'], value['xshift'], value['yshift'], head_width=value['head_width'],\
                        width=value['width'], head_length=value['head_length'], fc=value['fc'], ec=value['ec'])
              ax.text(self.arrow_label[i]['x'], self.arrow_label[i]['y'], self.arrow_label[i]['text'],\
                  color=self.arrow_label[i]['color'], fontsize = self.arrow_label[i]['fontsize'], fontweight=self.arrow_label[i]['fontweight'])
              
          if len(self.numerotation) >0:
            ax.text(self.numerotation[i][0], self.numerotation[i][1], self.numerotation[i][2], fontsize = self.ft, color=self.color_numerotation)
          if len(self.text) >0:
            for value in self.text[i]:
              if len(value) == 3:
                ax.text(value[0], value[1], value[2], fontsize = self.ft_text, color=self.color_text)
              elif len(value) == 4:
                ax.text(value[0], value[1], value[2], fontsize = value[3], color=self.color_text)
              else:
                raise ValueError('text must be 3 or 4 elements')
          
        fig.subplots_adjust(wspace = self.wspace, hspace=self.hspace, right=self.right)
        fig.savefig(self.prop.figname, format=self.prop.format_output)
        if self.prop.show==1:
          plt.show()



    else: #non-time plot

      i = 1
      for i in range(self.nb_graph):
        print(self.data[i]['fname'])

        if self.data[i]['graph_type'] == 'tddft_iter' or self.data[i]['graph_type'] == 'tddft_tem':
          if (self.data[i]['plot_freq'] is not None):
            self.r.args.plot_freq = self.data[i]['plot_freq']

          ax = fig.add_subplot(self.nrows, self.ncols, i+1)
          self.args.folder = self.data[i]['folder']

          data = self.r.Read(YFname=self.data[i]['fname'])
          
          if self.data[i]['plan'] is not None:
            y, ext, box = select_plan(self.data[i]['plan'], self.r.prop.plan_coord, data)
          else:
            y, ext, box = select_plan(self.plan, self.r.prop.plan_coord, data)

        
          if self.args.quantity == 'intensity':
            y = np.sqrt(y)

          self.intensity_max.append(np.max(y))
          print('Max(data) = ', np.max(y))
          y = y.transpose()
          vmax =  np.amax(abs(y))
          vmin =  0.0#-vmax

          #if len(self.axis) == 0:
          #self.axis[i] = ext

          im = ax.imshow(y, extent=ext, cmap=self.cmap, aspect=self.aspect[i], vmin =vmin, vmax=vmax)
          if self.data[i]['save_name'] is not None:
            np.savetxt(self.data[i]['save_name'], y)
    
          if self.data[i]['graph_type'] == 'tddft_tem':
            if 'tem_impact_param' in self.data[i].keys():
              if self.elec_dir == self.plan:
                x = np.arange(box[0], box[1], 0.1)
                ax.plot(self.data[i]['tem_impact_param'][0]*units.Bohr, self.data[i]['tem_impact_param'][1]*units.Bohr, 'o', color='black', markersize=10)
              elif self.elec_dir == 'x':
                x = np.arange(box[0], box[1], 0.1)
                if self.plan == 'y':
                  ax.plot(x + self.data[i]['tem_impact_param'][0]*units.Bohr, 
                            x*0 + self.data[i]['tem_impact_param'][1]*units.Bohr, '--', color='black', linewidth=3)
                elif self.plan == 'z':
                  ax.plot(x + self.data[i]['tem_impact_param'][0]*units.Bohr, 
                            x*0 + self.data[i]['tem_impact_param'][2]*units.Bohr, '--', color='black', linewidth=3)
              elif self.elec_dir == 'y':
                x = np.arange(box[2], box[3], 0.1)
                if self.plan == 'x':
                  ax.plot(x*0 + self.data[i]['tem_impact_param'][0]*units.Bohr, 
                            x, '--', color='black', linewidth=3)
                elif self.plan == 'z':
                  ax.plot(x, x*0 + self.data[i]['tem_impact_param'][0]*units.Bohr, '--', color='black', linewidth=3)
              elif self.elec_dir == 'z':
                if self.plan == 'x':
                  x = np.arange(box[0], box[1], 0.1)
                  ax.plot(x, x*0 + self.data[i]['tem_impact_param'][2]*units.Bohr, '--', color='black', linewidth=3)
                elif self.plan == 'y':
                  x = np.arange(box[2], box[3], 0.1)
                  ax.plot(x*0 + self.data[i]['tem_impact_param'][0]*units.Bohr, 
                            x, '--', color='black', linewidth=3)


          #COLOR BAR
          tick = np.arange(vmin, vmax, (vmax-vmin)/5)
          tick_list = []
          tick_l1 = []

          for p in tick:
            #tick_list.append(round(p))#, self.cb_precision))
            if self.cb_scientifique:
              tick_list.append('%.{0}e'.format(self.cb_precision) % p)#, self.cb_precision))
            else:
              tick_list.append('%.{0}f'.format(self.cb_precision) % p)#, self.cb_precision))
            tick_l1.append(p)
          
          cax = fig.add_axes(self.pos_cb[i])
          cbar = fig.colorbar(im, cax=cax, ticks = tick_l1, orientation='vertical')
          cbar.ax.set_yticklabels(tick_list, fontsize = self.ft_cb_tick)

          for key, value in self.title_cb.items():
            if key == i:
              cbar.ax.set_title(value[2], rotation='horizontal', position= (value[0],\
                                value[1]), fontsize=self.ft_cb_title)


          if self.plot_atoms:
            from ase.data.colors import cpk_colors
            if self.plan == 'x':
              r = abs(data.box[1][0]-data.box[0][0])/100
              for pos, Z in zip(data.atoms.positions, data.atoms.numbers):
                ax.add_patch(Circle((pos[1], pos[2]), r, fc=tuple(cpk_colors[Z]), alpha=0.6))
            elif self.plan == 'y':
              r = abs(data.box[1][1]-data.box[0][1])/100
              for pos, Z in zip(data.atoms.positions, data.atoms.numbers):
                ax.add_patch(Circle((pos[0], pos[2]), r, fc=tuple(cpk_colors[Z]), alpha=0.6))
            elif self.plan == 'z':
              r = abs(data.box[1][2]-data.box[0][2])/100
              for pos, Z in zip(data.atoms.positions, data.atoms.numbers):
                ax.add_patch(Circle((pos[0], pos[1]), r, fc=tuple(cpk_colors[Z]), alpha=0.6))
            else:
              raise ValueError('plan can be only x, y, or z')


        elif self.data[i]['graph_type'] == 'pcolormesh':
          ax = fig.add_subplot(self.nrows, self.ncols, i+1)
          r = np.load(self.radius_fname[i])
          z = np.load(self.z_fname[i])
          y = np.load(self.data[i]['fname'])

          ext = [z[0], z[z.shape[0]-1], r[0], r[r.shape[0]-1]]
          #if len(self.axis) == 0:
          #self.axis[i] = ext

          #if i == 2:
          #  y = 10*y/np.max(y)

          vmin =  np.amin(y)
          vmax =  np.amax(y)
          im = ax.pcolormesh(z, r, y, vmin=vmin, vmax=vmax, rasterized=True)
          #COLOR BAR
          tick = np.arange(vmin, vmax, (vmax-vmin)/5)
          tick_list = []
          tick_l1 = []

          for p in tick:
            tick_list.append(round(p, self.cb_precision))
            tick_l1.append(p)
          
          cax = fig.add_axes(self.pos_cb[i])
          cbar = fig.colorbar(im, cax=cax, ticks = tick_l1, orientation='vertical')
          cbar.ax.set_yticklabels(tick_list, fontsize = self.ft_cb_tick)

          for key, value in self.title_cb.items():
            if key == i:
              cbar.ax.set_title(value[2], rotation='horizontal', position= (value[0],\
                                value[1]), fontsize=self.ft_cb_title)


        elif self.data[i]['graph_type'] == 'imshow':
          ax = fig.add_subplot(self.nrows, self.ncols, i+1)
          if self.data[i]['fname'][len(self.data[i]['fname'])-3:len(self.data[i]['fname'])] =='npy':
            y = np.load(self.data[i]['fname'])
          else:
            y = np.loadtxt(self.data[i]['fname'])


          vmin =  np.amin(y)
          vmax =  np.amax(y)
          im = ax.imshow(y, extent = self.ext, vmin=vmin, vmax=vmax, aspect=self.aspect[i], cmap=self.cmap)
          #COLOR BAR
          tick = np.arange(vmin, vmax, (vmax-vmin)/5)
          tick_list = []
          tick_l1 = []

          for p in tick:
            tick_list.append(round(p, self.cb_precision))
            tick_l1.append(p)
          
          cax = fig.add_axes(self.pos_cb[i])
          cbar = fig.colorbar(im, cax=cax, ticks = tick_l1, orientation='vertical')
          cbar.ax.set_yticklabels(tick_list, fontsize = self.ft_cb_tick)

          for key, value in self.title_cb.items():
            if key == i:
              cbar.ax.set_title(value[2], rotation='horizontal', position= (value[0],\
                                value[1]), fontsize=self.ft_cb_title)


        elif self.data[i]['graph_type'] == 'plot':
          ax = fig.add_subplot(self.nrows, self.ncols, i+1)
          if len(self.data[i]['fname'])>1:
            plot_data = self.data[i]['fname']
            x = []
            y = []

            for k in range(len(plot_data)):
              if plot_data[len(self.data[i]['fname'])-3:len(self.data[i]['fname'])] =='npy':
                x = np.load(plot_data[k])[:, 0]
                y = np.load(plot_data[k])[:, 1]
              else:
                x = np.loadtxt(plot_data[k])[:, 0]
                y = np.loadtxt(plot_data[k])[:, 1]

              ax.plot(x, y, self.linestyle[k], color=self.color_plot[k],  linewidth=self.linewidth[k])
              x = 0
              y = 0

          else:
            if self.data[i]['fname'][len(self.data[i]['fname'])-3:len(self.data[i]['fname'])] =='npy':
              x = np.load(self.data[i]['fname'])[:, 0]
              y = np.load(self.data[i]['fname'])[:, 1]
            else:
              x = np.load(self.data[i]['fname'])[:, 0]
              y = np.load(self.data[i]['fname'])[:, 1]

            ax.plot(x, y, linestyle=self.linestyle[0], color=self.color_plot[0], linewidth=self.linewidth[0])
            #im = ax.plot(x, y)
        
        elif self.data[i]['graph_type'] == 'plot_3D':
          ax = fig.add_subplot(self.nrows, self.ncols, i+1, projection='3d')
          if self.data[i]['fname'][len(self.data[i]['fname'])-3:len(self.data[i]['fname'])] =='npy':
            Z = np.load(self.data[i]['fname'])
          else:
            Z = np.loadtxt(self.data[i]['fname'])

          X, Y = np.meshgrid(self.data[i]['x_3d'], self.data[i]['y_3d'][::-1])


          ax.plot_surface(X, Y, Z, rstride=1, cstride=1, linewidth=0, antialiased=False, cmap=self.cmap)
          #im = ax.imshow(y, extent = self.ext, vmin=vmin, vmax=vmax, aspect=self.aspect[i])
          ax.set_zlabel(self.data[i]['zlabel'], fontsize=self.ft)
          #COLOR BAR

        elif self.data[i]['graph_type'] is None:
          print('None')
   
        else:
          raise ValueError('ERROR!! Other plot type not yet implemented,\n \
              Can be only: tddft_iter, tddft_tem, plot, imshow, pcolormesh, plot_3D.\n \
              Exit')



        if self.data[i]['axis'] is not None:
          ax.axis(self.data[i]['axis'])
        if self.label_show[i][0]:
          ax.set_xlabel(self.data[i]['xlabel'], fontsize=self.ft)
        if self.label_show[i][1]:
          ax.set_ylabel(self.data[i]['ylabel'], fontsize=self.ft)

        for tick_ax in ax.xaxis.get_major_ticks():
          tick_ax.label1On = self.axis_tick[i][0]
          tick_ax.label1.set_fontweight('bold')
      
        for tick_ax in ax.yaxis.get_major_ticks():
          tick_ax.label1On = self.axis_tick[i][1]
          tick_ax.label1.set_fontweight('bold')

        if self.set_ticksx != 0:
          ax.xaxis.set_ticks(self.set_ticksx)
        if self.set_ticksy != 0:
          ax.yaxis.set_ticks(self.set_ticksy)
        #ax.yaxis.set_ticks([-30, -15, 0, 15, 30])

        for key, value in self.title.items():
          if key == i:
            ax.text(value[0], value[1], value[2], fontsize = self.ft_title, color='black')
         
        for key, value in self.arrow.items():
          if key == i:
            ax.arrow(value['x'], value['y'], value['xshift'], value['yshift'], head_width=value['head_width'],\
                      width=value['width'], head_length=value['head_length'], fc=value['fc'], ec=value['ec'])
            ax.text(self.arrow_label[i]['x'], self.arrow_label[i]['y'], self.arrow_label[i]['text'],\
                color=self.arrow_label[i]['color'], fontsize = self.arrow_label[i]['fontsize'], fontweight=self.arrow_label[i]['fontweight'])
            
        if len(self.numerotation) >0:
          ax.text(self.numerotation[i][0], self.numerotation[i][1], self.numerotation[i][2], fontsize = self.ft, color=self.color_numerotation)
        if len(self.text) >0:
          for value in self.text[i]:
            if len(value) == 3:
              ax.text(value[0], value[1], value[2], fontsize = self.ft_text, color=self.color_text)
            elif len(value) == 4:
              ax.text(value[0], value[1], value[2], fontsize = value[3], color=self.color_text)
            else:
              raise ValueError('text must be 3 or 4 elements')
        

        #if i == 0:
        #  #plt.annotate('',xy=(0.07, 0.93),xycoords='figure fraction',
        #  #                 xytext=(0.07,0.1), textcoords='figure fraction',
        #  #                              arrowprops=dict(frac=0.02, headwidth=14., width=6., color='black'))
        #  lw = 5
        #  l1 = matplotlib.lines.Line2D([0.06, 0.08], [0.17, 0.17], transform=fig.transFigure, figure=fig, color='black', linewidth=lw)
        #  l2 = matplotlib.lines.Line2D([0.06, 0.08], [0.33, 0.33], transform=fig.transFigure, figure=fig, color='black', linewidth=lw)
        #  l3 = matplotlib.lines.Line2D([0.06, 0.08], [0.50, 0.50], transform=fig.transFigure, figure=fig, color='black', linewidth=lw)
        #  l4 = matplotlib.lines.Line2D([0.06, 0.08], [0.66, 0.66], transform=fig.transFigure, figure=fig, color='black', linewidth=lw)
        #  l5 = matplotlib.lines.Line2D([0.06, 0.08], [0.82, 0.82], transform=fig.transFigure, figure=fig, color='black', linewidth=lw)
#
          #ft = 60
          #plt.text(-170, -5, '1', fontsize=ft)
          #plt.text(-170, 40, '3', fontsize=ft)
          #plt.text(-170, 87, '6', fontsize=ft)
          #plt.text(-175, 132, '10', fontsize=ft)
          #plt.text(-175, 177, '20', fontsize=ft)
          #plt.text(-175, 215, r'$\mathbf{d_{sep}}$ (\AA)', fontsize=ft)
          #fig.lines.extend([l1, l2, l3, l4, l5])
          #fig.canvas.draw()


#    lw = 3
#    l1 = matplotlib.lines.Line2D([0.57, 0.555], [0.52, 0.52], transform=fig.transFigure, figure=fig, color='green', linewidth=lw)
#    l2 = matplotlib.lines.Line2D([0.585, 0.57], [0.52, 0.52], transform=fig.transFigure, figure=fig, color='blue', linewidth=lw)
#    l4 = matplotlib.lines.Line2D([0.61, 0.585], [0.52, 0.52], transform=fig.transFigure, figure=fig, color='red', linewidth=lw)
#
#    plt.text(-16.5, 1.09, 'C', fontsize=self.ft-10, color='red')
#    plt.text(-15.8, 1.09, 'B', fontsize=self.ft-10, color='red')
#    plt.text(-14.7, 1.09, 'A', fontsize=self.ft-10, color='red')
#    
#    l5 = matplotlib.lines.Line2D([0.14, 0.125], [0.05, 0.05], transform=fig.transFigure, figure=fig, color='green', linewidth=lw)
#    l6 = matplotlib.lines.Line2D([0.155, 0.14], [0.05, 0.05], transform=fig.transFigure, figure=fig, color='blue', linewidth=lw)
#    l8 = matplotlib.lines.Line2D([0.18, 0.155], [0.05, 0.05], transform=fig.transFigure, figure=fig, color='red', linewidth=lw)
#    
#    plt.text(-38, -0.38, 'C', fontsize=self.ft-10, color='red')
#    plt.text(-37.4, -0.38, 'B', fontsize=self.ft-10, color='red')
#    plt.text(-36.3, -0.38, 'A', fontsize=self.ft-10, color='red')
#    
#    l9 = matplotlib.lines.Line2D([0.14, 0.125], [0.52, 0.52], transform=fig.transFigure, figure=fig, color='blue', linewidth=lw)
#    l12 = matplotlib.lines.Line2D([0.18, 0.14], [0.52, 0.52], transform=fig.transFigure, figure=fig, color='red',linewidth=lw)
#
#    plt.text(-38, 1.09, 'B', fontsize=self.ft-10, color='red')
#    plt.text(-36.7, 1.09, 'A', fontsize=self.ft-10, color='red')
# 
#    
#    fig.lines.extend([l1, l2, l4, l5, l6, l8, l9, l12])
#    fig.canvas.draw()

      #plt.tight_layout()
      fig.subplots_adjust(wspace = self.wspace, hspace=self.hspace, right=self.right)
      fig.savefig(self.r.prop.figname, format=self.r.prop.format_output)
#      if self.r.prop.show==1:
#        plt.show()

###################################################
#                                                 #
#         Functions of the library                #
#                                                 #
###################################################
"""
Functions Related to the plotting part:
  Matplotlib:
    mbpt_lcao_plot
    plot_xplane
    plot_yplane
    plot_zplane
    plot_imshow
    plot_profile
    draw_circle_axe
    draw_arrow
    plot_polarizability
    plot_EELS_spectrum
    bohrtoplnum
    Array_ind_to_pts_coord
    pts_coord_to_Array_ind
    plot_E_field_2D
    save_prof
    plot_complex

  Mayavi:
    plot_E_field
    change_scale
    draw_atom
    plot_maya
    anim
    save_anim
    create_video
    contour
"""



        ##########################################
        #     Matplotlib Plotting Functions      #
        ##########################################

#
#
#
def mbpt_lcao_plot(r, fname=None, figi=None, ax=None):
  """
  Root function of the plotting part, call from the graphical interface, or 
  yhe mbpt_lcao_plot.py script.
  Call read_data to recover the data from the TDDFT program
  Input parameters:
  -----------------
    r (Class read_mbpt_lcao_output from Read_data), define the input parameter
          to load with read_data
    fname (string) : name of the input file, only for EELS spectra
  """

  msg="""
         ##########################################
         #            mbpt_lcao_plot:             #
         #    python routine to plot tddft data   #
         ##########################################
      """
  print_verbose(msg, r.args.verbose)

  create_folder(r.prop.folder, verb = r.args.verbose)

  r.data = r.Read(YFname=fname)
  fig, ax1 = get_axis(figi, ax, r.prop, r.args.quantity)

  if r.prop.cmap is None:
    if r.prop.plot == "Mayavi":
      r.prop.cmap = 'jet'
    else:
      r.prop.cmap = cm.jet

  if r.args.quantity == 'Efield':
    plot_E_field(r.args, r.prop, ax1, r.data)
  elif r.args.quantity == 'polarizability':
    plot_polarizability(r.args, r.prop, fig, r.data, kind=r.prop.fit_kind)
  elif r.args.quantity in ['spectrum', 'virt']:
    plot_EELS_spectrum(r.args, r.prop, fig, ax1, r.data)
  elif r.args.quantity in ['e2map', 'i2map', 'nm2map']:
    plot_maps(r.args, r.prop, fig, ax1, r.data)
  elif r.args.quantity == 'E_loss':
    plot_E_loss(r.args, r.prop, ax1, r.data)
  #elif args.movie == 1:
  #  make_movie(args, prop, fname=fname)
  else:
    plot_choice(r.args, r.prop, ax1, r.data)

  if r.prop.plot_lib == 'plotly':
    if r.prop.plotly['fig'] is None:
      import plotly.offline as ply
      import plotly.graph_objs as go
      r.prop.plotly['fig'] = go.Figure(data=r.prop.plotly['trace'], 
            layout=r.prop.plotly['layout'])
      ply.plot(r.prop.plotly['fig'], filename=r.prop.figname)
  
  elif r.prop.plot_lib == 'matplotlib':
    if figi is None and r.prop.plot != 'Mayavi' and ax is None:
      plt.tight_layout()
    
      print_verbose('output: '+r.prop.figname, r.args.verbose)
      fig.savefig(r.prop.figname, format=r.prop.format_output)

      if r.prop.show==1:
        plt.show()
  else:
    raise ValueError('plot_lib can be only matplotlib or plotly, not '+r.prop.plot_lib+' !!')

#
#
#
def get_axis(fig, ax, prop, quantity):

  
  if ax is None and fig is None:
    fig = plt.figure(1, figsize=prop.figsize)
    if quantity == 'polarizability':
      ax = None
    elif prop.plot == '3D':
      ax = Axes3D(fig)
    elif prop.plot == '1D':
      ax = [fig.add_subplot(121), fig.add_subplot(122)]
    else:
    #if polarizability, the axis will be defined later
      ax = fig.add_subplot(111)

  return fig, ax

#
#
#
def make_movie(r, fname=None):
  
  print_verbose('Movie routine: this routine is use to create movie from tddft_tem program\n', 
                r.args.verbose)
  create_folder(r.prop.folder)

  data = r.Read(YFname=fname)
  fig = plt.figure(1)
  ax = fig.add_subplot(111)
  r.prop.format_output='png'

  if r.prop.plot == '2D':
    if r.args.quantity == 'density' or 'potential' or 'intensity':
      for i in range(data.t.shape[0]):
        r.args.time_num = i
        if i <10:
          r.prop.figname = 'fig00{0}'.format(i)
        elif i>9 and i <100:
          r.prop.figname = 'fig0{0}'.format(i)
        else:
          r.prop.figname = 'fig{0}'.format(i)

        r.prop.title = 't = {0}'.format(data.t[i])
        plot_choice(r.args, r.prop, ax, data)
    else:
      raise ValueError('only density, potential or intenstiy')
  elif r.prop.plot == 'Mayavi':
    from ase.mbpt_lcao_utils.plot_mayavi import mayavie_movie
    data = r.Read(YFname=fname)
    mayavie_movie(r.args, r.prop, data)
  else:
    raise ValueError('only 2D plot and Mayavi, other plot are not yet implemented')



#
#
#
def detemine_R0(args):
  dt = np.min(args.tem_input['dr'])

  N = 0
  while N <(2*np.sqrt(np.dot(args.tem_input['v'], args.tem_input['v']))*np.pi/(args.tem_input['dw']*dt)):
    N = N + 1

  tmin = -N*dt/2.0

  #v = args.tem_input['v']/np.sqrt(np.dot(args.tem_input['v'], args.tem_input['v']))
  R0 = tmin*args.tem_input['v'] + args.tem_input['b']

  return R0, tmin


#
#
#
def electron_trajectory(t, R0, v, tmin):
  r = np.zeros((t.shape[0], 3), dtype=float)
  for i in range(t.shape[0]):
    r[i, :] = R0 + v*(t[i]-tmin)

  return r


#
#
#
def plot_E_field(args, prop, ax, data):
  
  if prop.plot == '2D':
    raise ValueError('not implemented')

  elif prop.plot == 'Mayavi':
    from ase.mbpt_lcao_utils.plot_mayavi import plot_maya 
    plot_maya(args, prop, data)
  else:
    ValueError('prop.plot not set correctly\n \
        See help(Parameter) for help')


#
#
#
def plot_choice(args, prop, ax, data):
  
  if prop.plan == 'x':
    r = pts_coord_to_Array_ind(data, [prop.plan_coord, 0.0, 0.0])
    num = r[0]
    if args.time == 1:
      x = data.Array[args.time_num, num, :, :]
    else:
      x = data.Array[num, ::-1, :]
  elif prop.plan == 'y':
    r = pts_coord_to_Array_ind(data, [0.0, prop.plan_coord, 0.0])
    num = r[1]
    if args.time == 1:
      x = data.Array[args.time_num, :, num, :]
    else:
      x = data.Array[:, num, ::-1]
  elif prop.plan == 'z':
    r = pts_coord_to_Array_ind(data, [0.0, 0.0, prop.plan_coord])
    num = r[2]
    if args.time == 1:
      x = data.Array[args.time_num, :, :, num]
    else:
      x = data.Array[:, :, num]
  else:
    raise ValueError('plan can be only x, y, or z')
 

  if prop.plot == '3D':
    plot_surf(args, prop, ax, data, x)
  elif prop.plot == '2D':
    if prop.plot_lib == 'matplotlib':
      plot_imshow_spatial(args, prop, ax, data, x)
    elif prop.plot_lib == 'plotly':
      try: 
        import plotly
        print("plotly version: ", plotly.__version__)
        plot_imshow_plotly(args, prop, data, x)
      except:
        raise ValueError('plotly failled import')

  elif prop.plot == '1D':
    plot_profile(args, prop, ax, data, x)
  elif prop.plot == 'Mayavi':
    from ase.mbpt_lcao_utils.plot_mayavi import contour
    contour(args, prop, data)
  elif prop.plot=='Mayavi_vol':
    from ase.mbpt_lcao_utils.plot_mayavi import volumetric
    volumetric(args, prop, data)
  elif prop.plot=='contour_tem_mattin':
    from ase.mbpt_lcao_utils.plot_mayavi import contour_tem_mattin 
    contour_tem_mattin(args, prop, data)
  else:
    ValueError('plotting way ill define, exit!!!')


#
#
#
def plot_surf(args, prop, ax, data, x):
 
  
  if prop.plan == 'x':
    xlabel = 'y (Ang)'
    ylabel = 'z (Ang)'
    xmesh = data.xy_mesh
    ymesh = data.xz_mesh
  elif prop.plan == 'y':
    xlabel = 'x (Ang)'
    ylabel = 'z (Ang)'
    xmesh = data.yx_mesh
    ymesh = data.yz_mesh
  elif prop.plan == 'x':
    xlabel = 'x (Ang)'
    ylabel = 'y (Ang)'
    xmesh = data.zx_mesh
    ymesh = data.zy_mesh


  fig = plt.figure()
  ax = fig.add_subplot(111, projection='3d')
  surf = ax.plot_surface(xmesh, ymesh, x, rstride=1, cstride = 1, cmap = prop.cmap)

  cb = fig.colorbar(surf, shrink=0.5)
  cb.ax.set_ylabel(data.ylabel, rotation='vertical', fontsize=prop.fontsize)

  ax.set_xlabel(xlabel, fontsize=prop.fontsize)
  ax.set_ylabel(ylabel, fontsize=prop.fontsize)
  ax.set_zlabel(data.ylabel, fontsize=prop.fontsize)

  for tick in ax.xaxis.get_major_ticks():
    tick.label.set_fontsize(prop.axis_size[0])
  for tick in ax.yaxis.get_major_ticks():
    tick.label.set_fontsize(prop.axis_size[1])
  for tick in cb.ax.get_yticklabels():
    tick.set_fontsize(prop.axis_size[1])

  if prop.title is not None:
    ax.set_title(prop.title, fontsize=prop.fontsize)
  if prop.figname == 'default':
    prop.figname = prop.folder + '/' + args.quantity +'_'+ prop.plan + '_' + data.pl_file + '.' + prop.format_output
  else:
    prop.figname = prop.folder + '/' + prop.figname + '.' + prop.format_output

  if args.exportData['export']:
    if args.exportData['dtype'] == 'HDF5':
      try:
        import h5py
        print("h5py version: ", h5py.__version__)
      except:
        import warnings
        warnings.warn('no HDF5 library install, use npy file')
        args.exportData['dtype'] = 'npy'
      
    exportData(data = [xmesh, ymesh, x], 
               data_name = ['xmesh', 'ymesh', 'value'],
               fname = args.exportData['fname'], dtype=args.exportData['dtype'])

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def plot_imshow_plotly(args, prop, data, z):

  import plotly.graph_objs as go
  import warnings

  x_min = data.box[0][0]*units.Bohr
  y_min = data.box[0][1]*units.Bohr
  z_min = data.box[0][2]*units.Bohr
  x_max = data.box[1][0]*units.Bohr
  y_max = data.box[1][1]*units.Bohr
  z_max = data.box[1][2]*units.Bohr

  xaxis = {}
  yaxis = {}

  if prop.cmap is not None:
    try:
      cmap_extr = cm.get_cmap(prop.cmap)
      cmap = cmocean2plotly(cmap_extr)
    except:
      warnings.warn("Failed to load cmap, using default")
      cmap = "Viridis"
  else:
    cmap = "Viridis"
      
  vminmax = [None, None]
  for i, [pvm, minmax, sign] in enumerate(zip([prop.vmin, prop.vmax],
          [np.min(z), np.max(z)], [-1.0, 1.0])):
      if isinstance(pvm, str):
          if pvm == "equal":
              vminmax[i] = sign*np.max(abs(z))
          else:
              vminmax[i] = float(pvm)*minmax
      elif pvm is None:
          vminmax[i] = minmax
      elif isinstance(pvm, float):
          vminmax[i] = pvm
      else:
          raise ValueError("wrong option for prop.vmin or prop.vmax")

  vmin, vmax = vminmax[0], vminmax[1]


  z = z.T[::-1, :]
  if prop.plan== 'x':
    trace = go.Heatmap(z=z, colorscale=cmap, 
            x=np.linspace(y_min, y_max, z.shape[1]),
            y=np.linspace(z_min, z_max, z.shape[0]),
            zmin=vmin, zmax=vmax, zsmooth="best")

    if prop.xlabel is None:
      xaxis['title'] = "y (Ang)"
    else:
      xaxis['title'] = prop.xlabel
    xaxis['titlefont'] = {'size': prop.fontsize}

    if prop.ylabel is None:
      yaxis['title'] = "z (Ang)"
    else:
      yaxis['title'] = prop.ylabel
    yaxis['titlefont'] = {'size': prop.fontsize}

    if prop.plot_atm:
      warnings.warn("plot_atm not yet implemented with plotly")
    if prop.Edir == 1:
      warnings.warn("Edir not yet implemented with plotly")
  
  elif prop.plan == 'y':
    trace = go.Heatmap(z=z, colorscale=cmap, 
            x=np.linspace(x_min, x_max, z.shape[1]),
            y=np.linspace(z_min, z_max, z.shape[0]),
            zmin=vmin, zmax=vmax, zsmooth="best")
    dx = (x_max-x_min)/prop.nb_ticks #number of tick to show
    step = (z.shape[0])/prop.nb_ticks #number of tick to show
    

    if prop.xlabel is None:
      xaxis['title'] = "x (Ang)"
    else:
      xaxis['title'] = prop.xlabel
    xaxis['titlefont'] = {'size': prop.fontsize}
    if prop.ylabel is None:
      yaxis['title'] = "z (Ang)"
    else:
      yaxis['title'] = prop.ylabel
    yaxis['titlefont'] = {'size': prop.fontsize}

  elif prop.plan == 'z': 
    trace = go.Heatmap(z=z, colorscale=cmap, 
            x=np.linspace(x_min, x_max, z.shape[1]),
            y=np.linspace(y_min, y_max, z.shape[0]),
            zmin=vmin, zmax=vmax, zsmooth="best")

    if prop.xlabel is None:
      xaxis['title'] = "x (Ang)"
    else:
      xaxis['title'] = prop.xlabel
    xaxis['titlefont'] = {'size': prop.fontsize}
    if prop.ylabel is None:
      yaxis['title'] = "y (Ang)"
    else:
      yaxis['title'] = prop.ylabel
    yaxis['titlefont'] = {'size': prop.fontsize}

  if prop.figname == 'default':
    prop.figname = 'plotly_' + args.quantity + '_' + data.pl_file + '_2D_' + prop.plan
  else:
    prop.figname = prop.figname

  layout = go.Layout(xaxis=xaxis, yaxis=yaxis, title=prop.title)
  prop.plotly['trace'].append(trace)
  prop.plotly['layout'] = layout


#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


def plot_imshow_spatial(args, prop, ax, data, z):
  from ase.data.colors import cpk_colors
  
  x_min = data.box[0][0]*units.Bohr
  y_min = data.box[0][1]*units.Bohr
  z_min = data.box[0][2]*units.Bohr
  x_max = data.box[1][0]*units.Bohr
  y_max = data.box[1][1]*units.Bohr
  z_max = data.box[1][2]*units.Bohr

  vminmax = [None, None]
  for i, [pvm, minmax, sign] in enumerate(zip([prop.vmin, prop.vmax],
          [np.min(z), np.max(z)], [-1.0, 1.0])):
      if isinstance(pvm, str):
          if pvm == "equal":
              vminmax[i] = sign*np.max(abs(z))
          elif pvm.split("_")[1] == "max":
              vminmax[i] = float(pvm.split("_")[0])*np.max(z)
          else:
              vminmax[i] = float(pvm)*minmax
      elif pvm is None:
          vminmax[i] = minmax
      elif isinstance(pvm, float):
          vminmax[i] = pvm
      else:
          raise ValueError("wrong option for prop.vmin or prop.vmax")

  vmin, vmax = vminmax[0], vminmax[1]
  #prop.vmin, prop.vmax = vminmax[0], vminmax[1]
  cmap = cm.get_cmap(prop.cmap, lut=prop.cmap_lut)

  z = z.T[::-1, :]
  if prop.plan== 'x':
    
    #im = plt.imshow(z, extent=[y_min, y_max, z_min, z_max], vmin=vmin, vmax = vmax)
    ext = [y_min, y_max, z_min, z_max]
    im = ax.imshow(z, extent=ext, cmap=cmap, vmin=vmin, 
          vmax=vmax, aspect=prop.aspect, interpolation=prop.interpolation)

    r = abs(data.box[1][1]-data.box[0][1])/100

    if prop.plot_atm:
      for pos, Z in zip(data.atoms.positions, data.atoms.numbers):
        ax.add_patch(Circle((pos[1], pos[2]), r,\
            fc=tuple(cpk_colors[Z]), alpha=0.6))

    if prop.figname == 'default':
      prop.figname = prop.folder + '/' + args.quantity + '_' + data.pl_file + '_2D_x.' + prop.format_output
    else:
      prop.figname = prop.folder + '/' + prop.figname + '.' + prop.format_output

    if prop.axis is None:
      ax.axis([y_min, y_max, z_min, z_max])
    else:
      ax.axis(prop.axis)

    if prop.xlabel is None:
      ax.set_xlabel('y (Ang)', fontsize=prop.fontsize)
    else:
      ax.set_xlabel(prop.xlabel, fontsize=prop.fontsize)

    if prop.ylabel is None:
      ax.set_ylabel('z (Ang)', fontsize=prop.fontsize)
    else:
      ax.set_ylabel(prop.ylabel, fontsize=prop.fontsize)


  elif prop.plan == 'y':
    #im = plt.imshow(z, extent=[x_min, x_max, z_min, z_max], vmin=vmin, vmax = vmax)
    ext = [x_min, x_max, z_min, z_max]
    im = ax.imshow(z, extent=ext, cmap=cmap, vmin=vmin, 
          vmax=vmax, aspect=prop.aspect, interpolation=prop.interpolation)

    r = abs(data.box[1][0]-data.box[0][0])/100

    if prop.plot_atm:
      for pos, Z in zip(data.atoms.positions, data.atoms.numbers):
        ax.add_patch(Circle((pos[0], pos[2]), r,\
            fc=tuple(cpk_colors[Z]), alpha=0.6))

    if prop.figname == 'default':
      prop.figname = prop.folder + '/' + args.quantity + '_' + data.pl_file + '_2D_y.' + prop.format_output
    else:
      prop.figname = prop.folder + '/' + prop.figname + '.' + prop.format_output

    if prop.axis is None:
      ax.axis([x_min, x_max, z_min, z_max])
    else:
      ax.axis(prop.axis)

    if prop.xlabel is None:
      ax.set_xlabel('x (Ang)', fontsize=prop.fontsize)
    else:
      ax.set_xlabel(prop.xlabel, fontsize=prop.fontsize)

    if prop.ylabel is None:
      ax.set_ylabel('z (Ang)', fontsize=prop.fontsize)
    else:
      ax.set_ylabel(prop.ylabel, fontsize=prop.fontsize)

  elif prop.plan == 'z': 
    ext = [x_min, x_max, y_min, y_max]
    im = ax.imshow(z, extent=ext, cmap=cmap, vmin=vmin, 
        vmax=vmax, aspect=prop.aspect, interpolation=prop.interpolation)

    r = abs(data.box[1][0]-data.box[0][0])/100

    if prop.plot_atm:
      for pos, Z in zip(data.atoms.positions, data.atoms.numbers):
        ax.add_patch(Circle((pos[0], pos[1]), r,\
            fc=tuple(cpk_colors[Z]), alpha=0.6))

    if prop.figname == 'default':
      prop.figname = prop.folder + '/' + args.quantity + '_' + data.pl_file + '_2D_z.' + prop.format_output
    else:
      prop.figname = prop.folder + '/' + prop.figname + '.' + prop.format_output

    if prop.axis is None:
      ax.axis([x_min, x_max, y_min, y_max])
    else:
      ax.axis(prop.axis)

    if prop.xlabel is None:
      ax.set_xlabel('x (Ang)', fontsize=prop.fontsize)
    else:
      ax.set_xlabel(prop.xlabel, fontsize=prop.fontsize)

    if prop.ylabel is None:
      ax.set_ylabel('y (Ang)', fontsize=prop.fontsize)
    else:
      ax.set_ylabel(prop.ylabel, fontsize=prop.fontsize)

  if prop.cblabel is not None:

    if prop.cbar_yticklabels is not None:
      ticks = np.linspace(vmin, vmax, len(prop.cbar_yticklabels))
      prop.cbar = plt.colorbar(im, orientation=prop.cbar_orientation, ticks = ticks, cax=prop.cbar_cax,
          fraction=0.07)
      prop.cbar.ax.set_yticklabels(prop.cbar_yticklabels)
    else:
      prop.cbar = plt.colorbar(im, orientation=prop.cbar_orientation, cax=prop.cbar_cax)
    prop.cbar.ax.set_ylabel(prop.cblabel, rotation='vertical', fontsize=prop.fontsize,
        labelpad = prop.cblabelpad)

  if prop.title is not None:
    ax.set_title(prop.title, fontsize=prop.fontsize)

  if args.exportData['export']:
    if args.exportData['dtype'] == 'HDF5':
      try:
        import h5py
        print("h5py version: ", h5py.__version__)
      except:
        import warnings
        warnings.warn('no HDF5 library install, use npy file')
        args.exportData['dtype'] = 'npy'
      
    exportData(data = [np.array(ext), z], 
               data_name = ['extent', 'value'],
               fname = args.exportData['fname'], dtype=args.exportData['dtype'])



#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def plot_profile(args, prop, ax, data, z):
  #from utils import expected_value_1D

  if prop.plan == 'x':
    ind = pts_coord_to_Array_ind(data, np.array([0.0, prop.coord[1], prop.coord[2]]))
    Ix = z[:, ind[2]]
    Iy = z[ind[1], :]
    x=np.linspace(data.box[0][1], data.box[1][1], len(Ix))*prop.bohr_rad
    y=np.linspace(data.box[0][2], data.box[1][2], len(Iy))*prop.bohr_rad
    xlabel1=r'$y$ (Ang)'
    xlabel2=r'$z$ (Ang)'
    ylabel=data.ylabel
    title1=r'Profile along $y$, in the plan $(yoz)$'
    title2=r'Profile along $z$, in the plan $(yoz)$'
  elif prop.plan == 'y':
    ind = pts_coord_to_Array_ind(data, np.array([prop.coord[0], 0.0, prop.coord[2]]))
    Ix = z[:, ind[2]]
    Iy = z[ind[0], :]
    #prop.xorig = -data.box[0][0]
    x=np.linspace(data.box[0][0]-prop.xorig, data.box[1][0]-prop.xorig, len(Ix))*prop.bohr_rad
    y=np.linspace(data.box[0][2], data.box[1][2], len(Iy))*prop.bohr_rad
    
    #x_sp, Ix_sp = interpolate(x, Ix, 10*Ix.shape[0], k=5)
    #y_sp, Iy_sp = interpolate(y, Iy, 10*Iy.shape[0], k=5)
    xlabel1=r'$x$ (Ang)'
    xlabel2=r'$z$ (Ang)'
    ylabel=data.ylabel
    title1=r'Profile along $x$, in the plan $(xoz)$'
    title2=r'Profile along $z$, in the plan $(xoz)$'
  elif prop.plan == 'z':
    ind = pts_coord_to_Array_ind(data, np.array([prop.coord[0], prop.coord[1], 0.0]))
    Ix = z[:, ind[1]]
    Iy = z[ind[0], :]
    x=np.linspace(data.box[0][0], data.box[1][0], len(Ix))*prop.bohr_rad
    y=np.linspace(data.box[0][1], data.box[1][1], len(Iy))*prop.bohr_rad
    xlabel1=r'$x$ (Ang)'
    xlabel2=r'$y$ (Ang)'
    ylabel=data.ylabel
    title1=r'Profile along $x$, in the plan $(xoy)$'
    title2=r'Profile along $y$, in the plan $(xoy)$'
    

  i = 0
  while (x[i]<0):
    i = i + 1

  #xmax = 2.0/0.529 #2.0 Ang
  #ind_max = int(prop.xmax/data.dr[0])
  #args.peak_max = x[i+np.argmax(abs(Ix[i:Ix.shape[0]]))]
  #args.exp_value = expected_value_1D(x[i:Ix.shape[0]]/0.529, Ix[i:Ix.shape[0]])
  #args.charge = np.max(abs(Ix[i:i+ind_max]))
  #print(i, x[i], np.argmax(abs(Ix[i:Ix.shape[0]])), x[i+np.argmax(abs(Ix[i:Ix.shape[0]]))])
  #print('Peak max = ', args.peak_max)
  #ax[0].plot(x[i:i+ind_max], Ix[i:i+ind_max], color=prop.linecolor, linewidth=prop.linewidth, label=prop.legendLabel)
  ax[0].plot(x, Ix, color=prop.linecolor, linewidth=prop.linewidth, label=prop.legendLabel)
  ax[1].plot(y, Iy, color=prop.linecolor, linewidth=prop.linewidth, label=prop.legendLabel)

  if prop.xlabel is not None:
    ax[0].set_xlabel(xlabel1, fontsize=prop.fontsize)
    ax[1].set_xlabel(xlabel2, fontsize=prop.fontsize)
  if prop.ylabel is not None:
    ax[0].set_ylabel(ylabel, fontsize=prop.fontsize)
    ax[1].set_ylabel(ylabel, fontsize=prop.fontsize)


  if prop.title is None:
    ax[0].set_title(title1, fontsize=prop.fontsize)
    ax[1].set_title(title2, fontsize=prop.fontsize)
  else:
    ax[0].set_title(prop.title, fontsize=prop.fontsize)
    ax[1].set_title(prop.title, fontsize=prop.fontsize)

  if prop.axis is not None:
    ax[0].axis(prop.axis[0])
    ax[1].axis(prop.axis[1])

  for ax2 in ax:
    for tick in ax2.xaxis.get_major_ticks():
      tick.label.set_fontsize(prop.axis_size[0])
    for tick in ax2.yaxis.get_major_ticks():
      tick.label.set_fontsize(prop.axis_size[1])

  prop.figname = prop.folder + '/' + args.quantity + prop.plan + '_prof_' + data.pl_file + '.' + prop.format_output

  if args.exportData['export']:
    if args.exportData['dtype'] == 'HDF5':
      try:
        import h5py
        print("h5py version: ", h5py.__version__)
      except:
        import warnings
        warnings.warn('no HDF5 library install, use npy file')
        args.exportData['dtype'] = 'npy'
    exportData(data = [x, y, Ix, Iy], 
               data_name = ['x', 'y', 'Ix', 'Iy'],
               fname = args.exportData['fname'], dtype=args.exportData['dtype'])



#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def draw_circle_axe(x0, y0, r, prop):
  phi = np.linspace(0, 2*np.pi, 1000)

  x = x0+r*np.cos(phi)
  y = y0 + r*np.sin(phi)

  circle = plt.plot(x, y, color = prop.color_arrow, linewidth = 2)
  dot = plt.plot(x0, y0, 'o', color = prop.color_arrow, markersize = 2)
  plt.text(x0 - 2, y0-1, prop.arrow_label, fontsize=prop.ft_arrow_label, color = prop.color_arrow)

  return circle, dot

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def draw_arrow(prop, end, y_min, y_max, z_min, z_max):
  begin = (y_min + (y_max - y_min)/4, z_min + (z_max - z_min)/4)

  if prop.coord_Ef =='default':
    prop.coord_Ef = np.array([begin, end])

  frame = plt.gca()
  frame.add_patch(FancyArrowPatch(prop.coord_Ef[0], prop.coord_Ef[1], arrowstyle='->', linewidth=2, mutation_scale=30, color = prop.color_arrow))
  plt.text(prop.coord_Ef[1][0]-3, prop.coord_Ef[1][1]+1, prop.arrow_label, fontsize=prop.ft_arrow_label, color = prop.color_arrow)
  
#
#
#
def plot_polarizability(args, prop, fig, data=None, kind="quadratic"):
  """
    plot the polarizability from tddft_iter
  """
  if data == None or args.format_input == 'txt':
    if args.species != '':
      fname = args.folder + '/dipol_' + args.inter + '_iter_krylov_' + args.ReIm + '_' + args.species + '.txt'
    else:
      fname = args.folder + '/dipol_' + args.inter +'_iter_krylov_' + args.ReIm + '.txt'

    print_verbose('Load data: '+fname, args.verbose)
    freq = np.loadtxt(fname)[:, 0]
    P = np.loadtxt(fname)[:, 2:11].reshape((freq.shape[0], 3, 3))
    np.reshape(P, (freq.shape[0], 3, 3))
  elif args.format_input == 'hdf5':
    P = data.Array
    freq = data.freq
  else:
    raise ValueError('input only txt or hdf5')

  nb = 0
  quantity = np.array([(P[:, 0, 0] + P[:, 1, 1] + P[:, 2, 2])/3.0, P[:, 0, 0],
                       P[:, 1, 1], P[:, 2, 2]])

  sentence = ['Max <P> ', 'Max P_xx ', 'Max P_yy ', 'Max p_zz ']
  xlabel = [r'$\omega$ (eV)', r'$\omega$ (eV)', r'$\omega$ (eV)', r'$\omega$ (eV)']
  ylabel = [r'$<P>$', r'$P_{xx}$', r'$P_{yy}$', r'$P_{zz}$']
  data.output = {}
  
  x_sp = np.linspace(freq[0], freq[freq.shape[0]-1], freq.shape[0]*100)
  data.output['freq'] = x_sp 
  data.output['pol'] = np.zeros((2, 2, x_sp.shape[0]), dtype = np.float64)
  for i in range(2):
    for j in range(2):
      if (prop.units == 'nm^2'):
        from Read_data import pol2cross_sec
        for f in range(freq.shape[0]):
          quantity[nb][f] = pol2cross_sec(quantity[nb][f], freq[f])
      ax = fig.add_subplot(2, 2, nb+1)
      func, popt = interpolate(freq, quantity[nb], kind=kind)

      y_sp = func(x_sp, *popt)
      data.output['pol'][i, j, :] = y_sp
      print_verbose('{0}: {1} at {2} eV'.format(sentence[nb], np.max(y_sp), x_sp[np.argmax(y_sp)]), args.verbose)
      ax.plot(freq, quantity[nb], 'r', linewidth=3, label='data')
      ax.plot(x_sp, y_sp, '--g', linewidth=3, label='interpolation')
      ax.set_xlabel(xlabel[nb], fontsize=prop.fontsize)
      ax.set_ylabel(ylabel[nb], fontsize=prop.fontsize)
      ax.legend(loc=0)
      nb = nb + 1
  
      for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(prop.axis_size[0])
      for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(prop.axis_size[1])

  if prop.figname == 'default':
    if args.species != '':
      prop.figname = prop.folder + '/polarizability_' + args.inter + '_' + args.ReIm\
              + '_' + args.species+ '.'+ prop.format_output
    else:
      prop.figname = prop.folder + '/polarizability_' + args.inter + '_' + args.ReIm\
              + '.'+ prop.format_output
  else:
    prop.figname = prop.folder + '/' + prop.figname

#
#
#
def plot_EELS_spectrum(args, prop, fig, ax, data=None):
  """
  plot the EELS spectrum from tddft_tem data
  """
  if data == None:
    fname = 'tem_spectrum_' + args.inter + '_v{0:.8f}_bx{1:.8f}_by{2:.8f}_bz{3:.8f}.dat'.format(\
              args.tem_input['vnorm'], args.tem_input['b'][0], args.tem_input['b'][1],\
              args.tem_input['b'][2])
    P = np.loadtxt(fname)[:, 1]
    freq = np.loadtxt(fname)[:, 0]
  else:
    P = data.Array
    freq = data.freq

  x_sp, y_sp = interpolate(freq, P, 10*P.shape[0])
  print_verbose('Max(P) = -{0} at {1} eV'.format(np.max(abs(y_sp)), x_sp[np.argmax(abs(y_sp))]), args.verbose)
  ax.plot(freq, P, 'r', linewidth=3, label='data')
  ax.plot(x_sp, y_sp, '--g', linewidth=3, label='interpolation')
  ax.set_xlabel(r'$\omega$ (eV)', fontsize=prop.fontsize)
  ax.set_ylabel(r'$\Gamma_{EELS}$ (a.u.)', fontsize=prop.fontsize)
  ax.legend(loc=0)
  
  for tick in ax.xaxis.get_major_ticks():
    tick.label.set_fontsize(prop.axis_size[0])
  for tick in ax.yaxis.get_major_ticks():
    tick.label.set_fontsize(prop.axis_size[1])

  if prop.figname == 'default':
    prop.figname = prop.folder + '/EELS_spectra_' + args.inter + '.' + prop.format_output
  else:
    prop.figname = prop.folder + '/' + prop.figname


#
#
#
def plot_E_loss(args, prop, ax, data = None, b = 0, v = 0):
  """
  Plot the energy loss from tddft_tem data
  """

  if data is None:
    fname = 'E_loss_array_' + args.inter + '.dat'
    E = np.loadtxt(fname)[:, 1]
  else:
    E = data.Array

  if args.tem_input['brange'] != None and args.tem_input['vrange'] != None:
    if args.tem_input['vrange'].shape[0] != E.shape[1]\
        or args.tem_input['brange'].shape[0] != E.shape[0]:
      raise ValueError('Error shape')

    ext = [args.tem_input['brange'][0], args.tem_input['brange'][args.tem_input['brange'].shape[0]-1],
          args.tem_input['vrange'][0], args.tem_input['vrange'][args.tem_input['vrange'].shape[0]-1]]

    im = ax.imshow(E, extent = ext, cmap=cm.get_cmap(prop.cmap))
    ax.set_xlabel(r'impact parameter (a.u.)', fontsize=prop.fontsize)
    ax.set_ylabel(r'$v$ (a.u.)', fontsize=prop.fontsize)
    cbar = plt.colorbar(im, orientation='vertical')
    cbar.ax.set_ylabel(r'EELS (a.u.)', rotation='vertical', fontsize=prop.fontsize)

  elif args.tem_input['vrange'] != None:
    if args.tem_input['vrange'].shape[0] != E.shape[1]:
      raise ValueError('Error shape')
    x_sp, y_sp = interpolate(args.tem_input['vrange'], E[b, :], 10*E.shape[1])
    #print('Max(EELS) = -{0}'.format(np.max(abs(y_sp))), ' at', x_sp[np.argmax(abs(y_sp))], ' a.u.')
    ax.plot(args.tem_input['vrange'], E[b, :], 'r', linewidth=3, label='data')
    ax.plot(x_sp, y_sp, '--g', linewidth=3, label='interpolation')
    ax.set_xlabel(r'$v$ (a.u.)', fontsize=prop.fontsize)

  elif args.tem_input['brange'] != None:
    if args.tem_input['brange'].shape[0] != E.shape[0]:
      #print(args.tem_input['brange'].shape[0], E.shape[0])
      raise ValueError('Error shape')
    x_sp, y_sp = interpolate(args.tem_input['brange'], E[:, v], 10*E.shape[0])
    #print('Max(EELS) = -{0}'.format(np.max(abs(y_sp))), ' at', x_sp[np.argmax(abs(y_sp))], ' a.u.')
    ax.plot(args.tem_input['brange'], E[:, v], 'r', linewidth=3, label='data')
    ax.plot(x_sp, y_sp, '--g', linewidth=3, label='interpolation')
    ax.set_xlabel(r'impact parameter (a.u.)', fontsize=prop.fontsize)

  else:
    raise ValueError('Something is wrong!!')
  
  
  
  ax.set_ylabel(r'EELS (a.u.)', fontsize=prop.fontsize)
  ax.legend(loc=0)
  
  for tick in ax.xaxis.get_major_ticks():
    tick.label.set_fontsize(prop.axis_size[0])
  for tick in ax.yaxis.get_major_ticks():
    tick.label.set_fontsize(prop.axis_size[1])

  if prop.figname == 'default':
    prop.figname = prop.folder + '/EELS' + args.inter + '.' + prop.format_output
  else:
    prop.figname = prop.folder + '/' + prop.figname

#
#
#
def plot_maps(args, prop, fig, ax, data):
  
  if args.quantity == 'e2map':
    ext=[data.erange[0], data.erange[data.erange.shape[0]-1], data.freq[0], data.freq[data.freq.shape[0]-1]]
    im = ax.imshow(data.Array[::-1, :], aspect='auto', extent=ext)
    
    ax.set_ylabel(r'$\omega (eV)$', fontsize=prop.fontsize)
    ax.set_xlabel(r'Orbital Energy (eV)', fontsize=prop.fontsize)
    ax.set_title(prop.title, fontsize=prop.fontsize)
    if prop.pos_cb is None:
      fig.colorbar(im, orientation='vertical')
    else:
      cax = fig.add_axes(prop.pos_cb)
      fig.colorbar(im, cax=cax, orientation='vertical')

    if prop.figname == 'default':
      if args.tem_iter == 'iter':
        prop.figname = prop.folder + '/iter_e2map_' + args.inter + prop.format_output
      elif args.tem_iter == 'tem':
        prop.figname = prop.folder + '/tem_e2map_' + args.inter +\
        'v{0:.2f}_bx{1:.2f}_bx{2:.2f}_bx{3:.2f}.'.format(args.tem_input['vnorm'],\
        args.tem_input['b'][0], args.tem_input['b'][1], args.tem_input['b'][2]) + prop.format_output
      else:
        raise ValueError('only tem or iter')
    else:
      prop.figname = prop.folder + '/' + prop.figname



  elif args.quantity == 'i2map':
    ext=[0, data.Array.shape[1], data.freq[0], data.freq[data.freq.shape[0]-1]]
    im = ax.imshow(data.Array[::-1, :], aspect='auto', extent=ext)
    
    ax.set_ylabel(r'$\omega (eV)$', fontsize=prop.fontsize)
    ax.set_xlabel(r'Orbital', fontsize=prop.fontsize)
    ax.set_title(prop.title, fontsize=prop.fontsize)
    if prop.pos_cb is None:
      fig.colorbar(im, orientation='vertical')
    else:
      cax = fig.add_axes(prop.pos_cb)
      fig.colorbar(im, cax=cax, orientation='vertical')

    if prop.figname == 'default':
      prop.figname = prop.folder + '/tem_i2map_' + args.inter +\
        'v{0:.2f}_bx{1:.2f}_bx{2:.2f}_bx{3:.2f}.'.format(args.tem_input['vnorm'],\
        args.tem_input['b'][0], args.tem_input['b'][1], args.tem_input['b'][2]) + prop.format_output
    else:
      prop.figname = prop.folder + '/' + prop.figname


  elif args.quantity == 'nm2map':
    ext=[0, data.Array.shape[0], 0, data.Array.shape[1]]
    im = ax.imshow(data.Array[:, :], aspect='auto', extent=ext)
    
    ax.set_ylabel(r'orbitals', fontsize=prop.fontsize)
    ax.set_xlabel(r'Orbitals', fontsize=prop.fontsize)
    ax.set_title(prop.title, fontsize=prop.fontsize)
    if prop.pos_cb is None:
      plt.colorbar(im, orientation='vertical')
    else:
      cax = fig.add_axes(prop.pos_cb)
      fig.colorbar(im, cax=cax, orientation='vertical')

    if prop.figname == 'default':
      if args.tem_iter == 'iter':
        prop.figname = prop.folder + '/iter_nm2map_' + args.inter + prop.format_output
      elif args.tem_iter == 'tem':
        prop.figname = prop.folder + '/tem_nm2map_' + args.inter +\
        'v{0:.2f}_bx{1:.2f}_bx{2:.2f}_bx{3:.2f}.'.format(args.tem_input['vnorm'],\
        args.tem_input['b'][0], args.tem_input['b'][1], args.tem_input['b'][2]) + prop.format_output
      else:
        raise ValueError('only tem or iter')
    else:
      prop.figname = prop.folder + '/' + prop.figname




#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def bohrtoplnum(data, inp, plane):

  if plane=='x':
    ind = 0
  elif plane == 'y':
    ind = 1
  elif plane == 'z':
    ind = 2
  else:
    ValueError('Wrong plane define')

  i=data.box[0][ind]
  dr = data.dr[ind]
  inp = float(inp)

  if inp<data.box[0][ind] or inp>data.box[1][ind]:
    raise ValueError('Input parameter outside of the box!!!')

  num=0
  while i <inp:
    num = num+1
    i=i+dr

  if abs(inp-i)<abs(inp-(i+dr)):
    num=num
  else:
    num=num+1
  return num



#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def plot_E_field_2D(args, prop, data_x, data_y, num):

  #x_min = data_x.zx_mesh[0, 0] 
  #x_max = data_x.zx_mesh[len(data_x.zx_mesh[:, 0])-1, 0]
  #y_min = data_x.zy_mesh[0, 0]
  #y_max = data_x.zy_mesh[0, len(data_x.zy_mesh[0, :])-1]
  #z_min = data_x.yz_mesh[0, 0]
  #z_max = data_x.yz_mesh[0, data_x.yz_mesh.shape[1]-2]


  fig=plt.figure()
  ax=fig.add_subplot(111)


  for i in range(1, len(data_x.atoms)):
    data_x.atoms[i][1] = data_x.atoms[i][1]/0.529
  
  if prop.plan == 'x':
  
    x = data_x.Array[num, :, :]
    y = data_y.Array[num, :, :]

    plt.quiver(y, x)#, scale=[y_min, y_max, z_min, z_max])

    imname = prop.folder + '/' + args.quantity + '_' + data_x.pl_num + '_2D_x.png'
  
    plt.xlabel('y (Ang)', fontsize=prop.fontsize)
    plt.ylabel('z (Ang)', fontsize=prop.fontsize)

  elif prop.plan == 'y':

    x = data_x.Array[:, num, :]
    y = data_y.Array[:, num, :]

    plt.quiver(y, x)#, scale=[x_min, x_max, z_min, z_max])
    #r = abs(data_x.box[1][0]-data_x.box[0][0])/100

    imname = prop.folder + '/' + args.quantity + '_' + data_x.pl_num + '_2D_y.png'
  
    plt.xlabel('x (Ang)', fontsize=prop.fontsize)
    plt.ylabel('z (Ang)', fontsize=prop.fontsize)

  elif prop.plan == 'z': 
    x = data_x.Array[:, :, num]
    y = data_y.Array[:, :, num]
    

    new_x = np.zeros((int(x.shape[0]/10), int(x.shape[1]/10)), dtype=float)
    new_y = np.zeros((int(x.shape[0]/10), int(x.shape[1]/10)), dtype=float)

    for m in range(new_x.shape[0]):
      for n in range(new_y.shape[1]):
        new_x[m, n] = x[10*m, 10*n]
        new_y[m, n] = y[10*m, 10*n]


    C = np.arange(0.1, 1.0, 0.1)
    nbx = data_x.yx_mesh.shape[0]
    nby = data_x.xy_mesh.shape[0]
    xaxis = [data_x.yx_mesh[int(nbx/5), 0], data_x.yx_mesh[2*int(nbx/5), 0], \
        data_x.yx_mesh[3*int(nbx/5), 0], data_x.yx_mesh[4*int(nbx/5), 0], data_x.yx_mesh[5*int(nbx/5)-1, 0]]
    yaxis = [data_x.xy_mesh[int(nby/5), 0], data_x.xy_mesh[2*int(nby/5), 0], \
        data_x.xy_mesh[3*int(nby/5), 0], data_x.xy_mesh[4*int(nby/5), 0], data_x.xy_mesh[5*int(nby/5)-1, 0]]

    ax.xaxis.set_ticks(yaxis)
    ax.yaxis.set_ticks(xaxis)

    plt.quiver(new_y, new_x, C)#[x_min, x_max, y_min, y_max])
    plt.plot(12.95, 5.5, 'or', markersize=30)
    plt.plot(3.5, 5.5, 'or', markersize=30)
    #r = abs(data_x.box[1][0]-data_x.box[0][0])/100

    imname = prop.folder + '/' + args.quantity + '_' + data_x.pl_num + '_2D_z.png'
  
    plt.xlabel('x (Ang)', fontsize=prop.fontsize)
    plt.ylabel('y (Ang)', fontsize=prop.fontsize)


  plt.savefig(imname, format=prop.format_output)
  if prop.show == 1:
    plt.show()

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def save_prof(x, I, fname):
  f = open(fname, 'w')

  for i in range(len(x)):
    f.write('{0}     {1}\n'.format(x[i], I[i]))

  f.close()

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def plot_complex(prop, x, f, xlabel=r'x', ylabel=r'y'):
  """
  plot in a subplot 1x2 the real part and the imaginary
  part of a complex 1D function. use mainly to plot the suceptibility
  and the signal.
  Input parameters:
  -----------------
    prop: the constantes define for the program
    x (numpy 1D array, float): abscisse of the function to plot, must be real
    f (numpy 1D array, complex): complex function that is plotted
    xlabel (string): label of x axis
    ylabel (string): label of y axis
  """

  create_folder(prop.folder)

  fig, axes = plt.subplots(nrows=1, ncols=2, figsize=prop.figsize)
  
  axes.flat[0].plot(x, f.real, linewidth=prop.linewidth, color=prop.linecolor)
  axes.flat[0].set_xlabel(xlabel, fontsize=prop.fontsize)
  axes.flat[0].set_ylabel(r'Re(' + ylabel + r')', fontsize=prop.fontsize)

  axes.flat[1].plot(x, f.imag, linewidth=prop.linewidth, color=prop.linecolor)
  axes.flat[1].set_xlabel(xlabel, fontsize=prop.fontsize)
  axes.flat[1].set_ylabel(r'Im(' + ylabel + r')', fontsize=prop.fontsize)

  for tick in axes.flat[0].xaxis.get_major_ticks():
    tick.label.set_fontsize(prop.axis_size[0])
  for tick in axes.flat[0].yaxis.get_major_ticks():
    tick.label.set_fontsize(prop.axis_size[1])

  for tick in axes.flat[1].xaxis.get_major_ticks():
    tick.label.set_fontsize(prop.axis_size[0])
  for tick in axes.flat[1].yaxis.get_major_ticks():
    tick.label.set_fontsize(prop.axis_size[1])



  figname = prop.folder + prop.figname + prop.format_output
  fig.savefig(figname, format=prop.format_output)

  if prop.show == 1:
    plt.show()

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def plot_vector(prop, x, f, xlabel=r'x', ylabel=r'y', dtype=float):
  """
  plot in a subplot 1x2 the real part and the imaginary
  part of a complex 1D function. use mainly to plot the suceptibility
  and the signal.
  Input parameters:
  -----------------
    prop: the constantes define for the program
    x (numpy 1D array, float): abscisse of the function to plot, must be real
    f (numpy 1D array, complex): complex function that is plotted
    xlabel (string): label of x axis
    ylabel (string): label of y axis
  """

  create_folder(prop.folder)

  if dtype == complex:
    xlabel_list = [r'' + xlabel, r'' + xlabel, r'' + xlabel,\
        r'' + xlabel, r'' + xlabel, r'' + xlabel]
    ylabel_list = [r'Re($' + ylabel + r'_{x})$', r'Im($' + ylabel + r'_{x})$',\
        r'Re($' + ylabel + r'_{y})$', r'Im($' + ylabel + r'_{y})$',\
        r'Re($' + ylabel + r'_{z})$', r'Im($' + ylabel + r'_{z})$']
    f_list = [f[:, 0].real, f[:, 0].imag, f[:, 1].real, f[:, 1].imag,\
        f[:, 2].real, f[:, 2].imag]
    fig, axes = plt.subplots(nrows=3, ncols=2, figsize=prop.figsize)

    for i, ax in enumerate(axes.flat):
      ax.plot(x, f_list[i], linewidth=prop.linewidth, color=prop.linecolor)
      ax.set_xlabel(xlabel_list[i], fontsize=prop.fontsize)
      ax.set_ylabel(ylabel_list[i], fontsize=prop.fontsize)


      for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(prop.axis_size[0])
      for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(prop.axis_size[1])

    figname = prop.folder + prop.figname + prop.format_output
    fig.savefig(figname, format=prop.format_output)

    if prop.show == 1:
      plt.show()

  else:
    xlabel_list = [r'$' + xlabel + r'$', r'$' + xlabel + r'$',\
        r'$' + xlabel + r'$']
    ylabel_list = [r'$' + ylabel + r'_{x}$', r'$' + ylabel + r'_{y}$',\
        r'$' + ylabel + r'_{z}$']
    fig, axes = plt.subplots(nrows=1, ncols=3, figsize=prop.figsize)

    for i, ax in enumerate(axes.flat):
      ax.plot(x, f[:, i], linewidth=prop.linewidth, color=prop.linecolor)
      ax.set_xlabel(xlabel_list[i], fontsize=prop.fontsize)
      ax.set_ylabel(ylabel_list[i], fontsize=prop.fontsize)


      for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(prop.axis_size[0])
      for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(prop.axis_size[1])

    figname = prop.folder + prop.figname + prop.format_output
    fig.savefig(figname, format=prop.format_output)

    if prop.show == 1:
      plt.show()

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def create_time_anime(r):
  """
    plot all the figure of a time variation wih the parameter
    given by args and prop.
    Input parameters:
    -----------------
      args (class Parameter): type help(Parameter) for more informations
      prop (class Properties_windows): type help(Properties_windows) for more informations
      fxyz_name (string): name of the file for the atomic coordinate
  """
  
  raise ValueError("Obsolete!!")
  if r.args.time != 1:
    ValueError('you need time dependece to run this function\n \
        please set args.time to 1')

  create_folder(r.prop.folder)
    
  for i in range(r.Array.shape[0]):
    r.args.time_num = i
    if i <10:
      r.prop.figname = 'intensity_time_0{0}'.format(i)
    elif i>9:
      r.prop.figname = 'intensity_time_{0}'.format(i)

    #plot_3D(r.args, r.prop)

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def exportData(data = [], data_name = [], fname = 'exportData', dtype='npy'):
  """
    Export the plotted data to another file type for further analysis
  """
  if len(data) != len(data_name):
    raise ValueError('name and data not the same length')

  if dtype == 'npy':
    for i, d in enumerate(data):
      np.save(fname + '_' + data_name[i] + '.npy', d)
  elif dtype == 'txt':
    for i, d in enumerate(data):
      np.savetxt(fname + '_' + data_name[i] + '.txt', d)

  elif dtype == 'HDF5':
    import h5py
    F = h5py.File(fname + '.hdf5', 'w')
    for i, d in enumerate(data):
      F.create_dataset(data_name[i], data=d)

    F.close()

  else:
    raise ValueError('can export data only in npy or HDF5 formats')

def change_scale(r0, data):
  
  Delta_x = data.box[1][0] - data.box[0][0]
  Delta_y = data.box[1][1] - data.box[0][1]
  Delta_z = data.box[1][2] - data.box[0][2]

  r0[0] = (2*r0[0] + Delta_x/2)*data.nxyz[0]/Delta_x + data.dr[0]# + nb_x/2
  r0[1] = (2*r0[1] + Delta_y/2)*data.nxyz[1]/Delta_y + data.dr[1]# + nb_x/2
  r0[2] = (2*r0[2] + Delta_z/2)*data.nxyz[2]/Delta_z + data.dr[2]# + nb_x/2

  return r0

  


#################################################################

def convert(nb, power, choix):
  if choix == 1:
    nb_c = '{0}'.format(round(Decimal(nb*10**power), 1))
  elif choix == 2:
    nb_c = '%.0E' % Decimal(power)
  return nb_c
  
#################################################################

#
#
#
def obj_reader(fname):
  """
  Read an wavefront obj file in order to plot triangular mesh with Mayavi
  Input:
  ------
    fname (str): file name
  Output:
  -------
    vertex (np.array): array containing the vertex coordinate of dimension N*3
        Where N is the number of vertex
    facet (np.array): array containning the facet of ddimension Nf*3
        Where Nf is the number of facet

  Warning:
  --------
    Be careful that in the .obj file, the separation for the decimal number is given by a . and not a ,


  Example:
  --------

  from __future__ import division
  import numpy as np
  from ase.calculators.siesta.mbpt_lcao.pyiter.plot_lib import obj_reader
  import mayavi.mlab as mlab

  vertex, facet = obj_reader('dens_surf_recons.obj')

  mlab.triangular_mesh(vertex[:, 0], vertex[:, 1], vertex[:, 2], facet)
  mlab.show()
  """

  
  import ase.mbpt_lcao_utils.objmtl_mod as obj

  def get_facet(facets):
    out = np.array([0, 0, 0])
    if len(facets) != 3:
      raise ValueError('implemented only for triangular facets')

    out[0] = facets[0][0]-1
    out[1] = facets[1][0]-1
    out[2] = facets[2][0]-1
    return np.array(out)

  f = open(fname, 'r')
  r = obj.OBJReader()
  r.read(f)
  f.close()

  vertex = np.zeros((len(r.v_list), 4), dtype=float)
  facet = np.zeros((len(r.f_list), 3), dtype=int)

  for i in range(len(r.v_list)):
    vertex[i, :] = r.v_list[i]
  
  for i in range(len(r.f_list)):
    facet[i, :] = get_facet(r.f_list[i])

  return vertex[:, 0:3], facet


#
#
#
def calculate_confinement(args, prop, mini, maxi, d, omg, fname, folder_data=''):
  """
  calculate the confinement of the electric field in a small volume in a plane, define
  by : conf = (1/maxi-mini)\int frac{E_ind}{E_max}dV
  Input Parameters:
  -----------------
    args (class Parameter) information for the input data, see help(Parameter) for more detail
    prop (class Properties_figure) information for the output, see help(Properties_figure) for more detail
    mini (float) lower integration limit
    maxi (float) upper integration limit (maxi>mini)
  """
  from ase.calculators.siesta.mbpt_lcao_io import read_data

  if maxi<mini:
    raise ValueError('The upper integration limit must be larger than the lower one')

  f4 = open(fname, 'w')
  f4.write('#d(Ang)     E_conf      E_max\n')

  if prop.plan == 'x':
    for i in enumerate(d):
      folder = folder_data + 'calc_{0}Ang/'.format(i[1])
      args.folder = folder

      r = read_data(args, prop)
      print('box:')
      print(r.box)

      y_pl_min  = pts_coord_to_Array_ind(r, np.array([mini, 0.0, 0.0]))[0]
      y_pl_max  = pts_coord_to_Array_ind(r, np.array([maxi, 0.0, 0.0]))[0]
  
      E_max = np.max(r.Array[y_pl_min:y_pl_max+1, :, :])
      print('E_max:', E_max)

      E_enh = np.trapz(np.trapz(np.trapz(r.Array[y_pl_min:y_pl_max+1, :, :], dx = r.dr[0]), dx = r.dr[1]), dx=r.dr[2])
      E_conf = (E_enh/E_max)/(maxi-mini) #bohr**2
      print('confinement = ', E_conf*(0.0529**2), 'nm**2')

      f4.write('{0}     {1}       {2}\n'.format(i[1], E_conf*(0.0529**2), E_max))

  elif prop.plan == 'y':
    for i in enumerate(d):
      folder = folder_data + 'calc_{0}Ang/'.format(i[1])
      args.folder = folder

      r = read_data(args, prop)
      print('box:')
      print(r.box)

      y_pl_min  = pts_coord_to_Array_ind(r, np.array([0.0, mini, 0.0]))[1]
      y_pl_max  = pts_coord_to_Array_ind(r, np.array([0.0, maxi, 0.0]))[1]
  
      E_max = np.max(r.Array[:, y_pl_min:y_pl_max+1, :])
      print('E_max:', E_max)

      E_enh = np.trapz(np.trapz(np.trapz(r.Array[:, y_pl_min:y_pl_max+1, :], dx = r.dr[0]), dx = r.dr[1]), dx=r.dr[2])
      E_conf = (E_enh/E_max)/(maxi-mini) #bohr**2
      print('confinement = ', E_conf*(0.0529**2), 'nm**2')

      f4.write('{0}     {1}       {2}\n'.format(i[1], E_conf*(0.0529**2), E_max))

  elif prop.plan == 'z':
    for i in enumerate(d):
      folder = folder_data + 'calc_{0}Ang/'.format(i[1])
      args.folder = folder

      r = read_data(args, prop)
      print('box:')
      print(r.box)

      y_pl_min  = pts_coord_to_Array_ind(r, np.array([0.0, 0.0, mini]))[2]
      y_pl_max  = pts_coord_to_Array_ind(r, np.array([0.0, 0.0, maxi]))[2]
  
      E_max = np.max(r.Array[:, :, y_pl_min:y_pl_max+1])
      print('E_max:', E_max)

      E_enh = np.trapz(np.trapz(np.trapz(r.Array[:, :, y_pl_min:y_pl_max+1], dx = r.dr[0]), dx = r.dr[1]), dx=r.dr[2])
      E_conf = (E_enh/E_max)/(maxi-mini) #bohr**2
      print('confinement = ', E_conf*(0.0529**2), 'nm**2')

      f4.write('{0}     {1}       {2}\n'.format(i[1], E_conf*(0.0529**2), E_max))
    else:
      raise ValueError('prop.plan must be x, y or z')


  f4.close()


#
#
#
def plot_atomic_orbital(specie, ioncat_option = '-o 3', 
                        ioncat = "/home/marc/investigador/cours/Master-thesis/siesta/siesta-3.2-pl3/Util/Gen-basis/ioncat",
                        out_ioncat = "tmp_ioncat.dat", fig_title = 'atomic_orbital.pdf', fig_format='pdf'):
  """
  plot atomic orbital from specie.ion file, use the ioncat program in the Util/Gen-basis folder
  of the siesta folder.
  Input Parameters:
  -----------------
    specie (string): specie name
    ioncat_option (string): see ioncat -h for help
    ioncat (string): path to the ioncat executable
    out_ioncat (string): name of the output file produce by ioncat
    fig_title (string): figure title
    fig_format (string): figure format 
  """
  from ase.mbpt_lcao_utils.Read_data import run
  run(ioncat + " " + ioncat_option + " " + specie + " > " + out_ioncat)

  data = np.loadtxt(out_ioncat)

  plt.figure(1)
  plt.plot(data[:, 0], data[:, 1], 'r', linewidth=3, label='f')
  plt.plot(data[:, 0], data[:, 2], 'g', linewidth=3, label='grad(f)')
  plt.legend(loc=0)

  plt.savefig(fig_title, format='pdf')
  plt.show()


#
#
#
def select_plan(plan, plan_coord, data):
  #data = self.dat[i]
  x_min = data.box[0][0]*units.Bohr
  y_min = data.box[0][1]*units.Bohr
  z_min = data.box[0][2]*units.Bohr
  x_max = data.box[1][0]*units.Bohr
  y_max = data.box[1][1]*units.Bohr
  z_max = data.box[1][2]*units.Bohr

  box = np.array([x_min, x_max, y_min, y_max, z_min, z_max])
  #print('plan_coord: ', plan_coord)
  print(data.Array.shape)


  if plan == 'x':
    #num = bohrtoplnum(data, 0.0, 'x')
    r = pts_coord_to_Array_ind(data, [plan_coord, 0.0, 0.0])
    y = data.Array[r[0], :, :]
    ext=[y_min, y_max, z_min, z_max]
  elif plan == 'y':
    r = pts_coord_to_Array_ind(data, [0.0, plan_coord, 0.0])
    y = data.Array[:, r[1], ::-1]
    ext=[x_min, x_max, z_min, z_max]
  elif plan == 'z':
    r = pts_coord_to_Array_ind(data, [0.0, 0.0, plan_coord])
    y = data.Array[:, ::-1, r[2]]
    ext=[x_min, x_max, y_min, y_max]
  else:
    raise ValueError('ERROR!! plan must be: x, y or z!!!')


  return y, ext, box

#
#
#
def cmocean2plotly(cmap, pl_entries=100):
  #try:
  #  import cmocean
  #except:
  #  raise ValueError('need to install cmocean')

  h = 1.0/(pl_entries-1)
  pl_colorscale = []
  
  for k in range(pl_entries):
    C = list(map(np.uint8, np.array(cmap(k*h))[:3]*255))
    pl_colorscale.append([k*h, 'rgb'+str((C[0], C[1], C[2]))])
  
  return pl_colorscale

def readBands(fname):
    from ase.mbpt_lcao_utils.readxyz import str2float, str2int

    lendata = 0
    ite = 0
    npts = None
    pts = {}
    f = open(fname, "r")
    for i, line in enumerate(f):
        if i == 0:
            Efermi = str2float(line)[0]
        elif i == 1:
            kmin, kmax = str2float(line)
        elif i == 2:
            Emin, Emax = str2float(line)
        elif i == 3:
            nbands, nspin, nkpoints = str2int(line)
            nlines = np.ceil(nbands/9.0)
            indices = []
            ind = 0
            while ind < nbands:
                indices.append(ind)
                ind += 10
            indices.append(nbands)
            ind = 1
            if nspin != 1:
                raise ValueError("works only for nspin = 1, use otion SpinPolarized   .False. in siesta input")
            Krange = np.zeros(nkpoints, dtype=np.float64)
            bands = np.zeros((nkpoints, nspin, nbands), dtype=np.float64)
        else:

            data = str2float(line)
            if data.size > 1:
                if ind == 1:
                    Krange[ite] = data[0]
                    bands[ite, 0, indices[ind-1]:indices[ind]] = data[1:data.size]
                    if (ind + 1) == len(indices):
                        ind = 1
                        ite += 1
                    else:
                        ind += 1
                else:
                    bands[ite, 0, indices[ind-1]:indices[ind]] = data[0:data.size]
                    if (ind + 1) == len(indices):
                        ind = 1
                        ite += 1
                    else:
                        ind += 1
            else:
                if npts is None:
                    npts = str2int(line)[0]
                else:
                    key = line.split()[1].replace("'", "")
                    if key not in pts.keys():
                        pts[key] = [float(line.split()[0])]
                    else:
                        pts[key].append(float(line.split()[0]))

    f.close()

    return {"Efermi": Efermi, "Emin": Emin, "Emax": Emax, "kmin": kmin, "kmax": kmax,
            "Krange": Krange, "bands": bands, "nbands": nbands, "nkpoints": nkpoints,
            "nspin": nspin, "pts": pts}

def plot_bands(fname, ax = None, ft=20, lw = 3, figname="bands.pdf", figformat="pdf",
        Elim = [None, 10.0]):
    """
        the band structure has to be plotted in a reasonable energy scale that covers occupied states 
        and unoccupied states may be up to 10 eV or so, higher energies are not reliable with 
        a small basis set
        
        Elim: energy range (eV) at which the bands will be plotted, None means default

    """
    bands = readBands(fname)

    if Elim[0] is None:
        Elim[0] = bands["bands"].min()

    if Elim[1] == 10.0:
        Elim[1] -= bands["Efermi"]

    if ax is None:
        fig = plt.figure(1, figsize=(8, 8))
        ax = fig.add_subplot(111)
        ax.plot(bands["Krange"], bands["Krange"]*0.0, "--", color="black",
                linewidth=lw)

        for i in range(bands["nbands"]):
            ax.plot(bands["Krange"], bands["bands"][:, 0, i] - bands["Efermi"], linewidth=lw)

        xticks = []
        xticks_labels = []
        inv_dico = {}
        for k, v in bands["pts"].items():
            for E in v:
                xticks.append(E)
                if k in ["Gamma"]:
                    xticks_labels.append(r"$\Gamma$")
                else:
                    xticks_labels.append(r"$" + k + "$")

        ax.set_xticks(np.array(xticks))
        ax.set_xticklabels(xticks_labels, rotation='horizontal', fontsize=ft)
        ax.set_ylim(Elim)

        fig.savefig(figname, format=figformat)

    else:

        ax.plot(bands["Krange"], bands["Krange"]*0.0, "--", color="black",
                linewidth=lw)

        for i in range(bands["nbands"]):
            ax.plot(bands["Krange"], bands["bands"][:, 0, i] - bands["Efermi"], linewidth=lw)

        xticks = []
        xticks_labels = []
        inv_dico = {}
        for k, v in bands["pts"].items():
            for E in v:
                xticks.append(E)
                if k in ["Gamma"]:
                    xticks_labels.append(r"$\Gamma$")
                else:
                    xticks_labels.append(r"$" + k + "$")

        ax.set_xticks(np.array(xticks))
        ax.set_xticklabels(xticks_labels, rotation='horizontal')
        ax.set_ylim(Elim)


      #####################################
        #     FFT of the data from TDDFT    #
        #####################################
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def calculate_polarizability(t, P, E_ext):
  """
  Calculate the polarizability in frequency and time domain.
  Input parameters:
  -----------------
    t (1D numpy array): time variable in eV^{-1}
    P (2D numpy array): suceptibility in freqency domain, directly from
                      the data of TDDFT.
    E_ext (2D numpy array): the external field in time domain
  Output parameters:
  ------------------
    w (1D numpy array): frequency (in eV), fourier variable of t
    E_ext_w (2D numpy array): FT of E_ext
    signal_w (2D array): dot product of P and E_ext_w
    signal_t (2D array): FT of signal_w
  """
  from ase.mbpt_lcao_utils.FFT_routines import array2matrice
  E_ext_w = np.zeros(E_ext.shape, dtype=complex)
  for i in range(E_ext.shape[1]):
    w, E_ext_w[:, i] = mod_iFFT_1D(t, E_ext[:, i])

  chi = array2matrice(P, dtype=complex)
  signal_w = np.zeros(E_ext_w.shape, dtype = complex)
  for i in range(E_ext.shape[0]):
    signal_w[i, :] = np.dot(chi[i, :, :], E_ext_w[i, :])

  signal_t = np.zeros(E_ext_w.shape, dtype = complex)
  for i in range(E_ext.shape[1]):
    t, signal_t[:, i] = mod_FFT_1D(w, signal_w[:, i])

  return w, E_ext_w, signal_w, signal_t

#####################################################3

def create_folder(name, verb = True):
    #function which create the folder name
    try:
        import os
        print_verbose('mkdir ' + name, verb)
        os.mkdir(name)
    except OSError:
        pass

#####################################################3

def print_verbose(msg, verb):
    if verb:
        print(msg)
