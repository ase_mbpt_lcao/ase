from __future__ import division
import numpy as np

try:
  import enthought.mayavi.mlab as mlab
except ImportError:
  import mayavi.mlab as mlab
  
#
#
#
class mayavie_movie:
  def __init__(self, args, prop, x):

    self.x = x
    self.prop = prop

    mlab.figure(bgcolor=(1., 1., 1.), fgcolor=(0., 0., 0.), size=self.prop.maya_prop['figsize'])

    self.x.xmesh = x.xmesh*prop.bohr_rad
    self.x.ymesh = x.ymesh*prop.bohr_rad
    self.x.zmesh = x.zmesh*prop.bohr_rad
    
    for i in range(len(self.x.box)):
      for j in range(len(self.x.box[i])):
        self.x.box[i][j] = self.x.box[i][j]*prop.bohr_rad
    
    self.ext = np.array([self.x.box[0][0], self.x.box[1][0], self.x.box[0][1], 
                         self.x.box[1][1], self.x.box[0][2], self.x.box[1][2]])
    
    #print(self.x.xmesh.shape)
    #print(self.x.ymesh.shape)
    #print(self.x.zmesh.shape)
    #print(self.x.Array[0, :, :, :].shape)
    
    atom = self.x.atoms
    self.pos = atom.get_positions()
    #vmin = np.min(self.x.Array)
    #vmax = np.max(self.x.Array)

    for k in range(self.x.Array.shape[0]):
      mlab.clf() # clear the figure (to reset the colors)
      mlab.contour3d(self.x.xmesh, self.x.ymesh, self.x.zmesh, self.x.Array[k, :, :, :].T, 
                          contours=self.prop.maya_prop['contours'], opacity=self.prop.maya_prop['opacity'],
                          extent=self.ext, colormap = prop.cmap)
      
      mlab.view(distance=self.prop.maya_cam['distance'], focalpoint=self.prop.maya_cam['focalpoint'],
              azimuth=self.prop.maya_cam['azimuth'], elevation=self.prop.maya_cam['elevation'],
              roll=self.prop.maya_cam['roll'], reset_roll=self.prop.maya_cam['reset_roll'], figure=self.prop.maya_cam['figure'])

      if k <10:
        mlab.savefig('images-maya/fig00{0}.obj'.format(k))
      elif k <100 and k >9:
        mlab.savefig('images-maya/fig0{0}.obj'.format(k))
      else:
        mlab.savefig('images-maya/fig{0}.obj'.format(k))

    #print('end mayavi')
   
  #
  #
  #
  def make_frame(self, t):
    from ase.data.colors import cpk_colors
    mlab.clf() # clear the figure (to reset the colors)
    
    mlab.contour3d(self.x.xmesh, self.x.ymesh, self.x.zmesh, 
                         self.x.Array[int(t), :, :, :].T, contours=self.prop.maya_prop['contours'], 
                         opacity=self.prop.maya_prop['opacity'], extent=self.ext)
    
    for pos, Z in zip(self.x.atoms.positions, self.x.atoms.numbers):
      mlab.points3d(pos[0]+self.x.origin*self.prop.bohr_rad, pos[1]+self.x.origin*self.prop.bohr_rad,\
        pos[2]+self.x.origin*self.prop.bohr_rad, color=tuple(cpk_colors[Z]), 
        opacity = 1.0, scale_factor=self.prop.maya_prop['atoms_scale'], resolution=self.prop.maya_prop['atoms_resolution'])

   
      mlab.view(distance=self.prop.maya_cam['distance'], focalpoint=self.prop.maya_cam['focalpoint'],
              azimuth=self.prop.maya_cam['azimuth'], elevation=self.prop.maya_cam['elevation'],
              roll=self.prop.maya_cam['roll'], reset_roll=self.prop.maya_cam['reset_roll'], figure=self.prop.maya_cam['figure'])

  
    return mlab.screenshot(antialiased=True)

        #############################
        #   Mayavi Plotting part    #
        #############################

def plot_maya(args, prop, data):
  import ase.io as io
  from ase.data.colors import cpk_colors
  #plot vector field

  data.xmesh = data.xmesh*prop.bohr_rad
  data.ymesh = data.ymesh*prop.bohr_rad
  data.zmesh = data.zmesh*prop.bohr_rad
  if args.time == 1:
    x = data.Array[args.time_num, 0, :, :, :]
    y = data.Array[args.time_num, 1, :, :, :]
    z = data.Array[args.time_num, 2, :, :, :]
  else:
    x = data.Array[:, :, :, 0]
    y = data.Array[:, :, :, 1]
    z = data.Array[:, :, :, 2]

  mlab.figure(bgcolor=(1., 1., 1.), fgcolor=(0., 0., 0.), size=(1280, 960))
  src = mlab.pipeline.vector_field(data.xmesh, data.ymesh, data.zmesh, x, y, z)
  vectors = mlab.pipeline.vectors(src, mask_points=20, scale_factor=3.)

  vectors.glyph.mask_input_points = True
  vectors.glyph.mask_points.on_ratio = 2500


  magnitude = mlab.pipeline.extract_vector_norm(src)
  mlab.pipeline.iso_surface(magnitude, opacity=prop.maya_prop['opacity'], transparent=True, contours=10)

  S = list()
  mlab.yaw(25)
  mlab.move(5, 0.5, 0.5)
  mlab.pitch(10)

  mlab.yaw(-40)
  mlab.move(5, 0.5, 0.5)
  mlab.pitch(10)

  atoms = io.read(prop.fatoms)
  
  for pos, Z in zip(atoms.positions, atoms.numbers):
    S.append(mlab.points3d(pos[0]+data.origin*prop.bohr_rad, pos[1]+data.origin*prop.bohr_rad,\
        pos[2]+data.origin*prop.bohr_rad, color=tuple(cpk_colors[Z]),\
        opacity = 1.0, scale_factor=2, resolution=16))
  
  #mlab.savefig(prop.folder + '/' + prop.figname + '.' + prop.format_output)
  mlab.show()
  #if prop.animation == 1:
  #  a = anim(prop)
  #  mlab.show()
  #  create_video(prop.folder + '/efield.avi', prop.folder + '/anim/')
  #else:
  #  #mlab.show()

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#@mlab.animate(delay=50, ui=False)
def anim(prop):
  f = mlab.gcf()
  comp = 100
  while 1:
    comp = comp + 1
    f.scene.camera.azimuth(1)
    f.scene.render()
    save_anim(comp, prop)
    yield

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def save_anim(nb, prop):
  create_folder(prop.folder + '/anim')
  fname = prop.folder + '/anim/pic_{0}.jpg'.format(nb)

  mlab.savefig(fname)

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def create_video(vname, folder):
  from ase.mbpt_lcao_utils.Read_data import run
  pic = folder + '/*.jpg'
  run('mencoder mf://' + pic + ' -o ' + vname + ' -ovc lavc -lavcopts vcodec=mjpeg')


#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def kill_elements_array_cyl(data, r, axis, shift=np.array([0.0, 0.0])):
  """
  set to 0 all the element of the array arr that are inside a cylinder
  of radius r (in Ang) along the axis.
  """
  
  if axis == 'x':
    for j in range(data.Array.shape[1]):
      for k in range(data.Array.shape[2]):
        vec = Array_ind_to_pts_coord(data, np.array([j, k]), axis = axis) - shift
        rho = np.sqrt(vec[0]**2 + vec[1]**2)*0.529
        if rho < r:
          for i in range(data.Array.shape[0]):
            data.Array[i, j, k] = 0
  elif axis == 'y':
    for i in range(data.Array.shape[0]):
      for k in range(data.Array.shape[2]):
        vec = Array_ind_to_pts_coord(data, np.array([i, k]), axis = axis)-shift
        rho = np.sqrt(vec[0]**2 + vec[1]**2)*0.529
        if rho < r:
          for j in range(data.Array.shape[1]):
            data.Array[i, j, k] = 0
 
  elif axis == 'z':
    for i in range(data.Array.shape[0]):
      for j in range(data.Array.shape[1]):
        vec = Array_ind_to_pts_coord(data, np.array([i, j]), axis = axis) -shift
        rho = np.sqrt(vec[0]**2 + vec[1]**2)*0.529
        if rho < r:
          for k in range(data.Array.shape[2]):
            data.Array[i, j, k] = 0
  else:
    raise ValueError("axis can be only x, y, or z")
            

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def triangular_mesh(args, prop, extent, mlab_fig=None):
  
  if mlab_fig is None:
    mlab.figure(bgcolor=(0., 0., 0.), fgcolor=(1., 1., 1.), size=(1280, 960))

  vmax = np.max(abs(args.kwargs['scalars']))
  vmin = -vmax

  mlab.triangular_mesh(args.kwargs['vertex'][:, 0], args.kwargs['vertex'][:, 1], args.kwargs['vertex'][:, 2], 
                       args.kwargs['facet'], scalars=-args.kwargs['scalars'], extent=extent,
                       mode='2dtriangle', colormap=prop.cmap, vmin = vmin, vmax=vmax,
                       resolution=prop.maya_prop['atoms_resolution'], scale_factor=0.5, scale_mode='scalar')
  
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def volumetric(args, prop, x):
  from ase.data.colors import cpk_colors

  mlab.figure(bgcolor=(0., 0., 0.), fgcolor=(1., 1., 1.), size=(1280, 960))
  mlab.clf()

  x.xmesh = x.xmesh*prop.bohr_rad
  x.ymesh = x.ymesh*prop.bohr_rad
  x.zmesh = x.zmesh*prop.bohr_rad
  
  for i in range(len(x.box)):
    for j in range(len(x.box[i])):
      x.box[i][j] = x.box[i][j]*prop.bohr_rad
  
  #ext = [x.box[0][0], x.box[1][0], x.box[0][1], x.box[1][1], x.box[0][2], x.box[1][2]]
  
  #print(x.xmesh.shape)
  #print(x.ymesh.shape)
  #print(x.zmesh.shape)
  #print(x.Array.shape)
  #org radius 7.5 Bohr
  print('b = ', args.tem_input['b'])
#  kill_elements_array_cyl(x, 15*0.529, 'z', shift=args.tem_input['b'][0:2])
  #vmax = np.max(abs(x.Array))
  #print('vmax = ', vmax)
  #print('original vmax = ', np.max(abs(x.Array)))
  x.Array = np.sign(x.Array)*(abs(x.Array)+1)**10
  if prop.vmax is None:
    vmax = abs(min(np.max(x.Array), np.min(x.Array)))
    #vmax = np.max(abs(x.Array))
  else:
    vmax = prop.vmax

  if prop.vmin is None:
    vmin = -vmax
  else:
    vmin = prop.vmin
  #print('new vmax = ', vmax, vmin)

 
  atoms = x.atoms

  for pos, Z in zip(atoms.positions, atoms.numbers):
    mlab.points3d(pos[0]+x.origin*prop.bohr_rad, pos[1]+x.origin*prop.bohr_rad,\
        pos[2]+x.origin*prop.bohr_rad, color=tuple(cpk_colors[Z]), opacity = 1.0, 
             scale_factor=prop.maya_prop['atoms_scale'], resolution=prop.maya_prop['atoms_resolution'])

  source = mlab.pipeline.scalar_field(x.xmesh, x.ymesh, x.zmesh, x.Array, colormap = prop.cmap)
  vol = mlab.pipeline.volume(source, vmin=vmin/2, 
                                     vmax=vmax)
  mlab.colorbar(object=vol, title=prop.title, orientation='vertical')
#plot electron traj
  zmax = 35.0

  x = np.array([args.tem_input['b'][0], args.tem_input['b'][0]])
  y = np.array([args.tem_input['b'][1], args.tem_input['b'][1]])
  z = np.array([-zmax, zmax])
  mlab.plot3d(x*0.529, y*0.529, z, color = (1.0, 0.0, 0.0), line_width=5.0, tube_radius=0.25)
   
  mlab.orientation_axes()
  mlab.view()
  #print('view = ', v)
  mlab.view(azimuth=prop.maya_cam['azimuth'], elevation=prop.maya_cam['elevation'], distance=prop.maya_cam['distance'])
  prop.mayavi_screenshot = mlab.screenshot(antialiased=True)
  #if prop.animation == 1:
  #  a = anim(prop)
  #  mlab.show()
  #  create_video(prop.folder + args.quantity + '.avi', prop.folder + '/anim/')
  if prop.show == 1:
    mlab.show()
  else:
    mlab.close()

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


def contour(args, prop, x):
  from ase.data.colors import cpk_colors

  mlab.figure(bgcolor=(1., 1., 1.), fgcolor=(0., 0., 0.), size=prop.figsize)

  x.xmesh = x.xmesh*prop.bohr_rad
  x.ymesh = x.ymesh*prop.bohr_rad
  x.zmesh = x.zmesh*prop.bohr_rad
  
  for i in range(len(x.box)):
    for j in range(len(x.box[i])):
      x.box[i][j] = x.box[i][j]*prop.bohr_rad
  
  ext = [x.box[0][0], x.box[1][0], x.box[0][1], x.box[1][1], x.box[0][2], x.box[1][2]]
  
  #print(x.xmesh.shape)
  #print(x.ymesh.shape)
  #print(x.zmesh.shape)
  #print(x.Array.shape)
#  kill_elements_array_cyl(x, 15*0.529, 'z', shift=args.tem_input['b'][0:2])
  #print('original vmax = ', np.max(abs(x.Array)))
  if prop.vmax is None:
    vmax = abs(min(np.max(x.Array), np.min(x.Array)))
  else:
    vmax = prop.vmax

  if prop.vmin is None:
    vmin = -vmax
  else:
    vmin = prop.vmin
  #print('new vmax = ', vmax)
  
  trans = 0.0
  atoms = x.atoms

  for pos, Z in zip(atoms.positions, atoms.numbers):
    #plot only real atoms not ghost
    if Z > 0:
        mlab.points3d(pos[0]+x.origin*prop.bohr_rad, pos[1]+x.origin*prop.bohr_rad-trans,\
            pos[2]+x.origin*prop.bohr_rad, color=tuple(cpk_colors[Z]), opacity = 1.0, 
             scale_factor=prop.maya_prop['atoms_scale'], resolution=prop.maya_prop['atoms_resolution'])

  if args.time == 1:
    obj = mlab.contour3d(x.xmesh, x.ymesh, x.zmesh, x.Array[args.time_num, :, :, :], opacity=prop.maya_prop['opacity'], 
                                extent=ext, colormap = prop.cmap)
  else:
    #print(x.xmesh.shape)
    #print(x.ymesh.shape)
    #print(x.zmesh.shape)
    #print(x.Array.shape)
    obj = mlab.contour3d(x.xmesh, x.ymesh-trans, x.zmesh, x.Array, contours=prop.maya_prop['contours'], 
                                opacity=prop.maya_prop['opacity'], line_width=prop.maya_prop['line_width'],
                                colormap = prop.cmap, vmin=vmin, vmax=vmax)

  mlab.colorbar(object=obj, title=prop.title, orientation='vertical')

  #atoms_nb = np.array([326, 314, 338])
  #from ase.calculators.siesta.pyiter.utils import get_radius_center_incircle
  #rad, cnt = get_radius_center_incircle(atoms.positions[atoms_nb[0], :], atoms.positions[atoms_nb[1], :],
  #                                          atoms.positions[atoms_nb[2], :])
  #print("center = ", cnt)
  #print("radius =", rad)

  #cylinder = plot_cylinder(rad, np.array([0.0, 0.0, 0.0]), resolution = 50, 
  #        height=20.0, angle = np.pi/2, rotation_axis=np.array([1.0, 1.0, 0.0]))
  #cylinder.actor.property.color = (0.0, 0.0, 1.0)
  #cylinder.actor.property.opacity = 0.5

  #plot a circle in 3D
#  t = np.arange(0.0, 2*np.pi, 0.01)
#  n = np.array([1.0, 1.0, 1.0])
#  n = n/np.sqrt(np.dot(n, n))
#
#  theta = np.arccos(n[2])
#  phi = np.arcsin(n[1]/np.sin(theta))
#
#  u = np.array([-np.sin(phi), np.cos(phi), 0.0])
#  
#  # cross product
#  nXu = np.array([np.cos(theta)*np.cos(phi), np.cos(theta)*np.sin(phi), -np.sin(phi)])
#
#  # parametric equation circle in 3D
#  # http://demonstrations.wolfram.com/ParametricEquationOfACircleIn3D/
#  P = np.zeros((t.shape[0], 3), dtype=float)
#  for i in range(3):
#    P[:, i] = rad*np.cos(t)*u[i] + rad*np.sin(t)*nXu[i] + cnt[i]
#
#  mlab.plot3d(P[:, 0], P[:, 1], P[:, 2], color=(1.0, 0.0, 0.0), tube_radius=2.0)


  
  #plot electron traj
  if args.tem_iter =='tem':
    #plot_elec_traj(args, 0.0)
    plot_elec_traj(args, trans, coord_txt = prop.maya_prop['coord_txt']-np.array([0.0, trans]), 
                   text=prop.maya_prop['label'])
  
  mlab.orientation_axes()
  mlab.view()
  #print('view = ', v)
  mlab.view(azimuth=prop.maya_cam['azimuth'], elevation=prop.maya_cam['elevation'], 
            distance=prop.maya_cam['distance'], focalpoint=prop.maya_cam['focalpoint'])
  prop.mayavi_screenshot = mlab.screenshot(antialiased=True)
  if prop.notebook:
    prop.notebook_obj = obj
  else:
    #mlab.savefig(prop.figname+'.'+prop.format_output, magnification=prop.maya_prop['magnification'])
    if prop.show == 1:
      mlab.show()
    else:
      mlab.close()


#################################################################

def contour_tem_mattin(args, prop, x):
  from ase.data.colors import cpk_colors

  mlab.figure(bgcolor=(1., 1., 1.), fgcolor=(0., 0., 0.), size=prop.figsize)

  x.xmesh = x.xmesh*prop.bohr_rad
  x.ymesh = x.ymesh*prop.bohr_rad
  x.zmesh = x.zmesh*prop.bohr_rad
  
  for i in range(len(x.box)):
    for j in range(len(x.box[i])):
      x.box[i][j] = x.box[i][j]*prop.bohr_rad
  
  #ext = [x.box[0][0], x.box[1][0], x.box[0][1], x.box[1][1], x.box[0][2], x.box[1][2]]
  #print('ext', ext)
  tri_mesh_args = ['vertex', 'facet', 'scalars']
  
  #print(x.xmesh.shape)
  #print(x.ymesh.shape)
  #print(x.zmesh.shape)
  #print(x.Array.shape)
  #print('b = ', args.tem_input['b'])
  #print('original vmax = ', np.max(abs(x.Array)))

 
  #tri = False
  count_args = 0
  trans = 0.0
  for k in args.kwargs.keys():
    if k in tri_mesh_args:
      count_args = count_args + 1
  
  #print('count_args: ', count_args)
  #if count_args == len(tri_mesh_args):
  #  tri = True


  surf = get_scalars(args, x)
  #surf = np.sign(surf)*np.log(abs(surf)+1)
  ext_bis = [x.box[0][0], x.box[1][0], x.box[0][1]-trans, x.box[1][1]-trans, x.box[0][2], x.box[1][2]]

  if prop.vmax is None:
    vmax = abs(min(np.max(surf), np.min(surf)))
  else:
    vmax = prop.vmax

  if prop.vmin is None:
    vmin = -vmax
  else:
    vmin = prop.vmin
  #print('new vmax = ', vmax)
 
#  import ase
#
#  atm = ase.Atoms('H{0}'.format(args.kwargs['vertex'].shape[0]), positions=args.kwargs['vertex'])
#
#  atm.rotate('y', np.pi/2)
#  vert_bis = atm.get_positions()
  atoms = x.atoms

  for pos, Z in zip(atoms.positions, atoms.numbers):
    mlab.points3d(pos[0]+x.origin*prop.bohr_rad, pos[1]+x.origin*prop.bohr_rad,\
        pos[2]+x.origin*prop.bohr_rad, color=tuple(cpk_colors[Z]), opacity = 1.0, 
             scale_factor=prop.maya_prop['atoms_scale'], resolution=prop.maya_prop['atoms_resolution'])


  mlab.triangular_mesh(args.kwargs['vertex'][:, 0], args.kwargs['vertex'][:, 1]-trans, args.kwargs['vertex'][:, 2], 
                       args.kwargs['facet'], scalars=surf, extent=ext_bis,
                       mode='2dtriangle', colormap=prop.cmap, vmin = vmin, vmax=vmax,
                       resolution=prop.maya_prop['atoms_resolution'], scale_factor=0.5, scale_mode='scalar')
 
 
  #mlab.plot3d(x*0.529, y*0.529-trans, z, color = (1.0, 0.0, 0.0), line_width=5.0, tube_radius=0.25)
  plot_elec_traj(args, trans, coord_txt = prop.maya_prop['coord_txt']-np.array([0.0, trans]), text='')
 
#  if tri:
#    triangular_mesh(args, prop, ext, mfig)
#    #mlab.plot3d(x*0.529, y*0.529, z, color = (1.0, 0.0, 0.0), line_width=5.0, tube_radius=0.25)
#    plot_elec_traj(args, 0.0, coord_txt = prop.maya_prop['coord_txt'], text='')
  
  mlab.orientation_axes()
  mlab.view()
  #print('view = ', v)
  mlab.view(azimuth=prop.maya_cam['azimuth'], elevation=prop.maya_cam['elevation'], 
            distance=prop.maya_cam['distance'], focalpoint=prop.maya_cam['focalpoint'])
  prop.mayavi_screenshot = mlab.screenshot(antialiased=True)
  #if prop.animation == 1:
  #  a = anim(prop)
  #  mlab.show()
  #  create_video(prop.folder + args.quantity + '.avi', prop.folder + '/anim/')
  mlab.savefig(prop.figname, magnification=prop.maya_prop['magnification'])
  if prop.show == 1:
    mlab.show()
  else:
    mlab.close()


#################################################################

def get_scalars(args, x):

  surf=[]

  #print('facet.shape', args.kwargs['facet'].shape)
  for i in range(args.kwargs['facet'].shape[0]):
    vert1 = args.kwargs['vertex'][int(args.kwargs['facet'][i, 0])]
    vert2 = args.kwargs['vertex'][int(args.kwargs['facet'][i, 1])]
    vert3 = args.kwargs['vertex'][int(args.kwargs['facet'][i, 2])]

    coord = (vert1 + vert2 + vert3)/3
    ind = pts_coord_to_Array_ind(x, coord)

    surf.append(x.Array[ind[0], ind[1], ind[2]])

  return np.array(surf)

#################################################################

def plot_elec_traj(args, trans, coord_txt=np.array([0.0, 0.0]), text='', color=(0.0, 1.0, 0.0)):
  zmax = 35.0
  sf = 0.8
  color_txt = (0.0, 0.0, 0.0)
  if (args.tem_input['v'][0] == 1.0 and args.tem_input['v'][1] == 0.0 and args.tem_input['v'][2] == 0.0):
    x = np.array([-zmax, zmax])
    y = np.array([args.tem_input['b'][1], args.tem_input['b'][1]])*0.529 -trans
    z = np.array([args.tem_input['b'][2], args.tem_input['b'][2]])*0.529
    mlab.plot3d(x, y, z, color = color, line_width=5.0, tube_radius=0.25)
    mlab.points3d(x[0], y[0], z[0], color=color, resolution=16, scale_factor = sf)
    mlab.points3d(x[x.shape[0]-1], y[y.shape[0]-1], z[z.shape[0]-1], color=color, resolution=16, scale_factor = sf)

  elif (args.tem_input['v'][0] == 0.0 and args.tem_input['v'][1] == 1.0 and args.tem_input['v'][2] == 0.0):
    x = np.array([args.tem_input['b'][0], args.tem_input['b'][0]])*0.529
    y = np.array([-zmax, zmax])
    z = np.array([args.tem_input['b'][2], args.tem_input['b'][2]])*0.529-trans
    mlab.plot3d(x, y, z, color = color, line_width=5.0, tube_radius=0.25)
    mlab.points3d(x[0], y[0], z[0], color=color, resolution=16, scale_factor = sf)
    mlab.points3d(x[x.shape[0]-1], y[y.shape[0]-1], z[z.shape[0]-1], color=color, resolution=16, scale_factor = sf)

  elif (args.tem_input['v'][0] == 0.0 and args.tem_input['v'][1] == 0.0 and args.tem_input['v'][2] == 1.0):
    x = np.array([args.tem_input['b'][0], args.tem_input['b'][0]])*0.529
    y = np.array([args.tem_input['b'][1], args.tem_input['b'][1]])*0.529-trans
    z = np.array([-zmax, zmax])
    mlab.plot3d(x, y, z, color = color, line_width=5.0, tube_radius=0.25)
    mlab.points3d(x[0], y[0], z[0], color=color, resolution=16, scale_factor = sf)
    mlab.points3d(x[x.shape[0]-1], y[y.shape[0]-1], z[z.shape[0]-1], color=color, resolution=16, scale_factor = sf)

  else:
    raise ValueError('only x, y, and z direction implemented')
 
  mlab.text(coord_txt[0], coord_txt[1], text,  z=zmax, width=0.3, line_width = 1.5, color = color_txt)



#################################################################

def plot_dens_imp(param, obj_file=None):
  """
    INPUT PARAMETERS:
    -----------------
      param (dict): dictionnary containing all the necessary input parameters:
        the keys are:
         b: np.array, 
         figsize: (800, 600),
         freq: (np.array) array of the frequencies to plot, check into the input file, 
         vmax: (np.array) array of the vmax value for the contour in Mayavi (vmin=-vmax)
               should be the same shape than freq,
         fname: name of the ab-initio hdf5 file,
         Matt_name: list of the names of the files containing thez desity distribution from Mattin calculations,
         Matt_vert: (string) name of the file containing the vertex of the icosahedral structure from Mattin,
         Matt_facet: (string) name of the file containing the facet associated to the vertex of the icosahedral structure from Mattin,
         Matt_path: (string) path of the data of Mattin,
         titles: list of string containing the matplotlib title, same length than azimuth,
         azimuth: (np.array) array containing the different azimuth of the maya camera, 
         elevation: (np.array) array containing the different elevation of the maya camera (same shape than azimuth)
  """
  
  import matplotlib.ticker as ticker
  import matplotlib.image as mpimg
  from ase.calculators.siesta.mbpt_lcao_io import read_data
#mlab.options.offscreen = True
  r = read_data()
  r.args.quantity = 'dens'
  r.args.tem_iter = 'tem'
  r.args.tem_input['vnorm'] = 75
  r.args.tem_input['tem_method'] = 'N'
  r.args.ReIm = 'im'
  #print(r.args.tem_input['b'])
  #print(param['b'])
  r.args.tem_input['b'] = param['b']
  r.args.tem_input['v'] = param['v']

  r.prop.plan = 'x'
  r.prop.plan_coord = 3.58/0.529
  r.prop.plot = 'Mayavi'
  r.prop.plot_atm = False
  r.prop.maya_prop['atoms_scale'] = 1.5
  r.prop.maya_prop['atoms_resolution'] = 16
  r.prop.maya_prop['opacity'] = 1.0
  #r.prop.maya_cam['focalpoint'] = param['focalpoint']
  r.prop.maya_prop['magnification'] = 3
  r.prop.maya_prop['coord_txt'] = np.array([0.0, 38.0])
  r.prop.show = 0


#Mattin data
  #if obj_file is None:
  #  r.args.kwargs['vertex'] = np.loadtxt(param['Matt_path'] + param['Matt_vert'])*10.0/0.529
  #  r.args.kwargs['facet'] = np.loadtxt(param['Matt_path'] + param['Matt_facet'])[:, 0:3] - 1
  #else:
  #  r.args.kwargs['vertex'], r.args.kwargs['facet'] = obj_reader(obj_file)
  #  r.args.kwargs['vertex'] = r.args.kwargs['vertex']*0.3

#RdBu
#BrBG
  r.prop.cmap = 'RdBu'
  #r.prop.cmap = 'cool'
#r.prop.cmap = 'seismic'

#mayavi colormaps
#'Accent' or 'Blues' or 'BrBG' or 'BuGn' or 'BuPu' or 'Dark2' or 'GnBu' or 'Greens' 
#or 'Greys' or 'OrRd' or 'Oranges' or 'PRGn' or 'Paired' or 'Pastel1' or 'Pastel2' 
#or 'PiYG' or 'PuBu' or 'PuBuGn' or 'PuOr' or 'PuRd' or 'Purples' or 'RdBu' or 'RdGy' 
#or 'RdPu' or 'RdYlBu' or 'RdYlGn' or 'Reds' or 'Set1' or 'Set2' or 'Set3' or 'Spectral' 
#or 'YlGn' or 'YlGnBu' or 'YlOrBr' or 'YlOrRd' or 'autumn' or 'binary' or 'black-white' 
#or 'blue-red' or 'bone' or 'cool' or 'copper' or 'file' or 'flag' or 'gist_earth' or 'gist_gray' 
#or 'gist_heat' or 'gist_ncar' or 'gist_rainbow' or 'gist_stern' or 'gist_yarg' or 'gray' 
#or 'hot' or 'hsv' or 'jet' or 'pink' or 'prism' or 'spectral' or 'spring' or 'summer' or 'winter'

  #figsize = (1.2*r.prop.figsize[0]*param['azimuth'].shape[0], 1.2*r.prop.figsize[1]*param['freq'].shape[0])
  #print(figsize)
  fig = plt.figure(1)
  fig.patch.set_visible(False)
  #bnorm = np.sqrt(np.dot(r.args.tem_input['b'], r.args.tem_input['b']))

  ft=20
  plot = 0
  label = ['a)', 'b)', 'c)', 'd)']
  for i, f in enumerate(param['freq']):
    r.args.plot_freq = f
    r.prop.vmax = param['vmax'][i]
    r.prop.vmin = param['vmin'][i]

    if param['Matt_name'][i] is not None:
      dens = np.loadtxt(param['Matt_path'] + param['Matt_name'][i])
      dens_mod = np.sign(dens[:, 1])*np.log(abs(dens[:, 1])+1)
      r.args.kwargs['scalars'] = dens_mod
    for j in range(param['azimuth'].shape[0]):
      plot = plot + 1
      ax = fig.add_subplot(param['freq'].shape[0], param['azimuth'].shape[0], plot)
      ax.xaxis.set_major_formatter(ticker.NullFormatter())
      ax.xaxis.set_minor_formatter(ticker.NullFormatter())
      ax.yaxis.set_major_formatter(ticker.NullFormatter())
      ax.yaxis.set_minor_formatter(ticker.NullFormatter())
      ax.axis('off')
      
      r.prop.figsize = param['figsize'][j]
      r.prop.maya_cam['distance'] = param['distance'][j] 
      r.prop.maya_cam['azimuth'] = param['azimuth'][j]
      r.prop.maya_cam['elevation'] = param['elevation'][j]
      r.prop.maya_prop['contours'] = get_contour(param['contours'], r.prop.vmax)#[vmax, vmax/1.5, vmax/3, vmin/3, vmin/1.5, vmin]
      r.prop.figname = 'fig_facet_freq_{0}_bx_{1: .3f}_azimuth_{2}_elevation_{3}.png'.format(f, param['b'][0], \
                param['azimuth'][j], param['elevation'][j])
      r.prop.maya_prop['label'] = label[i] + '      Imp = {0:.3f} Bohr'.format(param['b'][0])
      mbpt_lcao_plot(r, fname=param['fname'])
      #sys.exit()
      img=mpimg.imread(r.prop.figname)
      ax.imshow(img)
      #ax.axis([0.0, r.prop.figsize[0], r.prop.figsize[1], 0.0])
      
      if plot <= param['azimuth'].shape[0]:
        ax.set_title(param['titles'][plot-1], fontsize=ft)
      #if plot == 1 or plot == 4:
      #  ax.set_ylabel(r'$\omega = {0}$ (eV)'.format(f), fontsize=ft)

  #plt.tight_layout()
  fig.savefig('dn_im_'+param['geo']+'_bx_{0:.3f}Bohr.png'.format(param['b'][0]), format='png', dpi=600)
  plt.close(fig)

#################################################################

def plot_cylinder(radius, center, height=1.0, resolution = 25,
        rotation_axis = np.array([1.0, 0.0, 0.0]), angle = 0):
    from mayavi.sources.builtin_surface import BuiltinSurface
    from mayavi.modules.surface import Surface
    from mayavi.filters.transform_data import TransformData
    from ase.calculators.siesta.pyiter.utils import rotMat3D

    engine = mlab.get_engine()

    # Add a cylinder builtin source
    cylinder_src = BuiltinSurface()
    engine.add_source(cylinder_src)
    cylinder_src.source = 'cylinder'
    cylinder_src.data_source.center = center
    cylinder_src.data_source.radius = radius
    cylinder_src.data_source.height = height 
    cylinder_src.data_source.capping = False
    cylinder_src.data_source.resolution = resolution

    # Add transformation filter to rotate cylinder about an axis
    #transform_data_filter = TransformData()
    #engine.add_filter(transform_data_filter, cylinder_src)

    # Add transformation filter to rotate cylinder about an axis
    transform_data_filter = TransformData()
    engine.add_filter(transform_data_filter, cylinder_src)
    Rt = np.eye(4)
    Rt[0:3,0:3] = rotMat3D(rotation_axis, angle) # in homogeneous coordinates
    Rtl = list(Rt.flatten()) # transform the rotation matrix into a list

    transform_data_filter.transform.matrix.__setstate__({'elements': Rtl})
    transform_data_filter.widget.set_transform(transform_data_filter.transform)
    transform_data_filter.filter.update()
    transform_data_filter.widget.enabled = False   # disable the rotation control further.

    # Add surface module to the cylinder source
    cyl_surface = Surface()
    engine.add_filter(cyl_surface, transform_data_filter)
    
    # add color property
    #cyl_surface.actor.property.opacity = 0.5
    #cyl_surface.actor.property.color = (1.0, 0.0, 0.0)
    return cyl_surface

################################################################

def get_contour(cont, vmax):
    if len(cont) == 1:
        return cont[0]
    else:
        contour = []
        vmin = -vmax
        for i in range(len(cont)):
            contour.append(vmax*cont[i])
            contour.append(vmin*cont[i])
        return contour

