from __future__ import division
import numpy as np
from ase.mbpt_lcao_utils.utils import pts_coord_to_Array_ind, Array_ind_to_pts_coord
from ase.mbpt_lcao_utils.Read_data import read_data

#
#
#
def trajectory_elec(args, data, dt):
  """
  Calculate the electron trajectory incide the box
  """
  t = 0
  R0 = t*args.tem_input['vnorm']*args.tem_input['vdir'] + args.tem_input['b']
  
  while (R0[0]>data.box[0, 0] and R0[1]>data.box[0, 1] and R0[2]>data.box[0, 2]):
    t = t - dt
    R0 = t*args.tem_input['vnorm']*args.tem_input['vdir'] + args.tem_input['b']
  tmin = t+dt

  t = 0
  R0 = t*args.tem_input['vnorm']*args.tem_input['vdir'] + args.tem_input['b']
  while (R0[0]<data.box[1, 0] and R0[1]<data.box[1, 1] and R0[2]<data.box[1, 2]):
    t = t + dt
    R0 = t*args.tem_input['vnorm']*args.tem_input['vdir'] + args.tem_input['b']
  tmax = t-dt

  t = np.arange(tmin, tmax+dt, dt)
  r_elec = np.zeros((t.shape[0], 3), dtype=float)
  

  r_elec[:, 0] = t*args.tem_input['vnorm']*args.tem_input['vdir'][0] + args.tem_input['b'][0]
  r_elec[:, 1] = t*args.tem_input['vnorm']*args.tem_input['vdir'][1] + args.tem_input['b'][1]
  r_elec[:, 2] = t*args.tem_input['vnorm']*args.tem_input['vdir'][2] + args.tem_input['b'][2]

  return r_elec, t

#
#
#
def calc_force(rp, t, E_re, E_im):
  """
  Calculate the force acting on the electron trajectory F = q.E,
  where E is the induced field 
  """
  
  F = np.zeros(rp.shape, dtype=complex)

  for i in range(t.shape[0]):
    ind = pts_coord_to_Array_ind(E_re, rp[i, :])
    F[i, :] = E_re.Array[ind[0], ind[1], ind[2], :] + complex(0.0, 1.0)*E_im.Array[ind[0], ind[1], ind[2], :]

  return F

def calc_scatt_angle(args, V_re, V_im):
  """
  Calculate the scaterring angle of the electron using the classical formulation
  work only if the electron is going outside the sphere
  """

  R_min = np.sqrt(np.dot(args.tem_input['b'], args.tem_input['b']))
  theta = complex(0.0, 0.0)

  for i in range(V_re.Array.shape[0]):
    for j in range(V_re.Array.shape[1]):
      for k in range(V_re.Array.shape[2]):
        r = Array_ind_to_pts_coord(V_re, np.array([i, j, k]))
        rnorm = np.sqrt(np.dot(r, r))
        if rnorm > R_min:
          y2 = (1-complex(V_re.Array[i, j, k], V_im.Array[i, j, k])/(0.5*args.tem_input['vnorm']**2))*rnorm**2
          theta = theta + 1/(rnorm*np.sqrt(y2 - np.dot(args.tem_input['b'], args.tem_input['b'])))

  return theta*V_re.dr[0]*V_re.dr[1]*V_re.dr[2]

#
#
#
def Force_electron(freq, vnorm, vdir, b, dt = 0.1, fname = 'tddft_tem_output.hdf5'):
  """
  Calculate the force acting on the electron along its trajectory for a given frequency
  Input parameters:
  -----------------
    freq (float): frequency at which calculate the force in eV
    vnorm (float): electron velocity in au
    vdir (float array (size=3)): velocity direction vector
    b (float array (size=3)): impact parameter vector (au)
    dt (float, default: 0.1): time step
    fname (string, default: tddft_tem_output.hdf5): name of the input data, must be hdf5 file
  """

  data = read_data()
  data.args.quantity = "efield"
  data.args.tem_iter = 'tem'
  data.args.ReIm = 're'
  data.args.plot_freq = freq
  data.args.tem_input['vnorm'] = vnorm
  data.args.tem_input['vdir'] = vdir
  data.args.tem_input['b'] = b

  E_re = data.Read(YFname=fname)

  data.args.ReIm = 'im'
  E_im = data.Read(YFname=fname)

  r_elec, t = trajectory_elec(data.args, E_re, dt)
  F_elec = calc_force(r_elec, t, E_re, E_im)

  return F_elec, r_elec, t


#
#
#
def scattering_angle(args, fname = 'tddft_tem_output.hdf5'):
  """
  Calculate the force acting on the electron along its trajectory for a given frequency
  Input parameters:
  -----------------
    args


  theta = pi - 2b\int_{R}^{+\infty}\frac{dr}{r}\frac{1}{\sqrt{y**2 - b**2}}
  with
  y(r) = r**2(1-\frac{V(r)}{E})
  """

  data = read_data()
  data.args = args
  data.args.quantity = "potential"
  data.args.tem_iter = 'tem'

  data.args.ReIm = 're'
  V_re = data.Read(YFname=fname)

  data.args.ReIm = 'im'
  V_im = data.Read(YFname=fname)
  b = np.sqrt(np.dot(args.tem_input['b'], args.tem_input['b']))

  theta = calc_scatt_angle(data.args, V_re, V_im)

  return np.pi - 2*b*theta
