#creates the tddft_inp.lr and siesta.fdf files + other small functions

#import lybraries
from __future__ import division
import ase.mbpt_lcao_utils.readxyz as readxyz
import numpy as np
import re
import os


###############################################
#                                             #
#             Class tddft_inp!!!!             #
#                                             #
#   WARNING: This class is outdated           #
#     please refer to the class tddft_inp in  #
#     ase.calculators.mbpt_lcao.mbpt_lcao     #
###############################################


class tddft_inp:
  """
  Class use to generate and read tddft_lr.inp files
  contains:
    __init__
    write_tddft_inp
    read_tddft

  The init functions contains 3 variables:
    self.list_param_total: list of all parameters of the tddft_lr.inp file
    self.param: dictionnary of all the parameters of self.list_param_total, 
    fix the default value of each parameter
    self.list_param: list of the minimum parameter use to write the tddft_lr.inp file
  """
  def __init__(self):
    
    self.list_param_total = ['nff', 'SystemLabel', 'iv', 'io_units', 'check_basis', 'prod_basis_type',\
        'gen_aug_lmult', 'solver_type', 'xc_spin_dens_method', 'xc_functional', 'xc_authors', 'xc_ord_lebedev',\
        'xc_ord_gl', 'xc_output_type', 'nr', 'akmx', 'eigmin_local', 'eigmin_bilocal', 'omega_max_win2', 'omega_min',\
        'omega_max', 'solver', 'gmres_krylov', 'gmres_eps', 'gmres_itermax', 'gmres_restart', 'gmres_verbose',\
        'gmres_criterion', 'rmax', 'hartree_use_mult', 'ihartree', 'iexchange', 'icorrelation', 'omega_max_win1',\
        'd_omega_win1', 'freq_eps_win1', 'omega_min_tddft', 'omega_max_tddft', 'ext_field_direction', 'comp_dens_chng_and_polarizability',\
        'enh_given_coordinate_freq_set', 'enh_given_segment_and_freq', 'enh_given_volume_and_freq', 'dens_chng_given_volume_and_freq_average',\
        'dens_chng_average_freq_min', 'dens_chng_average_freq_max', 'store_dens_chng', 'plot_inter', 'plot_nonin', 'dr',\
        'enh_coord', 'box_dens_chng_x1', 'box_dens_chng_y1', 'box_dens_chng_z1', 'box_dens_chng_x2', 'box_dens_chng_y2', 'box_dens_chng_z2',\
        'box_potential_x1', 'box_potential_y1', 'box_potential_z1', 'box_potential_x2', 'box_potential_y2', 'box_potential_z2', 'enh_format_output',\
        'chi0_v_algorithm', 'occ_diff_max', 'diag_hs', 'plot_freq', 'do_tddft_iter', 'do_tddft_tem', 'vmin', 'vmax',\
        'dv', 'vdir', 'bmin_part', 'bmax_part', 'db_part', 'species_dico', 'specie_algo', 'species_iter', 'gwa_initialization',\
        'save_enh_density', 'save_enh_potential', 'save_enh_efield', 'save_enh_intensity', 'tem_wmax', 'dt']

    self.param = dict()
    self.param['do_tddft_iter'] = 1
    self.param['do_tddft_tem'] = 1
    self.param['nff'] = 1000
    self.param['SystemLabel'] = 'siesta'
    self.param['iv'] = 1
    self.param['io_units'] = 'eV'
    self.param['check_basis'] = 1
    self.param['prod_basis_type'] = 'MIXED'
    self.param['gen_aug_lmult'] = 0
    self.param['solver_type'] = 3
    self.param['xc_spin_dens_method'] = 1
    self.param['xc_functional'] = 'LDA'
    self.param['xc_authors'] = 'PZ'
    self.param['xc_ord_lebedev'] = 14
    self.param['xc_ord_gl'] = 48
    self.param['xc_output_type'] = 'FULLMAT'
    self.param['nr'] = 256
    self.param['akmx'] = 100
    self.param['eigmin_local'] = 1E-4
    self.param['eigmin_bilocal'] = 1E-5
    self.param['omega_max_win2'] = 0.0
    self.param['omega_min'] = -1.0
    self.param['omega_max'] = -4.0
    self.param['solver'] = 3
    self.param['gmres_krylov'] = 8
    self.param['gmres_eps'] = 1E-2
    self.param['gmres_itermax'] = 30
    self.param['gmres_restart'] = 100
    self.param['gmres_verbose'] = 0
    self.param['gmres_criterion'] = 3
    self.param['rmax'] = 20
    self.param['hartree_use_mult'] = 1
    self.param['ihartree'] = 0
    self.param['iexchange'] = 0
    self.param['icorrelation'] = 0
    self.param['omega_max_win1'] = 10.0
    self.param['d_omega_win1'] = 0.05
    self.param['omega_min_tddft'] = 0.0
    self.param['omega_max_tddft'] = 10.0
    self.param['ext_field_direction'] = 2
    self.param['comp_dens_chng_and_polarizability'] = 1
    self.param['enh_given_coordinate_freq_set'] = 0.0
    self.param['enh_given_segment_and_freq'] = 0.0
    self.param['enh_given_volume_and_freq'] = 0.0
    self.param['dens_chng_given_volume_and_freq_average'] = 0.0
    self.param['dens_chng_average_freq_min'] = 0.0
    self.param['dens_chng_average_freq_max'] = 10.0
    self.param['store_dens_chng'] = 0
    self.param['plot_inter'] = 0
    self.param['plot_nonin'] = 0
    self.param['dr'] = np.array([0.5, 0.5, 0.5])
    self.param['enh_coord'] = np.array([0, 0, 0])
    self.param['box_dens_chng_x1'] = -20
    self.param['box_dens_chng_y1'] = -20
    self.param['box_dens_chng_z1'] = -20
    self.param['box_dens_chng_x2'] = 20
    self.param['box_dens_chng_y2'] = 20
    self.param['box_dens_chng_z2'] = 20
    self.param['box_potential_x1'] = -20
    self.param['box_potential_y1'] = -20
    self.param['box_potential_z1'] = -20
    self.param['box_potential_x2'] = 20
    self.param['box_potential_y2'] = 20
    self.param['box_potential_z2'] = 20
    self.param['enh_format_output'] = 'hdf5'
    self.param['chi0_v_algorithm'] = 1
    self.param['occ_diff_max'] = 1E-4
    self.param['vmin'] = 1.0
    self.param['vmax'] = 1.0
    self.param['dv'] = 0.0
    self.param['vdir'] = np.array([0.0, 0.0, 1.0])
    self.param['bmin_part'] = np.array([0.0, 0.0, 0.0]) 
    self.param['bmax_part'] = np.array([0.0, 10.0, 0.0])
    self.param['db_part'] = np.array([0.0, 1.0, 0.0])
    self.param['tem_wmax'] = 20.0
    self.param['dt'] = 0.1
    self.param['species_dico'] = {}
    self.param['specie_algo'] = 0
    self.param['species_iter'] = {}
    self.param['gwa_initialization'] = 'SIESTA_PB'
    self.param['save_enh_density'] = 1
    self.param['save_enh_potential'] = 1
    self.param['save_enh_efield'] = 1
    self.param['save_enh_intensity'] = 1

    self.param['diag_hs'] = 1
    self.param['plot_freq'] = 0.0
    self.param['freq_eps_win1'] = 0.15#self.param['omega_max_win1']/self.param['nff']

    self.list_param = ['prod_basis_type', 'solver_type', 'gmres_eps', 'gmres_itermax',\
        'gmres_restart', 'gmres_verbose', 'xc_ord_lebedev', 'xc_ord_gl', 'nr', 'akmx', 'eigmin_local',\
        'eigmin_bilocal', 'nff', 'd_omega_win1', 'ext_field_direction', 'dr']


  def set_tddft_param(self, param, value):
    
    self.param[param] = value

    if param not in self.list_param:
      self.list_param.append(param)


  def write_tddft_inp(self):
    f = open('tddft_lr.inp', 'w')

    for i in self.list_param:
      if type(self.param[i]) == type(np.array([0.5, 0.5, 0.5])):
        f.write(i + '   {0}    {1}    {2}\n'.format(self.param[i][0], self.param[i][1], self.param[i][2]))
      elif type(self.param[i]) == type('string'):
        f.write(i + '     ' + self.param[i] + '\n')
      elif i == 'group_species' or i == 'species_iter':
        gp = '{'
        for k, v in self.param[i].items():
          gp = gp + '{0}: ['.format(k)
          for w in range(len(v)-1):
            gp = gp + str(v[w]) + ', '
          if k < max(self.param[i].keys()):
            gp = gp + str(v[len(v)-1]) + '], '
          else:
            gp = gp + str(v[len(v)-1]) + ']'

        gp = gp + '}\n'
        f.write(i+'     ' + gp)
      else:
        f.write(i + '     {0}\n'.format(self.param[i]))

    f.close()
        
      
  def read_tddft(self):
    L = readxyz.read_file('tddft_lr.inp')
    numeric_const_pattern = r"""
    [-+]? # optional sign
    (?:
      (?: \d* \. \d+ ) # .1 .12 .123 etc 9.1 etc 98.1 etc
      |
      (?: \d+ \.? ) # 1. 12. 123. etc 1 12 123 etc
    )
    # followed by optional exponent part if desired
    (?: [Ee] [+-]? \d+ ) ?
    """

    rx = re.compile(numeric_const_pattern, re.VERBOSE)

    for i in L:
      for j in self.param:
        if i[0:len(j)] == j:
          if type(self.param[j]) == type(1):
            self.param[j] = int(rx.findall(i)[0])
          elif type(self.param[j]) == type(1.0):
            self.param[j] = float(rx.findall(i)[0])
          else:
            p = readxyz.delete_blanc(i[len(j): len(i)])
            self.param[j] = p[0:len(p)-1]
            if self.param[j][0] == '_':
              p = readxyz.delete_blanc(self.param[j][len(self.param[j]) - 5:len(self.param[j])])
              self.param[j] = p[0:len(p)]
  
  def read_tddft_out(self, fname, argument):
    L = readxyz.read_file(fname)
    numeric_const_pattern = r"""
    [-+]? # optional sign
    (?:
      (?: \d* \. \d+ ) # .1 .12 .123 etc 9.1 etc 98.1 etc
      |
      (?: \d+ \.? ) # 1. 12. 123. etc 1 12 123 etc
    )
    # followed by optional exponent part if desired
    (?: [Ee] [+-]? \d+ ) ?
    """

    rx = re.compile(numeric_const_pattern, re.VERBOSE)

    for i in L:
      if i[0:len(argument)] == argument:
        value = float(rx.findall(i)[0])

    return value


#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#############################################
#                                           #
#         Class Siesta!!!                   #
#                                           #
#   WARNING: This class is outdated         #
#     please refer to the class Siesta in   #
#     ase.calculators.siesta                #
#############################################

class siesta:
  """
  Class that write and read siesta.fdf files. (read not implemented yet).
  All the siesta parameter are save in the file list_param_siesta_total.txt.
  Then the parameter are also save in different .txt files, as function of them nature.
  Every siesta parameter are save in the dictionnary self.param
  """
  def __init__(self):

    self.fname = ''
    self.label_list = dict()
    home = find_user_folder()
    
    #file_list_tot = '/home/barbry/orgavolt/mbpt_domiprod.testing/tests/gwa/trunk/examples/plotting/pyiter/list_param_siesta_total.txt'
    #file_list_logic = '/home/barbry/orgavolt/mbpt_domiprod.testing/tests/gwa/trunk/examples/plotting/pyiter/list_param_siesta_logic.txt'
    #file_list_dic = '/home/barbry/orgavolt/mbpt_domiprod.testing/tests/gwa/trunk/examples/plotting/pyiter/list_param_siesta_dic.txt'
    #file_list_nb = '/home/barbry/orgavolt/mbpt_domiprod.testing/tests/gwa/trunk/examples/plotting/pyiter/list_param_siesta_nb.txt'
    #file_list_vec = '/home/barbry/orgavolt/mbpt_domiprod.testing/tests/gwa/trunk/examples/plotting/pyiter/list_param_siesta_vec.txt'
    #file_list_str = '/home/barbry/orgavolt/mbpt_domiprod.testing/tests/gwa/trunk/examples/plotting/pyiter/list_param_siesta_str.txt'
    
    file_list_tot = home + '/.pyfip/pyiter/list_param_siesta_total.txt'
    file_list_logic = home + '/.pyfip/pyiter/list_param_siesta_logic.txt'
    file_list_dic = home + '/.pyfip/pyiter/list_param_siesta_dic.txt'
    file_list_nb = home + '/.pyfip/pyiter/list_param_siesta_nb.txt'
    file_list_vec = home + '/.pyfip/pyiter/list_param_siesta_vec.txt'
    file_list_str = home + '/.pyfip/pyiter/list_param_siesta_str.txt'
    print(home + '/.pyfip/pyiter/list_param_siesta_dic.txt')
    
    self.list_param_total = readxyz.read_file_list(file_list_tot) 
    self.list_param_logic = readxyz.read_file_list(file_list_logic) 
    self.list_param_dic = readxyz.read_file_list(file_list_dic) 
    self.list_param_nb = readxyz.read_file_list(file_list_nb) 
    self.list_param_vec = readxyz.read_file_list(file_list_vec) 
    self.list_param_str = readxyz.read_file_list(file_list_str) 

    #input parameters
    self.param = dict()
    self.param['SystemName'] = 'Default'
    self.param['SystemLabel'] = 'siesta'
    self.param['NumberOfAtoms'] = 0
    self.param['NumberOfSpecies'] = 0

    self.param['PAO.BasisType'] = 'split'
    self.param['PAO.EnergyShift'] = {'Valeur':10, 'unit':'meV', 'quantity': 'energie'}
    self.param['PAO.BasisSize'] = 'DZP'
    self.param['PAO.BasisSizes'] = dict()
    self.param['PAO.SplitNorm'] = 0.15
    self.param['PAO.SplitNormH'] = 0.15
    self.param['PAO.NewSplitCode'] = '.false.'
    self.param['PAO.FixSplitTable'] = '.false.'
    self.param['PAO.SplitTailNorm'] = '.false.'
    self.param['PAO.SoftDefault'] = '.false.'
    self.param['PAO.SoftInnerRadius'] = 0.9
    self.param['PAO.SoftPotential'] = {'Valeur':40.0, 'unit':'Ry', 'quantity': 'energie'}

    self.param['User.Basis'] = '.false.'
    self.param['User.BasisNetCDF'] = '.false.'

    self.param['BasisPressure'] = {'Valeur':0.2, 'unit':'GPa', 'quantity': 'pressure'}

    self.param['Reparametrize.Pseudos'] = '.false.'
    self.param['New.A.Parameter'] = 0.001
    self.param['New.B.Parameter'] = 0.01
    if self.param['Reparametrize.Pseudos'] == '.false.':
      self.param['Rmax.Radial.Grid'] = 0.0
    else:
      self.param['Rmax.Radial.Grid'] = 50.0

    self.param['Restricted.Radial.Grid'] = '.true.'
    self.param['LatticeConstant'] = {'Valeur':1.0, 'unit':'Ang', 'quantity': 'length'}
    self.param['LatticeParameters'] = np.array([1.0, 1.0, 1.0, 90.0, 90.0, 90.0])
    self.param['LatticeVectors'] = np.array([[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]])
    self.param['SuperCell'] = np.array([[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]])
    
    self.param['AtomicCoordinatesFormat'] = 'Ang'
    self.param['AtomicCoorFormatOut'] = self.param['AtomicCoordinatesFormat']
    self.param['AtomicCoordinatesOrigin'] = np.array([0.0, 0.0, 0.0])

    self.param['WriteCoorXmol'] = '.false.'
    self.param['WriteCoorCerius'] = '.false.'
    self.param['WriteMDXmol'] = '.false.'

    self.param['MD.UseSaveXV'] = '.false.'
    self.param['MD.UseStructFile'] = '.false.'
    self.param['MD.UseSaveZM'] = '.false.'

    self.param['WarningMinimumAtomicDistance'] = {'Valeur':1.0, 'unit':'Bohr', 'quantity': 'length'}
    self.param['MaxBondDistance'] = {'Valeur':6.0, 'unit':'Bohr', 'quantity': 'length'}

    self.param['kgrid_cutoff'] = {'Valeur':0.0, 'unit':'Bohr', 'quantity': 'length'}
    self.param['kgrid_Monkhorst_Pack'] = np.array([[0.0, 0.0, 0.0, 0.0], [0.0, 0.0, 0.0, 0.0], [0.0, 0.0, 0.0, 0.0]])
    self.param['ChangeKgridInMD'] = '.false.'
    self.param['WriteKpoints'] = '.false.'

    self.param['XC.functional'] = 'LDA'
    self.param['XC.authors'] = 'PZ'

    self.param['SpinPolarized'] = '.false.'
    self.param['NonCollinearSpin'] = '.false.'
    self.param['FixSpin'] = '.false.'
    self.param['TotalSpin'] = 0.0
    self.param['SingleExcitation'] = '.false.'


    self.param['Haris_functional'] = ',false.'
    self.param['MaxSCFIterations'] = 50
    self.param['SCFMustConverge'] = '.false.'


    self.param['DM.MixingWeight'] = 0.25
    self.param['DM.NumberPulay'] = 0
    self.param['DM.Pulay.Avoid.First.After.Kick'] = '.false.'
    self.param['DM.PulayOnFile'] = '.false.'
    self.param['DM.NumberBroyden'] = 0
    self.param['DM.Broyden.Cycle.On.Maxit'] = '.true.'
    self.param['DM.Broyden.Variabla.Weight'] = '.true.'
    self.param['DM.NumberKick'] = 0
    self.param['DM.KickMixingWeight'] = 0.5
    self.param['DM.MixSCF1'] = '.false.'
    self.param['DM.UseSaveDM'] = '.false.'
    self.param['DM.FormattedFiles'] = '.false.'
    self.param['DM.FormattedInput'] = '.false.'
    self.param['DM.FormattedOutput'] = '.false.'
    self.param['DM.InitSpinAF'] = '.false.'
    self.param['DM.AllowReuse'] = '.true.'
    self.param['DM.AllowExtrapolation'] = '.true.'

    self.param['SCF.Read.Charge.NetCDF'] = '.true.'
    self.param['SCF.Read.Deformation.Charge.NetCDF'] = '.false.'

    self.param['WriteDM'] = '.true.'
    self.param['WriteDM.NetCDF'] = '.true.'
    self.param['WriteDMHS.NetCDF'] = '.true.'
    self.param['WriteDM.History.NetCDF'] = '.false.'
    self.param['WriteDMHS.History.NetCDF'] = '.false.'

    self.param['DM.Tolerance'] = 1e-4
    self.param['DM.Require.Energy.Convergence'] = '.false.'
    self.param['DM.Energy.Tolerance'] = {'Valeur':1e-4, 'unit':'eV', 'quantity': 'energie'}
    self.param['DM.Require.Harris.Convergence'] = '.false.'
    self.param['DM.Harris.Tolerance'] = {'Valeur':1e-4, 'unit':'eV', 'quantity': 'energie'}

    self.param['MeshCutOff'] = {'Valeur':100, 'unit':'Ry', 'quantity': 'energie'}
    self.param['MD.MaxForceTol'] = {'Valeur': 0.02, 'unit': 'eV/Ang', 'quantity': 'force'}
    self.param['MeshSubDivisions'] = 2
    self.param['EggboxScale'] = {'Valeur':1, 'unit':'eV', 'quantity': 'energie'}
    
    self.param['NeglNonOverlaptInt'] = '.false.'
    self.param['SaveHS'] = '.false.'
    self.param['FixAuxiliaryCell'] = '.false.'
    self.param['NaiveAuxiliaryCell'] = '.false.'
    self.param['SolutionMethod'] = 'diagon'
    self.param['NumberOfEigenStates'] = 0
    self.param['Use.New.Diagk'] = '.false.'
    self.param['Diag.DivideAndConquer'] = '.true.'
    self.param['Diag.AllInOne'] = '.false.'
    self.param['Diag.NoExpert'] = '.false.'
    self.param['Diag.PreRotate'] = 'false'
    self.param['Diag.Use2D'] = '.true.'
    
    self.param['WiteEigenvalues'] = '.false.'
    self.param['OccupationFunction'] = 'FD'
    self.param['OccupationMPOrder'] = 1
    self.param['ElectronicTemperature'] = {'Valeur':300.0, 'unit':'K', 'quantity': 'energie'}
    self.param['ON.functional'] = 'Kim'
    self.param['ON.MaxNumIter'] = 1000
    self.param['ON.etol'] = 1e-8
    self.param['ON.eta'] = {'Valeur':0.0, 'unit':'eV', 'quantity': 'energie'}
    self.param['ON.eta_alpha'] = {'Valeur':0.0, 'unit':'eV', 'quantity': 'energie'}
    self.param['ON.eta_beta'] = {'Valeur':0.0, 'unit':'eV', 'quantity': 'energie'}
    self.param['ON.RcLWF'] = {'Valeur':9.5, 'unit':'Bohr', 'quantity': 'length'}
    self.param['ON.ChemicalPotential'] = '.false.'
    self.param['ON.ChemicalPotentialUse'] = '.false.'
    self.param['ON.ChemicalPotentialRc'] = {'Valeur':9.5, 'unit':'Bohr', 'quantity': 'length'}
    self.param['ON.ChemicalPotentialTemperature'] = {'Valeur':0.05, 'unit':'Ry', 'quantity': 'energie'}
    self.param['ON.ChemicalPotentialOrder'] = 100
    self.param['ON.LowerMemory'] = '.false.'
    self.param['ON.UseSaveLWF'] = '.false.'
    
    self.param['OpticalCalculation'] = '.false.'
    self.param['Optical.EnergyMinimum'] = {'Valeur':0.0, 'unit':'Ry', 'quantity': 'energie'}
    self.param['Optical.EnergyMaximum'] = {'Valeur':10.0, 'unit':'Ry', 'quantity': 'energie'}
    self.param['Optical.broaden'] = {'Valeur':0.0, 'unit':'Ry', 'quantity': 'energie'}
    self.param['Optical.Scissor'] = {'Valeur':0.0, 'unit':'Ry', 'quantity': 'energie'}
    self.param['Optical.NumberOfBands'] = 10
    self.param['Optical.Mesh'] = np.array([0.0, 0.0, 0.0])
    self.param['Optical.OffsetMesh'] = '.false.'
    self.param['Optical.PolarizationType'] = 'polycrystal'
    self.param['Optical.Vector'] = np.array([0.0, 0.0, 0.0])
    
    self.param['COOP.Write'] = '.false.'

    #System with net charge or dipole and electric fields
    self.param['NetCharge'] = 0.0
    self.param['SimulateDoping'] = '.false.'
    self.param['ExternalElectricField'] = [0.0, 0.0, 0.0, 'V/Ang']
    self.param['SlabDipoleCorrection'] = '.false.'
    
    #Output of charge densities and potential on the grid
    self.param['SaveRho'] = '.false.'
    self.param['SaveDeltaRho'] = '.false.'
    self.param['SaveElectrostaticPotential'] = '.false.'
    self.param['SaveNeutralAtomPotential'] = '.false.'
    self.param['SaveTotalPotential'] = '.false.'
    self.param['SaveIonicCharge'] = '.false.'
    self.param['SaveTotalCharge'] = '.false.'
    self.param['SaveInitialChargeDensity'] = '.false.'

    #Parallel options
    self.param['BlockSize'] = 8
    self.param['ProcessorY'] = 1
    self.param['Diag.Memory'] = 1.0
    self.param['Diag.ParallelOverK'] = '.false.'

    #Parallel decompositions for O(N)
    self.param['UseDomainDecomposition'] = '.false.'
    self.param['UseSpatialDecomposition'] = '.false.'
    self.param['RcSpatial'] = 1.0

    #Efficiency option
    self.param['DirectPhi'] = '.false.'

    #Memory accounting options
    self.param['AllocReportLevel'] = 0

    #the catch-all option UseSaveData
    self.param['UseSaveData'] = '.false.'
    
    #output of information for Denchar
    self.param['WriteDenchar'] = '.false.'

    #Structural Relaxation
    self.param['MD.TypeOfRun'] = 'Verlet'
    self.param['MD.VariableCell'] = '.false.'
    self.param['MD.ConstantVolume'] = '.false.'
    self.param['MD.RelaxCellOnly'] = '.false.'
    self.param['MD.MaxForceTol'] = {'Valeur':0.04, 'unit':'eV/Ang', 'quantity': 'force'}
    self.param['MD.MaxStressTol'] = {'Valeur':1.0, 'unit':'GPa', 'quantity': 'pressure'}
    self.param['MD.NumCGsteps'] = 0
    self.param['MD.MaxCGDispl'] = {'Valeur':0.2, 'unit':'Bohr', 'quantity': 'length'}
    self.param['MD.PreconditionVariableCell'] = {'Valeur':5.0, 'unit':'Ang', 'quantity': 'length'}
    self.param['ZM.ForceTolLength'] = {'Valeur':0.00155574, 'unit':'Ry/Bohr', 'quantity': 'force'}
    self.param['ZM.ForceTolAngle'] = {'Valeur':0.00356549, 'unit':'Ry/rad', 'quantity': 'torque'}
    self.param['ZM.MaxDisplLength'] = {'Valeur':0.2, 'unit':'Bohr', 'quantity': 'length'}
    self.param['ZM.MaxDisplAngle'] = {'Valeur':0.003, 'unit':'rad', 'quantity': 'angle'}
    self.param['MD.UseSaveCG'] = '.false.'
    self.param['MD.Broyden.History.Steps'] = 5
    self.param['MD.Broyden.Cycle.On.Maxit'] = '.true.'
    self.param['MD.Broyden.Initial.Inverse.Jacobian'] = 1.0
    self.param['MD.FIRE.TimeStep'] = 1.0
    self.param['MD.Quench'] = '.false.'
    self.param['MD.FireQuench'] = '.false.'
    self.param['MD.TargetPressure'] = {'Valeur':0.0, 'unit':'GPa', 'quantity': 'pressure'}
    self.param['MD.TargetStress'] = np.array([-1.0, -1.0, -1.0, 0.0, 0.0, 0.0])
    self.param['MD.RemoveIntramolecularPressure'] = '.false.'
    self.param['GeometryConstraints'] = {}

    #Molecular dynamics
    self.param['MD.InitialTimeStep'] = 1
    self.param['MD.FinalTimeStep'] = 1
    self.param['MD.LengthTimeStep'] = {'Valeur':1.0, 'unit':'fs', 'quantity': 'time'}
    self.param['MD.InitialTemperature'] = {'Valeur':0.0, 'unit':'K', 'quantity': 'energie'}
    self.param['MD.TargetTemperature'] = {'Valeur':0.0, 'unit':'K', 'quantity': 'energie'}
    self.param['MD.NoseMass'] = {'Valeur':100.0, 'unit':'Ry*fs**2', 'quantity': 'MomInert'}
    self.param['MD.ParrinelloRahmanMass'] = {'Valeur':100.0, 'unit':'Ry*fs**2', 'quantity': 'MomInert'}
    self.param['MD.AnnealOption'] = 'TemperatureAndPressure'
    self.param['MD.TauRelax'] = {'Valeur':100.0, 'unit':'fs', 'quantity': 'time'}
    self.param['MD.BulkModulus'] = {'Valeur':100.0, 'unit':'Ry/Bohr**3', 'quantity': 'pressure'}

    #Projected density of stqte
    self.param['ProjectedDensityOfStates'] = {'Valeur': np.array([-20.0, 10.0, 0.2, 500]), 'unit':'eV', 'quantity':'energie'}

    #MullikenPopulation
    self.param['WriteMullikenPop'] = 0
    self.param['MullikenInSCF'] = '.false'

    #Output options for dynamics
    self.param['WriteCoorInitial'] = '.true.'
    self.param['WriteCoorStep'] = '.false.'
    self.param['WriteForces'] = '.false.'
    self.param['WriteMDhistory'] = '.false.'

    
    self.param['ChemicalSpeciesLabel'] = []
    self.param['AtomicMass'] = dict()
    self.param['LongOutput'] = '.false.'

    self.param['mass_unit'] = ['Kg', 'g', 'amu']
    self.param['length_unit'] = ['m', 'cm', 'nm', 'Ang', 'Bohr']
    self.param['time_unit'] = ['s', 'fs', 'ps', 'ns', 'mins', 'hours', 'days']
    self.param['energy_unit'] = ['J', 'erg', 'eV', 'meV', 'Ry', 'mRy', 'Hartree', 'mHartree', 'K', 'Kcal/mol',\
        'KJ/mol', 'Hz', 'THz', 'cm-1', 'cm**-1', 'cm^-1']
    self.param['force_unit'] = ['N', 'eV/Ang', 'Ry/Bohr']
    self.param['pressure_unit'] = ['Pa', 'MPa', 'GPa', 'atm', 'bar', 'Kbar', 'Mbar', 'Ry/Bohr**3', 'eV/Ang**3']
    self.param['charge_unit'] = ['C', 'e']
    self.param['dipole_unit'] = ['C*m', 'D', 'debye', 'e*Bohr', 'e*Ang']
    self.param['MomInert_unit'] = ['Kg*m**2', 'Ry*fs**2']
    self.param['Efield_unit'] = ['V/m', 'V/nm', 'V/Ang', 'V/Bohr', 'Ry/Bohr/e', 'Har/Bohr/e']
    self.param['angle_unit'] = ['deg', 'rad']
    self.param['torque_unit'] = ['eV/deg', 'eV/rad', 'Ry/deg', 'Ry/rad', 'meV/deg', 'meV/rad', 'mRy/deg', 'mRy/rad']

    self.list_param = ['SystemName', 'SystemLabel', 'NumberOfAtoms', 'NumberOfSpecies', 'ChemicalSpeciesLabel',\
        'AtomicCoordinatesFormat', 'AtomicCoordinatesAndAtomicSpecies']

    self.atoms_l = readxyz.Atoms_list()

    #output parameter
    self.spe = dict()
    self.species_charac = {}

  #End __init__ !!!!!

  def set_siesta_param(self, param, value=None, unit=None, quantity=None):
    
    if value != None:
      if param == 'GeometryConstraints':
        self.param[param] = value
      elif (type(self.param[param]) == dict):
        self.param[param]['Valeur'] = value
        if unit != None:
          self.param[param]['unit'] = unit
        if quantity != None:
          self.param[param]['quantity'] = quantity
      else:
        self.param[param] = value

    if param not in self.list_param:
      self.list_param.append(param)



  def build_ChemicalSpeciesLabel(self, label_list, atoms_ase):
    """
    Build the ChemicalSpeciesLabel parameter block of the siesta.fdf file
    """
    if atoms_ase == None:
      self.param['AtomicCoordinatesAndAtomicSpecies'] = readxyz.extractdata_xyz(self.fname)
      self.param['NumberOfAtoms'] = self.param['AtomicCoordinatesAndAtomicSpecies'][0]
    else:
      self.param['NumberOfAtoms'] = atoms_ase.positions.shape[0]
      self.param['AtomicCoordinatesAndAtomicSpecies'] = [atoms_ase.positions.shape[0]]
      symbols = atoms_ase.get_chemical_symbols()
      for i, pos in enumerate(atoms_ase.positions):
        self.param['AtomicCoordinatesAndAtomicSpecies'].append([symbols[i], pos])
    
    for i in label_list.keys():
      self.param['ChemicalSpeciesLabel'].append(i)
      self.atoms_l.atoms[i] = label_list[i]

    if len(label_list) < self.param['NumberOfAtoms']:
      for i in enumerate(self.param['AtomicCoordinatesAndAtomicSpecies'][1:self.param['NumberOfAtoms']+1]):
        if check_element_in_list(self.param['ChemicalSpeciesLabel'], i[1][0]) == False:
          self.param['ChemicalSpeciesLabel'].append(i[1][0])
    
    self.param['NumberOfSpecies'] = len(self.param['ChemicalSpeciesLabel'])

    
  def write_siesta_file(self, atoms_ase=None):
    """
    Write the siesta.fdf file with the input parameter enter in
    self.param
    """

    self.build_ChemicalSpeciesLabel(self.label_list, atoms_ase)

    for i in enumerate(self.param['ChemicalSpeciesLabel']):
      self.spe[i[1]] = i[0] +1
      

    f = open(self.param['SystemLabel'] + '.fdf', 'w')
    for i in self.list_param:
      if type(self.param[i]) == type(np.array([0.5, 0.5, 0.5])) and len(self.param[i].shape) == 1:
        f.write('\n')
        f.write('%block ')
        f.write(i + '\n')
        for j in self.param[i]:
          f.write('   {0}'.format(j))
        f.write('\n')
        f.write('%endblock ')
        f.write(i + '\n')
      elif type(self.param[i]) == type('string'):
        f.write(i + '     ' + self.param[i] + '\n')
      elif i == 'ChemicalSpeciesLabel':
        f.write('\n')
        f.write('%block ChemicalSpeciesLabel\n')
        for i in enumerate(self.param['ChemicalSpeciesLabel']):
          f.write('   {0}    {1}     '.format(self.spe[i[1]], self.atoms_l.atoms[i[1]]) + i[1] + '\n')
        
        f.write('%endblock ChemicalSpeciesLabel\n')
        f.write('\n')
        
      elif i == 'AtomicCoordinatesAndAtomicSpecies':
        f.write('\n')
        f.write('%block AtomicCoordinatesAndAtomicSpecies\n')
        for i in self.param['AtomicCoordinatesAndAtomicSpecies'][1:self.param['NumberOfAtoms']+1]:
          f.write('   {0}    {1}   {2}     {3}       '.format(i[1][0], i[1][1], i[1][2], self.spe[i[0]]) + i[0] + '\n')
        f.write('%endblock AtomicCoordinatesAndAtomicSpecies\n')
        f.write('\n')

      elif i == 'AtomicMass':
        f.write('\n')
        f.write('%block AtomicMass\n')
        for keys, values in self.param['AtomicMass'].items():
          f.write('   {0}    {1}\n'.format(self.spe[keys], values))
        f.write('%endblock AtomicMass\n')
        f.write('\n')

      elif i == 'ProjectedDensityOfStates':
        f.write('\n')
        f.write('%block ProjectedDensityOfStates\n')
        f.write('   {0}    {1}    {2}   {3}   '.format(self.param['ProjectedDensityOfStates']['Valeur'][0],\
                        self.param['ProjectedDensityOfStates']['Valeur'][1], self.param['ProjectedDensityOfStates']['Valeur'][2],\
                        int(self.param['ProjectedDensityOfStates']['Valeur'][3])) + self.param['ProjectedDensityOfStates']['unit'] + '\n')
        f.write('%endblock ProjectedDensityOfStates\n')
        f.write('\n')

      elif i == 'PAO.BasisSizes':
        f.write('\n')
        f.write('%block PAO.BasisSizes\n')
        for keys, values in self.param['PAO.BasisSizes'].items():
          f.write('   ' + keys+ '     ' + values + '\n')
        f.write('%endblock PAO.BasisSizes\n')
        f.write('\n')

      elif check_element_in_list(self.list_param_dic, i):
        f.write('\n')
        f.write(i + '     {0}   '.format(self.param[i]['Valeur']) + self.param[i]['unit'] + '\n')
      
      elif i == 'LatticeVectors':
        f.write('\n')
        f.write('%block LatticeVectors\n')
        f.write(' {0}   {1}    {2}\n'.format(self.param['LatticeVectors'][0][0], self.param['LatticeVectors'][0][1], self.param['LatticeVectors'][0][2]))
        f.write(' {0}   {1}    {2}\n'.format(self.param['LatticeVectors'][1][0], self.param['LatticeVectors'][1][1], self.param['LatticeVectors'][1][2]))
        f.write(' {0}   {1}    {2}\n'.format(self.param['LatticeVectors'][2][0], self.param['LatticeVectors'][2][1], self.param['LatticeVectors'][2][2]))
        f.write('%endblock LatticeVectors\n')
        f.write('\n')
      elif i == 'SuperCell':
        f.write('\n')
        f.write('%block SuperCell\n')
        f.write(' {0}   {1}    {2}\n'.format(self.param['LatticeVectors'][0][0], self.param['LatticeVectors'][0][1], self.param['LatticeVectors'][0][2]))
        f.write(' {0}   {1}    {2}\n'.format(self.param['LatticeVectors'][1][0], self.param['LatticeVectors'][1][1], self.param['LatticeVectors'][1][2]))
        f.write(' {0}   {1}    {2}\n'.format(self.param['LatticeVectors'][2][0], self.param['LatticeVectors'][2][1], self.param['LatticeVectors'][2][2]))
        f.write('%endblock SuperCell\n')
        f.write('\n')
      elif i == 'kgrid_Monkhorst_Pack':
        f.write('\n')
        f.write('%block kgrid_Monkhorst_Pack\n')
        f.write(' {0}   {1}    {2}    {3}\n'.format(int(self.param['kgrid_Monkhorst_Pack'][0][0]), int(self.param['kgrid_Monkhorst_Pack'][0][1]), int(self.param['kgrid_Monkhorst_Pack'][0][2]), self.param['kgrid_Monkhorst_Pack'][0][3]))
        f.write(' {0}   {1}    {2}    {3}\n'.format(int(self.param['kgrid_Monkhorst_Pack'][1][0]), int(self.param['kgrid_Monkhorst_Pack'][1][1]), int(self.param['kgrid_Monkhorst_Pack'][1][2]), self.param['kgrid_Monkhorst_Pack'][1][3]))
        f.write(' {0}   {1}    {2}    {3}\n'.format(int(self.param['kgrid_Monkhorst_Pack'][2][0]), int(self.param['kgrid_Monkhorst_Pack'][2][1]), int(self.param['kgrid_Monkhorst_Pack'][2][2]), self.param['kgrid_Monkhorst_Pack'][2][3]))
        f.write('%endblock kgrid_Monkhorst_Pack\n')
        f.write('\n')
      elif i == 'Optical.Mesh':
        f.write('\n')
        f.write('%block Optical.Mesh\n')
        f.write(' {0}   {1}    {2}\n'.format(self.param['Optical.Mesh'][0], self.param['Optical.Mesh'][1], self.param['Optical.Mesh'][2]))
        f.write('%endblock Optical.Mesh\n')
        f.write('\n')
      elif i == 'Optical.Vector':
        f.write('\n')
        f.write('%block Optical.Vector\n')
        f.write(' {0}   {1}    {2}\n'.format(self.param['Optical.Vector'][0], self.param['Optical.Vector'][1], self.param['Optical.Vector'][2]))
        f.write('%endblock Optical.Vector\n')
        f.write('\n')
      elif i == 'ExternalElectricField':
        f.write('\n')
        f.write('%block ExternalElectricField\n')
        f.write(' {0}   {1}    {2}    '.format(self.param['ExternalElectricField'][0], self.param['ExternalElectricField'][1], self.param['ExternalElectricField'][2]) + self.param['ExternalElectricField'][3] + '\n')
        f.write('%endblock ExternalElectricField\n')
        f.write('\n')
      elif i == 'MD.TargetStress':
        f.write('\n')
        f.write('%block MD.TargetStress\n')
        f.write(' {0}   {1}    {2}    {3}    {4}    {5}\n'.format(self.param['MD.TargetStress'][0], self.param['MD.TargetStress'][1], self.param['MD.TargetStress'][2],\
            self.param['MD.TargetStress'][3], self.param['MD.TargetStress'][4], self.param['MD.TargetStress'][5]))
        f.write('%endblock MD.TargetStress\n')
        f.write('\n')
      elif i == 'GeometryConstraints':
        f.write('\n')
        f.write('%block GeometryConstraints\n')
        for keys, values in self.param['GeometryConstraints'].items():
          if type(values) == list or type(values) == 'numpy.ndarray':
            for k in values:
              f.write('   ' + keys + '   ' + str(k) + '\n')
          else:
            f.write('   ' + keys + '   ' + str(values) + '\n')
        f.write('%endblock GeometryConstraints\n')
      else:
        f.write(i + '     {0}\n'.format(self.param[i]))

    f.close()
  #End Write_siesta

  def get_specie_charac(self, fname = 'siesta.out'):
#    print('number species: ', self.param['NumberOfSpecies'])
    if self.param['NumberOfSpecies'] == 0:
      self.build_ChemicalSpeciesLabel(self.label_list)

    self.species_charac = readxyz.readBasis_spec(fname, self.param['NumberOfSpecies'])


  def read_MullikenPop(self, atoms, fname='siesta.out'):
    L = readxyz.read_file(fname)
    length_start = len('mulliken: Mulliken Atomic and Orbital Populations:')
    length_end = len('mulliken: Qtot =')

    if self.param['WriteMullikenPop'] == 0:
      raise ValueError('WriteMullikenPop set to 0 in the input parameter')
    

    ind_st = 0
    ind_end=0
    for i, ligne in enumerate(L):
      if ligne[0:length_start] == 'mulliken: Mulliken Atomic and Orbital Populations:':
        ind_st = i

      if ligne[0:length_end] == 'mulliken: Qtot =':
        ind_end = i+1
        break

    if ind_end == 0:
      raise ValueError('No Mulliken Population found in ' + fname)

    Mull = L[ind_st:ind_end]

    specie = None
    data = False
    MullPop = []
    specie_org = ""
    for i, ligne in enumerate(Mull):
      if ligne[0:len('Species:')] == 'Species:':
        specie = ligne[len('Species:'):len(ligne)-2].replace(" ", "")
      
      if specie is not None:
        if data:
          if specie == specie_org:
            arr = readxyz.str2float(ligne)
            if arr != []:
              if MullPop != []:
                if int(arr[0]) == int(MullPop[len(MullPop)-1][0] +1):
                  MullPop.append(arr)
                else:
                  MullPop[len(MullPop)-1] = np.concatenate((MullPop[len(MullPop)-1], arr))
              else:
                MullPop.append(arr)
          else:
            data = False
        else:
          for j, letter in enumerate(ligne):
            if letter not in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', ' ', '\n']:
              data=False
              break
            else:
              data=True
              specie_org = specie

          if data:
            if specie == specie_org:
              arr = readxyz.str2float(ligne)
              if arr != []:
                if MullPop != []:
                  if int(arr[0]) == int(MullPop[len(MullPop)-1][0] +1):
                    MullPop.append(arr)
                  else:
                    MullPop[len(MullPop)-1] = np.concatenate((MullPop[len(MullPop)-1], arr))
                else:
                  MullPop.append(arr)
            else:
              data = False

    MullPop_new = []
    for i in range(len(MullPop)):
      MullPop_new.append(MullPop[i][1:MullPop[i].shape[0]])

    atoms.set_MullPop(np.array(MullPop_new))

#    for i, mull in enumerate(atoms.get_MullPop()):
#      print(i, mull)




#####End Siesta Class #########################################

def find_user_folder():
  """
  return the home directory of the actual user
  """

  return os.getenv('HOME')

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
def build_Na8():
  """
  build a Na8 cluster
  old function
  """
  bohr_cte = 0.529177
  atoms = list()
  atoms.append(8)
  atoms.append(['Na', np.array([-3.6, 2.95, 0])])
  atoms.append(['Na', np.array([3.6, 2.95, 0])])
  atoms.append(['Na', np.array([3.6, -2.95, 0])])
  atoms.append(['Na', np.array([-3.6, -2.95, 0])])
  atoms.append(['Na', np.array([0, 0, 3.94])])
  atoms.append(['Na', np.array([0, 0, -3.94])])
  atoms.append(['Na', np.array([0, 6.1, 3.94])])
  atoms.append(['Na', np.array([0, 6.1, -3.94])])

  for i in range(1, len(atoms)):
    for j in range(len(atoms[i][1])):
      atoms[i][1][j] = atoms[i][1][j]*bohr_cte

  fname = 'Na8.xyz'
  readxyz.write_xyz(atoms, fname)

  return atoms

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def build_Na8_bis():
  """
  build a Na8 cluster
  old function
  """
  bohr_cte = 0.529177
  atoms = list()
  atoms.append(8)
  atoms.append(['Na', np.array([-3.6, 2.95, 0])])
  atoms.append(['Na', np.array([3.6, 2.95, 0])])
  atoms.append(['Na', np.array([3.6, -2.95, 0])])
  atoms.append(['Na', np.array([-3.6, -2.95, 0])])
  atoms.append(['Na', np.array([0, 0, 3.94])])
  atoms.append(['Na', np.array([0, 0, -3.94])])
  atoms.append(['Na', np.array([0, -6.1, 3.94])])
  atoms.append(['Na', np.array([0, -6.1, -3.94])])

  for i in range(1, len(atoms)):
    for j in range(len(atoms[i][1])):
      atoms[i][1][j] = atoms[i][1][j]*bohr_cte

  fname = 'Na8.xyz'
  readxyz.write_xyz(atoms, fname)

  return atoms

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def build_2cluster(fname, sep, atom_edge):
  """
  create 2 cluster from one by doing the duplication
  of the original cluster and creating the mirror image
  the mirror plan is the plan y=0
  """
  atoms = readxyz.extractdata_xyz(fname)

  origine = atoms[atom_edge][1]

  atoms_2cluster = list()
  atoms_2cluster.append(2*atoms[0])

  for i in range(1, len(atoms)):
    atoms_2cluster.append([atoms[i][0], np.array([atoms[i][1][0], atoms[i][1][1] - origine[1] - sep/2, atoms[i][1][2]])])

  for i in range(1, len(atoms)):
    y = -atoms[i][1][1] + origine[1] + sep/2
    atoms_2cluster.append([atoms[i][0], np.array([atoms[i][1][0], y, atoms[i][1][2]])])

  fname = fname[0:len(fname)-4] + '_2cluster.xyz'
  readxyz.write_xyz(atoms_2cluster, fname)

  return atoms_2cluster

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def shift_mol(atoms, shift):
  """
  shift a molecule
  """
  for i in atoms[1:len(atoms)]:
    i[1] = i[1] + shift

  return atoms

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def build_2cluster_bis(fname, sep, atom_edge):
  """
  create 2 cluster from one by doing the duplication
  of the original cluster and creating the mirror image
  the mirror plan is the plan y=0
  """
  atoms = readxyz.extractdata_xyz(fname)

  origine = atoms[atom_edge][1]

  atoms_2cluster = list()
  atoms_2cluster.append(2*atoms[0])

  for i in range(1, len(atoms)):
    atoms_2cluster.append([atoms[i][0], np.array([atoms[i][1][0], atoms[i][1][1] - origine[1] - sep/2, atoms[i][1][2]])])

  for i in range(1, len(atoms)):
    x, y = rotation_2D(atoms[i][1][0], -atoms[i][1][1], 70)
    y = y + origine[1] + sep/2 -0.83
    atoms_2cluster.append([atoms[i][0], np.array([x, y, atoms[i][1][2]])])

  fname = fname[0:len(fname)-4] + '_2cluster.xyz'
  readxyz.write_xyz(atoms_2cluster, fname)

  return atoms_2cluster


#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def rotation_2D(x, y, angle):
  """
  rotate the point of coordonate x, y by the angle: angle
  """
  #angle = np.pi*angle/180
  r = np.sqrt(x**2 + y**2)

  phi = np.arctan2(y, x)

  x1 = r*np.cos(phi + angle)
  y1 = r*np.sin(phi + angle)

  return x1, y1

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def rotation_3D(coord, theta, phi):
  """
  rotate the point of coordonate x, y by the angle: angle
  """
  #theta = np.pi#*theta/180
  #phi = np.pi#*phi/180
  r = np.sqrt(coord[0]**2 + coord[1]**2 + coord[2]**2)

  phi_0 = 0
  phi_0 = np.arctan2(coord[1], coord[0])

  theta_0 = 0
  if r == 0:
    theta_0 = 0
  else:
    theta_0 = np.arccos(coord[2]/r) 
    
  x1 = r*np.sin(theta_0+theta)*np.cos(phi_0 + phi)
  y1 = r*np.sin(theta_0+theta)*np.sin(phi_0 + phi)
  z1 = r*np.cos(theta_0 + theta)

  coord_rot = np.array([x1, y1, z1])
  #print(theta_0*180/np.pi, coord, coord_rot)
  return coord_rot

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


def rotate_mol_2D(atoms, angle, axis):
  """
  rotate a molecule
  """
  atoms_rot = atoms

  for i in range(1, atoms[0]+1):
    if axis == 'x':
      atoms_rot[i][1][1], atoms_rot[i][1][2] = rotation_2D(atoms[i][1][1], atoms[i][1][2], angle)
    elif axis == 'y':
      atoms_rot[i][1][0], atoms_rot[i][1][2] = rotation_2D(atoms[i][1][0], atoms[i][1][2], angle)
    elif axis == 'z':
      atoms_rot[i][1][0], atoms_rot[i][1][1] = rotation_2D(atoms[i][1][0], atoms[i][1][1], angle)
    else:
      raise ValueError('axis can only be x, y or z')

  return atoms_rot

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def rotate_mol_3D(atoms, theta, phi):
  """
  rotate a molecule
  """
  atoms_rot = [atoms[0]]
  f = open('coord.dat', 'w')

  for i in range(1, atoms[0]+1):
    atoms_rot.append(['Na', rotation_3D(np.array([atoms[i][1][0], atoms[i][1][1], atoms[i][1][2]]),theta, phi)])
    f.write('i= {0},    coord=[{1}   {2}    {3}],     coord_rot=[{4}   {5}    {6}]\n'.format(i, atoms[i][1][0], atoms[i][1][1], atoms[i][1][2], atoms_rot[i][1][0], atoms_rot[i][1][1], atoms_rot[i][1][2]))
  f.close()

  return atoms_rot

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


def Add_element_list(L, string):
  inside = False
  for i in L:
    if i == string:
      inside = True
  
  if inside == False:
    L.append(string)

  return L

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1

def check_element_in_list(L, args):
  in_list = False
  
  i = 0
  while in_list == False and i<(len(L)):
    if L[i] == args: 
      in_list = True
    i = i + 1 
  
  return in_list

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#def check_element_in_list(L, elem):
#  inside = False
#  for i in L:
#    if i == elem:
#      inside = True
#
#  return inside

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def build_tip(atoms, atm_tip, angle1, angle2, angle3):
  
  atoms_tip = [atoms[0]]

  for i in atoms[1:atoms[0]+1]:
    #atoms_tip.append([i[0], i[1] - r_357])
    atoms_tip.append([i[0], i[1]])

  #atoms_tip = rotate_mol_3D(atoms_tip, angle2[0], angle2[1])
  for i in range(1, atoms_tip[0]+1):
    atoms_tip[i][1][0], atoms_tip[i][1][1] = rotation_2D(atoms_tip[i][1][0], atoms_tip[i][1][1], angle1)#90
    atoms_tip[i][1][0], atoms_tip[i][1][2] = rotation_2D(atoms_tip[i][1][0], atoms_tip[i][1][2], angle2)#-10
    atoms_tip[i][1][1], atoms_tip[i][1][2] = rotation_2D(atoms_tip[i][1][1], atoms_tip[i][1][2], angle3)#-20


  return atoms_tip

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def Add_mol(atm1, atm2, d, shift):
  """
  Add a molecule to the atm1 list
  input:
    atm1 (list): list contaigning the specie of the first mol and the coordinnates with the form
              ['specie', np.array([x, y, z])]
    atm2 (list): list contaigning the specie of the second mol and the coordinnates with the form
              ['specie', np.array([x, y, z])]
    d (float): distances between the two molecule (average in the y-direction)
  output:
    atoms (list): atm1 + atm2 with modify coordinnate
  """
  atoms = [atm1[0] + atm2[0]]

  for i in range(1, atm1[0]+1):
    atm1[i][1][1] = atm1[i][1][1] + d/2
    atoms.append([atm1[i][0], np.array([atm1[i][1][0], atm1[i][1][1]+d/2, atm1[i][1][2]])])

  for i in range(1, atm2[0]+1):
    atm2[i][1][1] = atm2[i][1][1] - d/2
    atoms.append([atm2[i][0], np.array([atm2[i][1][0], atm2[i][1][1]-d/2-shift, atm2[i][1][2]])])

  return atoms

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def remove_atm(atm, atm_number, d_orig):
  """
  Remove one atom from the atom list given in input.
  For the moment this function is apply only for the 2 Na380 clusters system
  Input parameters:
    atm (list): list of the atoms with the coordinnate
    atm_number (integer): number of the atom to delete
    d_orig (float): original distance between the clusters
  Output parameters:
    atm (list): new list of the atoms with the coordinnate
    file distance.txt: save some information about the distance between the clusters
  """

  #print(atm_number, len(atm_number))
  atm[0] = atm[0] - len(atm_number)

  atm_mod = list_max2min(atm_number)
  #print(atm_mod, len(atm_mod))

  for i in atm_mod:
    del atm[int(i)]

  y_atm1 = np.zeros((380), dtype= float)
  y_atm2 = np.zeros((380-len(atm_mod)), dtype= float)
  
  for i in range(1, 381):
    y_atm1[i-1] = atm[i][1][1]
 
  comp = 0
  for i in range(381, atm[0]+1):
    y_atm2[comp] = atm[i][1][1]
    comp = comp + 1

  if np.amin(y_atm1)>np.amax(y_atm2):
    d = np.amin(y_atm1) - np.amax(y_atm2)
  elif np.amax(y_atm1)<np.amin(y_atm2):
    d = np.amin(y_atm2) - np.amax(y_atm1)
  else:
    print('Error!! atoms mixed!!')
    print('min(y_atm1) = ', np.amin(y_atm1), 'max(y_atm1) = ', np.amax(y_atm1))
    print('min(y_atm2) = ', np.amin(y_atm2), 'max(y_atm2) = ', np.amax(y_atm2))
    d = np.amin(y_atm2) - np.amax(y_atm1)
    #sys.exit()

  f = open('distance.txt', 'w')
  f.write('Original distance:   {0} Ang\n'.format(d_orig))
  f.write('Real distance:   {0} Ang\n'.format(d))
  f.write('min(y_atm1) = {0}    max(y_atm1) = {1}\n'.format(np.amin(y_atm1), np.amax(y_atm1)))
  f.write('min(y_atm2) = {0}    max(y_atm2) = {1}\n'.format(np.amin(y_atm2), np.amax(y_atm2)))
  f.close()


  return atm

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def list_min2max(L):
  L_mod = np.zeros((len(L)), dtype=float)
  L_tot = []

  for i in L:
    L_tot.append(i)

  for i,j in enumerate(L_tot):
    L_mod[i] = min(L)
    ind = L.index(min(L))
    del L[ind]

  return L_mod

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def list_max2min(L):
  L_mod = np.zeros((len(L)), dtype=float)
  L_tot = []

  for i in L:
    L_tot.append(i)

  for i,j in enumerate(L_tot):
    L_mod[i] = max(L)
    ind = L.index(max(L))
    del L[ind]

  return L_mod

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


def check_file(fichier, folder):
  f = []
  for (dirpath, dirnames, filenames) in os.walk(folder):
    f.extend(filenames)
    break

  if check_element_in_list(f, fichier):
    bol = True
  else:
    bol = False

  return bol


#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def translate_mol(atm, r0):
  atm_bis = [atm[0]]
  for i in range(1, atm_bis[0]+1):
    atm_bis.append([atm[i][0], atm[i][1]-r0])

  return atm_bis

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def Do_triangle(nb_atoms_side, lp):
  """
   nb_atoms_side: nb of atom at the side of the equilateral triangle
   lp: lattice param
  """
  atoms = []
  origin = np.array([0.0, 0.0, 0.0])
  for i in range(nb_atoms_side):
    for j in range(i+1):
      atoms.append(origin + np.array([j*lp, 0.0, 0.0]))
    origin = origin + np.array([-lp/2.0, -np.sqrt(3)*lp/2.0, 0.0])
  return atoms
  
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def build_bcc_lattice(specie, nb_atoms_edges, a):
  from ase import Atoms
  atoms = []

  for i in range(nb_atoms_edges):
    for j in range(nb_atoms_edges):
      for k in range(nb_atoms_edges):
        atoms.append(np.array([i*a, j*a, k*a]))

  for i in range(nb_atoms_edges-1):
    for j in range(nb_atoms_edges-1):
      for k in range(nb_atoms_edges-1):
        atoms.append(np.array([i*a + 0.5*a, j*a + 0.5*a, k*a + 0.5*a]))


  bcc = Atoms(specie + str(len(atoms)), positions=atoms)
  return bcc

