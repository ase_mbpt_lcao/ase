"""
Module to calculate the Raman polarizability of a molecule.
Interfaced with the siesta and tddft programs
In the actual version require a recent version of numpy

Small script using this module:
#import the necessary libraries
from __future__ import division
import numpy as np
import sys
sys.path.append('/home/marc/programs/mbpt_domiprod.testing/tests/gwa/trunk/examples/plotting')
from pyiter.Raman import Raman_calc, test_pol, plot_pol_raman
from pyiter.Read_data import Parameter, Properties_figure

#set up the parameters
Ram = Raman_calc()
Ram.fileGeo = 'H2O.xyz'
Ram.tddft_exe = '/home/marc/programs/mbpt_domiprod.testing/tests/gwa/trunk/build/tddft'
Ram.atoms_mod = {1: np.array([0.0, 0.1, 0.0])}
Ram.nb_step = 10
Ram.set_Siesta_def()
Ram.set_tddft_input_def()

Ram.Siesta = Ram.Siesta_GS

#start the calculation with siesta and tddft
Ram.start_calculations()

#test if the polarizability array is correct
test_pol(Ram)


#######################################
#                                     #
#       Plot the polarizability       #
#                                     #
#######################################
args = Parameter()
args.fname = 'dipol_inter_iter_krylov_im_raman.txt'
args.interacting = 1
args.format_input = '.hdf5'
args.tem_iter = 2

prop = Properties_figure()
prop.fatoms = 'domiprod_atom2coord.xyz'
prop.format_output = 'pdf'
prop.figname = 'pol_raman_inter_im.pdf'

plot_pol_raman(args, prop)

"""

from __future__ import division
import numpy as np
#import sys
#import subprocess
from inp_files import tddft_inp, siesta
from readxyz import extractdata_xyz, readSiestaFA
from Read_data import run


class Raman:
  def __init__(self, MS):
    self.TDDFT_EXE = 'tddft'
    self.tddft_inp = tddft_inp()
    self.step = 0
    self.MS = MS
    self.Pol_inter_list = []
    self.Pol_nonin_list = []
    self.omega = 0
    self.nb_step = 0
    self.dipole_fname = ['dipol_inter_iter_krylov_re.txt', 'dipol_inter_iter_krylov_im.txt', 
                         'dipol_nonin_iter_krylov_re.txt', 'dipol_nonin_iter_krylov_im.txt']

    self.dip_raman_fname = ['dipol_inter_iter_krylov_re_raman.txt', 'dipol_inter_iter_krylov_im_raman.txt',
                             'dipol_nonin_iter_krylov_re_raman.txt', 'dipol_nonin_iter_krylov_im_raman.txt']
    self.atoms = []
    self.atoms_mod = {}



  def run_tddft(self, atoms):
    from ase.io import write

    self.tddft_inp.write_tddft_inp()
    run(self.TDDFT_EXE)

    if self.step == 0:
      self.omega = np.loadtxt(self.dipole_fname[0])[:, 0]

    run('mkdir step_{0}'.format(self.step))
    self.Pol_inter_list.append(np.loadtxt(self.dipole_fname[0])[:, 2:11]+complex(0.0, 1.0)*np.loadtxt(self.dipole_fname[1])[:, 2:11])
    self.Pol_nonin_list.append(np.loadtxt(self.dipole_fname[2])[:, 2:11]+complex(0.0, 1.0)*np.loadtxt(self.dipole_fname[3])[:, 2:11])
    
    self.atoms.append(atoms)
    write('Raman_{0}.traj'.format(self.step), atoms, format='traj')


    run('cp ' + self.MS.param['SystemLabel'] + '.* *.txt *.dat *.hdf5 *.out *.inp *.ion *.ion.xml ' + 'step_{0}'.format(self.step))
    self.step = self.step+1


  
  #
  #
  #
  def set_pol(self):
    self.Pol_inter = np.zeros((len(self.Pol_inter_list), self.Pol_inter_list[0].shape[0], self.Pol_inter_list[0].shape[1]), dtype=complex)
    self.Pol_nonin = np.zeros((len(self.Pol_nonin_list), self.Pol_nonin_list[0].shape[0], self.Pol_nonin_list[0].shape[1]), dtype=complex)

    for i in range(self.Pol_inter.shape[0]):
      self.Pol_inter[i, :, :] = self.Pol_inter_list[i]
      self.Pol_nonin[i, :, :] = self.Pol_nonin_list[i]


  #
  #
  #
  def calculate_raman_pol(self):
    """
      Calculate the raman polarizability
    """
    if len(self.Pol_inter_list) > 0:
      self.set_pol()

    self.Raman_pol_inter = np.zeros((self.Pol_inter.shape[1], self.Pol_inter.shape[2]), dtype = complex)
    self.Raman_pol_nonin = np.zeros((self.Pol_nonin.shape[1], self.Pol_nonin.shape[2]), dtype = complex)
    
    self.Raman_pol_inter = self.Pol_inter[0, :, :]
    self.Raman_pol_nonin = self.Pol_nonin[0, :, :]

    self.atoms_org = self.get_atoms_mod(self.atoms[0])

    for i in range(self.Pol_inter.shape[0]):
      self.atoms_mod = self.get_atoms_mod(self.atoms[i])
      for key, values in self.atoms_mod.items():
        self.atoms_mod[key] = values - self.atoms_org[key]
 
      for key in self.atoms_mod.keys():
        self.deriv_pol(key)
        Q = np.sqrt(np.dot(self.atoms_mod[key], self.atoms_mod[key]))#*self.nb_step
        self.Raman_pol_inter = self.deriv_Pol_inter[0, :, :]*Q
        self.Raman_pol_nonin = self.deriv_Pol_nonin[0, :, :]*Q

    save_array_pol(self.dip_raman_fname[0], self.Raman_pol_inter.real, self.omega)
    save_array_pol(self.dip_raman_fname[1], self.Raman_pol_inter.imag, self.omega)
    save_array_pol(self.dip_raman_fname[2], self.Raman_pol_nonin.real, self.omega)
    save_array_pol(self.dip_raman_fname[3], self.Raman_pol_nonin.imag, self.omega)

  #
  #
  #
  def get_atoms_mod(self, atoms):
    
    atoms_mod = {}
    for i, pos in enumerate(atoms.positions):
      atoms_mod[i+1] = pos

    return atoms_mod

  #
  #
  #
  def deriv_pol(self, key):
    """
      Do the first derivative of the polarizability
    """
    self.deriv_Pol_inter = np.zeros(self.Pol_inter.shape, dtype = complex)
    self.deriv_Pol_nonin = np.zeros(self.Pol_nonin.shape, dtype = complex)

    dr = np.sqrt(np.dot(self.atoms_mod[key], self.atoms_mod[key]))
    if dr == 0.0:
      return

    for i in range(self.deriv_Pol_inter.shape[1]):
      for j in range(self.deriv_Pol_inter.shape[2]):
        self.deriv_Pol_inter[:, i, j] = np.gradient(self.Pol_inter[:, i, j], dr, edge_order=2)
        self.deriv_Pol_nonin[:, i, j] = np.gradient(self.Pol_nonin[:, i, j], dr, edge_order=2)

  #
  #
  #
  def load_raman_pol(self):
    self.Raman_pol_inter = 0
    self.Raman_pol_nonin = 0

    self.Raman_pol_inter = np.loadtxt(self.dip_raman_fname[0])[:, 2:11] + complex(0.0, 1.0)*np.loadtxt(self.dip_raman_fname[1])[:, 2:11]
    self.Raman_pol_nonin = np.loadtxt(self.dip_raman_fname[2])[:, 2:11] + complex(0.0, 1.0)*np.loadtxt(self.dip_raman_fname[3])[:, 2:11]

  def load_Raman(self):
    self.load_pol_step()
    self.load_atoms()

  #
  #
  #
  def load_pol_step(self):
    
    self.omega = np.loadtxt('step_0/' + self.dipole_fname[0])[:, 0]
    self.Pol_inter = np.zeros((self.nb_step+1, self.omega.shape[0], 9), dtype = complex)
    self.Pol_nonin = np.zeros((self.nb_step+1, self.omega.shape[0], 9), dtype = complex)

    self.Pol_inter[0, :, :] = np.loadtxt('step_0/' + self.dipole_fname[0])[:, 2:11] + complex(0.0, 1.0)*np.loadtxt('step_0/' + self.dipole_fname[1])[:, 2:11]
    self.Pol_nonin[0, :, :] = np.loadtxt('step_0/' + self.dipole_fname[2])[:, 2:11] + complex(0.0, 1.0)*np.loadtxt('step_0/' + self.dipole_fname[3])[:, 2:11]

    for i in range(1, self.nb_step+1):
      self.Pol_inter[i, :, :] = np.loadtxt('step_{0}/'.format(i) + self.dipole_fname[0])[:, 2:11] + complex(0.0, 1.0)*np.loadtxt('step_{0}/'.format(i) + self.dipole_fname[1])[:, 2:11]
      self.Pol_nonin[i, :, :] = np.loadtxt('step_{0}/'.format(i) + self.dipole_fname[2])[:, 2:11] + complex(0.0, 1.0)*np.loadtxt('step_{0}/'.format(i) + self.dipole_fname[3])[:, 2:11]

  #
  #
  #
  def load_atoms(self):
    from ase.io import read

    for i in range(0, self.nb_step+1):
      self.atoms.append(read("Raman_{0}.traj".format(i)))



###########################################################
#                                                         #
#         Raman_calc class: main class of the module      #
#                     OLD VERSION                         #
#                                                         #
###########################################################

class Raman_calc:
  """
  Calculate the Raman polarizability using siesta and tddft by making a variation of the position
  of one or several atoms from them original positions
  Parameters:
  -----------
    self.sys_name (string, def:Raman): name of the siesta calculations
    self.fileGeo (string, def:geometry.xyz): name of the geometry file, must be xyz
    self.nb_step (integer, def: 0): number of calculation to do
    self.atoms_mod (dictionary, def: {}): dictionary containing the elemental step to move the atoms
                                          keys: intger corresponding to the atom index
                                          values: numpy array containing the dr coordinate to move the corresponding atom
    self.Siesta_GS (siesta class): to set the siesta parameters of the ground state, type help(siesta) for more information
    self.Siesta (siesta class): to set the siesta parameters of the excited states, type help(siesta) for more information
    self.tddft_inp (tddft_inp class): to set the tddft parameters, type help(tddft_inp) for more information
    self.GS_dir (string, def: GroundState): name of the ground state directory
    self.mpi (logical, def: false): mpi calculations (only for the siesta part) !! Failed for the moment
    self.nproc (integer, def: 1): number of processors use in mpi calculations
    self.siesta_path (string, def: siesta): path of the siesta program
    self.tddft_path (string, def: tddft): path of the tddft program
    self.dipole_fname (list of string): contain the name of the polarizability file provide by the tddft program
    self.dip_raman_fname (list of string): name of the Raman polarizability file in the same format than in the tddft program
                                           Output of the routine!!
    self.vibration (list): contains the informations about the vibration frequencies
                            The list is structure as follow:
                              it give a list over the number of step, that is a list over the moved atom
                              that contains 2 elements: the atom index, and the frequency in eV. For example
                              self.vibration = [[[1, 2.3], [5, 5.6]], [[1, 2.4], [5, 5.8]], [[1, 2.8], [5, 6.6]]]
                                              step1:  atom1: index, w (eV)
                                                      atom2: index, w (eV)
                                              step2:  atom1: index, w (eV)
                                                      atom2: index, w (eV)
                                              step3:  atom1: index, w (eV)
                                                      atom2: index, w (eV)
  """       
  def __init__(self):
    self.sys_name = 'Raman'
    self.fileGeo = 'geometry.xyz'
    self.nb_step = 0
    self.atoms_mod = {} #keys: atoms index, values: np.array with the dr movement
    self.Siesta_GS = siesta()
    self.Siesta = siesta()
    self.tddft_inp = tddft_inp()
    self.GS_dir = 'GroundState'
    self.mpi = False
    self.nproc = 1
    self.tddft_path = 'tddft'
    self.siesta_path = 'siesta'
    self.vibrations = []
    self.geo_vib = []
    self.dipole_fname = ['dipol_inter_iter_krylov_re.txt', 'dipol_inter_iter_krylov_im.txt', 
                         'dipol_nonin_iter_krylov_re.txt', 'dipol_nonin_iter_krylov_im.txt']

    self.dip_raman_fname = ['dipol_inter_iter_krylov_re_raman.txt', 'dipol_inter_iter_krylov_im_raman.txt',
                             'dipol_nonin_iter_krylov_re_raman.txt', 'dipol_nonin_iter_krylov_im_raman.txt']

  #
  #
  #
  def groundState_calculation(self, mpi=False, nproc=1, GS_dir = 'GroundState'):
    """
      Calculate the polarizability for the ground state
    """
    self.Siesta_GS.write_siesta_file()
    self.tddft_inp.write_tddft_inp()

    run('mkdir ' + GS_dir)
    if mpi:
      run('mpirun -np {0} siesta <'.format(nproc) + self.Siesta_GS.param['SystemLabel'] + '.fdf |tee ' +
          self.Siesta_GS.param['SystemLabel'] + '.out')
      run(self.tddft_exe)
    else:
      run('siesta <' + self.Siesta_GS.param['SystemLabel'] + '.fdf |tee ' + 
          self.Siesta_GS.param['SystemLabel'] + '.out')
      run(self.tddft_exe + '|tee ' + 'tddft.out')

    self.Pol_inter = np.zeros((self.nb_step+1, np.loadtxt(self.dipole_fname[0]).shape[0], 9), dtype = complex)
    self.Pol_nonin = np.zeros((self.nb_step+1, np.loadtxt(self.dipole_fname[0]).shape[0], 9), dtype = complex)

    self.omega = np.loadtxt(self.dipole_fname[0])[:, 0]
    self.Pol_inter[0, :, :] = np.loadtxt(self.dipole_fname[0])[:, 2:11] + complex(0.0, 1.0)*np.loadtxt(self.dipole_fname[1])[:, 2:11]
    self.Pol_nonin[0, :, :] = np.loadtxt(self.dipole_fname[2])[:, 2:11] + complex(0.0, 1.0)*np.loadtxt(self.dipole_fname[3])[:, 2:11]
    self.Siesta_GS.get_specie_charac()

    run('mv ' + self.Siesta_GS.param['SystemLabel'] + '.* *.txt *.dat *.hdf5 *.out *.inp *.ion *.ion.xml ' + GS_dir)



  #
  #
  #
  def State_calculation(self, step, mpi=False, nproc=1):
    """
      Calculate the polarizability for the excited state
    """
    self.Siesta.write_siesta_file()
    self.tddft_inp.write_tddft_inp()

   
    run('mkdir step_{0}'.format(step))
    if mpi:
      run('mpirun -np {0} siesta <'.format(nproc) + self.Siesta.param['SystemLabel'] + '.fdf |tee ' +
          self.Siesta.param['SystemLabel'] + '.out')
      run(self.tddft_exe)
    else:
      run('siesta <' + self.Siesta.param['SystemLabel'] + '.fdf |tee ' + 
          self.Siesta.param['SystemLabel'] + '.out')
      run(self.tddft_exe + '|tee ' + 'tddft.out')

    if step == 0:
      self.Pol_inter = np.zeros((len(self.geo_vib)+1, np.loadtxt(self.dipole_fname[0]).shape[0], 9), dtype = complex)
      self.Pol_nonin = np.zeros((len(self.geo_vib)+1, np.loadtxt(self.dipole_fname[0]).shape[0], 9), dtype = complex)
      self.omega = np.loadtxt(self.dipole_fname[0])[:, 0]
 
    self.Pol_inter[step, :, :] = np.loadtxt(self.dipole_fname[0])[:, 2:11] + complex(0.0, 1.0)*np.loadtxt(self.dipole_fname[1])[:, 2:11]
    self.Pol_nonin[step, :, :] = np.loadtxt(self.dipole_fname[2])[:, 2:11] + complex(0.0, 1.0)*np.loadtxt(self.dipole_fname[3])[:, 2:11]

    run('mv ' + self.Siesta.param['SystemLabel'] + '.* *.txt *.dat *.hdf5 *.out *.inp *.ion *.ion.xml ' + 'step_{0}'.format(step))


  #
  #
  #
#  def start_calculations(self):
#    """
#      Do the calculations, main method of the class
#    """
#    self.groundState_calculation(mpi = self.mpi, nproc = self.nproc, GS_dir = self.GS_dir)
#    for i in range(1, self.nb_step+1):
#      atoms = self.modify_geo()
#      self.fileGeo = 'step_{0}.xyz'.format(i)
#      write_xyz(atoms, self.fileGeo)
#      self.Siesta.fname = self.fileGeo
#      self.Siesta.param['SystemName'] = self.sys_name + '_step_{0}'.format(i)
#      self.State_calculation(i, mpi = self.mpi, nproc=self.nproc)
#  
#    self.calculate_raman_pol()
  
  #
  #
  #
  def start_calculations(self):
    """
      Do the calculations, main method of the class
    """
    from ase.io import read
    #self.groundState_calculation(mpi = self.mpi, nproc = self.nproc, GS_dir = self.GS_dir)
    for i in range(len(self.geo_vib)):
      #atoms = self.modify_geo()
      atoms = read(self.geo_vib[i])
      self.atoms_mod = {}
      if i == 0:
        self.atoms_org = self.get_atoms_mod(atoms)

      self.atoms_mod = self.get_atoms_mod(atoms)
      for key, values in self.atoms_mod.items():
        self.atoms_mod[key] = values - self.atoms_org[key]
      self.Siesta.fname = self.geo_vib[i]
      self.Siesta.param['SystemName'] = self.sys_name + '_step_{0}'.format(i)
      self.State_calculation(i, mpi = self.mpi, nproc=self.nproc)
  
    self.calculate_raman_pol()
 
  #
  #
  #
  def modify_geo(self):
    """
      modify the geometry for the next calculations
    """
    atoms = extractdata_xyz(self.fileGeo)
    
    for key, values in self.atoms_mod.items():
      atoms[key][1] = atoms[key][1] + values

    return atoms

  #
  #
  #
  def calculate_raman_pol(self):
    """
      Calculate the raman polarizability
    """
    self.Raman_pol_inter = np.zeros((self.Pol_inter.shape[1], self.Pol_inter.shape[2]), dtype = complex)
    self.Raman_pol_nonin = np.zeros((self.Pol_nonin.shape[1], self.Pol_nonin.shape[2]), dtype = complex)
    
    self.Raman_pol_inter = self.Pol_inter[0, :, :]
    self.Raman_pol_nonin = self.Pol_nonin[0, :, :]

    for key in self.atoms_mod.keys():
      self.deriv_pol(key)
      Q = np.sqrt(np.dot(self.atoms_mod[key], self.atoms_mod[key]))#*self.nb_step
      self.Raman_pol_inter = self.deriv_Pol_inter[0, :, :]*Q
      self.Raman_pol_nonin = self.deriv_Pol_nonin[0, :, :]*Q

    save_array_pol(self.dip_raman_fname[0], self.Raman_pol_inter.real, self.omega)
    save_array_pol(self.dip_raman_fname[1], self.Raman_pol_inter.imag, self.omega)
    save_array_pol(self.dip_raman_fname[2], self.Raman_pol_nonin.real, self.omega)
    save_array_pol(self.dip_raman_fname[3], self.Raman_pol_nonin.imag, self.omega)


  #
  #
  #
  def deriv_pol(self, key):
    """
      Do the first derivative of the polarizability
    """
    self.deriv_Pol_inter = np.zeros(self.Pol_inter.shape, dtype = complex)
    self.deriv_Pol_nonin = np.zeros(self.Pol_nonin.shape, dtype = complex)

    dr = np.sqrt(np.dot(self.atoms_mod[key], self.atoms_mod[key]))
    if dr == 0.0:
      return

    for i in range(self.deriv_Pol_inter.shape[1]):
      for j in range(self.deriv_Pol_inter.shape[2]):
        self.deriv_Pol_inter[:, i, j] = np.gradient(self.Pol_inter[:, i, j], dr, edge_order=2)
        self.deriv_Pol_nonin[:, i, j] = np.gradient(self.Pol_nonin[:, i, j], dr, edge_order=2)

  #
  #
  #
  def load_raman_pol(self):
    self.Raman_pol_inter = 0
    self.Raman_pol_nonin = 0

    self.Raman_pol_inter = np.loadtxt(self.dip_raman_fname[0])[:, 2:11] + complex(0.0, 1.0)*np.loadtxt(self.dip_raman_fname[1])[:, 2:11]
    self.Raman_pol_nonin = np.loadtxt(self.dip_raman_fname[2])[:, 2:11] + complex(0.0, 1.0)*np.loadtxt(self.dip_raman_fname[3])[:, 2:11]

  #
  #
  #
  def load_pol_step(self):
    
    self.omega = np.loadtxt(self.GS_dir + '/' + self.dipole_fname[0])[:, 0]
    self.Pol_inter = np.zeros((self.nb_step+1, self.omega.shape[0], 9), dtype = complex)
    self.Pol_nonin = np.zeros((self.nb_step+1, self.omega.shape[0], 9), dtype = complex)

    self.Pol_inter[0, :, :] = np.loadtxt(self.GS_dir + '/' + self.dipole_fname[0])[:, 2:11] + complex(0.0, 1.0)*np.loadtxt(self.GS_dir + '/' + self.dipole_fname[1])[:, 2:11]
    self.Pol_nonin[0, :, :] = np.loadtxt(self.GS_dir + '/' + self.dipole_fname[2])[:, 2:11] + complex(0.0, 1.0)*np.loadtxt(self.GS_dir + '/' + self.dipole_fname[3])[:, 2:11]

    for i in range(1, self.nb_step+1):
      self.Pol_inter[i, :, :] = np.loadtxt('step_{0}/'.format(i) + self.dipole_fname[0])[:, 2:11] + complex(0.0, 1.0)*np.loadtxt('step_{0}/'.format(i) + self.dipole_fname[1])[:, 2:11]
      self.Pol_nonin[i, :, :] = np.loadtxt('step_{0}/'.format(i) + self.dipole_fname[2])[:, 2:11] + complex(0.0, 1.0)*np.loadtxt('step_{0}/'.format(i) + self.dipole_fname[3])[:, 2:11]

  #
  #
  #
  def get_vibration_siesta(self, fname):
    """
    !!!!!!!!!!!!!!!!!
    ! NOT WORKING   !
    !!!!!!!!!!!!!!!!!
    Get the vibrations frequencies from the Siesta output file for each step.
    it fill the self.vibration list.
    Input parameters:
    -----------------
      fname (string) : the name of the sieta output file for the ground state
    """
    self.Siesta_GS.get_specie_charac(fname)
    
    for step in range(1, self.nb_step+1):
      Forces = readSiestaFA('step_{0}/'.format(step) + self.Siesta.param['SystemLabel']+ '.FA')

      self.vibrations.append([])
      for i in range(len(Forces)):
        Forces[i][1] = Forces[i][1]*0.0367493081366/1.88971616463207   #conversion to atomic unit
        norm = np.sqrt(np.dot(Forces[i][1], Forces[i][1]))
        if Forces[i][0] in self.atoms_mod:
          Q = np.sqrt(np.dot(self.atoms_mod[Forces[i][0]], self.atoms_mod[Forces[i][0]]))*self.nb_step*1.88971616463207
          if Q == 0.0:
            self.vibrations[step-1].append([Forces[i][0], 0.0])
          else:
            k = norm/Q
            atom = self.Siesta_GS.param['AtomicCoordinatesAndAtomicSpecies'][Forces[i][0]][0]
            self.vibrations[step-1].append([Forces[i][0], np.sqrt(k/(
                      self.Siesta_GS.species_charac[atom]['Mass']*1822.8788))*27.21140]) #atomic unit to eV

  
  #
  #
  #
  def get_vibration_gpaw(self, fname, relax=False, relax_fmax=0.05, vac=3.5, gpaw_mode='lcao', 
                          gpaw_basis='dzp', vib_method='frederiksen', vib_direction = 'central'):
    from ase.io import read
    from ase.optimize import QuasiNewton
    from ase.vibrations import Vibrations

    #import ase
    from gpaw import GPAW

    molecule = read(fname)

    molecule.center(vacuum=vac)

    calc = GPAW(h=0.2, txt='gpaw_calc_vibration.txt', mode=gpaw_mode, basis=gpaw_basis)

    molecule.set_calculator(calc)

    if relax:
      QuasiNewton(molecule).run(fmax=relax_fmax)


    """Calculate the vibrational modes of the molecule."""

    # Create vibration calculator
    vib = Vibrations(molecule)
    vib.run()
    vib.summary(method=vib_method, direction=vib_direction)

    self.vibrations_gpaw = vib.get_energies(method=vib_method, direction=vib_direction)

    # Make trajectory files to visualize normal modes:
    for mode in range(self.vibrations_gpaw.shape[0]):
      vib.write_mode(mode)

    #self.vibrations_gpaw = vib.get_energies(method=vib_method, direction=vib_direction)
    

  #
  #
  #
  def get_vib_geometry(self, mode):
    from ase.io import Trajectory
    from ase.io import write

    traj = Trajectory("vib.{0}.traj".format(mode))
    for i, atoms in enumerate(traj):
      #print(atoms.numbers)
      self.geo_vib.append('vib_geo_{0}.xyz'.format(i))
      write('vib_geo_{0}.xyz'.format(i), atoms)

  #
  #
  #
  def get_atoms_mod(self, atoms):
    
    atoms_mod = {}
    for i, pos in enumerate(atoms.positions):
      atoms_mod[i+1] = pos

    return atoms_mod

  #
  #
  #
  def set_Siesta_def(self):
    """
      Set the default Siesta parameter, optional
    """
    
    self.Siesta_GS.fname = self.fileGeo
    self.Siesta_GS.param['SystemName'] = self.sys_name + '_GroundState'
    self.Siesta_GS.param['AtomicCoordinatesFormat'] = 'Ang'
    self.Siesta_GS.param['PAO.EnergyShift'] = {'Valeur':10, 'unit':'meV', 'quantity':'energie'}
    self.Siesta_GS.param['PAO.BasisSize'] = 'DZP'
    self.Siesta_GS.param['DM.Tolerance'] = 0.0001
    self.Siesta_GS.param['DM.MixingWeight'] = 0.01
    self.Siesta_GS.param['MaxSCFIterations'] = 400
    self.Siesta_GS.param['DM.NumberPulay'] = 4
    self.Siesta_GS.param['MD.TypeOfRun'] = 'CG'
    self.Siesta_GS.param['MD.NumCGsteps'] = 0

    self.Siesta_GS.param['MeshCutOff'] = {'Valeur':150, 'unit':'Ry', 'quantity':'length'}

    self.Siesta_GS.param['MD.MaxForceTol'] = {'Valeur':0.02, 'unit':'eV/Ang', 'quantity':'force'}
    self.Siesta_GS.param['COOP.Write'] = '.true.'
    self.Siesta_GS.param['WriteDenchar'] = '.true.'
    self.Siesta_GS.param['WriteForces'] = '.true.'

    self.Siesta_GS.list_param.append('PAO.EnergyShift')
    self.Siesta_GS.list_param.append('PAO.BasisSize')
    self.Siesta_GS.list_param.append('DM.Tolerance')
    self.Siesta_GS.list_param.append('DM.MixingWeight')
    self.Siesta_GS.list_param.append('MaxSCFIterations')
    self.Siesta_GS.list_param.append('DM.NumberPulay')
    self.Siesta_GS.list_param.append('MD.TypeOfRun')
    self.Siesta_GS.list_param.append('MD.NumCGsteps')
    self.Siesta_GS.list_param.append('MeshCutOff')
    self.Siesta_GS.list_param.append('ProjectedDensityOfStates')
    self.Siesta_GS.list_param.append('MD.MaxForceTol')
    self.Siesta_GS.list_param.append('COOP.Write')
    self.Siesta_GS.list_param.append('WriteDenchar')
    self.Siesta_GS.list_param.append('WriteForces')

  #
  #
  #
  def set_tddft_input_def(self):
    """
      Set the default tddft parameter, optional
    """

    self.tddft_inp.param['prod_basis_type'] = 'MIXED'
    self.tddft_inp.param['solver_type'] = 3
    self.tddft_inp.param['gmres_eps'] = 0.001
    self.tddft_inp.param['gmres_itermax'] = 256
    self.tddft_inp.param['gmres_restart'] = 250
    self.tddft_inp.param['gmres_verbose'] = 20
    self.tddft_inp.param['xc_ord_lebedev'] = 14
    self.tddft_inp.param['xc_ord_gl'] = 48
    self.tddft_inp.param['nr'] = 256
    self.tddft_inp.param['akmx'] = 100
    self.tddft_inp.param['eigmin_local'] = 1e-06
    self.tddft_inp.param['eigmin_bilocal'] = 1e-08
    self.tddft_inp.param['nff'] = 1000
    self.tddft_inp.param['d_omega_win1'] = 0.05
    self.tddft_inp.param['freq_eps_win1'] = 0.15
    self.tddft_inp.param['omega_min_tddft'] = 0.0
    self.tddft_inp.param['omega_max_tddft'] = 10.0
    self.tddft_inp.param['ext_field_direction'] = 2
    self.tddft_inp.param['dr'] = np.array([0.3, 0.3, 0.3])
    self.tddft_inp.param['comp_dens_chng_and_polarizability'] = 1
    self.tddft_inp.param['store_dens_chng'] = 1
    self.tddft_inp.param['enh_given_volume_and_freq'] = 0
    self.tddft_inp.param['diag_hs']=0

    self.tddft_inp.param['do_tddft_iter'] = 1

    self.tddft_inp.list_param.append('comp_dens_chng_and_polarizability')
    self.tddft_inp.list_param.append('store_dens_chng')
    self.tddft_inp.list_param.append('enh_given_volume_and_freq')
    self.tddft_inp.list_param.append('diag_hs')
    self.tddft_inp.list_param.append('do_tddft_iter')

#
#
#
def save_array_pol(fname, array, omega):
  """
  Save the polarizability in the same format than in tddft
  """
  saving_array = np.zeros((array.shape[0], array.shape[1]+2), dtype = float)

  for i in range(saving_array.shape[1]):
    if i == 0:
      saving_array[:, i] = omega
    elif i == 1:
      saving_array[:, i] = (1/3.0)*(array[:, 0] + array[:, 4] + array[:, 8])
    else:
      saving_array[:, i] = array[:, i-2]

  np.savetxt(fname, saving_array)

#
#
#
def plot_pol_raman(args, prop):
  """
  plot polarizability
  """
  from pyiter.plot_lib import plot_polarizability, create_folder
  create_folder(prop.folder)
  plot_polarizability(args, prop)


def test_pol(Ram):
  """
  Check the polarizability
  """
  fname = ['GroundState/']
  for i in range(1, 11):
    fname.append('step_{0}/'.format(i))
  
  print(Ram.Pol_inter.shape)
  error = 0.0
  for i in range(Ram.Pol_inter.shape[0]):
    data = np.loadtxt(fname[i] + 'dipol_inter_iter_krylov_re.txt')
    error = error + np.sum(abs(Ram.Pol_inter[i, :, :].real-data[:, 2:11]))
  print('Error: ', error)


