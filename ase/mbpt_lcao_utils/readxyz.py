from __future__ import division
import numpy as np
import h5py as hdf
import re

class Atoms_list:
  def __init__(self):
    """
    Contains the list of the atoms save in the dictionnary
    self.atoms. The key correspond to the atom label, while the value
    is the number of electrons of the atoms (nuclear charge Z).
    And
    the list of the atomic numbers save in the dictionnary atomicNb
    the keys are the number of electrons of the atoms (nuclear charge Z)
    and the value are the specie label.
    """
    #dict specie -> atomic number
    self.atoms = dict()

    self.atoms['H'] = 1
    self.atoms['He'] = 2
    self.atoms['Li'] = 3
    self.atoms['Be'] = 4
    self.atoms['B'] = 5
    self.atoms['C'] = 6
    self.atoms['N'] = 7
    self.atoms['O'] = 8
    self.atoms['F'] = 9
    self.atoms['Ne'] = 10
    self.atoms['Na'] = 11
    self.atoms['Mg'] = 12
    self.atoms['Al'] = 13
    self.atoms['Si'] = 14
    self.atoms['P'] = 15
    self.atoms['S'] = 16
    self.atoms['Cl'] = 17
    self.atoms['Ar'] = 18
    self.atoms['K'] = 19
    self.atoms['Ca'] = 20
    self.atoms['Sc'] = 21
    self.atoms['Ti'] = 22
    self.atoms['V'] = 23
    self.atoms['Cr'] = 24
    self.atoms['Mn'] = 25
    self.atoms['Fe'] = 26
    self.atoms['Co'] = 27
    self.atoms['Ni'] = 28
    self.atoms['Cu'] = 29
    self.atoms['Zn'] = 30
    self.atoms['Ga'] = 31
    self.atoms['Ge'] = 32
    self.atoms['As'] = 33
    self.atoms['Se'] = 34
    self.atoms['Br'] = 35
    self.atoms['Kr'] = 36
    self.atoms['Rb'] = 37
    self.atoms['Sr'] = 38
    self.atoms['Y'] = 39
    self.atoms['Zr'] = 40
    self.atoms['Nb'] = 41
    self.atoms['Mo'] = 42
    self.atoms['Tc'] = 43
    self.atoms['Ru'] = 44
    self.atoms['Rh'] = 45
    self.atoms['Pd'] = 46
    self.atoms['Ag'] = 47
    self.atoms['Cd'] = 48
    self.atoms['Ln'] = 50
    self.atoms['Sn'] = 51
    self.atoms['Sb'] = 52
    self.atoms['Te'] = 53
    self.atoms['I'] = 54
    self.atoms['Xe'] = 55
    self.atoms['Cs'] = 56
    self.atoms['La'] = 57
    self.atoms['Ce'] = 58
    self.atoms['Pr'] = 59
    self.atoms['Nd'] = 60


    #dict atomic number -> specie
    self.atomicNb = dict()

    self.atomicNb[1] = 'H'
    self.atomicNb[2] = 'He'
    self.atomicNb[3] = 'Li'
    self.atomicNb[4] = 'Be'
    self.atomicNb[5] = 'B'
    self.atomicNb[6] = 'C'
    self.atomicNb[7] = 'N'
    self.atomicNb[8] = 'O'
    self.atomicNb[9] = 'F'
    self.atomicNb[10] = 'Ne'
    self.atomicNb[11] = 'Na'
    self.atomicNb[12] = 'Mg'
    self.atomicNb[13] = 'Al'
    self.atomicNb[14] = 'Si'
    self.atomicNb[15] = 'P'
    self.atomicNb[16] = 'S'
    self.atomicNb[17] = 'Cl'
    self.atomicNb[18] = 'Ar'
    self.atomicNb[19] = 'K'
    self.atomicNb[20] = 'Ca'
    self.atomicNb[21] = 'Sc'
    self.atomicNb[22] = 'Ti'
    self.atomicNb[23] = 'V'
    self.atomicNb[24] = 'Cr'
    self.atomicNb[25] = 'Mn'
    self.atomicNb[26] = 'Fe'
    self.atomicNb[27] = 'Co'
    self.atomicNb[28] = 'Ni'
    self.atomicNb[29] = 'Cu'
    self.atomicNb[30] = 'Zn'
    self.atomicNb[31] = 'Ga'
    self.atomicNb[32] = 'Ge'
    self.atomicNb[33] = 'As'
    self.atomicNb[34] = 'Se'
    self.atomicNb[35] = 'Br'
    self.atomicNb[36] = 'Kr'
    self.atomicNb[37] = 'Rb'
    self.atomicNb[38] = 'Sr'
    self.atomicNb[39] = 'Y'
    self.atomicNb[40] = 'Zr'
    self.atomicNb[41] = 'Nb'
    self.atomicNb[42] = 'Mo'
    self.atomicNb[43] = 'Tc'
    self.atomicNb[44] = 'Ru'
    self.atomicNb[45] = 'Rh'
    self.atomicNb[46] = 'Pd'
    self.atomicNb[47] = 'Ag'
    self.atomicNb[48] = 'Cd'
    self.atomicNb[49] = 'Ln'
    self.atomicNb[50] = 'Sn'
    self.atomicNb[51] = 'Sb'
    self.atomicNb[52] = 'Te'
    self.atomicNb[53] = 'I'
    self.atomicNb[54] = 'Xe'
    self.atomicNb[55] = 'Cs'
    self.atomicNb[56] = 'Ba'
    self.atomicNb[57] = 'La'
    self.atomicNb[58] = 'Ce'
    self.atomicNb[59] = 'Pr'
    self.atomicNb[60] = 'Nd'

#################################################

def read_file(fname):
  f = open(fname, 'r')
  LINE = list()

  for line in f:
    LINE.append(line)

  return LINE


#!!!!!!!!!!!!!!!!!!!!!!!!!11
def read_file_list(fname):
  f = open(fname, 'r')
  LINE = list()

  for line in f:
    LINE.append(line[0:len(line)-1])

  return LINE


#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def delete_blanc(L):
  compt = 0
  while L[compt] == ' ' or L[compt] == '\t':
    compt = compt + 1
  L = L[compt:len(L)]
  return L

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def read_number(L):
  compt = 0
  
  while L[compt] != ' ' and compt<(len(L)-1):
    compt = compt + 1
  
  nb1 = float(L[0:compt+1])
  L = L[compt:len(L)]

  return nb1, L

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def recover_data_string(fname, string):
  L = read_file(fname)

  for i in L:
    print(i[0:len(string)], string)
    if i[0:len(string)] == string:
      v = str2float(i)
  return v

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def save_line(Lnb, Cnb, LINE):
  number = LINE[Lnb]
  nombre = list()

  for i in range(Cnb):
    number = delete_blanc(number)
    nb1, number = read_number(number)
    nombre.append(nb1)

  return nombre

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def dim_y(Cnb, L):

  if len(L) == Cnb*10+Cnb*6 +1:
    nb_col = Cnb
  else:
    nb_col = Cnb + 1

  return nb_col

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1

def extractdata_xyz(fname):
#  try:
#    import Avogadro
#    mol=Avogadro.MoleculeFile.readMolecule(fname)
#    Atomic = Atoms_list()
#    atom = []
#    for atom_loop in mol.atoms:
#      specie = Atomic.atomicNb[atom_loop.atomicNumber]
#      coord = atom_loop.pos
#      atom.append([specie, coord])
#  except ImportError:
  L = read_file(fname)
  
  nb_atom = int(L[0])
  atom = list()
  atom.append(nb_atom)

  for i in range(2, len(L)):
    nb = np.array(str2float(L[i]))
    species = recover_specie(L[i])

    atom.append([species, nb])

  return atom

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def read_color_file(fname):

  L = read_file(fname)
  
  atom = list()

  for i in range(len(L)):
    nb = np.array(str2float(L[i]))
    species = recover_specie(L[i])
    #species = delete_number_string(L[i])[0:len(delete_number_string(L[i]))-1] 

    atom.append([species, nb])

  return atom


#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def write_xyz(atoms, fname, rw='w'):
  f = open(fname, rw)
  f.write("     {0}\n".format(atoms[0]))
  f.write("\n")
  
  for i in range(1, len(atoms)):
    f.write(atoms[i][0] + "   {0}    {1}    {2}\n".format(atoms[i][1][0], atoms[i][1][1], atoms[i][1][2]))
    if atoms[i][0] == 'Nae':
      print(i, atoms[i])
  
  f.close()

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def readPDB(fname):
  L = read_file(fname)
  L_data = list()
  atom = list()
  nb_atom = 0

  for i in range(len(L)):
    if L[i][0:4] == 'ATOM':
      nb_atom =nb_atom + 1
      L_data.append(L[i])

  atom.append(nb_atom)

  for i in range(len(L_data)):
    species = L_data[i][12:14]

    coord = L_data[i][26:len(L_data[i])-1]
    
    coord = delete_blanc(coord)
    nb1, coord = read_number(coord)

    coord = delete_blanc(coord)
    nb2, coord = read_number(coord)

    coord = delete_blanc(coord)
    nb3, coord = read_number(coord)

    atom.append([species, np.array([nb1, nb2, nb3])])

  return atom

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def write_HDF5(fname, data):
  outfile = hdf.File(fname, 'w')
  
  #grp_grid = outfile.create_group(groupe)
  
  for key, value in data.items():
    outfile.create_dataset(key, data=value)
  
  outfile.close()

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def read_HDF5(fname, keys, groupe):
  f = hdf.File(fname, 'r')
  
  dico = dict()
  
  for i in keys:
    dico[i] = f[groupe + i]
  
  return dico

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def readSiestaFA(fname):
  L = read_file(fname)

  Forces = []
  for i in range(1, len(L)):
    Forces.append([i, np.array(str2float(L[i])[1:4])])
  
  return Forces

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def XV2xyz(fin, fout):
  """
  Convert the .XV file provide by siesta during the relaxation in 
  an xyz file.
  Input parameters:
  -----------------
  fin (string): name of the unput file
  fout (string): name of the output file
  """
  L = read_file(fin)

  nb = []
  for i in L:
    nb.append(np.array(str2float(i)))

  ATM = Atoms_list()
  atoms = []
  for i, j in enumerate(nb):
    if len(atoms) !=0:
      atoms.append([ATM.atomicNb[j[1]], j[2:5]*0.52918])  #bohr to angstrom
                                                          #siesta write the coordinate in bohr

    if j.shape[0] == 1:
      atoms.append(int(j[0]))

  write_xyz(atoms, fout)

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
def readBasis_spec(fname, nb_specie):
  """
  Example Basis_specs from siesta output
   <basis_specs>
   ===============================================================================
   H                    Z=   1    Mass=  1.0100        Charge= 0.17977+309
   Lmxo=0 Lmxkb= 2    BasisType=split      Semic=F
   L=0  Nsemic=0  Cnfigmx=1
             n=1  nzeta=2  polorb=1
               splnorm:   0.15000
                  vcte:    0.0000
                  rinn:    0.0000
                   rcs:    0.0000      0.0000
               lambdas:    1.0000      1.0000
   -------------------------------------------------------------------------------
   L=0  Nkbl=1  erefs: 0.17977+309
   L=1  Nkbl=1  erefs: 0.17977+309
   L=2  Nkbl=1  erefs: 0.17977+309
   ===============================================================================
   </basis_specs>
  """

  L = read_file(fname)
  
  specie_charac = {}
  line = 0
  len_basis = len('<basis_specs>')
  i = 0
  while i<nb_specie:
    if L[line][0:len_basis] == '<basis_specs>':
      i = i + 1
      info = str2float(L[line+2])
      if L[line+2][1] == ' ':
        specie_charac[L[line+2][0]] = {'Z': info[0], 'Mass': info[1], 'Charge': info[2]}
      else:
        specie_charac[L[line+2][0:2]] = {'Z': info[0], 'Mass': info[1], 'Charge': info[2]}

      while L[line][0:len('</basis_specs>')] != '</basis_specs>':
        line = line + 1
    else:
      line = line + 1

  return specie_charac

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#  L = read_file(fname)
#  atom = list()
#  nb_atom = len(L)
#  atom.append(nb_atom)
#
#  for i in L:
#    coord = delete_blanc(i)
#    nb1, coord = read_number(coord)
#
#    coord = delete_blanc(coord)
#    nb2, coord = read_number(coord)
#
#    coord = delete_blanc(coord)
#    nb3, coord = read_number(coord)
#
#    atom.append([species, np.array([nb1, nb2, nb3])])
#
#  out_name = fname[0:len(fname)-3] + 'xyz'
#  write_xyz(atom, out_name)

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


def txt2xyz(fname, species):
  L = read_file(fname)
  numeric_const_pattern = r"""
  [-+]? # optional sign
  (?:
    (?: \d* \. \d+ ) # .1 .12 .123 etc 9.1 etc 98.1 etc
    |
    (?: \d+ \.? ) # 1. 12. 123. etc 1 12 123 etc
  )
  # followed by optional exponent part if desired
  (?: [Ee] [+-]? \d+ ) ?
  """
  rx = re.compile(numeric_const_pattern, re.VERBOSE)
  coord = list()
  
  for i in L:
    coord.append(rx.findall(i))

  nb_atoms = len(coord)

  atoms = [nb_atoms]
  comp = 0

  for i in coord[0:len(coord)]:
    c = np.array([float(i[0]), float(i[1]), float(i[2])])
    atoms.append([species, c])
    comp = comp + 1

  write_xyz(atoms, fname[0:len(fname)-4] + '.xyz')

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1

def str2float(string):
  numeric_const_pattern = r"""
  [-+]? # optional sign
  (?:
    (?: \d* \. \d+ ) # .1 .12 .123 etc 9.1 etc 98.1 etc
    |
    (?: \d+ \.? ) # 1. 12. 123. etc 1 12 123 etc
  )
  # followed by optional exponent part if desired
  (?: [Ee] [+-]? \d+ ) ?
  """
  rx = re.compile(numeric_const_pattern, re.VERBOSE)

  nb = rx.findall(string)
  for i in enumerate(nb):
    nb[i[0]] = float(i[1])

  return np.array(nb)

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def str2int(string):
  numeric_const_pattern = r"""
  [-+]? # optional sign
  (?:
    (?: \d* \. \d+ ) # .1 .12 .123 etc 9.1 etc 98.1 etc
    |
    (?: \d+ \.? ) # 1. 12. 123. etc 1 12 123 etc
  )
  # followed by optional exponent part if desired
  (?: [Ee] [+-]? \d+ ) ?
  """
  rx = re.compile(numeric_const_pattern, re.VERBOSE)

  nb = rx.findall(string)
  for i in enumerate(nb):
    nb[i[0]] = int(i[1])

  return np.array(nb)

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1

def recover_specie(string):
  """
  Select specie in a string of caractere from
  a .xyz file
  Input parameters:
    string (str): the string to analyse
  Output parameter:
    string_p (str): the specie
  """

  specie = list()
  comp = 0
  letter = string[0]
  if letter == ' ':
    while letter == ' ' or comp>= len(string):
      letter = string[comp]
      comp = comp + 1
    while letter != ' ' or comp>= len(string):
      letter = string[comp]
      specie.append(letter)
      comp = comp + 1
  else:
    while letter != ' ' or comp>= len(string):
      letter = string[comp]
      specie.append(letter)
      comp = comp + 1

  specie.remove(' ')

  string_p = ''
  for i in specie:
    string_p = string_p + i

  return string_p

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1

def delete_number_string(string):
#not working for exponential expression
  nb_list = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', '-']
  L = list()

  for i in string:
    L.append(i)

  L_P = list()
  for i in enumerate(L):
    inside = False
    for j in nb_list:
      if i[1] == j:
        inside  = True

    if inside == False:
      L_P.append(i[1])

  string_p = ''
  for i in L_P:
    if i != ' ':
      string_p = string_p + i

  return string_p
    


