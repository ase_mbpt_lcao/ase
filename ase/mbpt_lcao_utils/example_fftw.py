from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
import FFT_routines as fft_py
import time
import sys

x = np.linspace(-5, 5, 10000)
t = np.linspace(-5, 5, 1200)
X, T = np.meshgrid(x, t)

#f = np.exp(-(X**2+T**2))*complex(0, 1)
f = np.exp(-(t**2))#*complex(0, 1)

w = fft_py.fft_freq_1D(t)
t1 = time.time()
Fp = fft_py.mod_FFT_1D(w, t, f)
t2 = time.time()
tp = t2-t1
print('python:', tp)


t1 = time.time()
Ff = fft_py.FFT_fort(w, t, f, dtype=float)
t2 = time.time()
tf = t2-t1
print('f2py:', tf)
print('ratio:', tp/tf)
print(np.sum(abs(Ff-Fp)))
sys.exit()
#print(w-wf)

plt.figure(1)
plt.imshow(Fp.real)
plt.figure(2)
plt.imshow(Ff.real)
plt.show()
