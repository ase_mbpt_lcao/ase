module blas_wrapper
  contains

!
!
!
subroutine get_upper_triangulat_matrix(A, Ap, n) bind(c, name="get_upper_triangulat_matrix")
  use iso_c_binding

  integer(C_INT), intent(in), value :: n
  real(C_FLOAT), intent(in) :: A(n, n)
  real(C_FLOAT), intent(out) :: Ap(n*(n+1)/2)

  integer :: i, j, counter

  Ap = 0.0
  counter = 1

  do j=1, N
    do i=1, N
      if (j >= i) then
        Ap(counter) = A(i, j)
        counter = counter + 1
      endif
    enddo
  enddo

end subroutine !get_upper_triangulat_matrix

!
!
!
subroutine test_upper_triangular(n, A, x, y_gemv, y_spmv) bind(c, name="test_upper_triangular")
  ! A must be colum major
  use iso_c_binding

  integer(C_INT), intent(in), value :: n
  real(C_FLOAT), intent(in) :: A(n, n), x(n)
  real(C_FLOAT), intent(out) :: y_gemv(n), y_spmv(n) 

  real(4), allocatable :: Ap(:)
  integer :: counter, i, j
  real(4) :: alpha, beta

  alpha = 1.0
  beta = 0.0

  allocate(Ap(n*(n+1)/2))
  Ap = 0.0
  y_spmv = 0.0
  y_gemv = 0.0

  counter = 1
  !print*, "A = "
  do j=1, N
    do i=1, N
      if (j >= i) then
        Ap(counter) = A(i, j)
        counter = counter + 1
      endif
    enddo
    !print*, A(:, j)
  enddo
  !print*, "Ap = "
  !print*, Ap

  call sgemv ("N", N, N, alpha, A, N, x, 1, beta, y_gemv, 1);
  print*, "sgemv: sum(y) = ", sum(abs(y_gemv))
  
  call sspmv ("U", N, alpha, Ap, x, 1, beta, y_spmv, 1);
  print*, "sspmv: sum(y) = ", sum(abs(y_spmv))
  print*, "Error: ", sum(abs(y_gemv-y_spmv))

  deallocate(Ap)

end subroutine !test_upper_triangular


!
!
!
subroutine SSPMV_wrapper(uplo, n, alpha, ap, x, incx, beta, y, incy) bind(c, name="SSPMV_wrapper")
  use iso_c_binding

  implicit none
  integer(C_INT), intent(in), value :: uplo, n, incx, incy
  real(C_FLOAT), intent(in), value :: alpha, beta
  real(C_FLOAT), intent(in) :: ap(n*(n+1)/2), x(n)
  real(C_FLOAT), intent(out) :: y(n)

  character(1) :: uplo_ch

  if (uplo == 0) then
    uplo_ch = "U"
  else if (uplo == 1) then
    uplo_ch = "L"
  else
    stop "uplo not 0 or 1!"
  endif

  call sspmv (uplo_ch, n, alpha, ap, x, incx, beta, y, incy);

end subroutine !SSPMV_wrapper

!
!
!
subroutine DSPMV_wrapper(uplo, n, alpha, ap, x, incx, beta, y, incy) bind(c, name="DSPMV_wrapper")
  use iso_c_binding

  implicit none
  integer(C_INT), intent(in), value :: uplo, n, incx, incy
  real(C_DOUBLE), intent(in), value :: alpha, beta
  real(C_DOUBLE), intent(in) :: ap(n*(n+1)/2), x(n)
  real(C_DOUBLE), intent(out) :: y(n)

  character(1) :: uplo_ch


  if (uplo == 0) then
    uplo_ch = "U"
  else if (uplo == 1) then
    uplo_ch = "L"
  else
    stop "uplo not 0 or 1!"
  endif

  !print*, uplo_ch, n, incx, incy, alpha, beta, size(ap) 
  !print*, ap
  !print*, x 
  call dspmv (uplo_ch, n, alpha, ap, x, incx, beta, y, incy);

end subroutine !SSPMV_wrapper

!
!
!
subroutine SGEMV_wrapper(trans, m, n, alpha, ap, lda, x, incx, beta, y, incy) bind(c, name="SGEMV_wrapper")
  use iso_c_binding

  implicit none
  integer(C_INT), intent(in), value :: trans, m, n, lda, incx, incy
  real(C_FLOAT), intent(in), value :: alpha, beta
  real(C_FLOAT), intent(in) :: ap(n, n), x(n)
  real(C_FLOAT), intent(out) :: y(n)

  character(1) :: trans_ch

  if (trans == 0) then
    trans_ch = "N"
  else if (trans == 1) then
    trans_ch = "T"
  else
    stop "uplo not 0 or 1!"
  endif

  call sgemv (trans_ch, m, n, alpha, ap, lda, x, incx, beta, y, incy);

end subroutine !SSPMV_wrapper


!
!
!
subroutine DGEMV_wrapper(trans, m, n, alpha, ap, lda, x, incx, beta, y, incy) bind(c, name="DGEMV_wrapper")
  use iso_c_binding

  implicit none
  integer(C_INT), intent(in), value :: trans, m, n, lda, incx, incy
  real(C_DOUBLE), intent(in), value :: alpha, beta
  real(C_DOUBLE), intent(in) :: ap(n, n), x(n)
  real(C_DOUBLE), intent(out) :: y(n)

  character(1) :: trans_ch

  if (trans == 0) then
    trans_ch = "N"
  else if (trans == 1) then
    trans_ch = "T"
  else
    stop "uplo not 0 or 1!"
  endif

  call dgemv (trans_ch, m, n, alpha, ap, lda, x, incx, beta, y, incy);

end subroutine !SSPMV_wrapper

!
!
!
subroutine SCSRGEMV_wrapper(trans, m, a, size_a, ia, size_ia, ja, size_ja, x, y) bind(c, name="SCSRGEMV_wrapper")
  use iso_c_binding

  implicit none
  integer(C_INT), intent(in), value :: trans, m, size_a, size_ia, size_ja
  integer(C_INT), intent(in) :: ia(size_ia), ja(size_ja)
  real(C_FLOAT), intent(in) :: a(size_a), x(m)
  real(C_FLOAT), intent(out) :: y(m)

  character(1) :: trans_ch

  if (trans == 0) then
    trans_ch = "N"
  else if (trans == 1) then
    trans_ch = "T"
  else
    stop "uplo not 0 or 1!"
  endif

  call mkl_cspblas_scsrgemv (trans_ch, m, a, ia, ja, x, y);

end subroutine !CSRSSPMV_wrapper

!
!
!
subroutine DCSRGEMV_wrapper(trans, m, a, size_a, ia, size_ia, ja, size_ja, x, y) bind(c, name="DCSRGEMV_wrapper")
  use iso_c_binding

  implicit none
  integer(C_INT), intent(in), value :: trans, m, size_a, size_ia, size_ja
  integer(C_INT), intent(in) :: ia(size_ia), ja(size_ja)
  real(C_DOUBLE), intent(in) :: a(size_a), x(m)
  real(C_DOUBLE), intent(out) :: y(m)

  character(1) :: trans_ch

  if (trans == 0) then
    trans_ch = "N"
  else if (trans == 1) then
    trans_ch = "T"
  else
    stop "uplo not 0 or 1!"
  endif

  call mkl_cspblas_dcsrgemv (trans_ch, m, a, ia, ja, x, y);

end subroutine !CSRSSPMV_wrapper


end module !blas_wrapper.f90
