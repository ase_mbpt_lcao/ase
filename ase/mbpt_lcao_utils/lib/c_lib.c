#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h> 


extern "C" void calc_mean_dens_chng_cylinder_pecular_dir_c(float *dn_re, float *dn_im,
    double radius, float *xmesh, float *ymesh, float *zmesh, float *z, float *dn_out,
    int M, int N, int P, int Q)
{
  int i, j, k, zind, index, indexp, nthreads, id;
  float rho;

  # pragma omp parallel
  {
    id = omp_get_thread_num ( );
    nthreads = omp_get_num_threads();
    if ( id == 0 )
    {
      printf ( "  Using %d threads\n", nthreads); 
    }
  }

  # pragma omp parallel \
  shared ( nthreads, M, N, P, xmesh, ymesh, zmesh, z, dn_re, dn_im, dn_out) \
  private ( i, j, k, indexp, rho, zind) num_threads(nthreads)
  {
    # pragma omp for reduction(+:dn_out)
    for (k=0; k<P; k++)
    {
    for (j=0; j<N; j++)
    {
    for (i=0; i<M; i++)
    {
      //index = k + P*(j+N*i);
      indexp = i + M*(j+N*k); // the index for dn are inversed!!
      if (zmesh[indexp]>0.0)
      {
        rho = sqrt(pow(xmesh[indexp], 2) + pow(ymesh[indexp], 2));
        if (rho < radius)
        {
          zind = 0;
          while (z[zind]<zmesh[indexp])
          {
            zind = zind + 1;
          }
          dn_out[0 + zind*2] = dn_out[0 + zind*2] + dn_re[indexp];
          dn_out[1 + zind*2] = dn_out[1 + zind*2] + dn_im[indexp];
        }
      }
    }
    }
    }
  }
  
}
