module fortran_lib
  contains

!
!
!
subroutine calc_d_param_new_method_peter_fort(dn_re, dn_im, inter_atom, p2xyz, x, y, z, Edir, &
      I0, I1, I2, n, m, p, npnt, factor) bind(c, name="calc_d_param_new_method_peter_fort")
  use iso_c_binding

  implicit none
  integer(C_INT), intent(in), value :: n, m, p, npnt, Edir
  real(C_FLOAT), intent(in), value :: inter_atom, factor
  real(C_FLOAT), intent(in) :: dn_re(n, m, p), dn_im(n, m, p)
  real(C_DOUBLE), intent(in) :: p2xyz(npnt, 3)
  real(C_DOUBLE), intent(in) :: x(n), y(m), z(p)
  real(C_DOUBLE), intent(out) :: I0(2), I1(2), I2(2)

  integer :: i, j, k, pnt, pmin(1)
  real(8), allocatable :: p2d(:), part0(:, :, :), part1(:, :, :), part2(:, :, :)
  real(8) :: r, xyz(3), r0, d

  allocate(p2d(npnt))
  p2d = 0D0

  if (Edir == 1) then
    allocate(part0(1:m, 1:p, 2))
    allocate(part1(1:m, 1:p, 2))
    allocate(part2(1:m, 1:p, 2))
    do i = 1, n
      if (x(i)>0.0) then
        part0 = 0.0
        part1 = 0.0
        part2 = 0.0


        !$OMP PARALLEL DEFAULT(NONE) &
        !$OMP PRIVATE(j, k, pnt, xyz, r, d, p2d, pmin, r0) &
        !$OMP SHARED(p, m, n, i, x, y, z, p2xyz, dn_re, dn_im, factor) &
        !$OMP SHARED(part0, part1, part2, inter_atom, npnt)
        !$OMP DO SCHEDULE(DYNAMIC,1)
        do k = 1, p
        do j = 1, m
          xyz = (/x(i), y(j), z(k)/)
          r = sqrt( sum(xyz**2))
          do pnt = 1, npnt
            p2d(pnt)  = sum((xyz-p2xyz(pnt, :))**2)
          enddo
          pmin = minloc(p2d)
          r0 = sqrt(sum(p2xyz(pmin(1), :)**2))
          d  = get_sign(r-r0)*sqrt(p2d(pmin(1)))
          !d_arr(i, j, k) = d
          part0(j, k, :) = (/dn_re(i, j, k), dn_im(i, j, k)/)
          part1(j, k, :) = r*(/dn_re(i, j, k), dn_im(i, j, k)/)
          !d_dn_arr(i, j, k, :) = d*(/dn_re(i, j, k), dn_im(i, j, k)/)
          if (factor > 0.0) then
            if (abs(d)<factor*inter_atom) then
              part2(j, k, :) = d*(/dn_re(i, j, k), dn_im(i, j, k)/)
            endif
          else
            part2(j, k, :) = d*(/dn_re(i, j, k), dn_im(i, j, k)/)
          endif
        enddo
        enddo
        !$OMP END DO

        !$OMP END PARALLEL

        I0(:) = I0(:) + (/sum(part0(:, :, 1)), sum(part0(:, :, 2))/)
        I1(:) = I1(:) + (/sum(part1(:, :, 1)), sum(part1(:, :, 2))/)
        I2(:) = I2(:) + (/sum(part2(:, :, 1)), sum(part2(:, :, 2))/)

      endif
      
    enddo
    deallocate(part0)
    deallocate(part1)
    deallocate(part2)

  else if (Edir == 2) then
    allocate(part0(1:m, 1:p, 2))
    allocate(part1(1:m, 1:p, 2))
    allocate(part2(1:m, 1:p, 2))
    do j = 1, m
      if (x(i)>0.0) then
        part0 = 0.0
        part1 = 0.0
        part2 = 0.0


        !$OMP PARALLEL DEFAULT(NONE) &
        !$OMP PRIVATE(k, i, pnt, xyz, r, d, p2d, pmin, r0) &
        !$OMP SHARED(p, m, n, j, x, y, z, p2xyz, dn_re, dn_im, factor) &
        !$OMP SHARED(part0, part1, part2, inter_atom, npnt)
        !$OMP DO SCHEDULE(DYNAMIC,1)
        do k = 1, p
        do i = 1, n
          xyz = (/x(i), y(j), z(k)/)
          r = sqrt( sum(xyz**2))
          do pnt = 1, npnt
            p2d(pnt)  = sum((xyz-p2xyz(pnt, :))**2)
          enddo
          pmin = minloc(p2d)
          r0 = sqrt(sum(p2xyz(pmin(1), :)**2))
          d  = get_sign(r-r0)*sqrt(p2d(pmin(1)))
          part0(j, k, :) = (/dn_re(i, j, k), dn_im(i, j, k)/)
          part1(j, k, :) = r*(/dn_re(i, j, k), dn_im(i, j, k)/)
          if (factor > 0.0) then
            if (abs(d)<factor*inter_atom) then
              part2(j, k, :) = d*(/dn_re(i, j, k), dn_im(i, j, k)/)
            endif
          else
            part2(j, k, :) = d*(/dn_re(i, j, k), dn_im(i, j, k)/)
          endif
        enddo
        enddo
        !$OMP END DO

        !$OMP END PARALLEL

        I0(:) = I0(:) + (/sum(part0(:, :, 1)), sum(part0(:, :, 2))/)
        I1(:) = I1(:) + (/sum(part1(:, :, 1)), sum(part1(:, :, 2))/)
        I2(:) = I2(:) + (/sum(part2(:, :, 1)), sum(part2(:, :, 2))/)

      endif
      
    enddo
    deallocate(part0)
    deallocate(part1)
    deallocate(part2)

  else if (Edir == 3) then
    allocate(part0(1:m, 1:p, 2))
    allocate(part1(1:m, 1:p, 2))
    allocate(part2(1:m, 1:p, 2))
    do k = 1, p
      if (z(k)>0.0) then
        part0 = 0.0
        part1 = 0.0
        part2 = 0.0


        !$OMP PARALLEL DEFAULT(NONE) &
        !$OMP PRIVATE(j, i, pnt, xyz, r, d, p2d, pmin, r0) &
        !$OMP SHARED(p, m, n, k, x, y, z, p2xyz, dn_re, dn_im, factor) &
        !$OMP SHARED(part0, part1, part2, inter_atom, npnt)
        !$OMP DO SCHEDULE(DYNAMIC,1)
        do j = 1, m
        do i = 1, n
          xyz = (/x(i), y(j), z(k)/)
          r = sqrt( sum(xyz**2))
          do pnt = 1, npnt
            p2d(pnt)  = sum((xyz-p2xyz(pnt, :))**2)
          enddo
          pmin = minloc(p2d)
          r0 = sqrt(sum(p2xyz(pmin(1), :)**2))
          d  = get_sign(r-r0)*sqrt(p2d(pmin(1)))
          part0(j, k, :) = (/dn_re(i, j, k), dn_im(i, j, k)/)
          part1(j, k, :) = r*(/dn_re(i, j, k), dn_im(i, j, k)/)
          if (factor > 0.0) then
            if (abs(d)<factor*inter_atom) then
              part2(j, k, :) = d*(/dn_re(i, j, k), dn_im(i, j, k)/)
            endif
          else
            part2(j, k, :) = d*(/dn_re(i, j, k), dn_im(i, j, k)/)
          endif
        enddo
        enddo
        !$OMP END DO

        !$OMP END PARALLEL

        I0(:) = I0(:) + (/sum(part0(:, :, 1)), sum(part0(:, :, 2))/)
        I1(:) = I1(:) + (/sum(part1(:, :, 1)), sum(part1(:, :, 2))/)
        I2(:) = I2(:) + (/sum(part2(:, :, 1)), sum(part2(:, :, 2))/)

      endif
      
    enddo
    deallocate(part0)
    deallocate(part1)
    deallocate(part2)

  else if (Edir == 0) then
    allocate(part0(1:m, 1:p, 2))
    allocate(part1(1:m, 1:p, 2))
    allocate(part2(1:m, 1:p, 2))
    do k = 1, p
        part0 = 0.0
        part1 = 0.0
        part2 = 0.0


        !$OMP PARALLEL DEFAULT(NONE) &
        !$OMP PRIVATE(j, i, pnt, xyz, r, d, p2d, pmin, r0) &
        !$OMP SHARED(p, m, n, k, x, y, z, p2xyz, dn_re, dn_im, factor) &
        !$OMP SHARED(part0, part1, part2, inter_atom, npnt)
        !$OMP DO SCHEDULE(DYNAMIC,1)
        do j = 1, m
        do i = 1, n
          xyz = (/x(i), y(j), z(k)/)
          r = sqrt( sum(xyz**2))
          do pnt = 1, npnt
            p2d(pnt)  = sum((xyz-p2xyz(pnt, :))**2)
          enddo
          pmin = minloc(p2d)
          r0 = sqrt(sum(p2xyz(pmin(1), :)**2))
          d  = get_sign(r-r0)*sqrt(p2d(pmin(1)))
          part0(j, k, :) = (/dn_re(i, j, k), dn_im(i, j, k)/)
          part1(j, k, :) = r*(/dn_re(i, j, k), dn_im(i, j, k)/)
          if (factor > 0.0) then
            if (abs(d)<factor*inter_atom) then
              part2(j, k, :) = d*(/dn_re(i, j, k), dn_im(i, j, k)/)
            endif
          else
            part2(j, k, :) = d*(/dn_re(i, j, k), dn_im(i, j, k)/)
          endif
        enddo
        enddo
        !$OMP END DO

        !$OMP END PARALLEL

        I0(:) = I0(:) + (/sum(part0(:, :, 1)), sum(part0(:, :, 2))/)
        I1(:) = I1(:) + (/sum(part1(:, :, 1)), sum(part1(:, :, 2))/)
        I2(:) = I2(:) + (/sum(part2(:, :, 1)), sum(part2(:, :, 2))/)

    enddo
    deallocate(part0)
    deallocate(part1)
    deallocate(part2)
  else
    print*, 'Not implemented, Edir must be 1, 2, or 3'
    stop
  endif
  deallocate(p2d)


end subroutine !calc_d_param_new_method_peter_fort

!
!
!
function get_sign(a) result(signe)

  implicit none
  real(8), intent(in) :: a
  real(8) :: signe

  if (a<0D0) then 
    signe = -1D0
  else
    signe = 1D0
  endif

end function

!
!
!
subroutine calc_centroid_dens_chng_cylinder_pecular_dir_fort(dn_re, dn_im, radius, meshx, &
      meshy, meshz, z, dn_out, n, m, p, q) bind(c, name="calc_centroid_dens_chng_cylinder_pecular_dir_fort")
  use iso_c_binding

  implicit none
  integer(C_INT), intent(in), value :: n, m, p, q
  real(C_FLOAT), intent(in), value :: radius
  real(C_FLOAT), intent(in) :: dn_re(n, m, p), dn_im(n, m, p)
  real(C_FLOAT), intent(in) :: meshx(n, m, p), meshy(n, m, p), meshz(n, m, p), z(q)
  real(C_FLOAT), intent(out) :: dn_out(p, 2)

  integer :: i, j, k, zind
  real(4) :: rho

  do k = 1, p
  do j = 1, m
  do i = 1, n
    if (meshz(i, j, k)> 0.0) then
      rho = sqrt(meshx(i, j, k)**2 + meshy(i, j, k)**2)
      if (rho < radius) then
        zind = 0
        do while (z(zind)<meshz(i,j,k))
          zind = zind + 1
        enddo
        dn_out(zind, 1) = dn_out(zind, 1) + meshz(i,j,k)*dn_re(i, j, k)
        dn_out(zind, 2) = dn_out(zind, 2) + meshz(i,j,k)*dn_im(i, j, k)
      endif
    endif
  enddo
  enddo
  enddo

end subroutine !calc_mean_dens_chng_cylinder_pecular_dir_fort

!
!
!
subroutine calc_mean_dens_chng_cylinder_pecular_dir_fort(dn_re, dn_im, radius, meshx, &
      meshy, meshz, z, dn_out_re, dn_out_im, m, n, p, q) &
      bind(c, name="calc_mean_dens_chng_cylinder_pecular_dir_fort")
  use iso_c_binding

  implicit none
  integer(C_INT), intent(in), value :: n, m, p, q
  real(C_DOUBLE), intent(in), value :: radius
  real(C_FLOAT), intent(in) :: dn_re(m*n*p), dn_im(m*n*p)
  real(C_FLOAT), intent(in) :: meshx(m*n*p), meshy(m*n*p), meshz(m*n*p), z(q)
  real(C_FLOAT), intent(out) :: dn_out_re(q), dn_out_im(q)

  integer :: i, j, k, zind, ind
  real(8) :: rho

  dn_out_re = 0.0
  dn_out_im = 0.0
  print*, 'fortran'

  !$OMP PARALLEL DEFAULT(NONE) &
  !$OMP PRIVATE(i, j, k, ind, rho, zind) &
  !$OMP SHARED(m, n, p, meshx, meshy, meshz, dn_re, dn_im) &
  !$OMP SHARED(z, dn_out_re, dn_out_im, radius)

  !$OMP DO REDUCTION(+: dn_out_re, dn_out_im)
  do k = 1, p
  do j = 1, n
  do i = 1, m
    !ind = k + p*(j-1 + n*(i-1))
    ind = i + m*(j-1 + n*(k-1))

    if (meshz(ind)> 0.0) then
      rho = sqrt(meshx(ind)**2 + meshy(ind)**2)
      if (rho < radius) then
        zind = 1
        do while (z(zind)<meshz(ind))
          zind = zind + 1
        enddo
        dn_out_re(zind) = dn_out_re(zind) + dn_re(ind)
        dn_out_im(zind) = dn_out_im(zind) + dn_im(ind)
      endif
    endif
  enddo
  enddo
  enddo
  !$OMP END DO

  !$OMP END PARALLEL

end subroutine !calc_mean_dens_chng_cylinder_pecular_dir_fort


end module !fortran_lib
