from __future__ import division
import numpy as np
from ase.data.colors import cpk_colors

import paraview.simple as para

def plot_atoms(atoms, **kw):

    for pos, Z in zip(atoms.positions, atoms.numbers):
        sph = para.Sphere(Center=pos, **kw)
        dips = para.Show(sph)
        dips.DiffuseColor = cpk_colors[Z]

    para.Render()
