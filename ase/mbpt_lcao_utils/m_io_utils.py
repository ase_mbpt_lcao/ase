from __future__ import division
import numpy as np
import ase.io as io
import h5py
from ase.mbpt_lcao_utils.utils import interpolate

def read_tem(fname, v, b, **kw):

    assert b.size == 3

    inter = kw["inter"] if "inter" in kw else "inter"
    interpol = kw["interpolate"] if "interpolate" in kw else False
    show_iter = kw["show_iter"] if "show_iter" in kw else False

    f = h5py.File(fname, "r")["tem_spectrum"]

    if show_iter:
        niter = f["xyz_f2niter_v{0:.8f}_bx{1:.8f}_by{2:.8f}_bz{3:.8f}".format(v, *b)].value
        print(niter.size)
        print(niter)

    tem = f["tem_spectrum_" + inter + "_v{0:.8f}_bx{1:.8f}_by{2:.8f}_bz{3:.8f}".format(v, *b)].value
    
    if interpol:
        print(kw)
        func, popt = interpolate(tem[0, :], tem[1, :], **kw)
        freq = np.linspace(tem[0, 0], tem[0, tem.shape[1]-1], tem.shape[1]*100)

        return freq, func(freq, *popt)
    else:
        return tem[0, :], tem[1, :]

def write_geofdf(atoms, fname="geo.fdf"):
    """
    Write geometry file for siesta using geometry from ase atoms object
    """
    from ase.data import chemical_symbols

    f=open(fname, "w")
    f.write("NumberOfAtoms   {0}\n".format(len(atoms)))
    f.write("NumberOfSpecies {0}\n".format(len(set(atoms.numbers))))
    f.write("\n")
    f.write('%block ChemicalSpeciesLabel\n')
    species_label = {}
    for i, nb in enumerate(set(atoms.numbers)):
        species_label[chemical_symbols[nb]] = i+1
        f.write('  {0}  {1}  '.format(i+1, nb)+ chemical_symbols[nb]+'\n')
    f.write("%endblock ChemicalSpeciesLabel\n")
    f.write("\n")

    f.write("AtomicCoordinatesFormat  Ang\n")
    f.write("%block AtomicCoordinatesAndAtomicSpecies\n")
    for ia, atm in enumerate(atoms):
        pos = atm.position
        f.write("{0:.6f}  {1:.6f}  {2:.6f}  {3} {4}  ".format(pos[0], pos[1], pos[2],
            species_label[atm.symbol], ia+1) + atm.symbol + "\n")

    f.write("%endblock AtomicCoordinatesAndAtomicSpecies")

    f.close()
    
def write_geofdf_Zmatrix(atoms, fname="geo.fdf", species_relax = []):
    """
    Write geometry file for siesta using geometry from ase atoms object
    with the ZMatrix method allowing to relax only particular atoms
    """
    from ase.data import chemical_symbols

    f=open(fname, "w")
    f.write("NumberOfAtoms   {0}\n".format(len(atoms)))
    f.write("NumberOfSpecies {0}\n".format(len(set(atoms.numbers))))
    f.write("\n")
    f.write('%block ChemicalSpeciesLabel\n')
    species_label = {}
    for i, nb in enumerate(set(atoms.numbers)):
        species_label[chemical_symbols[nb]] = i+1
        f.write('  {0}  {1}  '.format(i+1, nb)+ chemical_symbols[nb]+'\n')
    f.write("%endblock ChemicalSpeciesLabel\n")
    f.write("\n")

    f.write('ZM.UnitsLength   Ang\n')
    f.write('%block Zmatrix\n')
    f.write('cartesian\n')
    for ia, atm in enumerate(atoms):
        #f.write("{0:.6f}  {1:.6f}  {2:.6f}  {3} {4}  ".format(pos[0], pos[1], pos[2],
        #    species_label[atm.symbol], ia+1) + atm.symbol + "\n")
        if isinstance(species_relax[0], str):
            if atm.symbol in species_relax:
                ox, oy, oz = 1, 1, 1
            else:
                ox, oy, oz = 0, 0, 0
        elif isinstance(species_relax[0], int):
            if ia in species_relax:
                ox, oy, oz = 1, 1, 1
            else:
                ox, oy, oz = 0, 0, 0
        else:
            raise ValueError("species_relax must be list of int or list of str")
 
        f.write("{0}  {1:.5f}  {2:.5f}  {3:.5f}  {4}  {5}  {6}  {7}  ".format(species_label[atm.symbol], atm.x, atm.y, atm.z, ox, oy, oz, ia)+atm.symbol+'\n')

    f.write('%endblock Zmatrix')
    f.close()
    
def write_eels_traj_x3d(atoms, fname, b_eels, color = np.array([1.0, 0.0, 0.0]), cam_pos = np.array([50.55178, 6.58028, -4.71022]),
        cam_orientation = np.array([0.48908, 0.69708, 0.52429, 2.10130]), cyl_orien = np.array([1.0, 0.0, 0.0]), cyl_rad = 0.20, cyl_height = 60.0):
    """
    write x3d geometry of the ase atoms object and electron beam trajectory of eels calculations
    """

    io.write(fname, atoms, format="x3d")
    f = open(fname, "r")
    lines = f.readlines()
    f.close()
    
    offset = 0
    for line in lines:
        if "</Scene>" in line:
            break
        offset += 1


    new_lines = lines[0:offset]
    
    if len(b_eels.shape) == 1:
        cyl_html =['<Transform translation="{0:.5f} {1:.5f} {2:.5f}" rotation="{3} {4} {5} 1.57">\n'.format(b_eels[0], b_eels[1], b_eels[2],
                                                                                                        cyl_orien[0], cyl_orien[1], cyl_orien[2]),
                '    <Shape>\n',
                '    <Appearance>\n',
                '    <Material diffuseColor="' +
                '{0:.2f} {1:.2f} {2:.2f}'.format(color[0], color[1], color[2]) +
                '" specularColor="0.5 0.5 0.5">\n',
                '    </Material>\n',
                '    </Appearance>\n',
                '        <Cylinder radius="{0}" height="{1}">\n'.format(cyl_rad, cyl_height),
                '        </Cylinder>\n',
                '    </Shape>\n',
                '</Transform>\n',
                ]
        for l in cyl_html:
            new_lines.append(l)
    elif len(b_eels.shape) == 2 and len(color.shape) == 1:
        for ib in range(b_eels.shape[1]):
            cyl_html =['<Transform translation="{0:.5f} {1:.5f} {2:.5f}" rotation="{3} {4} {5} 1.57">\n'.format(b_eels[ib, 0], b_eels[ib, 1], b_eels[ib, 2],
                                                                                                          cyl_orien[0], cyl_orien[1], cyl_orien[2]),
                    '    <Shape>\n',
                    '    <Appearance>\n',
                    '    <Material diffuseColor="' +
                    '{0:.2f} {1:.2f} {2:.2f}'.format(color[0], color[1], color[2]) +
                    '" specularColor="0.5 0.5 0.5">\n',
                    '    </Material>\n',
                    '    </Appearance>\n',
                    '        <Cylinder radius="{0}" height="{1}">\n'.format(cyl_rad, cyl_height),
                    '        </Cylinder>\n',
                    '    </Shape>\n',
                    '</Transform>\n',
                    ]
            for l in cyl_html:
                new_lines.append(l)
    elif len(b_eels.shape) == 2 and len(color.shape) == 2:
        for ib in range(b_eels.shape[0]):
            cyl_html =['<Transform translation="{0:.5f} {1:.5f} {2:.5f}" rotation="{3} {4} {5} 1.57">\n'.format(b_eels[ib, 0], b_eels[ib, 1], b_eels[ib, 2],
                                                                                                         cyl_orien[0], cyl_orien[1], cyl_orien[2]),
                    '    <Shape>\n',
                    '    <Appearance>\n',
                    '    <Material diffuseColor="' +
                    '{0:.2f} {1:.2f} {2:.2f}'.format(color[ib, 0], color[ib, 1], color[ib, 2]) +
                    '" specularColor="0.5 0.5 0.5">\n',
                    '    </Material>\n',
                    '    </Appearance>\n',
                    '        <Cylinder radius="{0}" height="{1}">\n'.format(cyl_rad, cyl_height),
                    '        </Cylinder>\n',
                    '    </Shape>\n',
                    '</Transform>\n',
                    ]
            for l in cyl_html:
                new_lines.append(l)
    else:
        raise ValueError("b_eels and color must be an 1D or 2D arrays!")


    for off in range(offset, len(lines), 1):
        new_lines.append(lines[off])
     
    f = open(fname, "w")
    f.write("".join(new_lines))
    f.close()


def write_eels_traj_html(atoms, fname, b_eels, color = np.array([1.0, 0.0, 0.0]), cam_pos = np.array([50.55178, 6.58028, -4.71022]),
        cam_orientation = np.array([0.48908, 0.69708, 0.52429, 2.10130]), cyl_orien = np.array([1.0, 0.0, 0.0]), cyl_rad = 0.20, cyl_height = 60.0):
    """
    write html geometry of the ase atoms object and electron beam trajectory of eels calculations
    """

    cam = """
        <Viewpoint position="{0} {1} {2}" orientation="{3} {4}, {5}, {6}" description="defaultX3DViewpointNode">
        </Viewpoint>
    """.format(cam_pos[0], cam_pos[1], cam_pos[2], cam_orientation[0], cam_orientation[1], cam_orientation[2], cam_orientation[3])

    io.write(fname, atoms, format="html")
    f = open(fname, "r")
    lines = f.readlines()
    f.close()
    
    offset = 0
    off_set_cam = 0
    for line in lines:
        if "</Scene>" in line:
            break
        offset += 1

    for line in lines:
        off_set_cam += 1
        if "<Scene>" in line:
            break

    new_lines = lines[0:off_set_cam]
    new_lines.append(cam)
    
    for off in range(off_set_cam, offset, 1):
        new_lines.append(lines[off])

    if len(b_eels.shape) == 1:
        cyl_html =['<Transform translation="{0:.5f} {1:.5f} {2:.5f}" rotation="{3} {4} {5} 1.57">\n'.format(b_eels[0], b_eels[1], b_eels[2],
                                                                                                        cyl_orien[0], cyl_orien[1], cyl_orien[2]),
                '    <Shape>\n',
                '    <Appearance>\n',
                '    <Material diffuseColor="' +
                '{0:.2f} {1:.2f} {2:.2f}'.format(color[0], color[1], color[2]) +
                '" specularColor="0.5 0.5 0.5">\n',
                '    </Material>\n',
                '    </Appearance>\n',
                '        <Cylinder radius="{0}" height="{1}">\n'.format(cyl_rad, cyl_height),
                '        </Cylinder>\n',
                '    </Shape>\n',
                '</Transform>\n',
                ]
        for l in cyl_html:
            new_lines.append(l)
    elif len(b_eels.shape) == 2 and len(color.shape) == 1:
        for ib in range(b_eels.shape[1]):
            cyl_html =['<Transform translation="{0:.5f} {1:.5f} {2:.5f}" rotation="{3} {4} {5} 1.57">\n'.format(b_eels[ib, 0], b_eels[ib, 1], b_eels[ib, 2],
                                                                                                          cyl_orien[0], cyl_orien[1], cyl_orien[2]),
                    '    <Shape>\n',
                    '    <Appearance>\n',
                    '    <Material diffuseColor="' +
                    '{0:.2f} {1:.2f} {2:.2f}'.format(color[0], color[1], color[2]) +
                    '" specularColor="0.5 0.5 0.5">\n',
                    '    </Material>\n',
                    '    </Appearance>\n',
                    '        <Cylinder radius="{0}" height="{1}">\n'.format(cyl_rad, cyl_height),
                    '        </Cylinder>\n',
                    '    </Shape>\n',
                    '</Transform>\n',
                    ]
            for l in cyl_html:
                new_lines.append(l)
    elif len(b_eels.shape) == 2 and len(color.shape) == 2:
        for ib in range(b_eels.shape[0]):
            cyl_html =['<Transform translation="{0:.5f} {1:.5f} {2:.5f}" rotation="{3} {4} {5} 1.57">\n'.format(b_eels[ib, 0], b_eels[ib, 1], b_eels[ib, 2],
                                                                                                         cyl_orien[0], cyl_orien[1], cyl_orien[2]),
                    '    <Shape>\n',
                    '    <Appearance>\n',
                    '    <Material diffuseColor="' +
                    '{0:.2f} {1:.2f} {2:.2f}'.format(color[ib, 0], color[ib, 1], color[ib, 2]) +
                    '" specularColor="0.5 0.5 0.5">\n',
                    '    </Material>\n',
                    '    </Appearance>\n',
                    '        <Cylinder radius="{0}" height="{1}">\n'.format(cyl_rad, cyl_height),
                    '        </Cylinder>\n',
                    '    </Shape>\n',
                    '</Transform>\n',
                    ]
            for l in cyl_html:
                new_lines.append(l)
    else:
        raise ValueError("b_eels and color must be an 1D or 2D arrays!")


    for off in range(offset, len(lines), 1):
        new_lines.append(lines[off])
     
    f = open(fname, "w")
    f.write("".join(new_lines))
    f.close()

def add_atoms_X3DOM(atoms, inp_name, out_name, term="<group>"):

    from ase.io.x3d import atom_lines

    fin = open(inp_name, "r")
    fout = open(out_name, "w")


    for line in fin:
        fout.write(line)
        if term in line:
            for atom in atoms:
                for indent, line in atom_lines(atom):
                    fout.write(line+"\n")

    fin.close()
    fout.close()

def X3D2html(inp_name, out_name):
    raise ValueError("not working use aopt")


    fin = open(inp_name, "r")
    fout = open(out_name, "w")

    fout.write('<html>\n')
    fout.write('<head>\n')
    fout.write('<title>ASE atomic visualization</title>\n')
    fout.write('<link rel="stylesheet" type="text/css\n"')
    fout.write(' href="https://www.x3dom.org/x3dom/release/x3dom.css">\n')
    fout.write('</link>\n')
    fout.write('<script type="text/javascript"\n')
    fout.write(' src="https://www.x3dom.org/x3dom/release/x3dom.js">\n')
    fout.write('</script>\n')
    fout.write('</head>\n')
    fout.write('<body>\n')
    fout.write('<X3D style="margin:0; padding:0; width:100%; height:100%;\n'
         ' border:none;">\n')

    started = False
    for line in fin:
        if "<Scene>" in line:
            started = True

        if started:
            fout.write(line)
        if "</Scene>" in line:
            started = False

    fout.write('</X3D>\n')
    fout.write('</body>\n')
    fout.write('</html>\n')

    fin.close()
    fout.close()
