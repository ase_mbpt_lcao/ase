from __future__ import division
import numpy as np
from ase.mbpt_lcao_utils.Read_data import read_data
from ase.mbpt_lcao_utils.utils import pts_coord_to_Array_ind


class calculate_current:
  """
  Class that is use to calculate the intensity of the electric current
  in the 2 clusters:
  
  INPUT PARAMETERS:
  -----------------
      args (class Parameter): type help(Parameter) for more informations
      prop_fig: (class Properties_figure): Properties of the figure, type help(Properties_figure) for more informations
      w0 (float): frequency at which the tddft calculation were done
      t (1D numpy array float): time range to calculate the total charge
  OUTPUT PARAMETERS:
  ------------------
      self.dens_Re (3D numpy array float): real part of the density
      self.dens_Im (3D numpy array float): imaginary part of the density
      self.Q_left (complex): total charge at w0 of the left cluster
      self.Q_right (complex): total charge at w0 of the right cluster
      self.Q_left_time (1D numpy array complex): evolution of the total charge of the left cluster as function of the time.
      self.Q_right_time (1D numpy array complex): evolution of the total charge of the right cluster as function of the time.
      self.I_left (1D numpy array complex) : evolution of the current in the left cluster as function of the time
      self.I_right (1D numpy array complex) : evolution of the current in the right cluster as function of the time
  FUNCTIONS:
  ----------
    load_density
    calculate_total_charge
    comp_current
  """
  def __init__(self, args, prop, w0, t, method='FT'):

    self.load_density(args, prop)
    self.get_total_charge(w0, t, check_total=True)
    dt = t[1]-t[0]
    if method == 'FT':
      self.get_current_FT(t)
    elif method == 'gradient':
      self.get_current_gradient(dt)
    elif method=='MSR':
      self.get_current_MSR(w0)
    else:
      raise ValueError('method can be only FT, gradient, or MSR')
    
  
  def load_density(self, args, prop):
    """
    Functions use to load the density change from tddft calculations
    INPUT PARAMETERS:
    -----------------
      args (class Parameter): type help(Parameter) for more informations
      prop_fig: (class Properties_figure): Properties of the figure, type help(Properties_figure) for more informations
    OUTPUT PARAMETERS:
    ------------------
      self.dens_Re (3D numpy array float): real part of the density
      self.dens_Im (3D numpy array float): imaginary part of the density
    """
    args.ReIm = 're'
    self.dens_Re = read_data(args, prop)
    print(self.dens_Re.box)
    print(self.dens_Re.Array.shape)

    args.ReIm = 'im'
    self.dens_Im = read_data(args, prop)


  def get_total_charge(self, w0, t, check_total=False):
    """
    get the total charge of each cluster as function of the time.
    INPUT PARAMETERS:
    -----------------
      w0 (float): frequency at which the tddft calculation were done
      t (1D numpy array float): time range to calculate the total charge
      check_total(boollean, default:False): check the total charge, must be close to zero
    OUTPUT PARAMETERS:
    ------------------
      self.Q_left (complex): total charge at w0 of the left cluster
      self.Q_right (complex): total charge at w0 of the right cluster
      self.Q_left_time (1D numpy array complex): evolution of the total charge of the left cluster as function of the time.
      self.Q_right_time (1D numpy array complex): evolution of the total charge of the right cluster as function of the time.
    """
    center_ind = pts_coord_to_Array_ind(self.dens_Re, np.array([0.0, 0.0, 0.0]))
    print(center_ind)
    
    print(self.dens_Re.dr)
    dV = self.dens_Re.dr[0]*self.dens_Re.dr[1]*self.dens_Re.dr[2]#*(0.529)**3
    if check_total:
      Q_tot = np.sum(self.dens_Re.Array)*dV + complex(0, 1)*np.sum(self.dens_Im.Array)*dV 
      print('total charge of the system:', np.sqrt(Q_tot.real**2+Q_tot.imag**2))

    self.Q_left = np.sum(self.dens_Re.Array[:, 0:center_ind[1], :])*dV +\
                            complex(0, 1)*np.sum(self.dens_Im.Array[:, 0:center_ind[1], :])*dV
    self.Q_right = np.sum(self.dens_Re.Array[:, center_ind[1]:self.dens_Re.Array.shape[1]-1, :])*dV +\
                        complex(0, 1)*np.sum(self.dens_Im.Array[:, center_ind[1]:self.dens_Im.Array.shape[1]-1, :])*dV

    self.phase_right = self.Q_right.imag/self.Q_right.real

    self.Q_left_time = (np.sum(self.dens_Re.Array[:, 0:center_ind[1], :]) +\
                            complex(0, 1)*np.sum(self.dens_Im.Array[:, 0:center_ind[1], :]))*\
                            (np.cos(w0*t)+complex(0, 1)*np.sin(w0*t))*dV
    self.Q_right_time = (np.sum(self.dens_Re.Array[:, center_ind[1]:self.dens_Re.Array.shape[1]-1, :]) +\
                        complex(0, 1)*np.sum(self.dens_Im.Array[:, center_ind[1]:self.dens_Im.Array.shape[1]-1, :]))*\
                        (np.cos(w0*t)+complex(0, 1)*np.sin(w0*t))*dV
  
  def integral_y(self):
    self.inte = np.zeros((self.dens_Re.Array.shape[1]), dtype=complex)
    #ds = self.dens_Re.dr[0]*self.dens_Re.dr[2]#/(0.529)**2
    dv = self.dens_Re.dr[0]*self.dens_Re.dr[1]*self.dens_Re.dr[2]#*(0.529)**3

    #self.inte[0] = np.sum(self.dens_Re.Array[:, 0, :])*ds + \
    #        complex(0, 1)*np.sum(self.dens_Im.Array[:, 0, :])*ds

    for i in range(0, self.dens_Re.Array.shape[1]):
      self.inte[i] = np.sum(self.dens_Re.Array[:, 0:i, :])*dv + \
                      complex(0, 1)*np.sum(self.dens_Im.Array[:, 0:i, :])*dv

  def get_current_derivate(self, dt):
    """
    get the current in the left and right cluster as function of the time.
    INPUT PARAMETERS:
    -----------------
      dt (float): time step
    OUTPUT PARAMETERS:
    ------------------
      self.I_left (1D numpy array complex) : evolution of the current in the left cluster as function of the time
      self.I_right (1D numpy array complex) : evolution of the current in the right cluster as function of the time
    """
    self.I_left = np.gradient(self.Q_left_time, dt, edge_order=2)
    self.I_right = np.gradient(self.Q_right_time, dt, edge_order=2)

  def get_current_FT(self, t):
    """
    get the current in the left and right cluster as function of the time with FFT
    INPUT PARAMETERS:
    -----------------
      t (np 1D array, float): time grid
    OUTPUT PARAMETERS:
    ------------------
      self.I_left (1D numpy array complex) : evolution of the current in the left cluster as function of the time
      self.I_right (1D numpy array complex) : evolution of the current in the right cluster as function of the time
    """
    from FFT_routines import fft_freq_1D, mod_FFT_1D, mod_iFFT_1D

    w = fft_freq_1D(t)
    self.Q_left_w = mod_FFT_1D(w, t, self.Q_left_time)
    self.Q_right_w = mod_FFT_1D(w, t, self.Q_right_time)

    self.I_left = mod_iFFT_1D(t, w, complex(0, 1)*w*self.Q_left_w)
    self.I_right = mod_iFFT_1D(t, w, complex(0, 1)*w*self.Q_right_w)

  def get_current_MSR(self, w0):
    """
    get the MSR average current define by Federico
    INPUT PARAMETERS:
    -----------------
      w0 (float): frequency at which the current is calculated
    OUTPUT PARAMETERS:
    ------------------
      self.I_MSR_left (1D numpy array float) : evolution of the current in the left cluster as function of the time
      self.I_MSR_right (1D numpy array float) : evolution of the current in the right cluster as function of the time
    """
    self.I_MSR_left = w0*np.sqrt(self.Q_left.real**2 + self.Q_left.imag**2)/(2*np.pi*np.sqrt(2))
    self.I_MSR_right = w0*np.sqrt(self.Q_right.real**2 + self.Q_right.imag**2)/(2*np.pi*np.sqrt(2))
    
    self.I_MSR_left_imag = w0*np.sqrt(self.Q_left.imag**2)/(2*np.pi*np.sqrt(2))
    self.I_MSR_right_imag = w0*np.sqrt(self.Q_right.imag**2)/(2*np.pi*np.sqrt(2))
    
