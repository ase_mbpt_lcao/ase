from __future__ import division
import numpy as np
import h5py
import ase.units as un


def export_data(r, output_name='data.hdf5'):
    """
    Example input:
        from ase.calculators.siesta.mbpt_lcao_io import read_mbpt_lcao_output
        r = read_mbpt_lcao_output()
        r.args.quantity = 'dens'
        r.args.plot_freq = 3.26
        r.args.ReIm='re'
        r.args.Edir= np.array([1.0, 0.0, 0.0])
    """


    data = r.Read()
    x = data.xmesh[:, 0, 0]
    y = data.ymesh[0, :, 0]
    z = data.zmesh[0, 0, :]

    positions = []
    ch_sym = []
    for pos, Z, ch in zip(data.atoms.positions, data.atoms.numbers, data.atoms.get_chemical_symbols()):
        if Z > 0:
            positions.append(pos)
            ch_sym.append(ch)

    f = h5py.File(output_name, 'w')
    f.create_dataset("Edir", data=r.args.Edir)
    f.create_dataset("freq", data=np.array([r.args.plot_freq]))
    f.create_dataset("dens", data=data.Array)
    f.create_dataset("species", data=np.array(ch_sym))
    f.create_dataset("xmesh", data=x*un.Bohr)
    f.create_dataset("ymesh", data=y*un.Bohr)
    f.create_dataset("zmesh", data=z*un.Bohr)
    f.create_dataset("atoms2coord", data=np.array(positions))

    f.close()
