from __future__ import division
import numpy as np
import math
try:
	import numba as nb
except ImportError:
	raise ValueError('numba need to be install to use numba module')
#import time
#import sys

@nb.guvectorize([(nb.float64[:], nb.float64[:], nb.float64[:], nb.float64[:, :],
    nb.float32[:, :, :], nb.float32[:, :, :], nb.float32[:, :, :])],
    '(m), (n), (p), (q, q) -> (m, n, p), (m, n, p), (m, n, p)',
    target='cpu', nopython=True)
def euler_rotation_mesh_numba(x, y, z, euler_mat, xmesh, ymesh, zmesh):
    
    M, N, P = xmesh.shape
    for i in range(M):
        for j in range(N):
            for k in range(P):
                #ind = np.dot(euler_mat, [x[i], y[j], z[k]])
                ind = [0.0, 0.0, 0.0]
                tmp_arr = [x[i], y[j], z[k]]
                for i1 in range(3):
                    for i2 in range(3):
                        ind[i1] = ind[i1] + euler_mat[i1, i2]*tmp_arr[i2]
                xmesh[i, j, k] = ind[0]
                ymesh[i, j, k] = ind[1]
                zmesh[i, j, k] = ind[2]
     

@nb.guvectorize([(nb.float32[:, :, :], nb.float32[:, :, :], nb.float64[:], 
  nb.float32[:, :, :], nb.float32[:, :, :], nb.float32[:, :, :], nb.float32[:], 
  nb.float32[:], nb.float32[:])],
    '(m, n, p), (m, n, p), (), (m, n, p), (m, n, p), (m, n, p), (q) -> (q), (q)',
    target='cpu', nopython=True)
def calc_mean_dens_chng_cylinder_pecular_dir_numba(dn_re, dn_im, radius, xmesh, ymesh, zmesh, z,
    dn_out_re, dn_out_im):
  
  M, N, P = dn_re.shape
  for i in range(M):
    for j in range(N):
      for k in range(P):
        if (zmesh[i, j, k]>0.0):
          rho = math.sqrt(xmesh[i, j, k]**2 + ymesh[i, j, k]**2)
          if (rho < radius[0]):
            zind = 0
            while(z[zind]<zmesh[i, j, k]):
              zind = zind + 1

            dn_out_re[zind] = dn_out_re[zind] + dn_re[i, j, k]
            dn_out_im[zind] = dn_out_im[zind] + dn_im[i, j, k]


 
@nb.guvectorize([(nb.float64[:, :, :], nb.float64[:, :, :], nb.float64[:, :], nb.float64[:], nb.float64[:],
    nb.float64[:], nb.float64[:])],
    '(m, n, p), (m, n, p), (q, t), (m), (n), (p) -> (t)', target='cpu', nopython=True)
def calc_d_param_new_method_peter_numba(dn_re, dn_im, p2xyz, x, y, z, I):
  
  raise ValueError("broken!!")
#  a = 0
#  for i in range(x.shape[0]):
#    for j in range(y.shape[0]):
#      for k in range(z.shape[0]):
        #a = 1
        #xyz = np.array([x[i], y[j], z[k]])
        #r    = np.sqrt( np.sum(xyz**2) )
        #
        #p2d = np.zeros((p2xyz.shape[0]), dtype=np.float64)
        #for pnt in range(p2xyz.shape[0]):
        #  for l in range(3):
        #    p2d[pnt] = p2d[pnt] + (xyz[l]-p2xyz[pnt, l])**2
        #pmin = np.argmin(p2d)
        #r0 = 0.0
        #for l in range(3):
        #  r0 = r0 + p2xyz[pmin]*p2xyz[pmin]
        #r0  = np.sqrt(np.sum(p2xyz[pmin]**2))
        #d  = np.sign(r-r0)*np.sqrt(p2d[pmin])
        #d  = np.sqrt(p2d[pmin])
        #I[0] = I[0] + dn_re[i, j, k]#complex(dn_re[i, j, k], dn_im[i, j, k])
        #I[1] = I[1] + r*dn_re[i, j, k]#complex(dn_re[i, j, k], dn_im[i, j, k])
        #I[2] = I[2] + d*dn_re[i, j, k]#complex(dn_re[i, j, k], dn_im[i, j, k])


@nb.jit([(nb.float64[:, :], nb.float64[:], nb.float64[:])], nopython=True)
def sphe_part_deriv_numba_jit(mat_conv, dxyz, dsphe):

    for i in range(3):
        for j in range(3):
            dsphe[i] = dsphe[i] + mat_conv[i, j]*dxyz[j]


#@numba.jit(nopython=True, nogil=True, cache=True)
@nb.guvectorize([(nb.float64[:, :], nb.float64[:], nb.float64[:])], '(n, n), (n) -> (n)', 
        nopython=True, target='cpu')
def sphe_part_deriv_numba(mat_conv, dxyz, dsphe):

    for i in range(3):
        for j in range(3):
            dsphe[i] = dsphe[i] + mat_conv[i, j]*dxyz[j]

@nb.guvectorize([(nb.float64[:, :, :], nb.float64[:, :, :], nb.float64[:, :, :], nb.float64[:], nb.float64[:],
    nb.float64[:], nb.float64[:], nb.float64[:, :], nb.float64[:, :], nb.float64[:, :])],
    '(m, n, p), (m, n, p), (m, n, p), (m), (n), (p), (q) -> (q, q), (n, q), (n, q)', target='cpu', nopython=True)
def centroid_dens_chang_numba_half_numba_xdir(dn_re, dn_im, dn_mod, x, y, z, dxyz, d, inte_num, inte_den):

  M, N, K = dn_re.shape
  for i in range(M):
    if (x[i]>=0.0):
      for j in range(N):
        for k in range(K):
          r = np.sqrt(x[i]**2 + y[j]**2 + z[k]**2)
          if r>0.0:
            fact = r
            if (dn_re[i, j, k] != 0.0):
              inte_num[i, 0] = inte_num[j, 0] + fact*dn_re[i, j, k]
              inte_den[i, 0] = inte_den[j, 0] + dn_re[i, j, k]
              
              d[0, 0] = d[0, 0] + fact*dn_re[i, j, k]
              d[0, 1] = d[0, 1] + dn_re[i, j, k]
            
            if (dn_im[i, j, k] != 0.0):
              inte_num[i, 1] = inte_num[j, 1] + fact*dn_im[i, j, k]
              inte_den[i, 1] = inte_den[j, 1] + dn_im[i, j, k]
 
              d[1, 0] = d[1, 0] + fact*dn_im[i, j, k]
              d[1, 1] = d[1, 1] + dn_im[i, j, k]
            
            if (dn_mod[i, j, k] != 0.0):
              d[2, 0] = d[2, 0] + fact*dn_mod[i, j, k]
              d[2, 1] = d[2, 1] + dn_mod[i, j, k]

      inte_num[i, :] = inte_num[i, :]*dxyz[0]*dxyz[1]*dxyz[2]
  
  d = d*dxyz[0]*dxyz[1]*dxyz[2]

@nb.guvectorize([(nb.float64[:, :, :], nb.float64[:, :, :], nb.float64[:, :, :], nb.float64[:], nb.float64[:],
    nb.float64[:], nb.float64[:], nb.float64[:, :], nb.float64[:, :], nb.float64[:, :])],
    '(m, n, p), (m, n, p), (m, n, p), (m), (n), (p), (q) -> (q, q), (n, q), (n, q)', target='cpu', nopython=True)
def centroid_dens_chang_numba_half_numba_ydir(dn_re, dn_im, dn_mod, x, y, z, dxyz, d, inte_num, inte_den):

  M, N, K = dn_re.shape
  for j in range(N):
    if (y[j]>=0.0):
      for i in range(M):
        for k in range(K):
          r = np.sqrt(x[i]**2 + y[j]**2 + z[k]**2)
          if r>0.0:
            fact = r
            if (dn_re[i, j, k] != 0.0):
              inte_num[j, 0] = inte_num[j, 0] + fact*dn_re[i, j, k]
              inte_den[j, 0] = inte_den[j, 0] + dn_re[i, j, k]
              
              d[0, 0] = d[0, 0] + fact*dn_re[i, j, k]
              d[0, 1] = d[0, 1] + dn_re[i, j, k]
            
            if (dn_im[i, j, k] != 0.0):
              inte_num[j, 1] = inte_num[j, 1] + fact*dn_im[i, j, k]
              inte_den[j, 1] = inte_den[j, 1] + dn_im[i, j, k]
 
              d[1, 0] = d[1, 0] + fact*dn_im[i, j, k]
              d[1, 1] = d[1, 1] + dn_im[i, j, k]
            
            if (dn_mod[i, j, k] != 0.0):
              d[2, 0] = d[2, 0] + fact*dn_mod[i, j, k]
              d[2, 1] = d[2, 1] + dn_mod[i, j, k]

      inte_num[j, :] = inte_num[j, :]*dxyz[0]*dxyz[1]*dxyz[2]
  
  d = d*dxyz[0]*dxyz[1]*dxyz[2]

@nb.guvectorize([(nb.float64[:, :, :], nb.float64[:, :, :], nb.float64[:, :, :], nb.float64[:], nb.float64[:],
    nb.float64[:], nb.float64[:], nb.float64[:, :], nb.float64[:, :], nb.float64[:, :])],
    '(m, n, p), (m, n, p), (m, n, p), (m), (n), (p), (q) -> (q, q), (n, q), (n, q)', target='cpu', nopython=True)
def centroid_dens_chang_numba_half_numba_zdir(dn_re, dn_im, dn_mod, x, y, z, dxyz, d, inte_num, inte_den):

  M, N, K = dn_re.shape
  for k in range(K):
    if (z[k]>=0.0):
      for j in range(N):
        for i in range(M):
          r = np.sqrt(x[i]**2 + y[j]**2 + z[k]**2)
          if r>0.0:
            fact = r
            if (dn_re[i, j, k] != 0.0):
              inte_num[k, 0] = inte_num[j, 0] + fact*dn_re[i, j, k]
              inte_den[k, 0] = inte_den[j, 0] + dn_re[i, j, k]
              
              d[0, 0] = d[0, 0] + fact*dn_re[i, j, k]
              d[0, 1] = d[0, 1] + dn_re[i, j, k]
            
            if (dn_im[i, j, k] != 0.0):
              inte_num[k, 1] = inte_num[j, 1] + fact*dn_im[i, j, k]
              inte_den[k, 1] = inte_den[j, 1] + dn_im[i, j, k]
 
              d[1, 0] = d[1, 0] + fact*dn_im[i, j, k]
              d[1, 1] = d[1, 1] + dn_im[i, j, k]
            
            if (dn_mod[i, j, k] != 0.0):
              d[2, 0] = d[2, 0] + fact*dn_mod[i, j, k]
              d[2, 1] = d[2, 1] + dn_mod[i, j, k]

      inte_num[k, :] = inte_num[k, :]*dxyz[0]*dxyz[1]*dxyz[2]
  
  d = d*dxyz[0]*dxyz[1]*dxyz[2]



@nb.guvectorize([(nb.float64[:, :, :], nb.float64[:, :, :], nb.float64[:, :, :], nb.float64[:], nb.float64[:],
    nb.float64[:], nb.float64[:], nb.float64[:, :], nb.float64[:, :], nb.float64[:, :])],
    '(m, n, p), (m, n, p), (m, n, p), (m), (n), (p), (q) -> (q, q), (n, q), (n, q)', target='cpu', nopython=True)
def centroid_dens_chang_numba_full_numba(dn_re, dn_im, dn_mod, x, y, z, dxyz, d, inte_num, inte_den):

  M, N, K = dn_re.shape
  for j in range(N):
    for i in range(M):
      for k in range(K):
        r = np.sqrt(x[i]**2 + y[j]**2 + z[k]**2)
        if r>0.0:
          if (dn_re[i, j, k] != 0.0):
            inte_num[j, 0] = inte_num[j, 0] + r*dn_re[i, j, k]
            inte_den[j, 0] = inte_den[j, 0] + dn_re[i, j, k]
            
            d[0, 0] = d[0, 0] + r*dn_re[i, j, k]
            d[0, 1] = d[0, 1] + dn_re[i, j, k]
          
          if (dn_im[i, j, k] != 0.0):
            inte_num[j, 1] = inte_num[j, 1] + r*dn_im[i, j, k]
            inte_den[j, 1] = inte_den[j, 1] + dn_im[i, j, k]

            d[1, 0] = d[1, 0] + r*dn_im[i, j, k]
            d[1, 1] = d[1, 1] + dn_im[i, j, k]
          
          if (dn_mod[i, j, k] != 0.0):
            d[2, 0] = d[2, 0] + r*dn_mod[i, j, k]
            d[2, 1] = d[2, 1] + dn_mod[i, j, k]

    inte_num[j, :] = inte_num[j, :]*dxyz[0]*dxyz[1]*dxyz[2]

  d = d*dxyz[0]*dxyz[1]*dxyz[2]
  
@nb.guvectorize([(nb.float64[:, :, :], nb.float64[:], nb.float64[:], nb.float64[:], 
    nb.float64[:], nb.float64[:], nb.float64[:])],
    '(m, n, p), (m), (n), (p), (q) -> (), ()', target='cpu', nopython=True)
def centroid_dens_GS_numba(dens, x, y, z, dxyz, d_num, d_den):

  M, N, K = dens.shape
  for j in range(N):
    for i in range(M):
      for k in range(K):
        r = np.sqrt(x[i]**2 + y[j]**2 + z[k]**2)
        if r>0.0:
          if (dens[i, j, k] != 0.0):
            d_num[0] = d_num[0] + r*dens[i, j, k]
            d_den[0] = d_den[0] + dens[i, j, k]

  d_num[0] = d_num[0]*dxyz[0]*dxyz[1]*dxyz[2]
  d_den[0] = d_den[0]*dxyz[0]*dxyz[1]*dxyz[2]
  
@nb.guvectorize([(nb.float64[:, :, :], nb.float64[:, :, :], nb.float64[:, :, :], nb.float64[:], nb.float64[:],
  nb.float64[:], nb.float64[:], nb.float64[:, :], nb.float64[:, :], nb.float64[:, :])],
  '(m, n, p), (m, n, p), (m, n, p), (m), (n), (p), (q) -> (q, q), (n, q), (n, q)', target='cpu', nopython=True)
def calc_eff_dens_sphe_numba(dn_re, dn_im, dn_mod, x, y, z, dxyz, d, inte_num, inte_den):

  M, N, K = dn_re.shape
  for j in range(N):
    if (y[j]>=0.0):
      for i in range(M):
        for k in range(K):
          r = np.sqrt(x[i]**2 + y[j]**2 + z[k]**2)
          if r>0.0:
            theta = np.arccos(z[k]/r)
            if np.sin(theta) != 0.0:
              phi = np.arctan2(y[j], x[i])
              #mat_conv = gen_mat_con(r, phi, theta)
              #sphe_deriv = np.zeros((3), dtype = np.float64)
              #sphe_part_deriv_numba_jit(mat_conv, dxyz, sphe_deriv)
              sphe_deriv = [0.0, 0.0, 0.0]
              mat_conv = [math.cos(phi)*math.sin(theta), math.sin(phi)*math.sin(theta), math.cos(theta),
                         -math.sin(phi)/r*math.sin(theta), math.cos(phi)/r*math.sin(theta), math.sin(0.0),
                         math.cos(phi)*math.cos(theta)/r, math.sin(phi)*math.cos(theta)/r, -math.sin(theta)/r]

              count = 0
              for l in range(3):
                for m in range(3):
                  sphe_deriv[l] = sphe_deriv[l] + mat_conv[count]*dxyz[m]
                  count = count + 1


              if (dn_re[i, j, k] != 0.0):
                inte_num[j, 0] = inte_num[j, 0] + (dn_re[i, j, k]*
                    np.sin(theta)*r**3)*sphe_deriv[0]*sphe_deriv[1]*sphe_deriv[2]
                inte_den[j, 0] = inte_den[j, 0] + (dn_re[i, j, k]*
                    np.sin(theta)*r**2)*sphe_deriv[0]*sphe_deriv[1]*sphe_deriv[2]
                
                d[0, 0] = d[0, 0] + (dn_re[i, j, k]*
                    np.sin(theta)*r**3)*sphe_deriv[0]*sphe_deriv[1]*sphe_deriv[2]
                d[0, 1] = d[0, 1] + (dn_re[i, j, k]*
                    np.sin(theta)*r**2)*sphe_deriv[0]*sphe_deriv[1]*sphe_deriv[2]
              
              if (dn_im[i, j, k] != 0.0):
                inte_num[j, 1] = inte_num[j, 1] + (dn_im[i, j, k]*
                    np.sin(theta)*r**3)*sphe_deriv[0]*sphe_deriv[1]*sphe_deriv[2]
                inte_den[j, 1] = inte_den[j, 1] + (dn_im[i, j, k]*
                    np.sin(theta)*r**2)*sphe_deriv[0]*sphe_deriv[1]*sphe_deriv[2]
   
                d[1, 0] = d[1, 0] + (dn_im[i, j, k]*
                    np.sin(theta)*r**3)*sphe_deriv[0]*sphe_deriv[1]*sphe_deriv[2]
                d[1, 1] = d[1, 1] + (dn_im[i, j, k]*
                    np.sin(theta)*r**2)*sphe_deriv[0]*sphe_deriv[1]*sphe_deriv[2]
              
              if (dn_mod[i, j, k] != 0.0):
                inte_num[j, 2] = inte_num[j, 2] + (dn_mod[i, j, k]*
                    np.sin(theta)*r**3)*sphe_deriv[0]*sphe_deriv[1]*sphe_deriv[2]
                inte_den[j, 2] = inte_den[j, 2] + (dn_mod[i, j, k]*
                    np.sin(theta)*r**2)*sphe_deriv[0]*sphe_deriv[1]*sphe_deriv[2]
   
                d[2, 0] = d[2, 0] + (dn_mod[i, j, k]*
                    np.sin(theta)*r**3)*sphe_deriv[0]*sphe_deriv[1]*sphe_deriv[2]
                d[2, 1] = d[2, 1] + (dn_mod[i, j, k]*
                    np.sin(theta)*r**2)*sphe_deriv[0]*sphe_deriv[1]*sphe_deriv[2]

@nb.guvectorize([(nb.float64[:, :, :], nb.float64[:], nb.float64[:], nb.float64[:], nb.float64[:, :, :])],
  '(m, n, p), (m), (n), (p) -> (m, n, p)', target='cpu', nopython=True)
def calc_potential_dens_numba(dens, x, y, z, potential):
  M, N, K = dens.shape

  dx = x[1]-x[0]
  dy = y[1]-y[0]
  dz = z[1]-z[0]
  for i in range(M):
    for j in range(N):
      for k in range(K):
        xp = x[i]+dx/10.0
        yp = y[j]+dy/10.0
        zp = z[k]+dz/10.0

        for l in range(M):
          for m in range(N):
            for n in range(K):
              potential[i, j, k] = potential[i, j, k] +\
                  dens[l, m, n]/np.sqrt((x[l]-xp)**2 +\
                                        (y[m]-yp)**2 +\
                                        (z[n]-zp)**2)
        potential[i, j, k] = potential[i, j, k]*dx*dy*dz


@nb.guvectorize([(nb.float64[:], nb.float64[:], nb.float64[:], nb.float64[:], nb.float64[:, :, :])],
  '(m), (n), (p), (q) -> (m, n, p)', target='cpu', nopython=True)
def calc_coul_pot_numba(x, y, z, dr, coul_pot):
  M, N, K = coul_pot.shape 
  
  for i in range(M):
    for j in range(N):
      for k in range(K):
        coul_pot[i, j, k] = 1.0/np.sqrt((x[i]+dr[0]/10.0)**2 +\
                                        (y[j]+dr[1]/10.0)**2 +\
                                        (z[k]+dr[2]/10.0)**2)

@nb.guvectorize([(nb.float64[:, :, :], nb.float64[:, :, :], nb.float64[:, :, :], nb.float64[:, :, :],
  nb.float64[:], nb.float64[:], nb.float64[:], nb.float64[:], nb.float64[:], nb.float64[:])],
  '(m, n, p), (m, n, p), (m, n, p), (m, n, p), (m), (n), (p), (q), () -> ()', target='cpu', nopython=True)
def get_radius_andrei_numba(dn, gradx, grady, gradz, x, y, z, dxyz, dn_max, rad):

    M, N, K = dn.shape
    num = 0.0
    den = 0.0
    delta_gauss = 0.0
    sigma = 1e-2

    for i in range(M):
        for j in range(N):
            for k in range(K):
                delta_gauss = (dn[i, j, k]-dn_max[0]/2.0)*np.exp(-((dn[i, j, k]-dn_max[0]/2.0)/sigma)**2)
                r = np.sqrt(x[i]**2 + y[j]**2 + z[k]**2)
                num = num + r*gradx[i,j,k]*grady[i,j,k]*gradz[i,j,k]*delta_gauss
                den = den + gradx[i,j,k]*grady[i,j,k]*gradz[i,j,k]*delta_gauss
    
    num = num*dxyz[0]*dxyz[1]*dxyz[2]
    den = den*dxyz[0]*dxyz[1]*dxyz[2]
    
    rad[0] = num/den


