from __future__ import division
import numpy as np

def rotate2Z(axis, center = np.array([0.0, 0.0, 0.0])):
  """
    Rotate the cartesien axis in order that the
    vector z = (1.0, 0.0, 0.0) is parallel to the input axis.
  """

  axis = axis/np.sqrt(np.dot(axis, axis))
  theta = 0.0
  phi = 0.0
  psi = 0.0

  if axis[2] == 1.0:
    euler_mat = np.array([[1.0, 0.0, 0.0],
                          [0.0, 1.0, 0.0], 
                          [0.0, 0.0, 1.0]])
    x = np.array([1.0, 0.0, 0.0])
    y = np.array([0.0, 1.0, 0.0])
    z = np.array([0.0, 0.0, 1.0])
    return x, y, z, euler_mat
  elif axis[2] == -1.0:
    euler_mat = np.array([[1.0, 0.0, 0.0],
                          [0.0, 1.0, 0.0], 
                          [0.0, 0.0, -1.0]])
    x = np.array([1.0, 0.0, 0.0])
    y = np.array([0.0, 1.0, 0.0])
    z = np.array([0.0, 0.0, -1.0])
    return x, y, z, euler_mat
  elif axis[2] == 0.0:
    theta = np.pi/2.0
    psi = np.arctan2(axis[0], axis[1])
  else:
    theta = np.arccos(axis[2])
    if (abs(axis[0]/np.sin(theta))<1.0):
      psi = np.arcsin(axis[0]/np.sin(theta))
    elif (abs(axis[1]/np.sin(theta)) < 1.0):
      psi = np.arcsin(axis[1]/np.sin(theta))
    else:
      raise ValueError('Psi can not be compute??')
   
 
  x = rotate_euler_axis(np.array([1.0, 0.0, 0.0]), phi = phi, theta = theta, psi=psi, center = center)
  y = rotate_euler_axis(np.array([0.0, 1.0, 0.0]), phi = phi, theta = theta, psi=psi, center = center)
  z = rotate_euler_axis(np.array([0.0, 0.0, 1.0]), phi = phi, theta = theta, psi=psi, center = center)

  euler_mat = get_euler_mat(phi, theta, psi)

  return x, y, z, euler_mat

def get_euler_mat(phi, theta, psi):
  
  #first euler rotation about z in matrix form
  D = np.array([[np.cos(phi), np.sin(phi), 0.0], 
                [-np.sin(phi), np.cos(phi), 0.0], 
                [0.0, 0.0, 1.0]])
  
  # Second euler angle, rotation about x
  C = np.array([[1.0, 0.0, 0.0], 
                [0.0, np.cos(theta), np.sin(theta)], 
                [0.0, -np.sin(theta), np.cos(theta)]])

  # Third euler angle, rotation about z
  B = np.array([[np.cos(psi), np.sin(psi), 0.0], 
                [-np.sin(psi), np.cos(psi), 0.0], 
                [0.0, 0.0, 1.0]])

  # Total euler rotation
  return np.dot(B, np.dot(C, D))
 
def rotate_euler_axis(axis, phi=0.0, theta=0.0, psi=0.0, center = np.array([0.0, 0.0, 0.0])):
  
  rcoord = axis - center
  A = get_euler_mat(phi, theta, psi) 
  rcoord = np.dot(A, rcoord.T)

  return rcoord.T + center
 
def get_cylinder(data, radius):

  cylinder = []

  for i in range(data.Array.shape[0]):
    for j in range(data.Array.shape[1]):
      for k in range(data.Array.shape[2]):
        if (data.Rotzmesh[i, j, k] > 0.0):
          rho = np.sqrt(data.Rotxmesh[i, j, k]**2 + data.Rotymesh[i, j, k]**2)
          if (rho < radius):
            cylinder.append(np.array([data.Rotxmesh[i,j,k], data.Rotymesh[i,j,k], data.Rotzmesh[i, j, k]]))

  return np.array(cylinder)
