from __future__ import division
#import sys
import FFT_routines as FFT
from readxyz import write_HDF5
import h5py as hdf
import numpy as np

class Parameter_array:
  """
  data_type (string, default: 'full'): kind of data that one wish to look at
                                    can be: full
                                            plan
                                            line
                                            dot
  plan (string, default: 'xy'): plane where one widh to look the data
  line (string, default: 'z'): line where one wish to look the data
  coord (1D numpy array, default: np.array([0.0, 0.0, 0.0])): coordinate of the point
                                              where one wish to look the data
  """
  def __init__(self):
    self.data_type = 'full'
    self.plan = 'xy'
    self.line = 'z'
    self.coord = np.array([0.0, 0.0, 0.0])

class read_data_freq:
  """
  Load space data from tddft calculations and 
  create a 4D array that is space and frequency dependent
  Input Parameter:
  -----------------
    w (1D numpy array): frequency range
    folders (list of string): list of the different folders where are save the data
    kwargs (optionnal):
        fname: (string, default: '/enh_data_inter.hdf5'): name of the input file
        quantity: (string, default: 'intensity'): quantity than one wish to study, can be
                                  Efield
                                  intensity
                                  density
                                  potential
  """
  def __init__(self, w, folders, param, **kwargs):

    self.key_args = {'fname':'/enh_data_inter.hdf5', 'quantity':'intensity', 'ncpus':1}

    if len(kwargs)>0:
      for keys, values in kwargs.items():
        self.key_args[keys] = values

    if self.key_args['quantity'] == 'Efield' or self.key_args['quantity'] == 'potential' or\
        self.key_args['quantity'] == 'intensity' or self.key_args['quantity'] == 'density':
          print(self.key_args['quantity'])
    else:
      raise ValueError('quantity can be only density, efield, potential or density')
    
    self.w = w
    self.folders = folders

    data_full = self.load_data()
    self.determine_box()

    self.data_w = self.define_data(param, data_full)


  ###############################
  #                             #
  #   Functions of the class    #
  #                             #
  ###############################


  def readhdf5(self, fname):
    """
    Read the hdf5 file fname from tddft calculations, and extract the data
    Input parameter:
    ----------------
      fname: (string): name of the input file
    Output parameter:
    -----------------
      Array (3D or 4D numpy array): array containing the spatial data
    """
    if self.key_args['quantity'] == 'density':
      key = ['dr', 'origin_dens', 'ibox_dens', 'dens_re', 'dens_im']
    elif self.key_args['quantity'] == 'Efield':
      key = ['dr', 'origin', 'ibox', 'efield_re', 'efield_im']
    elif self.key_args['quantity'] == 'potential':
      key = ['dr', 'origin', 'ibox', 'potential_re', 'potential_im']
    elif self.key_args['quantity'] == 'intensity':
      key = ['dr', 'origin', 'ibox', 'intensity']

    f = hdf.File(fname, 'r')
    dico = dict()

    for i in key:
      dico[i] = f['/' + i].value

    self.dr = dico['dr']

    if self.key_args['quantity'] == 'density':
      self.origin = dico['origin_dens']
      self.lbound = dico['ibox_dens'][:, 0]
      self.ubound = dico['ibox_dens'][:, 1]
      Array = dico['dens_re'] + complex(0, 1)*dico['dens_im']
    elif self.key_args['quantity'] == 'intensity':
      self.origin = dico['origin']
      self.lbound = dico['ibox'][:, 0]
      self.ubound = dico['ibox'][:, 1]
      Array = dico['intensity']
    elif self.key_args['quantity'] == 'Efield':
      self.origin = dico['origin']
      self.lbound = dico['ibox'][:, 0]
      self.ubound = dico['ibox'][:, 1]
      Array = dico['efield_re'] + complex(0, 1)*dico['efield_im']
    elif self.key_args['quantity'] == 'potential':
      self.origin = dico['origin']
      self.lbound = dico['ibox'][:, 0]
      self.ubound = dico['ibox'][:, 1]
      Array = dico['potential_re'] + complex(0, 1)*dico['potential_im']

    return Array

  #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  def load_data(self):
    """
    Load the all the file from tddft calculations for
    several frequencies. Save all the data into a 4D or 5D
    numpy array self.data_w
    """
    init = self.readhdf5(self.folders[0]+self.key_args['fname'])

    if self.key_args['quantity'] == 'Efield':
      data_w = np.zeros((self.w.shape[0], init.shape[0], init.shape[1], init.shape[2], init.shape[3]), dtype=complex)
      for i, j in enumerate(self.folders):
        data_w[i, :, :, :, :] = self.readhdf5(j+self.key_args['fname'])

    elif self.key_args['quantity'] == 'intensity':
      data_w = np.zeros((self.w.shape[0], init.shape[0], init.shape[1], init.shape[2]), dtype=float)
      for i, j in enumerate(self.folders):
        data_w[i, :, :, :] = self.readhdf5(j+self.key_args['fname'])

    else:
      data_w = np.zeros((self.w.shape[0], init.shape[0], init.shape[1], init.shape[2]), dtype=complex)
      for i, j in enumerate(self.folders):
        data_w[i, :, :, :] = self.readhdf5(j+self.key_args['fname'])

    return data_w

  #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  def parametrize_data(self, jit=False, target = 'cpu'):
    """
    Perform the parametrization of the self.data_w array in order 
    to perform symmetrization then FFT
    Output parameters:
    ------------------
      self.data_w_param (2D numpy array): parametrize array
    """
    
    if self.key_args['quantity'] == 'intensity':
      if jit:
        from numba import jit, complex128
        fast_transform_NDto2D = jit(complex128(complex128), target=target)(FFT.transform_NDto2D)
        self.data_w_param = fast_transform_NDto2D(self.data_w)
      else:
        self.data_w_param = FFT.transform_NDto2D(self.data_w)
    else:
      if jit:
        from numba import jit, complex128
        fast_transform_NDto2D = jit(complex128(complex128), target=target)(FFT.transform_NDto2D)
        self.data_w_param = fast_transform_NDto2D(self.data_w, dtype=complex)
      else:
        self.data_w_param = FFT.transform_NDto2D(self.data_w)

    return self.data_w_param

  #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  def parametrize_data_parrallel(self, ncpus):
    """
    Perform the parametrization of the self.data_w array in order 
    to perform symmetrization then FFT
    Output parameters:
    ------------------
      self.data_w_param (2D numpy array): parametrize array
    """

    try:
      import pp
    except ImportError:
      raise ValueError('Error: import pp not install')
    
    ppservers = ()
    job_server = pp.Server(ncpus, ppservers=ppservers)

    if self.key_args['quantity'] == 'intensity':
      f = job_server.submit(FFT.transform_NDto2D, (self.data_w))
      self.data_w_param = f()
    else:
      f = job_server.submit(FFT.transform_NDto2D, (self.data_w))
      self.data_w_param = f()

    return self.data_w_param

  #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  def symetrize(self, data):
    """
    Symmetrize the parametric array self.data_w_param in order
    to perform FFT
    Output parameters:
    ------------------
      self.data_w_sym (2D numpy array): symmetric array
    """
    if self.key_args['quantity'] == 'intensity':
      self.data_w_sym = FFT.data_freq_sym(data, op='hermitian', dtype=float)
    else:
      self.data_w_sym = FFT.data_freq_sym(data, op='hermitian', dtype=np.complex64)

    return self.data_w_sym

  #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  def determine_box(self):
    """
    Determine the size of the box
    """
    self.box = list()
    self.box.append(self.dr*self.lbound + self.origin)
    self.box.append(self.dr*self.ubound + self.origin)

    self.dim = self.ubound - self.lbound + 1

  #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  def save_data(self, output):
    """
    Save the array self.data_w as well that all the data concerning
    the box and the frequency in the file output
    """
    dico = {'dr':self.dr, 'origin':self.origin, 'ibox': self.box, 'freq': self.w,\
        'data': self.data_w}
    
    write_HDF5(output, dico)


  def define_data(self, param, data):

    indices = pts_coord_to_Array_ind(self.lbound, self.dr, self.origin, param.coord)
    if self.key_args['quantity'] == 'Efield':
      if param.data_type == 'full':
        data_w = data
      elif param.data_type == 'plan':
        if param.plan == 'xy':
          data_w = data[:, :, :, indices[2], :]
        elif param.plan == 'xz':
          data_w = data[:, :, indices[1], :, :]
        elif param.plan == 'yz':
          data_w = data[:, indices[0], :, :, :]
        else:
          raise ValueError('plan can be only xy, xz, yz')

      elif param.data_type == 'line':
        if param.line == 'x':
          data_w = data[:, :, indices[1], indices[2], :]
        elif param.line == 'y':
          data_w = data[:, indices[0], :, indices[2], :]
        elif param.line == 'z':
          data_w = data[:, indices[0], indices[1], :, :]
        else:
          raise ValueError('plan can be only xy, xz, yz')

      elif param.data_type == 'dot':
        data_w = data[:, indices[0], indices[1], indices[2], :]
      else:
        raise ValueError('data_type can be only full, plan line, dot')
   
    else:
      if param.data_type == 'full':
        data_w = data
      elif param.data_type == 'plan':
        if param.plan == 'xy':
          data_w = data[:, :, :, indices[2]]
        elif param.plan == 'xz':
          data_w = data[:, :, indices[1], :]
        elif param.plan == 'yz':
          data_w = data[:, indices[0], :, :]
        else:
          raise ValueError('plan can be only xy, xz, yz')

      elif param.data_type == 'line':
        if param.line == 'x':
          data_w = data[:, :, indices[1], indices[2]]
        elif param.line == 'y':
          data_w = data[:, indices[0], :, indices[2]]
        elif param.line == 'z':
          data_w = data[:, indices[0], indices[1], :]
        else:
          raise ValueError('plan can be only xy, xz, yz')

      elif param.data_type == 'dot':
        data_w = data[:, indices[0], indices[1], indices[2]]
      else:
        raise ValueError('data_type can be only full, plan line, dot')
    
    return data_w


#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def Array_ind_to_pts_coord(lbound, dr, origin, indice):
  coord = (indice + lbound)*dr + origin
  return coord


def pts_coord_to_Array_ind(lbound, dr, origin, coord):
  ind = np.zeros((3), dtype=int)
  
  for i in enumerate(coord):
    ind[i[0]] = np.rint(((i[1]-origin[i[0]])/dr[i[0]]) - lbound[i[0]])
  
  return ind
