from __future__ import division
import numpy as np
import prettyplotlib as ppl
from scipy.odr import odrpack as odr
from scipy.odr import models

def poly_lsq(x,y,n,verbose=False,itmax=200, func=None, beta0=None):
    ''' Performs a polynomial least squares fit to the data,
    with errors! Uses scipy odrpack, but for least squares.
    
    IN:
       x,y (arrays) - data to fit
       n (int)      - polinomial order
       verbose      - can be 0,1,2 for different levels of output
                      (False or True are the same as 0 or 1)
       itmax (int)  - optional maximum number of iterations
       
    OUT:
       coeff -  polynomial coefficients, lowest order first
       err   - standard error (1-sigma) on the coefficients

    --Tiago, 20071114
    '''

    # http://www.scipy.org/doc/api_docs/SciPy.odr.odrpack.html
    # see models.py and use ready made models!!!!
    
    if func is None:
      func = models.polynomial(n)
    else:
      func = odr.Model(func)
    mydata = odr.Data(x, y)
    if beta0 is None:
      myodr  = odr.ODR(mydata, func,maxit=itmax)
    else:
      myodr  = odr.ODR(mydata, func,beta0=beta0,maxit=itmax)

    # Set type of fit to least-squares:
    myodr.set_job(fit_type=2)
    if verbose == 2: myodr.set_iprint(final=2)
          
    fit = myodr.run()

    # Display results:
    if verbose: fit.pprint()

    if fit.stopreason[0] == 'Iteration limit reached':
        print('(WWW) poly_lsq: Iteration limit reached, result not reliable!')

    # Results and errors
    coeff = fit.beta[::-1]
    err   = fit.sd_beta[::-1]

    return coeff, err



def fit_second_order(ax, elec_nb, time, ms, weight, color, label, func=None, beta0=None,
        lw=3):
  elec = np.arange(0, 15*2100, 1)
  p = np.polyfit(elec_nb, time, 2, w=weight)
  p_odr, err_odr = poly_lsq(elec_nb, time, 2, func=func, beta0=beta0)
  
  ppl.plot(ax, elec_nb, time, 'o', color = color, markersize=ms, label=label)
  fit = p[0]*elec**2 + p[1]*elec + p[2]
  if func is None:
    fit_odr = p_odr[0]*elec**2 + p_odr[1]*elec + p_odr[2]
    lb = r'$y = {0:.2e}x^2 + {1:.2e}x + {2:.2e}$'.format(p_odr[0], p_odr[1], p_odr[2])
  else:
    fit_odr = p_odr[0]*elec**2 + p_odr[1]*elec
    lb = r'$y = {0:.2e}x^2 + {1:.2e}x$'.format(p_odr[0], p_odr[1])

  print(p[0])
  ppl.plot(ax, elec, fit, '--', color = color, linewidth=lw, label=r'$y = {0:.2e}x^2 + {1:.2e}x + {2:.2e}$'.format(
              p[0], p[1], p[2]))
  ppl.plot(ax, elec, fit_odr, ':', color = color, linewidth=lw, label=lb)
  print('coefs error odr ' + label + ' : ', err_odr)


def fit_third_order(ax, elec_nb, time, ms, weight, color, label, func=None, beta0 = None,
        lw = 3):
  #ax2.plot(elec_nb, matrix_tot_time, 'ro', markersize=ms, label='matrix')
  elec = np.arange(0, 15*2100, 1)
  p = np.polyfit(elec_nb, time, 3, w=weight)
  p_odr, err_odr = poly_lsq(elec_nb, time, 3, func=func, beta0=beta0)

  #fit = p[0]*elec**3 + p[1]*elec**2 + p[2]*elec + p[3]
  if func is None:
    fit_odr = p_odr[0]*elec**3 + p_odr[1]*elec**2 + p_odr[2]*elec + p_odr[3]
    lb = r'$y = {0:.2e}x^3 + {1:.2e}x^2 + {2:.2e}x +  {3:.2e}$'.format(p_odr[0], p_odr[1], p_odr[2], p_odr[3])
  else:
    fit_odr = p_odr[0]*elec**3 + p_odr[1]*elec**2 + p_odr[2]*elec 
    lb = r'$y = {0:.2e}x^3 + {1:.2e}x^2 + {2:.2e}x$'.format(p_odr[0], p_odr[1], p_odr[2])
  print(p[0])
#  ppl.plot(ax, elec, fit, '--', linewidth=lw, color=color, label=r'$y = {0:.2e}x^3 + {1:.2e}x^2 + {2:.2e}x +  {3:.2e}$'.format(
#              p[0], p[1], p[2], p[3]))
  ppl.plot(ax, elec, fit_odr, ':', linewidth=lw, color=color, label=lb)

  print('coefs error odr ' + label + ' : ', err_odr)

def parabol(B, x):
  return abs(B[0])*x**2 + abs(B[1])*x

def cubic(B, x):
  return abs(B[0])*x**3 + abs(B[1])*x**2 + abs(B[2])*x


#example
#
#elec_nb = 15*np.array([0.0, 147, 561, 923, 1415, 2057])
#weight = np.array([100.0, 1.0, 1.0, 1.0, 1.0, 1.0])
#matrix_mem = np.array([0.0, 4315.97, 18528.19, 37047.75, 66797.72, 120194.14])
#p_mat_mem = np.polyfit(elec_nb, matrix_mem, 2, w=weight)
#gpu_mem = np.array([0.0, 4639.26, 31734.35, 57906.67])
#p_gpu_mem = np.polyfit(elec_nb[0:4], gpu_mem, 2, w=weight[0:4])
#
#matrix_tot_time = np.array([0.0, 329, 14977, 83015, 182033, 187635*4])
#matrix_chi0_time = np.array([0.0, 196.88085, 8918.3280, 63305.473, 154974.89])
#matrix_tddft_solver = np.array([0.0, 327.65540, 12631.201, 83009.136, 182022.32])
#matrix_other = matrix_tddft_solver - matrix_chi0_time
#gpu_tot_time = np.array([0.0, 222, 3905, 22703])
#
#ft = 20
#lw = 3
#ms = 10
#elec = np.arange(0, 15*2100, 1)
#
#fig1 = plt.figure(1, figsize=(15, 25))
#ax1 = fig1.add_subplot(311)
#
#ppl.plot(ax1, elec_nb, matrix_mem, 'ro', markersize=ms, label='matrix')
#fit = p_mat_mem[0]*elec**2 + p_mat_mem[1]*elec + p_mat_mem[2]
#print(p_mat_mem[0])
#ppl.plot(ax1, elec, fit, 'r--', linewidth=lw, label=r'$y = {0:.2e}x^2 + {1:.2f}x + {2:.2f}$'.format(
#            p_mat_mem[0], p_mat_mem[1], p_mat_mem[2]))
#
#ppl.plot(ax1, elec_nb[0:4], gpu_mem, 'bo', markersize=ms, label='gpu')
#fit = p_gpu_mem[0]*elec**2 + p_gpu_mem[1]*elec + p_gpu_mem[2]
#print(p_gpu_mem[0])
#ppl.plot(ax1, elec, fit, 'b--', linewidth=lw, label=r'$y = {0:.2e}.x^2 + {1:.2f}.x + {2:.2f}$'.format(
#            p_gpu_mem[0], p_gpu_mem[1], p_gpu_mem[2]))
#
#ax1.set_xlabel(r'Orbital number', fontsize=ft)
#ax1.set_ylabel(r'Memory (MB)', fontsize=ft)
#ax1.legend(loc=0)
#
#
#ax2 = fig1.add_subplot(312)
#fit_second_order(ax2, elec_nb, matrix_tot_time, ms, weight, 'r', 'MATRIX')
#fit_second_order(ax2, elec_nb[0:4], gpu_tot_time, ms, weight[0:4], 'b', 'GPU')
#fit_third_order(ax2, elec_nb, matrix_tot_time, ms, weight, 'g', 'MATRIX')
#fit_third_order(ax2, elec_nb[0:4], gpu_tot_time, ms, weight[0:4], 'm', 'GPU')
#
#ax3 = fig1.add_subplot(313)
#fit_second_order(ax3, elec_nb[0:5], matrix_chi0_time, ms, weight[0:5], 'r', 'Chi0', func=parabol, beta0=[1.0, 1.0])
##fit_second_order(ax3, elec_nb[0:5], matrix_other, ms, weight[0:5], 'b', 'total - chi0', func=parabol, beta0=[1e-4, 1.0])
#fit_third_order(ax3, elec_nb[0:5], matrix_chi0_time, ms, weight[0:5], 'g', 'Chi0', func = cubic, beta0=[1e-15, 0.01, 1.0])
##fit_third_order(ax3, elec_nb[0:5], matrix_other, ms, weight[0:5], 'm', 'total - chi0', func=cubic, beta0=[0.0, 0.1, 1.0])
#
#ax2.set_xlabel(r'Orbital number', fontsize=ft)
#ax2.set_ylabel(r'Total running time (s)', fontsize=ft)
#ax2.legend(loc=0)
#ax3.set_xlabel(r'Orbital number', fontsize=ft)
#ax3.set_ylabel(r'Chi0 time (s)', fontsize=ft)
#ax3.legend(loc=0)
#
#fig1.tight_layout()
#fig1.savefig('fitting_final.pdf', format='pdf')
#plt.show()
